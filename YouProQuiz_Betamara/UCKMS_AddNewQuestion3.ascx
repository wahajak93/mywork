<!--ASCX page header @1-060D942A-->
<%@ Control language="C#" CodeFile="UCKMS_AddNewQuestion3Events.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_AddNewQuestion3_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-C2EC6438-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-B206E41E
</script>
<mt:MTPanel ID="___link_panel_09853666243618575" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/jquery-ui.css">
</mt:MTPanel>
<mt:MTPanel ID="___link_panel_0230225768883368" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/ccs-jquery-ui-calendar.css">
</mt:MTPanel>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-098F55F5
    $('body').ccsBind(function() {
        features["UCKMS_AddNewQuestion3tblQuestionQuestionInsertedOnInlineDatePicker1"] = $('*:ccsControl(UCKMS_AddNewQuestion3, tblQuestion, QuestionInsertedOn)');
        features["UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedOnInlineDatePicker2"] = $('*:ccsControl(UCKMS_AddNewQuestion3, tblQuestion, QuestionUpdatedOn)');
    });
//End Controls Selecting

//Feature Parameters @1-4848F92B
    params["UCKMS_AddNewQuestion3tblQuestionQuestionInsertedOnInlineDatePicker1"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
    params["UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedOnInlineDatePicker2"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
//End Feature Parameters

//Feature Init @1-ACCE564C
    features["UCKMS_AddNewQuestion3tblQuestionQuestionInsertedOnInlineDatePicker1"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_AddNewQuestion3tblQuestionQuestionInsertedOnInlineDatePicker1"]);
    });
    features["UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedOnInlineDatePicker2"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedOnInlineDatePicker2"]);
    });
//End Feature Init

//UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad Event Start @-9B2B6B19
    actions["UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad Event Start

//Attach CKeditor @179-2B80D626
        try {
            var UCKMS_AddNewQuestion3tblQuestionQuestion_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "250",
                width: "350",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad Event End

//UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad Event Start @-F07A997D
    actions["UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad Event Start

//Attach CKeditor @49-83628291
        try {
            var UCKMS_AddNewQuestion3tblChoiceChoice_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "300",
                width: "350",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad Event End

//UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad Event Start @-859B3515
    actions["UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad Event Start

//Attach CKeditor @249-D744E264
        try {
            var UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "200",
                width: "300",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad Event End

//Event Binding @1-738CAD3B
    $('*:ccsControl(UCKMS_AddNewQuestion3, tblQuestion, Question)').ccsBind(function() {
        this.each(function(){ actions["UCKMS_AddNewQuestion3tblQuestionQuestionOnLoad"].call(this); });
    });
    $('*:ccsControl(UCKMS_AddNewQuestion3, tblChoice, Choice)').ccsBind(function() {
        this.each(function(){ actions["UCKMS_AddNewQuestion3tblChoiceChoiceOnLoad"].call(this); });
    });
    $('*:ccsControl(UCKMS_AddNewQuestion3, tbl_Quiz_Question_Choice, TextArea1)').ccsBind(function() {
        this.each(function(){ actions["UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1OnLoad"].call(this); });
    });
//End Event Binding

//Plugin Calls @1-EA4813A8
    $('*:ccsControl(UCKMS_AddNewQuestion3, tblQuestion, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<p>&nbsp;</p>
<br>
<mt:Record PreserveParameters="Get" DataSourceID="tblQuestionDataSource" OnBeforeInsert="tblQuestion_BeforeInsert" OnAfterInsert="tblQuestion_AfterInsert" OnBeforeUpdate="tblQuestion_BeforeUpdate" OnAfterDelete="tblQuestion_AfterDelete" ID="tblQuestion" runat="server" OnValidating="tblQuestion_Validating"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion3tblQuestion" id="UCKMS_AddNewQuestion3tblQuestion">







  <h2>Add/Edit Question </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="4"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").ClientID %>">Question Name</label></td> 
      <td colspan="3"><mt:MTTextBox Source="Question_Name" ErrorControl="Label1" Caption="Question Name" maxlength="50" Columns="50" ID="Question_Name" data-id="UCKMS_AddNewQuestion3tblQuestionQuestion_Name" runat="server"/><mt:MTLabel ID="Label1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question").ClientID %>">Question</label></td> 
      <td colspan="3">
        <p>&nbsp;</p>
 
        <p>&nbsp; 
<mt:MTTextBox TextMode="Multiline" Source="Question" DataType="Memo"  ErrorControl="Label2" Caption="Question" Rows="3" Columns="10" ID="Question" data-id="UCKMS_AddNewQuestion3tblQuestionQuestion" runat="server"/>
<mt:MTLabel ID="Label2" runat="server"/></p>
 </td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionInsertedOn" DataType="Date" Format="d" Caption="Question Inserted On" ID="QuestionInsertedOn" data-id="UCKMS_AddNewQuestion3tblQuestionQuestionInsertedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionInsertedBy" DataType="Float" Caption="Question Inserted By" ID="QuestionInsertedBy" data-id="UCKMS_AddNewQuestion3tblQuestionQuestionInsertedBy" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionUpdatedOn" DataType="Date" Format="d" Caption="Question Updated On" OnLoad="tblQuestionQuestionUpdatedOn_Load" ID="QuestionUpdatedOn" data-id="UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionUpdatedBy" DataType="Float" Caption="Question Updated By" ID="QuestionUpdatedBy" data-id="UCKMS_AddNewQuestion3tblQuestionQuestionUpdatedBy" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionStatusID").ClientID %>">Question Status</label></td> 
      <td colspan="3">
        <mt:MTListBox Rows="1" Source="QuestionStatusID" DataType="Float" ErrorControl="Label3" Caption="Question Status ID" DataSourceID="QuestionStatusIDDataSource" DataValueField="StatusID" DataTextField="Status" ID="QuestionStatusID" data-id="UCKMS_AddNewQuestion3tblQuestionQuestionStatusID" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="QuestionStatusIDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblRefStatus {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label3" runat="server"/></td> 
    </tr>
 
    <tr>
      <td>&nbsp;Question Type For Current Quiz </td> 
      <td>&nbsp; 
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Type_ID"  ErrorControl="Label4" DataSourceID="ListBox1DataSource" DataValueField="Quest_Type_ID" DataTextField="Quest_Type" ID="ListBox1" data-id="UCKMS_AddNewQuestion3tblQuestionListBox1" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionType {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label4" runat="server"/></td> 
      <td></td> 
      <td>&nbsp;</td> 
    </tr>
 
    <tr>
      <td>&nbsp;&nbsp;Question Category For Current Quiz</td> 
      <td>&nbsp; 
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Category_ID"  ErrorControl="Label5" DataSourceID="ListBox2DataSource" DataValueField="CategoryID" DataTextField="Category" ID="ListBox2" data-id="UCKMS_AddNewQuestion3tblQuestionListBox2" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox2DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionCategory {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label5" runat="server"/> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td></td> 
    </tr>
 
    <tr>
      <td>&nbsp;Question Time For Current Quiz</td> 
      <td>&nbsp;<mt:MTTextBox Source="Quiz_Quest_Time"  ErrorControl="Label6" ID="TextBox1" data-id="UCKMS_AddNewQuestion3tblQuestionTextBox1" runat="server"/><mt:MTLabel ID="Label6" runat="server"/></td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="4">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion3tblQuestionButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="UCKMS_AddNewQuestion3tblQuestionButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="UCKMS_AddNewQuestion3tblQuestionButton_Delete" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblQuestionDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="SQL" DeleteCommandType="SQL" OnInitializeUpdateParameters="tblQuestion_InitializeUpdateParameters" runat="server">
   <SelectCommand>
SELECT * 
FROM tblQuestion LEFT JOIN tbl_Quiz_Question ON
tblQuestion.QuestionID = tbl_Quiz_Question.Quiz_Quest_ID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblQuestion([Question_Name], [Question], [QuestionInsertedOn], [QuestionInsertedBy], [QuestionUpdatedOn], [QuestionUpdatedBy], [QuestionStatusID]) VALUES ({Question_Name}, {Question}, {QuestionInsertedOn}, {QuestionInsertedBy}, {QuestionUpdatedOn}, {QuestionUpdatedBy}, {QuestionStatusID})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblQuestion SET Question_Name='{Question_Name}', QuestionUpdatedOn='{QuestionUpdatedOn}', QuestionUpdatedBy={QuestionUpdatedBy}, QuestionStatusID=1, Question='{TextArea1}' WHERE  QuestionID = {QuestionID}
UPDATE tbl_Quiz_Question SET Quiz_Quest_Category_ID={catid},Quiz_Quest_Type_ID={typid},Quiz_Quest_Time={time} WHERE  Quiz_Quest_ID= {QuestionID} AND Quiz_ID={quiz_id}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tbl_Quiz_Question WHERE  Quiz_Quest_ID = {QuestionID} AND Quiz_ID={quiz_id}
DELETE FROM tbl_Quiz_Question_Answers WHERE QuestionID={QuestionID} AND Quiz_ID={quiz_id}
DELETE FROM tbl_Quiz_Question_Choice WHERE Quest_ID={QuestionID} AND Quiz_ID={quiz_id}
DELETE FROM tblSectionQuestions WHERE QuizID={quiz_id} AND QuestionID={QuestionID}
  </DeleteCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="tblQuestion.QuestionID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="Question_Name" SourceType="Control" Source="Question_Name" DataType="Text" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Question" SourceType="Control" Source="Question" DataType="Memo" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionInsertedOn" SourceType="Control" Source="QuestionInsertedOn" DataType="Date" Format="d" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionInsertedBy" SourceType="Control" Source="QuestionInsertedBy" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionUpdatedOn" SourceType="Control" Source="QuestionUpdatedOn" DataType="Date" Format="d" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionUpdatedBy" SourceType="Control" Source="QuestionUpdatedBy" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionStatusID" SourceType="Control" Source="QuestionStatusID" DataType="Float" IsOmitEmptyValue="True"/>
   </InsertParameters>
   <UpdateParameters>
     <mt:SqlParameter Name="Question_Name" SourceType="Control" Source="Question_Name" DataType="Text"/>
     <mt:SqlParameter Name="QuestionUpdatedOn" SourceType="Control" Source="QuestionUpdatedOn" DataType="Text" DBFormat="yyyy-mm-dd HH:nn:ss.S" Format="GeneralDate"/>
     <mt:SqlParameter Name="QuestionUpdatedBy" SourceType="Session" Source="UserID" DataType="Text"/>
     <mt:SqlParameter Name="QuestionStatusID" SourceType="Control" Source="QuestionStatusID" DataType="Text"/>
     <mt:SqlParameter Name="TextArea1" SourceType="Control" Source="Question" DataType="Text"/>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="catid" SourceType="Control" Source="ListBox2" DataType="Text"/>
     <mt:SqlParameter Name="typid" SourceType="Control" Source="ListBox1" DataType="Text"/>
     <mt:SqlParameter Name="time" SourceType="Control" Source="TextBox1" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
   </UpdateParameters>
   <DeleteParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
   </DeleteParameters>
</mt:MTDataSource>
<p><br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>
<p>&nbsp;</p>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tbl_Quiz_Question_ChoiceDataSource" AllowInsert="False" EmptyRows="0" ID="tbl_Quiz_Question_Choice" runat="server" OnBeforeShow="tbl_Quiz_Question_Choice_BeforeShow">
<HeaderTemplate><div data-emulate-form="UCKMS_AddNewQuestion3tbl_Quiz_Question_Choice" id="UCKMS_AddNewQuestion3tbl_Quiz_Question_Choice">


   
 <h2>Choices</h2>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="ID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:ID}" ID="Sorter_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceSorter_ID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="Quiz_ID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:Quiz_ID}" ID="Sorter_Quiz_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceSorter_Quiz_ID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="Choice_ID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:Choice_ID}" ID="Sorter_Choice_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceSorter_Choice_ID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="Quest_ID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:Quest_ID}" ID="Sorter_Quest_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceSorter_Quest_ID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="Choice_Score" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:Choice_Score}" ID="Sorter_Choice_Score" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceSorter_Choice_Score" runat="server"/></th>
 
      <th scope="col"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Choice)%></mt:MTPanel></th>
 
      <th scope="col"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_Delete)%></mt:MTPanel></th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceID_{tbl_Quiz_Question_Choice:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_ID)%></mt:MTPanel></label><mt:MTHidden Source="ID" DataType="Float" Required="True" Caption="{res:ID}" ID="ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceID_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></td> 
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceQuiz_ID_{tbl_Quiz_Question_Choice:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Quiz_ID)%></mt:MTPanel></label><mt:MTHidden Source="Quiz_ID" DataType="Float" Required="True" Caption="{res:Quiz_ID}" ID="Quiz_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceQuiz_ID_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></td> 
      <td>
        <p><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceChoice_ID_{tbl_Quiz_Question_Choice:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Choice_ID)%></mt:MTPanel></label><mt:MTHidden Source="Choice_ID" DataType="Float" Required="True" Caption="{res:Choice_ID}" ID="Choice_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceChoice_ID_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></p>
 
        <p><mt:MTLabel Source="Choice_ID" ID="Label1" runat="server"/></p>
 </td> 
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceQuest_ID_{tbl_Quiz_Question_Choice:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Quest_ID)%></mt:MTPanel></label><mt:MTHidden Source="Quest_ID" DataType="Float" Required="True" Caption="{res:Quest_ID}" ID="Quest_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceQuest_ID_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></td> 
      <td><label for="<%# tbl_Quiz_Question_Choice.GetControl<InMotion.Web.Controls.MTTextBox>("Choice_Score").ClientID %>" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Choice_Score)%></mt:MTPanel></label><mt:MTTextBox Source="Choice_Score" DataType="Integer" Caption="{res:Choice_Score}" maxlength="10" Columns="10" ID="Choice_Score" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceChoice_Score_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></td> 
      <td>
        <p><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceChoice_{tbl_Quiz_Question_Choice:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Choice)%></mt:MTPanel></label>&nbsp; </p>
 
        <p>
<mt:MTTextBox TextMode="Multiline" Source="Choice" Rows="3" Columns="10" ID="TextArea1" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceTextArea1" runat="server"/>
</p>
 </td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# tbl_Quiz_Question_Choice.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_Delete)%></mt:MTPanel></label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="tbl_Quiz_Question_ChoiceCheckBox_Delete_Init" OnLoad="tbl_Quiz_Question_ChoiceCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceCheckBox_Delete_PanelCheckBox_Delete_{tbl_Quiz_Question_Choice:rowNumber}" runat="server"/></mt:MTPanel>&nbsp;</td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_NoRecords)%></mt:MTPanel></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="7">
        <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" PageSizeItems="1;5;10;25;50" ID="Navigator" runat="server"/>
        <mt:MTButton CommandName="Submit" Text="{res:CCS_Update}" CssClass="Button" ID="Button_Submit" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_ChoiceButton_Submit" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tbl_Quiz_Question_ChoiceDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="SQL" DeleteCommandType="SQL" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} tbl_Quiz_Question_Choice.*, Choice 
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID
   </CountCommand>
   <InsertCommand>
INSERT INTO tblChoice(ID, [Quiz_ID], [Choice_ID], [Quest_ID], [Choice_Score], [Choice]) VALUES ({ID}, {Quiz_ID}, {Choice_ID}, {Quest_ID}, {Choice_Score}, {Choice})
  </InsertCommand>
   <UpdateCommand>
update tblChoice set Choice='{choice}' where ChoiceID={choiceid} AND QuestionID={questid}
update tbl_Quiz_Question_Choice set Choice_Score={score} where Quest_ID={questid} AND Choice_ID={choiceid} AND Quiz_ID={quizid}
  </UpdateCommand>
   <DeleteCommand>
delete from tblChoice where ChoiceID={choiceid}
delete from tblAnswer where ChoiceID={choiceid}
delete from tbl_Quiz_Question_Answers where ChoiceID={choiceid} AND Quiz_ID={quizid}
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="Quiz_ID" Type="Float" />
     <mt:PrimaryKeyInfo FieldName="Choice_ID" Type="Float" />
     <mt:PrimaryKeyInfo FieldName="Quest_ID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
   </SelectParameters>
   <UpdateParameters>
     <mt:SqlParameter Name="quizid" SourceType="Control" Source="Quiz_ID" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="Control" Source="Choice_ID" DataType="Text"/>
     <mt:SqlParameter Name="questid" SourceType="Control" Source="Quest_ID" DataType="Text"/>
     <mt:SqlParameter Name="choice" SourceType="Control" Source="TextArea1" DataType="Text"/>
     <mt:SqlParameter Name="score" SourceType="Control" Source="Choice_Score" DataType="Text"/>
   </UpdateParameters>
   <DeleteParameters>
     <mt:SqlParameter Name="quizid" SourceType="Control" Source="Quiz_ID" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="Control" Source="Choice_ID" DataType="Text"/>
     <mt:SqlParameter Name="questid" SourceType="Control" Source="Quest_ID" DataType="Text"/>
   </DeleteParameters>
</mt:MTDataSource><br>
<p>&nbsp;</p>
&nbsp; 
<mt:Record PreserveParameters="Get" DataSourceID="tblChoiceDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" OnBeforeInsert="tblChoice_BeforeInsert" OnAfterInsert="tblChoice_AfterInsert" ID="tblChoice" runat="server" OnBeforeShow="tblChoice_BeforeShow" OnValidating="tblChoice_Validating"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion3tblChoice" id="UCKMS_AddNewQuestion3tblChoice">


  <h2>Add/Edit Choice </h2>
 
  <p>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td></td> 
      <td><mt:MTHidden Source="QuestionID" DataType="Float" Required="True" Caption="Question ID" ID="QuestionID" data-id="UCKMS_AddNewQuestion3tblChoiceQuestionID" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("Choice").ClientID %>">Choice</label></td> 
      <td>
        <p></p>
 
        <p>
<mt:MTTextBox TextMode="Multiline" Source="Choice"  ErrorControl="Label7" Caption="Choice" Rows="3" Columns="10" ID="Choice" data-id="UCKMS_AddNewQuestion3tblChoiceChoice" runat="server"/>
<mt:MTLabel ID="Label7" runat="server"/>&nbsp;&nbsp; </p>
 </td> 
    </tr>
 
    <tr>
      <td>Score</td> 
      <td><mt:MTTextBox  ErrorControl="Label8" type="number" ID="TextBox1" data-id="UCKMS_AddNewQuestion3tblChoiceTextBox1" runat="server">Score</mt:MTTextBox>
          <mt:MTLabel ID="Label8" runat="server" ContentType="Text" DataType="Text" DBFormat="" Format="" OnBeforeShow="Label8_BeforeShow" Source="" SourceType="DatabaseColumn"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2"><mt:MTHidden ID="Hidden1" data-id="UCKMS_AddNewQuestion3tblChoiceHidden1" runat="server"/>
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion3tblChoiceButton_Insert" runat="server"/></td> 
    </tr>
 
  </table>
 </p>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblChoiceDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="SQL" UpdateCommandType="Table" OnInitializeInsertParameters="tblChoice_InitializeInsertParameters" runat="server">
   <SelectCommand>
SELECT * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblChoice(QuestionID, Choice) VALUES({QuestionID}, '{Choice}')
INSERT INTO tbl_Quiz_Question_Choice SELECT {quiz_id},MAX(tblChoice.ChoiceID),{QuestionID},'{score}' FROM tblChoice where tblChoice.QuestionID={QuestionID}
  </InsertCommand>
   <UpdateCommand>
UPDATE tblChoice SET [QuestionID]={QuestionID}, [Choice]={Choice}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlChoice" SourceType="URL" Source="Choice" DataType="Text" Operation="And" Condition="Equal" SourceColumn="Choice" Required="true"/>
     <mt:WhereParameter Name="UrlChoiceID" SourceType="URL" Source="ChoiceID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="ChoiceID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="Control" Source="QuestionID" DataType="Float"/>
     <mt:SqlParameter Name="Choice" SourceType="Control" Source="Choice" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="Control" Source="Hidden1" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
     <mt:SqlParameter Name="score" SourceType="Control" Source="TextBox1" DataType="Text"/>
   </InsertParameters>
</mt:MTDataSource>
<p>&nbsp;</p>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tbl_Quiz_Question_AnswersDataSource" AllowInsert="False" EmptyRows="0" ID="tbl_Quiz_Question_Answers" runat="server">
<HeaderTemplate><div data-emulate-form="UCKMS_AddNewQuestion3tbl_Quiz_Question_Answers" id="UCKMS_AddNewQuestion3tbl_Quiz_Question_Answers">


   
  <h2>Answers</h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col"></th>
 
      <th scope="col"></th>
 
      <th scope="col"></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="ChoiceID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:ChoiceID}" ID="Sorter_ChoiceID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersSorter_ChoiceID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="{res:CCS_ASC}" DescOnAlt="{res:CCS_DESC}" SortOrder="Score" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="{res:Score}" ID="Sorter_Score" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersSorter_Score" runat="server"/></th>
 
      <th scope="col"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Choice)%></mt:MTPanel></th>
 
      <th scope="col"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_Delete)%></mt:MTPanel></th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_Answersid_{tbl_Quiz_Question_Answers:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_id)%></mt:MTPanel></label><mt:MTHidden Source="id" DataType="Float" Required="True" Caption="{res:id}" ID="id" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_Answersid_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td> 
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersQuiz_ID_{tbl_Quiz_Question_Answers:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Quiz_ID)%></mt:MTPanel></label><mt:MTHidden Source="Quiz_ID" DataType="Float" Required="True" Caption="{res:Quiz_ID}" ID="Quiz_ID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersQuiz_ID_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td> 
      <td><label for="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersQuestionID_{tbl_Quiz_Question_Answers:rowNumber}" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_QuestionID)%></mt:MTPanel></label><mt:MTHidden Source="QuestionID" DataType="Float" Required="True" Caption="{res:QuestionID}" ID="QuestionID" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersQuestionID_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td> 
      <td><mt:MTLabel Source="ChoiceID" DataType="Float" ID="ChoiceID" runat="server"/></td> 
      <td><label for="<%# tbl_Quiz_Question_Answers.GetControl<InMotion.Web.Controls.MTTextBox>("Score").ClientID %>" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_Score)%></mt:MTPanel></label><mt:MTTextBox Source="Score" DataType="Float" Caption="{res:Score}" maxlength="12" Columns="12" ID="Score" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersScore_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td> 
      <td><mt:MTLabel Source="Choice" DataType="Memo" ID="Choice" runat="server" ClientIDMode="Inherit" Mode="PassThrough" ContentType="Html" /></td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# tbl_Quiz_Question_Answers.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_Delete)%></mt:MTPanel></label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="tbl_Quiz_Question_AnswersCheckBox_Delete_Init" OnLoad="tbl_Quiz_Question_AnswersCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersCheckBox_Delete_PanelCheckBox_Delete_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></mt:MTPanel>&nbsp;</td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTPanel runat=server><%= ResManager.GetString(Attribute_CCS_NoRecords)%></mt:MTPanel></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="7">
        <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/{res:CCS_LanguageID}/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" PageSizeItems="1;5;10;25;50" ID="Navigator" runat="server"/>
        <mt:MTButton CommandName="Submit" Text="{res:CCS_Update}" CssClass="Button" ID="Button_Submit" data-id="UCKMS_AddNewQuestion3tbl_Quiz_Question_AnswersButton_Submit" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tbl_Quiz_Question_AnswersDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="SQL" DeleteCommandType="SQL" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} tbl_Quiz_Question_Answers.*, Choice 
FROM tbl_Quiz_Question_Answers LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question_Answers LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID
   </CountCommand>
   <InsertCommand>
INSERT INTO (id, [Quiz_ID], [QuestionID], [Score]) VALUES ({id}, {Quiz_ID}, {QuestionID}, {Score})
  </InsertCommand>
   <UpdateCommand>
update tbl_Quiz_Question_Answers set Score={score} where Quiz_ID={quizid} AND QuestionID={questid} AND ChoiceID={choiceid}
  </UpdateCommand>
   <DeleteCommand>
Delete from tbl_Quiz_Question_Answers where QuestionID={questid} AND Quiz_ID={quizid} AND ChoiceID={choiceid}
Delete from tblAnswer where QuestionID={questid} AND ChoiceID={choiceid}
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="ChoiceID" Type="Float" />
     <mt:PrimaryKeyInfo FieldName="QuestionID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tblChoice.QuestionID"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
   </SelectParameters>
   <UpdateParameters>
     <mt:SqlParameter Name="score" SourceType="Control" Source="Score" DataType="Text"/>
     <mt:SqlParameter Name="quizid" SourceType="Control" Source="Quiz_ID" DataType="Text"/>
     <mt:SqlParameter Name="questid" SourceType="Control" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="DataSourceColumn" Source="ChoiceID" DataType="Text"/>
   </UpdateParameters>
   <DeleteParameters>
     <mt:SqlParameter Name="questid" SourceType="Control" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="quizid" SourceType="Control" Source="Quiz_ID" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="DataSourceColumn" Source="ChoiceID" DataType="Text"/>
   </DeleteParameters>
</mt:MTDataSource><br>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<mt:Record PreserveParameters="Get" DataSourceID="tblAnswerDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" ID="tblAnswer" runat="server" OnBeforeShow="tblAnswer_BeforeShow"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion3tblAnswer" id="UCKMS_AddNewQuestion3tblAnswer">


  <h2>Add/Edit Answer </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# tblAnswer.GetControl<InMotion.Web.Controls.MTListBox>("ChoiceID").ClientID %>">Choice </label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="ChoiceID" DataType="Float" Required="True" Unique="True" ErrorControl="Label9" Caption="Choice ID" DataSourceID="ChoiceIDDataSource" DataValueField="ChoiceID" DataTextField="ChoiceID" ID="ChoiceID" data-id="UCKMS_AddNewQuestion3tblAnswerChoiceID" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ChoiceIDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
           <SelectParameters>
             <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
           </SelectParameters>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label9" runat="server"/></td> 
    </tr>
 
    <tr>
      <td>Score</td> 
      <td><mt:MTTextBox Required="True" ErrorControl="Label10" OnValidating="tblAnswerTextBox1_Validating" ID="TextBox1" data-id="UCKMS_AddNewQuestion3tblAnswerTextBox1" type="number" runat="server"/><mt:MTLabel ID="Label10" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion3tblAnswerButton_Insert" runat="server"/><mt:MTHidden Source="QuestionID" DataType="Float" Required="True" Caption="Question ID" ID="QuestionID" data-id="UCKMS_AddNewQuestion3tblAnswerQuestionID" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblAnswerDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="SQL" UpdateCommandType="Table" ValidateUniqueCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM tblAnswer {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblAnswer(QuestionID, ChoiceID) VALUES({QuestionID}, {ChoiceID})
INSERT INTO tbl_Quiz_Question_Answers(Quiz_ID,QuestionID,ChoiceID,Score) VALUES({Quiz_id},{QuestionID},{ChoiceID},{score})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblAnswer SET [QuestionID]={QuestionID}, [ChoiceID]={ChoiceID}
  </UpdateCommand>
   <ValidateUniqueCommand>
SELECT COUNT(*)
FROM tblAnswer
  </ValidateUniqueCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlAnswerID" SourceType="URL" Source="AnswerID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="AnswerID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="Control" Source="QuestionID" DataType="Float"/>
     <mt:SqlParameter Name="ChoiceID" SourceType="Control" Source="ChoiceID" DataType="Float"/>
     <mt:SqlParameter Name="Quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
     <mt:SqlParameter Name="score" SourceType="Control" Source="TextBox1" DataType="Text"/>
   </InsertParameters>
</mt:MTDataSource>



<!--End ASCX page-->

