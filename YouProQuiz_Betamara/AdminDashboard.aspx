﻿<%--<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-1F39CBA5-->--%>
<%@ Page language="C#" CodeFile="AdminDashboardEvents.aspx.cs" Inherits="YouProQuiz_Beta.AdminDashboard_Page"  MasterPageFile="~/Dashboard.master" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC1_Edit_Existing_Quiz" Src="UC1_Edit_Existing_Quiz.ascx" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC2_Quiz_Queries" Src="UC2_Quiz_Queries.ascx" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_LogOut" Src="UC_LogOut.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/UCKMS/UCKMS_AdminApproval.ascx" TagPrefix="YouProQuiz_Beta" TagName="UCKMS_AdminApproval" %>


<%--<!--End ASPX page header-->

<!--ASPX page @1-9ED9C54D-->
<html>
<head>--%>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>AdminDashboard</title>


<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>
<%--</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">--%>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
<YouProQuiz_Beta:UC_LogOut ID="UC_LogOut" runat="server"/>
        </div>
    <div>
    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Reset_Settings.aspx" >Reset Settings</asp:LinkButton>
<mt:MTLabel ID="WelcomeNote" runat="server"/>
        </div>
<hr/>

<hr/>


<YouProQuiz_Beta:UC1_Edit_Existing_Quiz ID="UC1_Edit_Existing_Quiz" runat="server"/> 
<hr/>Queries 


<YouProQuiz_Beta:UC2_Quiz_Queries ID="UC2_Quiz_Queries" runat="server"/> 
<hr/>

<div class="container" align="center">
  <div class="col-lg-12">  

<h3>
Create Quiz 

<mt:MTButton Text="Add New Quiz" CssClass="btn btn-primary btn-lg" ID="Button1" data-id="Button1" runat="server" OnClick="Button1_Click"/>
 </h3>   </div>
    </div>


    <h1>Upgrade/Edit Existing Users</h1>

    <YouProQuiz_Beta:UCKMS_AdminApproval runat="server" ID="UCKMS_AdminApproval" />






    </asp:Content>
<%--</form>
</body>
</html>

<!--End ASPX page-->--%>

