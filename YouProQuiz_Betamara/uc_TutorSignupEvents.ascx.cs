//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using System.Net;
using System.Net.Mail;
using System.IO;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-9B0ED3BB
public partial class uc_TutorSignup_Page : MTUserControl
{
//End Page class
	public string user_status_id="1";
	public string user_group_id="1";
	
	
	protected static readonly string senderAddress = ConfigurationManager.AppSettings ["smtpUser"];
	protected static readonly string senderPassword = ConfigurationManager.AppSettings ["smtpPass"];
	protected static readonly string smtpHost = ConfigurationManager.AppSettings ["smtpServer"];
	protected static readonly int smtpPort = Convert.ToInt32 (ConfigurationManager.AppSettings ["smtpPort"]);
	//protected static string receiverAddress = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text;
	protected const string welcomeSubject = "Welcome To Learner's Club";
	protected const string welcomeBody = "<html><body><div>Dear Tutor,</div><div>&ensp;Thank You For Joining Us.</div></body></html>";
	
	protected const string verifySubject = "Verification Code";
	//protected const string verifyBody = "Plese verify your account first by loggin into your account. Thank You \nVerification code = " + tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Text;
	            	
	protected void setEmail(string senderAddress, string senderPassword, string receiverAddress, string subject, string body)
	{
		MailMessage message = new MailMessage();
		message.From = new MailAddress(senderAddress);
		message.To.Add(receiverAddress);
		message.Subject = subject;
		message.SubjectEncoding = System.Text.Encoding.UTF8;
		message.Body = body;
		message.BodyEncoding = System.Text.Encoding.UTF8;
		message.IsBodyHtml = true;
		SmtpClient client = new SmtpClient();
		client.Host = smtpHost;
		client.Port = smtpPort;
		client.Credentials = new System.Net.NetworkCredential(senderAddress, senderPassword);
		client.EnableSsl = true;
		try
		{
			client.Send(message);
		}
		catch(Exception ex)
		{
			Response.Write(ex.ToString());	
			//Response.Write("There is a problem in the server. Please be patient, you will get the verification code soon");
		}
	}
	
	protected int sixDigitRandom()
	{
		Random rand = new Random();
		int r = rand.Next(100000, 999999);
		return r;
	}

//Page uc_TutorSignup Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page uc_TutorSignup Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page uc_TutorSignup Init event tail @1-FCB6E20C
    }
//End Page uc_TutorSignup Init event tail

//Record tbl_Tutor Event Initialize Insert Parameters @4-5303A492
    protected void tbl_Tutor_InitializeInsertParameters(object sender, EventArgs e) {
//End Record tbl_Tutor Event Initialize Insert Parameters
		
//Initialize expression parameters and default values  @4-DF5AD0A8
        ((MTDataSourceView)sender).InsertParameters["user_status_id"].DefaultValue = 1;
        ((MTDataSourceView)sender).InsertParameters["user_group_id"].DefaultValue = 1;
        ((MTDataSourceView)sender).InsertParameters["user_status_id"].Value = user_status_id;
        ((MTDataSourceView)sender).InsertParameters["user_group_id"].Value = user_group_id;
//End Initialize expression parameters and default values 

//Record tbl_Tutor Initialize Insert Parameters event tail @4-FCB6E20C
    }
//End Record tbl_Tutor Initialize Insert Parameters event tail

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          Link1.Visible = false;
//DEL          // -------------------------

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, welcomeSubject, welcomeBody);
//DEL         	setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, verifySubject, "Plese verify your account first by loggin into your account. Thank You \nVerification code = " + tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Text);
//DEL         	Response.Redirect("~/registeredmsg.aspx");
//DEL          // -------------------------

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorUsername").Text = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text;
//DEL          tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorAddedOn").Value = DateTime.Now;
//DEL          tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorGroupID").Value = 2;
//DEL          tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorStatusID").Value = 1;
//DEL          tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Value = sixDigitRandom();
//DEL          // -------------------------

//Record tbl_Tutor Event Before Build Insert @4-4D8601A2
    protected void tbl_Tutor_BeforeBuildInsert(object sender, DataOperationEventArgs e) {
//End Record tbl_Tutor Event Before Build Insert
		
//Record tbl_Tutor Event Before Build Insert. Action Encrypt Password @18-657A56BC
        e.Command.Parameters["user_password"].Value = InMotion.Security.Cryptography.MD5(e.Command.Parameters["user_password"].Value.ToString());
//End Record tbl_Tutor Event Before Build Insert. Action Encrypt Password

//Record tbl_Tutor Before Build Insert event tail @4-FCB6E20C
    }
//End Record tbl_Tutor Before Build Insert event tail

//Record tbl_Tutor Event After Insert @4-9373F0AD
    protected void tbl_Tutor_AfterInsert(object sender, DataOperationCompletingEventArgs e) {
//End Record tbl_Tutor Event After Insert

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          //Uri verificationUri = new Uri("http://tutors.learners.club/activate.aspx", UriKind.Absolute);      
//DEL   	      Uri verificationUri = new Uri("http://localhost/Tutor_LearnersClub/activate.aspx", UriKind.Absolute); 
//DEL          
//DEL          string bodyText = string.Format(@"
//DEL      	<html>
//DEL        	<body>
//DEL          	<div>
//DEL            		<span class=""name"">Please verify your account by clicking the below link. Thank You</span>
//DEL          	</div>
//DEL          	<div>
//DEL  				<span class=""value"">{0}</span>
//DEL          	</div>
//DEL          	<div>
//DEL            		<span class=""name"">Verification code:</span> <span class=""value"">{1}</span>
//DEL          	</div>
//DEL        	</body>
//DEL      	</html>", verificationUri.ToString(), tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Text);
//DEL          
//DEL          setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, welcomeSubject, welcomeBody);
//DEL         	//setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, verifySubject, "Plese verify your account by entering verification code. Thank You \nVerification code = " + tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Text);
//DEL         	setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, verifySubject, bodyText);
//DEL         	Response.Redirect("~/accountcreated.aspx");
//DEL         	//Label1.Value = "Your account has been created. Please click below to Login";
//DEL         	//Link1.Visible = true;
//DEL          // -------------------------

//Record tbl_Tutor Event After Insert. Action Custom Code @19-2A29BDB7
        // -------------------------
        // Write your own code here.
        string qsEmail = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text;
        string qsVerification = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Value.ToString();

        Uri verificationUri = new Uri("http://localhost/YouProQuiz_Betamara/Default.aspx?email=" + qsEmail + "&code=" + qsVerification, UriKind.Absolute);
        //Uri verificationUri = new Uri("http://tutors.learners.club/activate.aspx", UriKind.Absolute);      
 	      //Uri verificationUri = new Uri("http://localhost/Tutor_LearnersClub/activate.aspx", UriKind.Absolute);
 	   
 	   
 	   //	StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Welcome.htm"));
        try
        {
            //	string readFile = reader.ReadToEnd();
            string StrContent = "";
            //  StrContent = readFile;
            //Here replace the name with [MyName]
            //    StrContent = StrContent.Replace("[TutorName]", qsEmail);
            StrContent = verificationUri.ToString();




            /*string verificationBody = string.Format(@"
            <html>
            <body>
                <div>
                    <span class=""name"">Please verify your account by clicking the below link. Thank You</span>
                </div>
        	
        	
                <div>
                    <span class=""value"">{0}</span>
                </div>
            </body>
            </html>", verificationUri.ToString()); */
            string verificationBody = StrContent.ToString();

            setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, welcomeSubject, welcomeBody);
            //setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, verifySubject, "Plese verify your account by entering verification code. Thank You \nVerification code = " + tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode").Text);
            // 	setEmail(senderAddress, senderPassword, tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text, verifySubject, verificationBody);
            Session["signupEmail"] = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text;


            string x = "abc";

            Response.Write("<script>alert(" + x + ");</script>");


           

        }
        catch (Exception ex)
        {
            Response.Write("<script>alert(" + ex.Message + ");</script>");
            
        }


        finally
        {
          
            Response.Redirect("~/home.aspx");
        }
       	//Label1.Value = "Your account has been created. Please click below to Login";
       	//Link1.Visible = true;
        // -------------------------
//End Record tbl_Tutor Event After Insert. Action Custom Code

//Record tbl_Tutor After Insert event tail @4-FCB6E20C
    }
//End Record tbl_Tutor After Insert event tail

//Button Button_Insert Event On Click @5-8AD24B6E
    protected void tbl_TutorButton_Insert_Click(object sender, EventArgs e) {
//End Button Button_Insert Event On Click

//Button Button_Insert Event On Click. Action Custom Code @6-2A29BDB7
        // -------------------------
        // Write your own code here.
        MTTextBox tbEmail = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail");
        MTTextBox tbPwd = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword");
        MTTextBox tbCnfPwd = tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("confirmPassword");
        
        //Hidden TextBox
         MTHidden hdUser = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorUsername");
         MTHidden hdAddedOn = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorAddedOn");
         MTHidden hdGroupID = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorGroupID");
         MTHidden hdStatusID = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorStatusID");
         MTHidden hdVerification = tbl_Tutor.GetControl<InMotion.Web.Controls.MTHidden>("TutorVerificationCode");
        
        if(!tbPwd.IsEmpty && !tbCnfPwd.IsEmpty && !(tbPwd.Value.ToString()).Equals(tbCnfPwd.Value.ToString()))
        {
        	tbCnfPwd.Errors.Add("Password and confirm password not match");
        }
        
        hdUser.Text = tbEmail.Text;
        hdAddedOn.Value = DateTime.Now;
        hdGroupID.Value = 2;
      //  hdStatusID.Value = 1;
        hdVerification.Value = sixDigitRandom();
        // -------------------------
//End Button Button_Insert Event On Click. Action Custom Code

//Button Button_Insert On Click event tail @5-FCB6E20C
    }
//End Button Button_Insert On Click event tail

//TextBox TutorEmail Event On Validate @8-EC4D93EE
    protected void tbl_TutorTutorEmail_Validating(object sender, ValidatingEventArgs e) {
//End TextBox TutorEmail Event On Validate

//TextBox TutorEmail Event On Validate. Action Validate Email @25-2FDE1777
        if (!tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").IsEmpty)
        {
            Regex mask = new Regex(@"^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (!mask.Match(tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text).Success)
            {
                tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Errors.Add(String.Format("Please Enter Valid Email", tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Caption));
                e.HasErrors = true;
            }
        }
//End TextBox TutorEmail Event On Validate. Action Validate Email

//TextBox TutorEmail On Validate event tail @8-FCB6E20C
    }
//End TextBox TutorEmail On Validate event tail

//TextBox TutorPassword Event On Validate @9-0A11E707
    protected void tbl_TutorTutorPassword_Validating(object sender, ValidatingEventArgs e) {
//End TextBox TutorPassword Event On Validate

//TextBox TutorPassword Event On Validate. Action Validate Minimum Length @26-CAC36612
        if (!tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").IsEmpty)
        {
        if(tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Text.Length < 8)
        {
            tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add(String.Format("Password should contain minimum 8 characters", tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Caption, "8"));
            e.HasErrors = true;
        }
        }
//End TextBox TutorPassword Event On Validate. Action Validate Minimum Length

//TextBox TutorPassword On Validate event tail @9-FCB6E20C
    }
//End TextBox TutorPassword On Validate event tail

//Page class tail @1-FCB6E20C
    protected void tbl_Tutor_Validating(object sender, ValidatingEventArgs e)
    {
        if (tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").Errors.Add("Cannot be empty");

            // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }



    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

