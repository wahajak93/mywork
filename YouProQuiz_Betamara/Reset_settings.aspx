﻿

<%@ Page language="C#" CodeFile="Reset_SettingsEvents.aspx.cs" Inherits="YouProQuiz_Beta.Reset_Settings_Page" MasterPageFile="~/Dashboard.master"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/uc_resetpassword.ascx" TagPrefix="uc1" TagName="uc_resetpassword" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Dashboard</title>


<script language="JavaScript" type="text/javascript">
    //Begin CCS script
    //Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
    //End Include Common JSFunctions

    //Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
    //End Include User Scripts

    //End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Reset Settings
    </h1>

   
    <uc1:uc_resetpassword runat="server" ID="uc_resetpassword" />
      
<hr>


    </asp:Content>
