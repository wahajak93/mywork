﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Reflection;

public partial class service_provider_page : System.Web.UI.Page
{

    public string user_id;
    public static ArrayList w;

    protected void Page_Load(object sender, EventArgs e)
    {



    }


    [WebMethod]
    public static ArrayList VCheck(string quiz_id)
    {
        
            string user_id = HttpContext.Current.Session["UserID"].ToString();
        DataTable dt1 = clsdb.readdataKMS("SELECT COALESCE(MAX(Quiz_AttemptID),0) AS max_quizattempt_id,COALESCE(MAX(Quest_AttemptID),0) AS max_questattempt_id FROM tblUserQuizAttempt WHERE QuizID=" + quiz_id + " AND UserID="+user_id+"");
        // int quest_attempt_id = Convert.ToInt32(dt1.Rows[0]["max_questattempt_id"].ToString()) + 1;
          int attempt = Convert.ToInt32(dt1.Rows[0]["max_quizattempt_id"].ToString());

        DataTable dt2 = clsdb.readdataKMS("SELECT Quiz_User_Attempt_Limit FROM TBL_Quiz WHERE Quiz_id="+quiz_id+"");
        string max_attempt=dt2.Rows[0]["Quiz_User_Attempt_Limit"].ToString();


        //Array abcd = new Array[5];
        w = new ArrayList();
        //here we will check if the user is eligible for the quiz or not
        // return "w";
        // 
        w.Add("" + attempt + "");
        //attempt no. of current quiz w1
        w.Add("1");
        // HttpContext.Current.Session["eligible"] = w ;//.ToString();
        // return "w";

        if (attempt >= int.Parse(max_attempt))
        {
            w.Add("Attempt Limit for this quiz exceeds. Maximum " + max_attempt + " Attempts Limit");
        }
        return w;

    }


    //[WebMethod(EnableSession = true)]
    //public static Object[] GetEmployeeDetail(string quiz)
    //{
    //    //User_Quiz_Authenticater ABC = new User_Quiz_Authenticater();

    //    string v=HttpContext.Current.Session["UserID"].ToString();
    //    string a = quiz;




    //    Employee objEmp12 = new Employee();
    //    objEmp12.EmpFirstName = "wahaj";
    //    objEmp12.EmpLastName = "Illumination";
    //    DataTable dt = clsdb.readdataKMS("Select Question from tblQuestion where Question_Name='q4';");
    //    objEmp12.lc = dt.Rows[0]["Question"].ToString();
    //    objEmp12.answer = new List<string>();
    //    objEmp12.answer.Add("asdasdwqeqwdzxcwerwrwqr");
    //    objEmp12.answer.Add("2");
    //    objEmp12.answer.Add("3");
    //    objEmp12.answer.Add("4");
    //    objEmp12.single_or_multiple=true;



    //    Object[] ab = new Object[] { objEmp12 };
    //    return ab;
    //}

    [WebMethod]
    public static Object[] Quiz_Loader(string quiz, string chk)
    {
        Object[] abc = new Object[2];
        string x = chk;

        if (w[0] != "0" && w[0].ToString() == x)
        {
            string v = HttpContext.Current.Session["UserID"].ToString();
            int a = int.Parse(quiz);




            DataTable dt_for_sections = clsdb.readdataKMS("SELECT * FROM tblSection WHERE QuizID=" + quiz + "");


            if (dt_for_sections.Rows.Count > 0)
            {

                abc[0] = Sectioning(quiz, dt_for_sections);

            }
            else
            {
                abc[0] = null;
                // abc[1] = Quest;
            }


            //
            // abc[1] = Quest;
















            DataTable dt1 = clsdb.readdataKMS("SELECT * FROM view_for_questions WHERE Quiz_ID=" + a + "");
            int tot_quest = dt1.Rows.Count;
            Question[] Quest = new Question[tot_quest];
            for (int i = 0; i < tot_quest; i++)
            {
                Quest[i] = new Question();

                Quest[i].quiz_id = a;
                Quest[i].question_id = int.Parse(dt1.Rows[i]["Quiz_Quest_ID"].ToString());
                Quest[i].question_name = dt1.Rows[i]["Question_Name"].ToString();
                Quest[i].question_text = dt1.Rows[i]["Question"].ToString();
                Quest[i].single_or_multiple = dt1.Rows[i]["Quest_Type"].ToString();
                Quest[i].time_given = int.Parse(dt1.Rows[i]["Quiz_Quest_Time"].ToString());

                //Quest[i].answers_of_question.Add(1, "dassd");
                // answers of each questions tot be filled separately 
                // Quest[i].answers_of_question=


                DataTable dt2 = clsdb.readdataKMS("SELECT * FROM view_for_question_choices where Quiz_ID=" + Quest[i].quiz_id + " AND Quest_ID=" + Quest[i].question_id + "");
                int tot_answers_of_curr_quest = dt2.Rows.Count;
                Quest[i].answers_of_question = new ArrayList();
                for (int j = 0; j < tot_answers_of_curr_quest; j++)
                {
                    if (Quest[i].single_or_multiple == "SingleChoice")
                    {
                        Quest[i].answers_of_question.Add("<input type=radio id='" + dt2.Rows[j]["Choice_ID"] + "' name='q" + Quest[i].question_id + "'  value=" + dt2.Rows[j]["Choice_ID"] + " />" + dt2.Rows[j]["Choice"] + "");

                    }
                    else if (Quest[i].single_or_multiple == "MultipleChoice")
                    {
                        string xx = dt2.Rows[j]["Choice_ID"].ToString();
                        string yy = dt2.Rows[j]["Choice"].ToString();

                        Quest[i].answers_of_question.Add("<input type='checkbox' id='" + dt2.Rows[j]["Choice_ID"] + "' name='q" + Quest[i].question_id + "'  value=" + dt2.Rows[j]["Choice_ID"] + " />" + dt2.Rows[j]["Choice"] + "");

                    }
                }
            }

            //from here we are performing the 

            // Object[] ab = new Object[] {  };






            //Random rnd = new Random();
            //Question[] MyRandomArray = Quest.OrderBy(aa => rnd.Next()).ToArray(); 

            //return MyRandomArray;

            abc[1] = Quest;

            //this is working for sections


            //here we will check if isRandom is true for quiz or not?
            //bool isRandom=true;
            //if (isRandom == true)
            //{
            //    abc[1] = demo_for_questio_shuffle();

            //}
            bool isRandom = true;
            if (isRandom == true)
            {
                abc[1] = shuffler(Quest);

            }

        }
        else
        {
            Question[] q1 = new Question[0];
            abc[0] = null;
            abc[1] = q1;
        }

        return abc;
    }






    private static Question[] shuffler(Question[] quest)
    {
        Random random = new Random();
        for (int i = quest.Length - 1; i > 0; i--)
        {
            int swapIndex = random.Next(i + 1);
            Question temp = quest[i];
            quest[i] = quest[swapIndex];
            quest[swapIndex] = temp;
        }
        return quest;

        //throw new NotImplementedException();
    }



    private static ArrayList shuffler(ArrayList quest)
    {
        Random random = new Random();
        for (int i = quest.Count - 1; i > 0; i--)
        {
            int swapIndex = random.Next(i + 1);
            object temp = quest[i];
            quest[i] = quest[swapIndex];
            quest[swapIndex] = temp;
        }
        return quest;

        //throw new NotImplementedException();
    }




    private static Section[] Sectioning(string quiz, DataTable dt_for_sec)
    {
        Section[] s1 = new Section[dt_for_sec.Rows.Count];

        for (int k = 0; k < s1.Length; k++)
        {
            Section sec1 = new Section();
            sec1.Section_id = int.Parse(dt_for_sec.Rows[k]["SectionID"].ToString());
            sec1.quiz_id = int.Parse(quiz);
            sec1.Sec_Note = dt_for_sec.Rows[k]["Section_Note"].ToString();
            sec1.Sec_Title = dt_for_sec.Rows[k]["SectionTitle"].ToString();
            sec1.Show_Random = dt_for_sec.Rows[k]["Show_Random"].ToString();
            sec1.Sec_Questions = new ArrayList();
            sec1.Sec_Max_Time = int.Parse(dt_for_sec.Rows[k]["SectionMaxTime"].ToString());
            DataTable dtx = clsdb.readdataKMS("SELECT * FROM tblSectionQuestions WHERE QuizID=" + quiz + " AND SectionID=" + sec1.Section_id + "");
            if (dtx.Rows.Count > 0)
            {
                for (int x = 0; x < dtx.Rows.Count; x++)
                {

                    sec1.Sec_Questions.Add(dtx.Rows[x]["QuestionID"]);

                }


            }

         

            //if the section property randomize question is set to true then we will do
            //the following code 

            bool isRandomForCurrSection=true;

            if (isRandomForCurrSection == true)
            {
                ArrayList a1 = new ArrayList();
                a1 = sec1.Sec_Questions;
           
                sec1.Sec_Questions = shuffler(a1);
            
            }

            


            s1[k] = sec1;

        }


        return s1;









        //throw new NotImplementedException();
    }
    public class Employee
    {
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }


        public bool single_or_multiple { get; set; }
        public List<string> answer { get; set; }

        public string lc { get; set; }
    }


    public class Question
    {
        public int quiz_id { get; set; }
        public int question_id { get; set; }
        public string question_name { get; set; }
        public string question_text { get; set; }
        public string single_or_multiple { get; set; }
        //public List<string> answer { get; set; }
        //public List<string>
        public int time_given { get; set; }
        // public int time_taken_to_attempt { get; set; }
        //    public int time_diff { get; set; }
        public ArrayList answers_of_question { get; set; }






        public string quiz_attempt_id { get; set; }
        //  public string quiz_id { get; set; }
        // public string quest_id { get; set; }
        public ArrayList answers_given { get; set; }

        // public DateTime time_loaded { get; set; }
        //   public DateTime time_responded { get; set; }
        public int delta { get; set; }

        public int section_id { get; set; }
        //which will by default be zero 


    }


    public class Section
    {
        public int Section_id { get; set; }
        public int quiz_id { get; set; }
        public int Sec_Max_Time { get; set; }
        public string Sec_Title { get; set; }
        public string Show_Random { get; set; }
        //public List<string> answer { get; set; }
        //public List<string>
        public string Sec_Note { get; set; }
        // public int time_taken_to_attempt { get; set; }
        //    public int time_diff { get; set; }
        public ArrayList Sec_Questions { get; set; }

        public int time_taken { get; set; }



    }



    [WebMethod]
    public static string ResponseCollecter(Question[] questionAttempted, Section[] SectionData)
    {
        string v = HttpContext.Current.Session["UserID"].ToString();
        // var json = new JavaScriptSerializer().Serialize(questionAttempted);
        //int a = int.Parse(quiz);

        // QuestionAttempted[] quest_response = questionAttempted;
        Question[] quest_response = questionAttempted;

        for (int y = 0; y < quest_response.Length; y++)
        {
            double score_of_curr_quest = 0;
            //QuestionAttempted qx = quest_response[y];

            Question qx = quest_response[y];

            DataTable dt1 = clsdb.readdataKMS("SELECT COALESCE(MAX(Quiz_AttemptID),0) AS max_quizattempt_id,COALESCE(MAX(Quest_AttemptID),0) AS max_questattempt_id FROM tblUserQuizAttempt WHERE QuizID=" + qx.quiz_id + " AND QuestionID=" + qx.question_id + "");
            int quest_attempt_id = Convert.ToInt32(dt1.Rows[0]["max_questattempt_id"].ToString()) + 1;




            var json_record_of_curr_quest = new JavaScriptSerializer().Serialize(qx);

            DataTable dtx1 = clsdb.readdataKMS("SELECT Quiz_Quest_Time FROM tbl_Quiz_Question WHERE Quiz_ID=" + qx.quiz_id + " AND Quiz_Quest_ID=" + qx.question_id + "");
            if (qx.time_given > int.Parse(dtx1.Rows[0][0].ToString()) + 5)
            {
                string xz = "something fishy happened at client";
            }
            else
            {
                score_of_curr_quest = ScoreCalculator(qx.quiz_id, qx.question_id, qx.answers_given, qx.single_or_multiple);

            }

            clsdb.writedataKMS("INSERT INTO tblUserQuizAttempt VALUES(" + qx.quiz_id + "," + qx.question_id + "," + v + "," + qx.quiz_attempt_id + "," + quest_attempt_id + ",'" + DateTime.Now + "'," + score_of_curr_quest + "," + qx.delta + ",'" + json_record_of_curr_quest + "')");

        }


        //  return quest_response.answers_given[0].ToString();
        //return quest_response.delta.ToString() ;
        //here we will gather the responses of the answers of the user input of the questions individually


        //below is the code for sendting the user response to db

        //  Add_to_tbl_User_Quiz_Attempt(v,quest_response);

        return questionAttempted[0].delta.ToString();

    }

    //[WebMethod]
    //public static string ResponseCollecter2(Question[] questionAttempted)
    //{
    //    // Object obj1 = questionAttempted[0];



    //    // var PropertyInfos = questionAttempted[0].GetType().GetField("Delta") ;
    //    //var PropertyInfos = SomeObject.GetType().GetProperties();
    //    return "ikfhuisdfuisa";



    //    // return xxx[0].Delta.ToString();



    //    //  d q1 = new Question();



    //    //   x.answers_given=



    //    // return "asdsad";


    //}


    private static double ScoreCalculator(int quizid, int questid, ArrayList answers_arr, string quest_type)
    {
        double Score = 0;

        if (quest_type == "SingleChoice")
        {
            if (answers_arr.Count <= 0)
            {
                answers_arr.Add("0000");
            }
           // string ax = answers_arr[0].ToString();
            DataTable dt1 = clsdb.readdataKMS("select * From tbl_Quiz_Question_Answers where Quiz_ID=" + quizid + " AND QuestionID=" + questid + " AND ChoiceID=" + answers_arr[0] + "");
            if (dt1.Rows.Count > 0)
            {
                Score = int.Parse(dt1.Rows[0]["Score"].ToString());

            }
            else
            {
                Score = 0;

            }
            return Score;

        }
        else if (quest_type == "MultipleChoice")
        {
            double choice_score = 0;
            int wrong = 0;

            if (answers_arr.Count <= 0)
            {
                answers_arr.Add("0000");
            }


            for (int a = 0; a < answers_arr.Count; a++)
            {
                DataTable dt1 = clsdb.readdataKMS("select * From tbl_Quiz_Question_Answers where Quiz_ID=" + quizid + " AND QuestionID=" + questid + " AND ChoiceID=" + answers_arr[a] + "");
                if (dt1.Rows.Count > 0)
                {

                    choice_score += int.Parse(dt1.Rows[0]["Score"].ToString());

                }
                else
                {
                    wrong = 1;
                }

            }

            if (wrong == 0)
            {
                Score = choice_score;
                return Score;
            }
            else
            {
                return Score;
            }
        }
        else
        {

            return Score;
        }

        //  DataTable dt1 = clsdb.readdataKMS("select * From view_for_questions where Quiz_ID="+quizid+" AND Quiz_Quest_ID="+questid+"");
        //  string quest
        //  DataTable dt2=dt1.Select().Where


        //throw new NotImplementedException();
    }
    private static void Add_to_tbl_User_Quiz_Attempt(string User_id, Question User_Response)
    {

        DataTable dt1 = clsdb.readdataKMS("SELECT COALESCE(MAX(Quiz_AttemptID),1) AS max_quizattempt_id,COALESCE(MAX(Quest_AttemptID),1) AS max_questattempt_id FROM tblUserQuizAttempt WHERE QuizID=" + User_Response.quiz_id + " AND QuestionID=" + User_Response.question_id + "");
        int quest_attempt_id = Convert.ToInt32(dt1.Rows[0]["max_questattempt_id"].ToString()) + 1;

        int attempt_time_taken = User_Response.delta - 1;
        int score = 5;

        var ax = User_Response;
        // JavaScriptSerializer serializer = new JavaScriptSerializer();
        //  var receivedResponse = (JsonObject)JsonObject.Load(responseStream);



        clsdb.writedataKMS("INSERT INTO tblUserQuizAttempt (QuizID,QuestionID,UserID,Quiz_AttemptID,Quest_AttemptID,AttemptDate,Score,Attempt_Time_Taken,Curr_Attempt_Record) VALUES(" + User_Response.quiz_id + "," + User_Response.question_id + "," + User_id + "," + User_Response.quiz_attempt_id + "," + quest_attempt_id + ",'" + DateTime.Now + "'," + score + "," + attempt_time_taken + ",'" + User_Response + "')");
        //throw new NotImplementedException();
    
    
    }
    public class QuestionAttempted
    {
        //   public string quiz_attempt_id { get; set; }
        // //  public string quiz_id { get; set; }
        //  // public string quest_id { get; set; }
        //   public ArrayList answers_given { get; set; }

        //  // public DateTime time_loaded { get; set; }
        ////   public DateTime time_responded { get; set; }
        //   public int delta { get; set; }

    }

    //return objEmp;


    [WebMethod]
    public static ResultObject Result_Maker(string quiz_id, string attempt,string ptmnd)
    {
        string user_id = HttpContext.Current.Session["UserID"].ToString();

        DataTable dt1 = clsdb.readdataKMS("SELECT * FROM tblUserQuizAttempt WHERE UserID=" + user_id + " AND QuizID=" + quiz_id + "");

        if (dt1.Rows.Count > 0)
        {

            ResultObject x = new ResultObject();

            //double[] MaxPointAchievable;
            ArrayList questions_name = new ArrayList();
            ArrayList question_id = new ArrayList();
            //  ArrayList Users_Scores_Avg = new ArrayList();
            //ArrayList Overall_Scores_Avg = new ArrayList();
            //  ArrayList Best_Scores_Avg = new ArrayList();
            //double[] User_Scores_For_Current_Attempt;
            int[] xxx;

            DataTable dt2 = new DataTable();
            DataTable dtx2 = clsdb.readdataKMS("SELECT DISTINCT Quiz_Quest_ID,Question_Name FROM view_for_questions WHERE Quiz_Quest_ID IN (SELECT QuestionID FROM tblSectionQuestions where QuizID=" + quiz_id + ")");
            if (dtx2.Rows.Count <= 0)
            {
                dt2 = clsdb.readdataKMS("SELECT Quiz_Quest_ID,Question_Name FROM view_for_questions WHERE Quiz_ID=" + quiz_id + "");

            }
            else
            {
                dt2 = dtx2;
            }



            DataTable dt_title = clsdb.readdataKMS("select Quiz_Title from TBL_Quiz where Quiz_id="+quiz_id+"");
            x.QuizTitle = dt_title.Rows[0]["Quiz_Title"].ToString();

            x.QuizID = quiz_id;
            x.AttemptID = attempt;
            xxx = new int[dt2.Rows.Count];
            double[] MaxPointAchievable = new double[dt2.Rows.Count];
            double[] User_Scores_For_Current_Attempt = new double[dt2.Rows.Count];
            x.time_taken_for_last_attempt_curr_user=new double[dt2.Rows.Count];
            x.average_time_taken_curr_usr = new double[dt2.Rows.Count];
            x.average_time_taken_all_usr = new double[dt2.Rows.Count];

            double[] Best_Scores_Avg = new double[dt2.Rows.Count];
            double[] Users_Scores_Avg = new double[dt2.Rows.Count];
            double[] Overall_Scores_Avg = new double[dt2.Rows.Count];
            for (int a = 0; a < dt2.Rows.Count; a++)
            {
                string q_name = a.ToString() + "-(" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + ")-" + dt2.Rows[a]["Question_Name"].ToString();
                questions_name.Add(q_name);

                DataTable dt3 = clsdb.readdataKMS("select COALESCE(Avg(Score),0) as avg_score_of_user,COALESCE(Avg(Attempt_Time_Taken),1) as average_time_taken_curr_usr from tblUserQuizAttempt where UserID=" + user_id + " AND QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + " AND QuizID=" + quiz_id + "");
                Users_Scores_Avg[a] = double.Parse(dt3.Rows[0]["avg_score_of_user"].ToString());
                x.average_time_taken_curr_usr[a] = double.Parse(dt3.Rows[0]["average_time_taken_curr_usr"].ToString());

                DataTable dt6 = clsdb.readdataKMS("select COALESCE(Score,0) as User_Scores_For_Current_Attempt,COALESCE(Attempt_Time_Taken,1) as time_taken_for_last_attempt_curr_user from tblUserQuizAttempt where UserID=" + user_id + " AND QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + " AND QuizID=" + quiz_id + " AND Quiz_AttemptID=" + attempt + "");

                if (dt6.Rows.Count > 0)
                {
                    User_Scores_For_Current_Attempt[a] = double.Parse(dt6.Rows[0]["User_Scores_For_Current_Attempt"].ToString());
                    x.time_taken_for_last_attempt_curr_user[a] = double.Parse(dt6.Rows[0]["time_taken_for_last_attempt_curr_user"].ToString());
                }


                DataTable dt4 = clsdb.readdataKMS("select COALESCE(Avg(Score),0) as overall_avg_score_of_users,COALESCE(Avg(Attempt_Time_Taken),1) as average_time_taken_all_usr from tblUserQuizAttempt where QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + " AND QuizID=" + quiz_id + "");
                Overall_Scores_Avg[a] = double.Parse(dt4.Rows[0]["overall_avg_score_of_users"].ToString());
                x.average_time_taken_all_usr[a] = double.Parse(dt4.Rows[0]["average_time_taken_all_usr"].ToString());

                DataTable dt5 = clsdb.readdataKMS("select COALESCE(MAX(Score),0) as Best_Scores_Avg from tblUserQuizAttempt where QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + " AND QuizID=" + quiz_id + "");
                Best_Scores_Avg[a] = double.Parse(dt5.Rows[0]["Best_Scores_Avg"].ToString());

                DataTable dt7 = clsdb.readdataKMS("SELECT COALESCE(SUM(Score),0) as xscore from tbl_Quiz_Question_Answers WHERE  Quiz_ID=" + quiz_id + " AND QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + "");
                //DataTable dt7 = clsdb.readdataKMS("SELECT COALESCE(SUM(Score),0) as xscore from tbl_Quiz_Question_Answers WHERE  Quiz_ID=" + quiz_id + " AND QuestionID=" + dt2.Rows[a]["Quiz_Quest_ID"].ToString() + "");




                MaxPointAchievable[a] = double.Parse(dt7.Rows[0]["xscore"].ToString());




            }

            //  questions_name.Add("q1");
            //questions_name.Add("q2");


            // ArrayList Users_Scores_Avg = new ArrayList();
            // Users_Scores_Avg.Add(14);
            //Users_Scores_Avg.Add(16);


            // ArrayList Overall_Scores_Avg = new ArrayList();
            //Overall_Scores_Avg.Add(25);
            //Overall_Scores_Avg.Add(21);

            // ArrayList Best_Scores_Avg = new ArrayList();
            // Best_Scores_Avg.Add(30);
            // Best_Scores_Avg.Add(23);


            double perc = 0;
            //for user current percentage

            double d1 = MaxPointAchievable.Average();
            double d2 = User_Scores_For_Current_Attempt.Average();
            perc = (d2 / d1) * 100;

            double perc_avg_of_curr_user = 0;
            double d3 = Users_Scores_Avg.Average();

            perc_avg_of_curr_user = (d3 / d1) * 100;


            //////////////////////
            double perc_avg_of_best_scores = 0;

            double d4 = Best_Scores_Avg.Average();

            perc_avg_of_best_scores = (d4 / d1) * 100;

            ////////////////////////////////////////////

            double perc_avg_of_curr_quiz = 0;
            double d5 = Overall_Scores_Avg.Average();
            perc_avg_of_curr_quiz = (d5 / d1) * 100;



            // x.aa=new string[2];
            //x.bb = new int[2];
            x.Questions = new ArrayList(questions_name);
            x.User_Scores = new ArrayList(Users_Scores_Avg);
            x.Best_Scores = new ArrayList(Best_Scores_Avg);
            x.Avg_Scores = new ArrayList(Overall_Scores_Avg);
            x.User_Scores_For_Current_Attempt = new ArrayList(User_Scores_For_Current_Attempt);
            x.Avg_Overall_Percentage_of_Current_Quiz = Math.Round(perc_avg_of_curr_quiz, 2);
            
         ///
         /// calculating best score and username 
            ///

            DataTable dt_best_scorer = clsdb.readdataKMS("SELECT top 1 UserID as uid,Avg(ResultInPercent) as best_avg from tblUserQuizResultData where QuizID=" + quiz_id + " group by UserID order by best_avg desc");
            if (dt_best_scorer.Rows.Count > 0)
            {

                double bst = double.Parse(dt_best_scorer.Rows[0]["best_avg"].ToString());
                x.Best_Overall_Percentage_of_Current_Quiz = Math.Round(perc_avg_of_best_scores, 2);

                DataTable dt_best_scorer_name = clsdb.readdataKMS("Select user_name from TBL_users where id="+dt_best_scorer.Rows[0]["uid"].ToString()+"");
                x.Best_scorer_Name = dt_best_scorer_name.Rows[0][0].ToString();
            }
            else
            {
                DataTable dt_best_scorer_name = clsdb.readdataKMS("Select user_name from TBL_users where id=" +user_id+ "");
               
                x.Best_Overall_Percentage_of_Current_Quiz = Math.Round(perc_avg_of_best_scores, 2);

                x.Best_scorer_Name = dt_best_scorer_name.Rows[0][0].ToString();
            }
            x.User_Overall_Percentage_of_Current_Quiz = Math.Round(perc_avg_of_curr_user, 2);
            x.User_Overall_Percentage_of_Current_Attempt = Math.Round(perc, 2);

            //  User_Scores_For_Current_Attempt
          


            ////////////////////////
            /// calculation for section performance
            /// 

            x.curr_quiz_section = new ArrayList();
            x.curr_quiz_section_curr_user_avg = new ArrayList();

            DataTable dts1 = clsdb.readdataKMS("SELECT * FROM tblSection WHERE QuizID="+quiz_id+"");
            if (dts1.Rows.Count > 0)
            {
                for (int k = 0; k < dts1.Rows.Count; k++)
                {
                    x.curr_quiz_section.Add(k.ToString() + "-" + dts1.Rows[k]["SectionTitle"].ToString());

                    DataTable dts1a = clsdb.readdataKMS("SELECT COALESCE(SUM(Score),0) as tot_score_of_section from tbl_Quiz_Question_Answers where QuestionID IN (SELECT QuestionID from tblSectionQuestions WHERE SectionID=" + dts1.Rows[k]["SectionID"] + " and QuizID=" + quiz_id + ") AND Quiz_ID=" + quiz_id + "");
                    double dd1_sect_max_score = double.Parse(dts1a.Rows[0]["tot_score_of_section"].ToString());

                  //  DataTable dts1b = clsdb.readdataKMS("SELECT SUM(Score) as avg_score_of_section_for_curr_usr from tblUserQuizAttempt where QuestionID IN (SELECT QuestionID from tblSectionQuestions WHERE SectionID=" + dts1.Rows[k]["SectionID"] + " and QuizID=" + quiz_id + ") AND QuizID=" + quiz_id + " AND UserID=" + user_id + "");
                   
                    
                    // double dd_user_score_avg = double.Parse(dts1b.Rows[0]["avg_score_of_section_for_curr_usr"].ToString());
                    
                    DataTable dts1b = clsdb.readdataKMS(" SELECT QuestionID,COALESCE(Avg(Score),0) as score FROM tblUserQuizAttempt WHERE QuizID="+quiz_id+" AND UserID="+user_id+" AND QuestionID IN (SELECT QuestionID from tblSectionQuestions WHERE SectionID="+dts1.Rows[k]["SectionID"]+" and QuizID="+quiz_id+") GROUP BY QuestionID");
                    double[] dd_user_score_avg = new double[dts1b.Rows.Count];
                    if (dts1b.Rows.Count > 0)
                    {
                       
                        for (int b = 0; b < dts1b.Rows.Count; b++)
                        { 
                        dd_user_score_avg[b]=double.Parse(dts1b.Rows[b]["score"].ToString());
                        
                        }
                    
                    }



                    double user_sec_percentage = (dd_user_score_avg.Sum()/ dd1_sect_max_score) * 100;
                    x.curr_quiz_section_curr_user_avg.Add(user_sec_percentage);

                }


            }



        
            if (ptmnd == "101110110111110110111010011111")
            {

                var json_record_of_curr_quest = new JavaScriptSerializer().Serialize(x);

                clsdb.writedataKMS("INSERT INTO tblUserQuizResultData VALUES(" + user_id + "," + quiz_id + "," + attempt + ",'" + DateTime.Now + "','" + json_record_of_curr_quest + "'," + x.User_Overall_Percentage_of_Current_Attempt + ")");
       
            
            }







            HttpContext.Current.Session["eligible"] = "no";
            return x;




        }
        else
        {
            ResultObject x = new ResultObject();

            return x;

        }

    }


    [WebMethod]
    public static object Result_Maker1(string quiz_id, string attempt)
    {
        string user_id = HttpContext.Current.Session["UserID"].ToString();

        DataTable dtx1 = clsdb.readdataKMS("SELECT Distinct QuizID,MAX(AttemptID) as att_id FROM tblUserQuizResultData WHERE UserID=" + user_id + " GROUP BY QuizID");
        object[] attempt_datas = new object[dtx1.Rows.Count];

        for (int j = 0; j < dtx1.Rows.Count;j++ )
        {

            DataTable dt1 = clsdb.readdataKMS("SELECT AttemptResultData FROM tblUserQuizResultData WHERE UserID=" + user_id + " AND QuizID=" + dtx1.Rows[j]["QuizID"] + " AND AttemptID=" + dtx1.Rows[j]["att_id"] + "");
             attempt_datas[j] = dt1.Rows[0]["AttemptResultData"].ToString();

        }

        //DataTable dt1 = clsdb.readdataKMS("SELECT * FROM tblUserQuizResultData WHERE UserID=" + user_id + "");



        //for (int z = 0; z < dt1.Rows.Count; z++)
        //{

        //    attempt_datas[z] = dt1.Rows[z]["AttemptResultData"].ToString();

        //}


        //var json_record_of_curr_quest = new JavaScriptSerializer().DeserializeObject(attempt_datas.ToString());
        // var zzz=JsonSerialize

        return attempt_datas;


      //  return x;

       // return attempt_datas;
        //return "adasdad";


    }

    [WebMethod]
    public static object Result_Maker2(string quiz_id, string attempt)
    {
        string user_id = HttpContext.Current.Session["UserID"].ToString();

        //DataTable dtx1 = clsdb.readdataKMS("SELECT Distinct QuizID,MAX(AttemptID) as att_id FROM tblUserQuizResultData WHERE UserID=" + user_id + " GROUP BY QuizID");
        object attempt_data = new object();

        //for (int j = 0; j < dtx1.Rows.Count; j++)
        //{

            DataTable dt1 = clsdb.readdataKMS("SELECT AttemptResultData FROM tblUserQuizResultData WHERE UserID=" + user_id + " AND QuizID=" + quiz_id + " AND AttemptID=" + attempt + "");
            attempt_data = dt1.Rows[0]["AttemptResultData"].ToString();

        

        //DataTable dt1 = clsdb.readdataKMS("SELECT * FROM tblUserQuizResultData WHERE UserID=" + user_id + "");



        //for (int z = 0; z < dt1.Rows.Count; z++)
        //{

        //    attempt_datas[z] = dt1.Rows[z]["AttemptResultData"].ToString();

        //}


        //var json_record_of_curr_quest = new JavaScriptSerializer().DeserializeObject(attempt_datas.ToString());
        // var zzz=JsonSerialize

        return attempt_data;


        //  return x;

        // return attempt_datas;
        //return "adasdad";


    }









    public class ResultObject
    {
        public string QuizID { get; set; }

        public string QuizTitle { get; set; }
        public string AttemptID { get; set; }

        public ArrayList Questions { get; set; }
        public ArrayList User_Scores { get; set; }
        public ArrayList Avg_Scores { get; set; }

        public ArrayList Best_Scores { get; set; }

        public double[] time_taken_for_last_attempt_curr_user { get; set; }

        public double[] average_time_taken_all_usr { get; set; }

        public double[] average_time_taken_curr_usr { get; set; }

        public ArrayList User_Scores_For_Current_Attempt { get; set; }

        public double User_Overall_Percentage_of_Current_Quiz { get; set; }

        public double Avg_Overall_Percentage_of_Current_Quiz { get; set; }

        public double Best_Overall_Percentage_of_Current_Quiz { get; set; }

        public string Best_scorer_Name { get; set; }

        public double User_Overall_Percentage_of_Current_Attempt { get; set; }



        /// <summary>
        /// ///for section calculation,,
        /// below 
        /// </summary>
        public ArrayList curr_quiz_section { get; set; }
        public ArrayList curr_quiz_section_curr_attempt_score { get; set; }
        public ArrayList curr_quiz_section_curr_user_avg { get; set; }
        public ArrayList curr_quiz_section_best_score { get; set; }
        public ArrayList curr_quiz_section_avg_score { get; set; }

        ////////////////////////////
        //section wise distribution 
        ////////////////////////////
    }


}