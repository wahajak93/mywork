<!--ASCX page header @1-5F476BC8-->
<%@ Control language="C#" CodeFile="UCKMS_AddNewQuizEvents.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_AddNewQuiz_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-CD9F93B9-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
&nbsp;&nbsp; 
<mt:Record PreserveParameters="Get" ReturnPage="~/KMS_AddNewQuiz.aspx" DataSourceID="TBL_QuizDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" OnBeforeInsert="TBL_Quiz_BeforeInsert" ID="TBL_Quiz" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuizTBL_Quiz" id="UCKMS_AddNewQuizTBL_Quiz">







  <h2>Add/Edit TBL Quiz </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").ClientID %>">Quiz Descp</label></td> 
      <td>
<mt:MTTextBox TextMode="Multiline" Source="Quiz_Descp" DataType="Memo" Caption="Quiz Descp" Rows="3" Columns="50" ID="Quiz_Descp" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Descp" runat="server"/>
</td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").ClientID %>">Quiz Title</label></td> 
      <td><mt:MTTextBox Source="Quiz_Title" Caption="Quiz Title" maxlength="100" Columns="50" ID="Quiz_Title" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Title" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").ClientID %>">Time Limit Secs</label></td> 
      <td><mt:MTTextBox Source="Time_limit_secs" DataType="Integer" Caption="Time Limit Secs" maxlength="10" Columns="10" ID="Time_limit_secs" data-id="UCKMS_AddNewQuizTBL_QuizTime_limit_secs" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").ClientID %>">Quiz Mode</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Mode" DataType="Integer" Caption="Quiz Mode" DataSourceID="Quiz_ModeDataSource" DataValueField="id" DataTextField="Quiz_Mode" ID="Quiz_Mode" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Mode" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_ModeDataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM TBL_Quiz_Mode {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").ClientID %>">Quiz Total Points</label></td> 
      <td><mt:MTTextBox Source="Quiz_Total_Points" DataType="Integer" Caption="Quiz Total Points" maxlength="10" Columns="10" ID="Quiz_Total_Points" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Total_Points" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").ClientID %>">Quiz Category</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Category" DataType="Integer" Caption="Quiz Category" DataSourceID="Quiz_CategoryDataSource" DataValueField="id" DataTextField="Category_Name" ID="Quiz_Category" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Category" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_CategoryDataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM TBL_Category {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").ClientID %>">Quiz User Attempt Limit</label></td> 
      <td><mt:MTTextBox Source="Quiz_User_Attempt_Limit" DataType="Integer" Caption="Quiz User Attempt Limit" maxlength="10" Columns="10" ID="Quiz_User_Attempt_Limit" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_User_Attempt_Limit" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTImageButton CommandName="Insert" ImageUrl="{page:pathToRoot}Styles/None/Images/en/ButtonInsert.gif" style="BORDER-RIGHT-WIDTH: 0px; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px" AlternateText="Add" ID="Button_Insert" data-id="UCKMS_AddNewQuizTBL_QuizButton_Insert" runat="server"/><mt:MTHidden Source="Added_on" Caption="Added On" ID="Added_on" data-id="UCKMS_AddNewQuizTBL_QuizAdded_on" runat="server"/><mt:MTHidden Source="Added_By" DataType="Integer" Caption="Added By" OnInit="TBL_QuizAdded_By_Init" ID="Added_By" data-id="UCKMS_AddNewQuizTBL_QuizAdded_By" runat="server"/><mt:MTHidden Source="Quiz_Value_Type" DataType="Integer" Caption="Quiz Value Type" OnLoad="TBL_QuizQuiz_Value_Type_Load" ID="Quiz_Value_Type" data-id="UCKMS_AddNewQuizTBL_QuizQuiz_Value_Type" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="TBL_QuizDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO TBL_Quiz([Quiz_Descp], [Quiz_Title], [Time_limit_secs], [Quiz_Mode], [Added_on], [Added_By], [Quiz_Total_Points], [Quiz_Category], [Quiz_Value_Type], [Quiz_User_Attempt_Limit]) VALUES ({Quiz_Descp}, {Quiz_Title}, {Time_limit_secs}, {Quiz_Mode}, {Added_on}, {Added_By}, {Quiz_Total_Points}, {Quiz_Category}, {Quiz_Value_Type}, {Quiz_User_Attempt_Limit})
  </InsertCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Text" Operation="And" Condition="Equal" SourceColumn="Quiz_id" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="Quiz_id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
<mt:Record ID="TBL_QuizSearch" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuizTBL_QuizSearch" id="UCKMS_AddNewQuizTBL_QuizSearch">


  <h2>Search </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").ClientID %>">Quiz Id</label></td> 
      <td><mt:MTTextBox Source="Quiz_id" DataType="Float" Caption="Quiz Id" maxlength="12" Columns="12" ID="s_Quiz_id" data-id="UCKMS_AddNewQuizTBL_QuizSearchs_Quiz_id" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_Title").ClientID %>">Quiz Title</label></td> 
      <td><mt:MTTextBox Source="Quiz_Title" Caption="Quiz Title" maxlength="100" Columns="50" ID="s_Quiz_Title" data-id="UCKMS_AddNewQuizTBL_QuizSearchs_Quiz_Title" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_Category").ClientID %>">Quiz Category</label></td> 
      <td><mt:MTTextBox Source="Quiz_Category" DataType="Integer" Caption="Quiz Category" maxlength="10" Columns="10" ID="s_Quiz_Category" data-id="UCKMS_AddNewQuizTBL_QuizSearchs_Quiz_Category" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="UCKMS_AddNewQuizTBL_QuizSearchButton_DoSearch" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="TBL_Quiz1DataSource" ID="TBL_Quiz1" runat="server">
<HeaderTemplate>
<h2>List of TBL Quiz</h2>
<p>
<table>
  <tr>
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Id" ID="Sorter_Quiz_id" data-id="UCKMS_AddNewQuizTBL_Quiz1Sorter_Quiz_id" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Title" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Title" ID="Sorter_Quiz_Title" data-id="UCKMS_AddNewQuizTBL_Quiz1Sorter_Quiz_Title" runat="server"/></th>
 
    <th scope="col">Quiz Descp</th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Added_on" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Added On" ID="Sorter_Added_on" data-id="UCKMS_AddNewQuizTBL_Quiz1Sorter_Added_on" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Category" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Category" ID="Sorter_Quiz_Category" data-id="UCKMS_AddNewQuizTBL_Quiz1Sorter_Quiz_Category" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_User_Attempt_Limit" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz User Attempt Limit" ID="Sorter_Quiz_User_Attempt_Limit" data-id="UCKMS_AddNewQuizTBL_Quiz1Sorter_Quiz_User_Attempt_Limit" runat="server"/></th>
 
    <th scope="col">&nbsp;</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTLabel Source="Quiz_id" DataType="Float" ID="Quiz_id" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Title" ID="Quiz_Title" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Descp" DataType="Memo" ID="Quiz_Descp" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Added_on" DataType="Date" ID="Added_on" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_Category" DataType="Integer" ID="Quiz_Category" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_User_Attempt_Limit" DataType="Integer" ID="Quiz_User_Attempt_Limit" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLink Text="Edit Quiz" ID="Link1" data-id="UCKMS_AddNewQuizTBL_Quiz1Link1_{TBL_Quiz1:rowNumber}" runat="server" HrefSource="~/KMS_EditQuiz.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="Quiz_id" SourceType="DataSourceColumn" Source="Quiz_id"/>
    </Parameters></mt:MTLink>&nbsp;</td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="7">No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td colspan="7">
      <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/>&nbsp;</td> 
  </tr>
</table>
&nbsp;</p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="TBL_Quiz1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" OnInitializeSelectParameters="TBL_Quiz1_InitializeSelectParameters" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} Quiz_id, Quiz_Title, Quiz_Descp, Added_on, Quiz_Category, Quiz_User_Attempt_Limit 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM TBL_Quiz
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Added_By"/>
     <mt:WhereParameter Name="Urls_Quiz_id" SourceType="URL" Source="s_Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="Quiz_id"/>
     <mt:WhereParameter Name="Urls_Quiz_Title" SourceType="URL" Source="s_Quiz_Title" DataType="Text" Operation="And" Condition="Contains" SourceColumn="Quiz_Title"/>
     <mt:WhereParameter Name="Urls_Quiz_Category" SourceType="URL" Source="s_Quiz_Category" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_Category"/>
   </SelectParameters>
</mt:MTDataSource>
<p><br>
<br>
&nbsp; </p>
<p>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>




<!--End ASCX page-->

