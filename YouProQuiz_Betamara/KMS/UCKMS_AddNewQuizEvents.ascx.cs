//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-A2CAB1C2
public partial class UCKMS_AddNewQuiz_Page : MTUserControl
{
//End Page class

//Attributes constants @1-52B30067
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UCKMS_AddNewQuiz Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_AddNewQuiz Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UCKMS_AddNewQuiz Init event tail @1-FCB6E20C
    }
//End Page UCKMS_AddNewQuiz Init event tail

//Record TBL_Quiz Event Before Insert @2-2F8ACA51
    protected void TBL_Quiz_BeforeInsert(object sender, EventArgs e) {
//End Record TBL_Quiz Event Before Insert

//Record TBL_Quiz Event Before Execute Insert. Action Retrieve Value for Control @33-303E9136
        TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_By").Value = Session["UserID"].ToString();
  TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_on").Value =DateTime.Now.ToString();

//Record TBL_Quiz Event Before Insert. Action Custom Code @34-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record TBL_Quiz Event Before Insert. Action Custom Code

//Record TBL_Quiz Before Insert event tail @2-FCB6E20C
    }
//End Record TBL_Quiz Before Insert event tail

//Hidden Added_By Event Init @13-13DB822B
    protected void TBL_QuizAdded_By_Init(object sender, EventArgs e) {
//End Hidden Added_By Event Init

//Set Attributes @13-BCD2EB53
        ((InMotion.Web.Controls.MTHidden)sender).Attributes.Add("aaa", Convert.ToString(System.Web.HttpContext.Current.Session["UserID"]));
//End Set Attributes

//Hidden Added_By Init event tail @13-FCB6E20C
    }
//End Hidden Added_By Init event tail

//Record TBL_Quiz Event Before Execute Insert @2-E41D91C1

//End Record TBL_Quiz Before Execute Insert event tail

//Record TBL_Quiz Event After Insert @2-7C288F94
 
//End Record TBL_Quiz After Insert event tail

//Hidden Added_By Event Init @13-13DB822B
  
//End Hidden Added_By Init event tail

//Hidden Quiz_Value_Type Event Load @17-1F25DD5D
    protected void TBL_QuizQuiz_Value_Type_Load(object sender, EventArgs e) {
//End Hidden Quiz_Value_Type Event Load

//Set Default Value @17-8869B3A0
        ((InMotion.Web.Controls.MTHidden)sender).DefaultValue = 1;
//End Set Default Value

//Hidden Quiz_Value_Type Load event tail @17-FCB6E20C
    }
//End Hidden Quiz_Value_Type Load event tail

//Grid TBL_Quiz1 Event Initialize Select Parameters @35-C168C7F4
    protected void TBL_Quiz1_InitializeSelectParameters(object sender, EventArgs e) {
//End Grid TBL_Quiz1 Event Initialize Select Parameters

//Initialize expression parameters and default values  @35-9B059B48
        ((MTDataSourceView)sender).SelectParameters["SesUserID"].DefaultValue = 0;
//End Initialize expression parameters and default values 

//Grid TBL_Quiz1 Initialize Select Parameters event tail @35-FCB6E20C
    }
//End Grid TBL_Quiz1 Initialize Select Parameters event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

