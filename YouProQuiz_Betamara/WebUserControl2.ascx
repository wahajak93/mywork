﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebUserControl2.ascx.cs" Inherits="WebUserControl2" %>

   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
      <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

  <script>

        var params = {};

        if (location.search) {
            var parts = location.search.substring(1).split('&');

            for (var i = 0; i < parts.length; i++) {
                var nv = parts[i].split('=');
                if (!nv[0]) continue;
                params[nv[0]] = nv[1] || true;
            }
        }


        var us_id = params.user_id;
        var quiz_id=params.quiz_id;
        

    
 
    

    </script>

<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Time Taken to attempt each questions'
        },
        subtitle: {
            text: 'Efficiency comparison'
        },
        xAxis: {
            categories: questions
        },
        yAxis: {
            title: {
                text: 'Time(Sec)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series:series_maker
        
    });
});
   
    </script>

<script>
    $(function () {
        $('#container2').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Points comparison for each questions'
            },
            subtitle: {
                text: 'Efficiency comparison'
            },
            xAxis: {
                categories: questions
            },
            yAxis: {
                title: {
                    text: 'Points'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: series_maker2

        });
    });


   

    </script>


<h1>User Overall Record</h1>


<div id="container" style="min-width: 200px; height: 300px; margin: 0 auto;"></div>
<div id="container2" style="min-width: 200px; height: 300px; margin: 0 auto;"></div>
<p>
    &nbsp;</p>
<p>
    &nbsp;</p>

