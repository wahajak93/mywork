﻿<%--<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-83597163-->--%>
<%@ Page language="C#" CodeFile="DashboardEvents.aspx.cs" Inherits="YouProQuiz_Beta.Dashboard_Page"  MasterPageFile="~/Dashboard.master" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_User_Record" Src="UC_User_Record.ascx" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_Quiz_Lists" Src="UC_Quiz_Lists.ascx" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_LogOut" Src="UC_LogOut.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/UC1_UserOverallResult.ascx" TagPrefix="YouProQuiz_Beta" TagName="UC1_UserOverallResult" %>
<%@ Register Src="~/UCKMS_UserQuizRecordKeeper.ascx" TagPrefix="YouProQuiz_Beta" TagName="UserQuizRecordKeeper" %>




<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

  
 

<title>Dashboard</title>


<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</asp:Content>
<%--</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">
--%>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
<YouProQuiz_Beta:UC_LogOut ID="UC_LogOut" runat="server"/>
    </div>

<mt:MTLabel OnBeforeShow="Label2_BeforeShow" ID="Label2" runat="server" />
    <div>
    <asp:LinkButton ID="LinkButton1" runat="server"  PostBackUrl="~/Reset_Settings.aspx">Reset Settings</asp:LinkButton>
        </div>
    <%--    <YouProQuiz_Beta:UC_UserOverallQuizChart runat="server" id="UC_UserOverallQuizChart" />--%>

    <YouProQuiz_Beta:UserQuizRecordKeeper runat="server" ID="UserQuizRecordKeeper" />
 
<YouProQuiz_Beta:UC_User_Record ID="UC_User_Record" runat="server"/> 

    <YouProQuiz_Beta:UC_Quiz_Lists ID="UC_Quiz_Lists" runat="server" />


</asp:Content>
<%--</form>
</body>
</html>

<!--End ASPX page-->--%>

