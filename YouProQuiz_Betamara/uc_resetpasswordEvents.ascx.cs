//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-B12BB90D
public partial class uc_resetpassword_Page : MTUserControl
{
//End Page class

//Attributes constants @1-93D58DF7
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page uc_resetpassword Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page uc_resetpassword Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page uc_resetpassword Init event tail @1-FCB6E20C
    }
//End Page uc_resetpassword Init event tail

//Record TBL_users Event Before Show @2-B2039AE7
    protected void TBL_users_BeforeShow(object sender, EventArgs e) {
//End Record TBL_users Event Before Show

//Record TBL_users Event Before Show. Action Preserve Password @4-7912C677
        TBL_users.GetControl<MTHidden>("user_password_Shadow").Text = InMotion.Security.Cryptography.EncodeString(TBL_users.GetControl<MTTextBox>("user_password").Text, ConfigurationManager.AppSettings["RC4EncryptionKey"]);
        TBL_users.GetControl<MTTextBox>("user_password").Text = "";
//End Record TBL_users Event Before Show. Action Preserve Password

//Record TBL_users Before Show event tail @2-FCB6E20C
    }
//End Record TBL_users Before Show event tail

//Record TBL_users Event On Validate @2-D452BD7D
    protected void TBL_users_Validating(object sender, ValidatingEventArgs e) {
//End Record TBL_users Event On Validate

//Record TBL_users Event On Validate. Action Custom Code @19-2A29BDB7
        // -------------------------
        // Write your own code here.
        
         MTTextBox tbPwd = TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_password");
        MTTextBox tbCnfPwd = TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("confirmpassword");
        
        if(!(tbPwd.IsEmpty && tbCnfPwd.IsEmpty))
        {
        	if(!(tbPwd.Text).Equals(tbCnfPwd.Text))
        	{
        		tbPwd.Errors.Add("Password and Confirm Password not match");
        	}
        }
        // -------------------------
//End Record TBL_users Event On Validate. Action Custom Code

//Record TBL_users On Validate event tail @2-FCB6E20C
    }
//End Record TBL_users On Validate event tail

//Record TBL_users Event Before Build Update @2-0ECC98BA
    protected void TBL_users_BeforeBuildUpdate(object sender, DataOperationEventArgs e) {
//End Record TBL_users Event Before Build Update

//Record TBL_users Event Before Build Update. Action Encrypt Password @6-61A54B64
        if (e.Command.Parameters["user_password"].Value.ToString() != "")
            e.Command.Parameters["user_password"].Value = InMotion.Security.Cryptography.MD5(e.Command.Parameters["user_password"].Value.ToString());
        else
            e.Command.Parameters["user_password"].Value = InMotion.Security.Cryptography.DecodeString(TBL_users.GetControl<MTHidden>("user_password_Shadow").Text, ConfigurationManager.AppSettings["RC4EncryptionKey"]);
//End Record TBL_users Event Before Build Update. Action Encrypt Password

//Record TBL_users Before Build Update event tail @2-FCB6E20C
    }
//End Record TBL_users Before Build Update event tail

//Record TBL_users Event After Update @2-CC8FF7CD
    protected void TBL_users_AfterUpdate(object sender, DataOperationCompletingEventArgs e) {
//End Record TBL_users Event After Update

//Record TBL_users Event After Update. Action Custom Code @18-2A29BDB7
        // -------------------------
        // Write your own code here.
          TBL_users.Errors.Add("Your Password has changed");
        Session["passChanged"] = "Your Password has changed";
        // -------------------------
//End Record TBL_users Event After Update. Action Custom Code

//Record TBL_users After Update event tail @2-FCB6E20C
    }
//End Record TBL_users After Update event tail

//TextBox user_password Event On Validate @8-AF4CB325
    protected void TBL_usersuser_password_Validating(object sender, ValidatingEventArgs e) {
//End TextBox user_password Event On Validate

//TextBox user_password Event On Validate. Action Reset Password Validation @9-DCFF5233
        if (!TBL_users.IsInsertMode && TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").IsEmpty)
        {
            TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").Errors.Clear();
            TBL_users.RemoveError(TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_password"));
            e.HasErrors = false;
        }
//End TextBox user_password Event On Validate. Action Reset Password Validation

//TextBox user_password On Validate event tail @8-FCB6E20C
    }
//End TextBox user_password On Validate event tail

//TextBox oldpassword Event On Validate @16-6F00BBCC
    protected void TBL_usersoldpassword_Validating(object sender, ValidatingEventArgs e) {
//End TextBox oldpassword Event On Validate

//TextBox oldpassword Event On Validate. Action Custom Code @17-2A29BDB7
        // -------------------------
        // Write your own code here.
          MTTextBox tbOldPassword = TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("oldpassword");
        string oldPassword = InMotion.Security.Cryptography.MD5(tbOldPassword.Text);
        string dOldPassword = DataUtility.DLookup<MTText>("user_password", "TBL_users", "ID='"+Session["UserID"]+"'", "InMotion:DBYouProQuiz");
        if(!tbOldPassword.IsEmpty)
        {
        	if(!oldPassword.Equals(dOldPassword))
        	{
        		TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("oldpassword").Errors.Add("Wrong Old Password");
        		e.HasErrors = true;
        	}
        }
        // -------------------------
//End TextBox oldpassword Event On Validate. Action Custom Code

//TextBox oldpassword On Validate event tail @16-FCB6E20C
    }
//End TextBox oldpassword On Validate event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

