//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-A0E896F2
public partial class UC_LoginPage_Page : MTUserControl
{
//End Page class

//Attributes constants @1-93D58DF7
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page UC_LoginPage Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_LoginPage Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UC_LoginPage Init event tail @1-FCB6E20C
    }
//End Page UC_LoginPage Init event tail

//Button Button_DoLogin Event On Click @3-44947DFE
    protected void LoginButton_DoLogin_Click(object sender, ImageClickEventArgs e) {
//End Button Button_DoLogin Event On Click

//Button Button_DoLogin Event On Click. Action Login @6-2EF623F8
        if (Membership.ValidateUser(Login.GetControl<InMotion.Web.Controls.MTTextBox>("login1").Text, InMotion.Security.Cryptography.MD5(Login.GetControl<InMotion.Web.Controls.MTTextBox>("password").Text)))
        {
            FormsAuthentication.SetAuthCookie(Login.GetControl<InMotion.Web.Controls.MTTextBox>("login1").Text, false);
            if (HttpContext.Current.Request.QueryString["ReturnUrl"] != null)
                if (sender is MTImageButton)
                    ((MTImageButton)sender).ReturnPage = FormsAuthentication.GetRedirectUrl(Login.GetControl<InMotion.Web.Controls.MTTextBox>("login1").Text, false);
                else
                    ((MTButton)sender).RedirectUrl = FormsAuthentication.GetRedirectUrl(Login.GetControl<InMotion.Web.Controls.MTTextBox>("login1").Text, false);
        
        
        
        }
        else
        {
            Login.Errors.Add(InMotion.Common.Resources.ResManager.GetString("CCS_LoginError"));
        }
//End Button Button_DoLogin Event On Click. Action Login

//Button Button_DoLogin On Click event tail @3-FCB6E20C
    }
//End Button Button_DoLogin On Click event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

