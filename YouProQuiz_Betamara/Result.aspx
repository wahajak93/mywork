﻿<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-83597163-->
<%@ Page language="C#" CodeFile="ResultEvents.aspx.cs" Inherits="YouProQuiz_Beta.Result_Page"  %>

<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_LogOut" Src="UC_LogOut.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/WebUserControl2.ascx" TagPrefix="YouProQuiz_Beta" TagName="WebUserControl2" %>
<%@ Register Src="~/UC_Result_Message.ascx" TagPrefix="YouProQuiz_Beta" TagName="UC_Result_Message" %>





<!--End ASPX page header-->

<!--ASPX page @1-5372152B-->
<html>
<head>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Dashboard</title>


<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">


<p>&nbsp;</p>
<p><YouProQuiz_Beta:UC_LogOut ID="UC_LogOut" runat="server"/></p>
<p>


    <YouProQuiz_Beta:WebUserControl2 runat="server" ID="WebUserControl2" />

    <YouProQuiz_Beta:UC_Result_Message runat="server" id="UC_Result_Message" />

</form>
<center><font face="Arial"><small>&#71;&#101;n&#101;ra&#116;&#101;&#100; ;&#100; <!-- SCC -->&#119;&#105;&#116;&#104; <!-- CCS -->&#67;od&#101;Ch&#97;&#114;g&#101; <!-- SCC -->&#83;tu&#100;&#105;&#111;.</small></font></center></body>
</html>

<!--End ASPX page-->

