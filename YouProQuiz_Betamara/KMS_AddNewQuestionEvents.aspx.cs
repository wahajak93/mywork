//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-EC7FE933
public partial class KMS_AddNewQuestion_Page : MTPage
{
//End Page class

//Page KMS_AddNewQuestion Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page KMS_AddNewQuestion Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page KMS_AddNewQuestion Load event tail @1-FCB6E20C
    }
//End Page KMS_AddNewQuestion Load event tail

//Page KMS_AddNewQuestion Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page KMS_AddNewQuestion Event On PreInit
        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page Dashboard access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", false);
        this.UserRights.Add("Premium", true);

        //this.UserRights.Add("Premium", false);
//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page KMS_AddNewQuestion On PreInit event tail @1-FCB6E20C
    }
//End Page KMS_AddNewQuestion On PreInit event tail

//Page class tail @1-FCB6E20C
    protected void Button1_Click(object sender, EventArgs e)
    {
        string quiz_id="";

        if(Request.QueryString["Quiz_id"]!=null)
        {
        quiz_id=Request.QueryString["Quiz_id"];
        };

        Response.Redirect("~/KMS_EditQuiz.aspx?Quiz_id=" + quiz_id + "");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/AdminDashboard.aspx");
    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

