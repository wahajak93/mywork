//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-9C28FF34
public partial class Pg_QuizHomePage_Page : MTPage
{
//End Page cla
    string a;
    protected void Page_Init(object sender, EventArgs e)
    {
        //End Page NewPage2 Event Init

        //Access Denied Page Url @1-92F104EC
        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page NewPage2 access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", true); this.UserRights.Add("Premium", true);
        //End Set Page NewPage2 access rights.
     a= Request.QueryString["Quiz_id"].ToString();
        //Page NewPage2 Init event tail @1-FCB6E20C
    }

//Page Pg_QuizHomePage Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page Pg_QuizHomePage Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();

      



//End DataBind

//Page Pg_QuizHomePage Load event tail @1-FCB6E20C
    }
//End Page Pg_QuizHomePage Load event tail

//Page Pg_QuizHomePage Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page Pg_QuizHomePage Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page Pg_QuizHomePage On PreInit event tail @1-FCB6E20C
    }

//End Page Pg_QuizHomePage On PreInit event tail

//Page class tail @1-FCB6E20C
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["quiz_id"] = null;
        Session["result"] = null;


        Session["eligible"] = "yes";
        Response.Redirect("~/Default2.aspx?QuizID=" + a + "");

    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

