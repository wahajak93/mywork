//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;


namespace InMotion.Data
{
    /// <summary>
    /// Represents a parameter of stored procedure for using with <see cref="DataCommand"/>.
    /// </summary>
    public class SPParameter : DataParameter
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SPParameter"/> class. 
        /// </summary>
        public SPParameter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SPParameter"/> class.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="dbType">One of the <see cref="DbType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="parameterDirection">One of the <see cref="ParameterDirection"/> values.</param>
        /// <param name="value">The value of the new <see cref="SPParameter"/> object.</param>
        public SPParameter(String parameterName, DbType dbType, int size, ParameterDirection parameterDirection, Object value)
        {
            this.ParameterName = parameterName;
            this.DbType = dbType;
            this.Size = size;
            this.Direction = parameterDirection;
            this.Value = value;
        }
        #endregion

        private int _size;
        /// <summary>
        /// Gets or sets the maximum size, in bytes, of the data within the column.
        /// </summary>
        public override int Size
        {
            get
            {
                int num1 = this._size;
                if (num1 == 0)
                {
                    num1 = DataUtility.ValueSize(DbType);
                }
                return num1;
            }
            set
            {
                _size = value;
            }
        }

        /// <summary>
        /// Gets the <see cref="DataType"/> of the parameter.
        /// </summary>
        public override DataType Type
        {
            get
            {
                return InMotion.Data.DataUtility.MapDbType(this.DbType);
            }
            set
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The property Set method is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The property Set method is not implemented.");
            }
        }

        /// <summary>
        /// Gets a string that contains the formatted value.
        /// </summary>
        /// <returns>A string that contains the formatted value.</returns>
        public override string ToString()
        {
            Connection conn = (Connection)Owner.Connection;
            string strVal;
            if (Value == null)
                strVal = "";
            else
                strVal = Value.ToString();
            return conn.EncodeSqlValue(strVal, Type, false);
        }

    }
}
