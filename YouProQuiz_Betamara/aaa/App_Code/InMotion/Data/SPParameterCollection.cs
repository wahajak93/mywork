//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using InMotion.Common;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a collection of stored procedure parameters relevant to an DataCommand. 
    /// </summary>
    public class SPParameterCollection : DataParameterCollection
    {
        private List<SPParameter> _innerList = new List<SPParameter>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SPParameterCollection"/> class.
        /// </summary>
        /// <param name="owner">The <see cref="DataCommand"/> to which collection belongs.</param>
        public SPParameterCollection(DataCommand owner)
            : base(owner)
        {
        }

        #region Add method
        /// <summary>
        /// Adds the specified <see cref="SPParameter"/> object to the <see cref="SPParameterCollection"/>.
        /// </summary>
        /// <param name="value">An Object.</param>
        /// <returns>The index of the new <see cref="SPParameter"/> object.</returns>
        public override int Add(object value)
        {
            _innerList.Add((SPParameter)value);
            ((SPParameter)value).Owner = Owner;
            return _innerList.Count - 1;
        }
        /// <summary>
        /// Adds the specified <see cref="SPParameter"/> object to the <see cref="SPParameterCollection"/>.
        /// </summary>
        /// <param name="value">A <see cref="SPParameter"/> object.</param>
        /// <returns>The index of the new <see cref="SPParameter"/> object.</returns>
        public int Add(SPParameter value)
        {
            return Add((object)value);
        }

        /// <summary>
        /// Adds a <see cref="SPParameter"/> to the <see cref="SPParameterCollection"/> given the specified parameter name, DB type, size, direction and value.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="dbType">One of the <see cref="DbType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="parameterDirection">One of the <see cref="ParameterDirection"/> values.</param>
        /// <param name="value">The value of the new <see cref="SPParameter"/> object.</param>
        /// <returns>Added <see cref="SPParameter"/> object.</returns>
        public SPParameter Add(String parameterName, DbType dbType, int size, ParameterDirection parameterDirection, Object value)
        {
            SPParameter param = new SPParameter(parameterName, dbType, size, parameterDirection, value);
            Add(param);
            return param;
        }

        #endregion

        /// <summary>
        /// Adds an array of values to the end of the <see cref="SPParameterCollection"/>.
        /// </summary>
        /// <param name="values">The Array values to add.</param>
        public override void AddRange(Array values)
        {
            foreach (object var in values)
            {
                ((SPParameter)var).Owner = Owner;
                _innerList.Add((SPParameter)var);

            }
        }
        /// <summary>
        /// Removes all the SPParameter objects from the <see cref="SPParameterCollection"/>. 
        /// </summary>
        public override void Clear()
        {
            _innerList.Clear();
        }
        /// <summary>
        /// Determines whether the parameter with the specified name 
        /// is in this <see cref="SPParameterCollection"/>. 
        /// </summary>
        /// <param name="parameterName">The name of parameter.</param>
        /// <returns>true if the <see cref="SPParameterCollection"/> contains the parameter; otherwise false.</returns>
        public override bool Contains(string parameterName)
        {
            foreach (SPParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return true;
            }
            return false;
        }
        /// <summary>
        /// Determines whether the specified Object is in this <see cref="SPParameterCollection"/>. 
        /// </summary>
        /// <param name="value">The Object value.</param>
        /// <returns>true if the <see cref="SPParameterCollection"/> contains the value; otherwise false.</returns>
        public override bool Contains(object value)
        {
            return _innerList.Contains((SPParameter)value);
        }
        /// <summary>
        /// Copies all the elements of the current <see cref="SPParameterCollection"/> to the specified one-dimensional 
        /// Array starting at the specified destination Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the 
        /// elements copied from the current <see cref="SPParameterCollection"/>.</param>
        /// <param name="index">A 32-bit integer that represents the index in the Array at which copying starts.</param>
        public override void CopyTo(Array array, int index)
        {
            int k = 0;
            for (int i = index; i < array.Length; i++)
            {
                array.SetValue(this[k], i);
                k++;
            }
        }
        /// <summary>
        /// Returns an Integer that contains the number of elements in the <see cref="SPParameterCollection"/>. Read-only.
        /// </summary>
        public override int Count
        {
            get
            {
                return _innerList.Count;
            }
        }
        /// <summary>
        /// Returns an enumerator that iterates through the <see cref="SPParameterCollection"/>.
        /// </summary>
        /// <returns>An enumerator that iterates through the <see cref="SPParameterCollection"/>.</returns>
        public override System.Collections.IEnumerator GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        /// <summary>
        /// Returns DbParameter the object with the specified name.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter in the collection.</param>
        /// <returns>The DbParameter the object with the specified name. </returns>
        protected override DbParameter GetParameter(string parameterName)
        {
            foreach (DataParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return p;
            }
            return null;
        }

        /// <summary>
        /// Returns the DbParameter object at the specified index in the collection. 
        /// </summary>
        /// <param name="index">The index of the DbParameter in the collection.</param>
        /// <returns>The DbParameter object at the specified index in the collection. </returns>
        protected override DbParameter GetParameter(int index)
        {
            return (DbParameter)_innerList[index];
        }

        /// <summary>
        /// Returns the index of the specified DbParameter object.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter object in the collection.</param>
        /// <returns>The index of the DbParameter object with the specified name.</returns>
        public override int IndexOf(string parameterName)
        {
            foreach (SPParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) return ((IList)this).IndexOf(p);
            }
            return -1;
        }

        /// <summary>
        /// Returns the index of the DbParameter object with the specified name.
        /// </summary>
        /// <param name="value">The DbParameter object in the collection.</param>
        /// <returns>The index of the specified DbParameter object.</returns>
        public override int IndexOf(object value)
        {
            return _innerList.IndexOf((SPParameter)value);
        }

        /// <summary>
        /// Inserts the specified the index of the DbParameter object with the specified name into the collection at the specified index.
        /// </summary>
        /// <param name="index">The index at which to insert the DbParameter object.</param>
        /// <param name="value">The DbParameter object to insert into the collection.</param>
        public override void Insert(int index, object value)
        {
            ((SPParameter)value).Owner = Owner;
            _innerList.Insert(index, (SPParameter)value);
        }


        /// <summary>
        /// Inserts the specified the index of the SPParameter object with the specified name into the collection at the specified index.
        /// </summary>
        /// <param name="index">The index at which to insert the SPParameter object.</param>
        /// <param name="value">The SPParameter object to insert into the collection.</param>
        public void Insert(int index, SPParameter value)
        {
            ((SPParameter)value).Owner = Owner;
            _innerList.Insert(index, value);
        }

        /// <summary>
        /// Specifies whether the collection is a fixed size.
        /// </summary>
        public override bool IsFixedSize
        {
            get { return ((IList)_innerList).IsFixedSize; }
        }
        /// <summary>
        /// Specifies whether the collection is read-only. 
        /// </summary>
        public override bool IsReadOnly
        {
            get { return ((IList)_innerList).IsReadOnly; }
        }
        /// <summary>
        /// Specifies whether the collection is synchronized.
        /// </summary>
        public override bool IsSynchronized
        {
            get { return ((ICollection)_innerList).IsSynchronized; }
        }
        /// <summary>
        /// Removes the specified DbParameter object from the collection.
        /// </summary>
        /// <param name="value">The DbParameter object to remove.</param>
        public override void Remove(object value)
        {
            ((IList)this).Remove(value);
        }

        /// <summary>
        /// Removes the specified SPParameter object from the collection.
        /// </summary>
        /// <param name="value">The SPParameter object to remove.</param>
        public void Remove(SPParameter value)
        {
            ((IList)this).Remove(value);
        }
        /// <summary>
        /// Removes the DbParameter object with the specified name from the collection. 
        /// </summary>
        /// <param name="parameterName">The name of parameter to remove.</param>
        public override void RemoveAt(string parameterName)
        {
            foreach (DataParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) _innerList.Remove((SPParameter)p);
            }
        }

        /// <summary>
        /// Gets and sets the DbParameter at the specified index. 
        /// </summary>
        /// <param name="index">The zero-based index of the parameter.</param>
        /// <returns>The DbParameter at the specified index.</returns>
        public new SPParameter this[int index]
        {
            get
            {
                return (SPParameter)GetParameter(index);
            }
            set
            {
                SetParameter(index, value);
            }
        }

        /// <summary>
        /// Gets and sets the DbParameter with the specified name. 
        /// </summary>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <returns>The DbParameter with the specified name.</returns>
        public new SPParameter this[string parameterName]
        {
            get
            {
                int i = IndexOf(parameterName);
                return (SPParameter)GetParameter(i);
            }
            set
            {
                int i = IndexOf(parameterName);
                SetParameter(i, value);
            }
        }
        /// <summary>
        /// Removes the DbParameter object at the specified from the collection. 
        /// </summary>
        /// <param name="index">The index where the DbParameter object is located.</param>
        public override void RemoveAt(int index)
        {
            _innerList.RemoveAt(index);
        }

        /// <summary>
        /// Sets the DbParameter object with the specified name to a new value.
        /// </summary>
        /// <param name="parameterName">The name of the DbParameter object in the collection.</param>
        /// <param name="value">The new DbParameter value.</param>
        protected override void SetParameter(string parameterName, DbParameter value)
        {
            ((SPParameter)value).Owner = Owner;
            foreach (SPParameter p in _innerList)
            {
                if (p.ParameterName == parameterName) _innerList[_innerList.IndexOf(p)] = (SPParameter)value;
                return;
            }
            Add(value);

        }
        /// <summary>
        /// Sets the DbParameter object at the specified index to a new value.
        /// </summary>
        /// <param name="index">The index where the DbParameter object is located.</param>
        /// <param name="value">The new DbParameter value.</param>
        protected override void SetParameter(int index, DbParameter value)
        {
            ((SPParameter)value).Owner = Owner;
            _innerList[index] = (SPParameter)value;
        }
        /// <summary>
        /// Specifies the Object to be used to synchronize access to the collection.
        /// </summary>
        public override object SyncRoot
        {
            get { return ((ICollection)_innerList).SyncRoot; }
        }

        /// <summary>
        /// Copies the elements of the ParameterCollections to an array of Parameter, starting at a particular Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ICollection. The Array must have zero-based indexing. </param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        public void CopyTo(SPParameter[] array, int index)
        {
            this.CopyTo((Array)array, index);
            
        }
    }
}
