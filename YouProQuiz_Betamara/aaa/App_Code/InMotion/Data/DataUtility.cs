//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Data
{
    /// <summary>
    /// Provide the helper methods for the Data namespace.
    /// </summary>
    public static class DataUtility
    {
        internal static InMotion.Common.DataType MapDbType(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.String:
                case DbType.StringFixedLength:
                    return DataType.Text;
                case DbType.Boolean:
                    return DataType.Boolean;
                case DbType.Date:
                case DbType.DateTime:
                case DbType.Time:
                    return DataType.Date;
                case DbType.Byte:
                case DbType.SByte:
                case DbType.Int16:
                case DbType.Int32:
                case DbType.Int64:
                case DbType.UInt16:
                case DbType.UInt32:
                case DbType.UInt64:
                    return DataType.Integer;
                case DbType.Double:
                    return DataType.Float;
                case DbType.Single:
                case DbType.Decimal:
                case DbType.Currency:
                    return DataType.Single;
                /*
                case DbType.Binary:
                case DbType.Guid:
                case DbType.Object:
                case DbType.VarNumeric:
                case DbType.Xml:
                */
                default:
                    return DataType.Text;
            }
        }

        /// <summary>
        /// Checks if the value is null or implements <see cref="INullable"/> interface and contain null value or equals to <see cref="DBNull"/>.
        /// </summary>
        /// <param name="value">The checked object.</param>
        /// <returns><b>true</b> if the object is null or implements <see cref="INullable"/> interface and contain null value or equals to <see cref="DBNull"/>; otherwise <b>false</b></returns>
        public static bool IsNull(object value)
        {
            if ((value == null) || (DBNull.Value == value))
            {
                return true;
            }
            INullable nullable1 = value as INullable;
            if (nullable1 != null)
            {
                return nullable1.IsNull;
            }
            return false;
        }

        /// <summary>
        /// Returns the default value size depending of the type for using in <see cref="SPParameter"/>
        /// </summary>
        /// <param name="value">The <see cref="DbType"/> of value.</param>
        /// <returns>The <see cref="int"/> size fo supplied type.</returns>
        public static int ValueSize(DbType value)
        {
            int size = 0;
            switch (value)
            {
                case DbType.AnsiString:
                    size = 8000;
                    break;
                case DbType.AnsiStringFixedLength:
                    size = 8000;
                    break;
                case DbType.Binary:
                    size = 8000;
                    break;
                case DbType.Boolean:
                    size = 1;
                    break;
                case DbType.Byte:
                    size = 1;
                    break;
                case DbType.Currency:
                    size = 8;
                    break;
                case DbType.Date:
                    size = 8;
                    break;
                case DbType.DateTime:
                    size = 8;
                    break;
                case DbType.Decimal:
                    size = 2;
                    break;
                case DbType.Double:
                    size = 8;
                    break;
                case DbType.Int16:
                    size = 2;
                    break;
                case DbType.Int32:
                    size = 4;
                    break;
                case DbType.Int64:
                    size = 8;
                    break;
                case DbType.SByte:
                    size = 1;
                    break;
                case DbType.Single:
                    size = 4;
                    break;
                /*case DbType.String:
                    size = 8000;
                    break;
                case DbType.StringFixedLength:
                    size = 8000;
                    break;*/
                case DbType.Time:
                    size = 8;
                    break;
                case DbType.UInt16:
                    size = 2;
                    break;
                case DbType.UInt32:
                    size = 4;
                    break;
                case DbType.UInt64:
                    size = 8;
                    break;
            }
            return size;
        }

        /// <summary>
        /// Creates the <see cref="Connection"/> object based on name in config file or OleDb connection string.
        /// </summary>
        /// <param name="connectionName">The <see cref="string"/> contains connection name or connection string. If the string is start with "InMotion:" prefix it will be assumed as name of connection in the .config file. Otherwise the value will be used as OleDb connection string.</param>
        /// <returns>A <see cref="Connection"/>.</returns>
        public static Connection GetConnectionObject(string connectionName)
        {
            Connection conn = null;
            if (connectionName.StartsWith("InMotion:"))
            {
                conn = AppConfig.GetConnection(connectionName.Substring(9));
            }
            else
            {
                conn = new Connection("System.Data.OleDb");
                conn.ConnectionString = connectionName;
            }
            return conn;
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against given connection.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="conn">The data connection.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public static DataSet Execute(string commandText, Connection conn)
        {
            DataCommand dc = (DataCommand)conn.CreateCommand();
            dc.CommandText = commandText;

            bool IsClosed = false;
            if (conn.State == ConnectionState.Closed)
            {
                IsClosed = true;
                conn.Open();
            }
            DataSet result = dc.Execute();
            if (IsClosed)
            {
                conn.Close();
            }
            return result;
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against connection with given name.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="connectionName">The name of connection in .config file or OleDb connection string.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public static DataSet Execute(string commandText, string connectionName)
        {
            return Execute(commandText, GetConnectionObject(connectionName));
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against default connection.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public static DataSet Execute(string commandText)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static DataSet Execute(string commandText) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static DataSet Execute(string commandText) method must be used instead.");
            }

            return Execute(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against given connection.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="conn">The data connection.</param>
        /// <returns>A <see cref="DbDataReader"/></returns>
        /// <remarks>The DbDataReader is created with <see cref="CommandBehavior.CloseConnection"/> option.</remarks>
        public static DbDataReader ExecuteReader(string commandText, Connection conn)
        {
            DataCommand dc = (DataCommand)conn.CreateCommand();
            dc.CommandText = commandText;
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            return dc.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against connection with given name.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="connectionName">The name of connection in .config file or OleDb connection string.</param>
        /// <returns>A <see cref="DbDataReader"/></returns>
        /// <remarks>The DbDataReader is created with <see cref="CommandBehavior.CloseConnection"/> option.</remarks>
        public static DbDataReader ExecuteReader(string commandText, string connectionName)
        {
            Connection conn = GetConnectionObject(connectionName);
            return ExecuteReader(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against default connection.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <returns>A <see cref="DbDataReader"/></returns>
        /// <remarks>The DbDataReader is created with <see cref="CommandBehavior.CloseConnection"/> option.</remarks>
        public static DbDataReader ExecuteReader(string commandText)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static DbDataReader ExecuteReader(string commandText) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static DbDataReader ExecuteReader(string commandText) method must be used instead.");
            }
            return ExecuteReader(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against given connection and returns a value of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="conn">The data connection.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T ExecuteScalar<T>(string commandText, Connection conn)
            where T : IMTField
        {
            DataCommand dc = (DataCommand)conn.CreateCommand();
            dc.CommandText = commandText;

            T result = default(T);
            DataType resType = DataType.Text;
            if (result is MTText)
                resType = DataType.Text;
            if (result is MTMemo)
                resType = DataType.Memo;
            if (result is MTInteger)
                resType = DataType.Integer;
            if (result is MTBoolean)
                resType = DataType.Boolean;
            if (result is MTFloat)
                resType = DataType.Float;
            if (result is MTSingle)
                resType = DataType.Single;
            if (result is MTDate)
                resType = DataType.Date;

            bool IsClosed = false;
            if (conn.State == ConnectionState.Closed)
            {
                IsClosed = true;
                conn.Open();
            }
            result = (T)TypeFactory.CreateTypedField(resType, dc.ExecuteScalar(), "");
            if (IsClosed)
            {
                conn.Close();
            }
            return result;
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against connection with given name and returns a value of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="connectionName">The name of connection in .config file or OleDb connection string.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T ExecuteScalar<T>(string commandText, string connectionName)
            where T : IMTField
        {
            Connection conn = GetConnectionObject(connectionName);
            return ExecuteScalar<T>(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against default connection and returns a value of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T ExecuteScalar<T>(string commandText)
            where T : IMTField
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static T ExecuteScalar<T>(string commandText) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static T ExecuteScalar<T>(string commandText) method must be used instead.");
            }
            return ExecuteScalar<T>(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against given connection and returns a value of type <i>object</i>.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="conn">The data connection.</param>
        /// <returns>Result after execution of query</returns>
        public static object ExecuteScalar(string commandText, Connection conn)
        {
            DataCommand dc = (DataCommand)conn.CreateCommand();
            dc.CommandText = commandText;

            bool IsClosed = false;
            if (conn.State == ConnectionState.Closed)
            {
                IsClosed = true;
                conn.Open();
            }
            object result = dc.ExecuteScalar();
            if (IsClosed)
            {
                conn.Close();
            }
            return result;
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against connection with given name and returns a value of type <i>object</i>.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <param name="connectionName">The name of connection in .config file or OleDb connection string.</param>
        /// <returns>Result after execution of query</returns>
        public static object ExecuteScalar(string commandText, string connectionName)
        {
            Connection conn = GetConnectionObject(connectionName);
            return ExecuteScalar(commandText, conn);
        }

        /// <summary>
        /// Creates and executes <see cref="DataCommand"/> with given command text against default connection and returns a value of type <i>object</i>.
        /// </summary>
        /// <param name="commandText">The text of the command to execute.</param>
        /// <returns>Result after execution of query</returns>
        public static object ExecuteScalar(string commandText)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static object ExecuteScalar(string commandText) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static object ExecuteScalar(string commandText) method must be used instead.");
            }
            return ExecuteScalar(commandText, conn);
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <param name="connectionName">Connection from which the database value will be retrieved.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T DLookup<T>(string expression, string domain, string criteria, string connectionName)
            where T : IMTField
        {
            return DLookup<T>(expression, domain, criteria, connectionName, null);
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T DLookup<T>(string expression, string domain, string criteria)
            where T : IMTField
        {
            string command = "SELECT " + expression + " FROM " + domain;
            if (!String.IsNullOrEmpty(criteria))
                command += " WHERE " + criteria;
            return ExecuteScalar<T>(command);
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression or default value if result is null.
        /// </summary>
        /// <typeparam name="T">The type that implement <see cref="IMTField"/> interface.</typeparam>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <param name="connectionName">Connection from which the database value will be retrieved.</param>
        /// <param name="defaultValue">Default value that will be returned in case if command returns the null value.</param>
        /// <returns>A <see cref="IMTField"/></returns>
        public static T DLookup<T>(string expression, string domain, string criteria, string connectionName, object defaultValue)
            where T : IMTField
        {
            string command = "SELECT " + expression + " FROM " + domain;
            if (!String.IsNullOrEmpty(criteria))
                command += " WHERE " + criteria;
            T result = ExecuteScalar<T>(command, connectionName);
            if (result.IsNull && defaultValue != null)
                return (T)defaultValue;
            else
                return result;
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression.
        /// </summary>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <returns>Result after execution of query</returns>
        public static object DLookup(string expression, string domain, string criteria)
        {
            string command = "SELECT " + expression + " FROM " + domain;
            if (!String.IsNullOrEmpty(criteria))
                command += " WHERE " + criteria;
            return ExecuteScalar(command);
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression.
        /// </summary>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <param name="connectionName">Connection from which the database value will be retrieved.</param>
        /// <returns>Result after execution of query</returns>
        public static object DLookup(string expression, string domain, string criteria, string connectionName)
        {
            return DLookup(expression, domain, criteria, connectionName, null);
        }

        /// <summary>
        /// Retrieves the value of a single database field or expression or default value if result is null.
        /// </summary>
        /// <param name="expression">Field or expression to be used in the SELECT clause of the SQL statement to be executed.</param>
        /// <param name="domain">Table or view name to be used in the FROM clause of the SQL statement to be executed.</param>
        /// <param name="criteria">Criteria to be used in the WHERE clause of the SQL statement to be executed.</param>
        /// <param name="connectionName">Connection from which the database value will be retrieved.</param>
        /// <param name="defaultValue">Default value that will be returned in case if command returns the null value.</param>
        /// <returns>Result after execution of query</returns>
        public static object DLookup(string expression, string domain, string criteria, string connectionName, object defaultValue)
        {
            string command = "SELECT " + expression + " FROM " + domain;
            if (!String.IsNullOrEmpty(criteria))
                command += " WHERE " + criteria;
            object result = ExecuteScalar(command, connectionName);
            if (result == null && defaultValue != null)
                return defaultValue;
            else
                return result;
        }

        #region Convert to DB format
        /// <summary>
        /// Converts the <see cref="MTText"/> value of this instance to its equivalent string representation, using the DBFormat string of default connection.
        /// </summary>
        /// <param name="text">A value to be formatted into a string.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTText text)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection object.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection object.");
            }
            return ((InMotion.Data.Connection)conn).EncodeSqlValue(text.ToString(), DataType.Text, false);
        }

        /// <summary>
        /// Converts the <see cref="MTText"/> value of this instance to its equivalent string representation, using the DBFormat string of connection with supplied name.
        /// </summary>
        /// <param name="text">A value to be formatted into a string.</param>
        /// <param name="connectionName">The name of the connection.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTText text, string connectionName)
        {
            Connection conn = InMotion.Data.DataUtility.GetConnectionObject(connectionName);
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection object.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection object.");
            }
            return ((InMotion.Data.Connection)conn).EncodeSqlValue(text.ToString(), DataType.Text, false);
        }

        /// <summary>
        /// Converts the <see cref="MTDate"/> value of this instance to its equivalent string representation, using the DBFormat string of default connection.
        /// </summary>
        /// <param name="date">A value to be formatted into a string.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTDate date)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static string ToDbFormat(MTDate date, string connectionName) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static string ToDbFormat(MTDate date, string connectionName) method must be used instead.");
            }
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection Date format info.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection Date format info.");
            }
            return date.ToString(((InMotion.Data.Connection)conn).DateFormat);
        }

        /// <summary>
        /// Converts the <see cref="MTDate"/> value of this instance to its equivalent string representation, using the DBFormat string of connection with supplied name.
        /// </summary>
        /// <param name="date">A value to be formatted into a string.</param>
        /// <param name="connectionName">The name of the connection.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTDate date, string connectionName)
        {
            Connection conn = InMotion.Data.DataUtility.GetConnectionObject(connectionName);
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection Date format info.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection Date format info.");
            }
            return date.ToString(((InMotion.Data.Connection)conn).DateFormat);
        }

        /// <summary>
        /// Converts the <see cref="MTBoolean"/> value of this instance to its equivalent string representation, using the DBFormat string of default connection.
        /// </summary>
        /// <param name="boolean1">A value to be formatted into a string.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTBoolean boolean1)
        {
            Connection conn = InMotion.Configuration.AppConfig.GetDefaultConnection();
            if (conn == null)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("To use this method site should contain only one connection. The public static string ToDbFormat(MTBoolean date, string connectionName) method must be used instead.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("To use this method site should contain only one connection. The public static string ToDbFormat(MTBoolean date, string connectionName) method must be used instead.");
            }
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection Boolean format info.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection Boolean format info.");
            }
            return boolean1.ToString(((InMotion.Data.Connection)conn).BooleanFormat);
        }

        /// <summary>
        /// Converts the <see cref="MTBoolean"/> value of this instance to its equivalent string representation, using the DBFormat string of connection with supplied name.
        /// </summary>
        /// <param name="boolean1">A value to be formatted into a string.</param>
        /// <param name="connectionName">The name of the connection.</param>
        /// <returns>The string representation of the value of this instance</returns>
        public static string ToDbFormat(MTBoolean boolean1, string connectionName)
        {
            Connection conn = InMotion.Data.DataUtility.GetConnectionObject(connectionName);
            if (!(conn is InMotion.Data.Connection))
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Unable to obtain connection Boolean format info.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to obtain connection Boolean format info.");
            }
            return boolean1.ToString(((InMotion.Data.Connection)conn).BooleanFormat);
        }
        #endregion
    }
}