//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Resources;
using System.Globalization;

namespace InMotion.Resources
{
    /// <summary>
    /// The InMotion specific implemenation of <see cref="ResourceManager"/> class.
    /// </summary>
    public class MTResourceManager : ResourceManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MTResourceManager"/> class that looks up resources contained in files derived from the specified root name using the given Assembly.
        /// </summary>
        /// <param name="baseName">The root name of the resources. For example, the root name for the resource file named "MyResource.en-US.resources" is "MyResource".</param>
        /// <param name="assembly">The main <see cref="System.Reflection.Assembly"/> for the resources.</param>
        public MTResourceManager(string baseName, System.Reflection.Assembly assembly)
            : base(baseName, assembly)
        { }

        /// <summary>
        /// Returns the value of the specified String resource.
        /// </summary>
        /// <param name="name">The name of the resource to get.</param>
        /// <returns>The value of the resource localized for the caller's current culture settings. If a match is not possible, a null reference (Nothing in Visual Basic) is returned.</returns>
        public override string GetString(string name)
        {
            string val = null;
            val = base.GetString(name);
            if (val == null) val = name;
            return val;
        }

        /// <summary>
        /// Gets the value of the String resource localized for the specified culture.
        /// </summary>
        /// <param name="name">The name of the resource to get.</param>
        /// <param name="culture">
        /// The <see cref="CultureInfo"/> object that represents the culture for which the resource is localized.
        /// Note that if the resource is not localized for this culture, the lookup will fall back using the culture's
        /// <see cref="CultureInfo.Parent"/> property, stopping after looking in the neutral culture.If this value is null,
        /// the System.Globalization.CultureInfo is obtained using the culture's <see cref="CultureInfo.CurrentUICulture"/>
        /// property.
        /// </param>
        /// <returns>
        /// The CultureInfo object that represents the culture for which the resource is localized.
        /// Note that if the resource is not localized for this culture, the lookup will fall back using the culture's
        /// Parent property, stopping after looking in the neutral culture. If this value is a null reference (Nothing
        /// in Visual Basic), the CultureInfo is obtained using the culture's CurrentUICulture property.
        /// </returns>
        public override string GetString(string name, CultureInfo culture)
        {
            string val = null;
            val = base.GetString(name, culture);
            if (val == null) val = name;
            return val;
        }
    }
}