//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Configuration;

namespace InMotion.Configuration
{
    /// <summary>
    /// This class support framework infrastructure and is not 
    /// intended to be used directly from your code. 
    /// Handles configuration sections that are represented by a 
    /// connection section under mtConnections sectionGroup in the .config file.
    /// </summary>
    public class ConnectionSection : ConfigurationSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionSection"/> class.
        /// </summary>
        public ConnectionSection() { }

        /// <summary>
        /// Gets a collection of <see cref="ConnectionSettings"/> objects.
        /// </summary>
        [ConfigurationProperty("connections", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(ConnectionSettingsCollection))]
        public ConnectionSettingsCollection Connections
        {
            get
            {
                ConnectionSettingsCollection c =
                (ConnectionSettingsCollection)base["connections"];
                return c;
            }
        }
    }
}