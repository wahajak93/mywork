//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using System.Resources;
using System.Globalization;
using System.Collections.Specialized;

namespace InMotion.Web
{
    /// <summary>
    /// Override wrapping filter object used to modify the HTTP entity body before transmission.
    /// </summary>
    public class MTResponseFilter : Stream
    {
        Stream responseStream;
        StringBuilder html;

        #region Events
        /// <summary>
        /// Occurs before the Response Filter close.
        /// </summary>
        public event EventHandler<MTResponseFilterEventArgs> FilterClose;

        /// <summary>
        /// Raises the <see cref="FilterClose"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnFilterClose(MTResponseFilterEventArgs e)
        {
            if (FilterClose != null)
                FilterClose(this, e);
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the MTResponseFilter class. 
        /// </summary>
        /// <param name="inputStream">Initiate stream.</param>
        public MTResponseFilter(Stream inputStream)
        {
            responseStream = inputStream;
            html = new StringBuilder();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the current stream supports reading.
        /// </summary>
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports seeking.
        /// </summary>
        public override bool CanSeek
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the length in bytes of the stream.
        /// </summary>
        public override long Length
        {
            get
            {
                return 0;
            }
        }

        long _Position;
        /// <summary>
        /// Gets or sets the position within the current stream.
        /// </summary>
        public override long Position
        {
            get
            {
                return _Position;
            }
            set
            {
                _Position = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports writing.
        /// </summary>
        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
        /// </summary>
        public override void Close()
        {
            MTResponseFilterEventArgs e = new MTResponseFilterEventArgs(html.ToString(), responseStream);
            OnFilterClose(e);
            if (!e.Cancel)
            {
                byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(e.Content);
                responseStream.Write(data, 0, data.Length);
                responseStream.Close();
            }
        }

        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
        /// </summary>
        public override void Flush()
        {
            responseStream.Flush();
        }

        /// <summary>
        /// Sets the position within the current stream.
        /// </summary>
        /// <param name="offset">A byte offset relative to the origin parameter.</param>
        /// <param name="direction">A value of type SeekOrigin indicating the reference point used to obtain the new position.</param>
        /// <returns>The new position within the current stream.</returns>
        public override long Seek(long offset, SeekOrigin direction)
        {
            return responseStream.Seek(offset, direction);
        }

        /// <summary>
        /// Sets the length of the current stream.
        /// </summary>
        /// <param name="length">The desired length of the current stream in bytes.</param>
        public override void SetLength(long length)
        {
            responseStream.SetLength(length);
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between offset and (offset + count - 1) replaced by the bytes read from the current source.</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read from the current stream.</param>
        /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
        /// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            return responseStream.Read(buffer, offset, count);
        }

        /// <summary>
        /// Writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies count bytes from buffer to the current stream.</param>
        /// <param name="offset">The zero-based byte offset in buffer at which to begin copying bytes to the current stream.</param>
        /// <param name="count">The number of bytes to be written to the current stream.</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            html.Append(System.Text.UTF8Encoding.UTF8.GetString(buffer, offset, count));
        }
        #endregion
    }

    /// <summary>
    /// Provides args for <see cref="MTResponseFilter"/> events.
    /// </summary>
    public class MTResponseFilterEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MTResponseFilterEventArgs"/>
        /// </summary>
        /// <param name="content">Stream content.</param>
        /// <param name="responseStream">Response stream.</param>
        public MTResponseFilterEventArgs(string content, Stream responseStream)
        {
            _Cancel = false;
            _Content = content;
            _Stream = responseStream;
        }

        private bool _Cancel;
        /// <summary>
        /// Gets or sets value that indicating whether operation will be executed.
        /// </summary>
        public bool Cancel
        {
            get
            {
                return _Cancel;
            }
            set
            {
                _Cancel = value;
            }
        }

        private string _Content;
        /// <summary>
        /// Gets stream content.
        /// </summary>
        public string Content
        {
            get
            {
                return _Content;
            }
        }

        private Stream _Stream;
        /// <summary>
        /// Gets stream content.
        /// </summary>
        public Stream Stream
        {
            get
            {
                return _Stream;
            }
        }
    }
}