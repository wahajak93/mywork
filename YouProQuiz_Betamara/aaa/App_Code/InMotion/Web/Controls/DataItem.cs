//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a data item associated with section of form.
    /// </summary>
    public class DataItem
    {
        /// <summary>
        /// Initializes a new instance of <see cref="DataItem"/> class.
        /// </summary>
        /// <param name="value">The value of DataItem</param>
        public DataItem(object value)
        {
            Value = value;
        }

        private object _Value;
        /// <summary>
        /// Gets or sets the value of DataItem. 
        /// </summary>
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        /// <summary>
        /// Gets the value of specified field of data item.
        /// </summary>
        /// <param name="key">The name of the field.</param>
        /// <returns>The value of the field.</returns>
        public object this[string key]
        {
            get
            {
                if (Value == null) return null;
                return DataSourceHelper.GetFieldValue(key, Value);
            }
        }
    }
}
