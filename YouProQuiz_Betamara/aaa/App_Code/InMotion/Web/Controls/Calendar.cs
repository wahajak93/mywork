//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using InMotion.Common;
using InMotion.Security;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies how empty week row will be added to the month. 
    /// </summary>
    public enum ExtendWeeksMode
    {
        /// <summary>
        /// The empty week row will not be added.
        /// </summary>
        Never,
        /// <summary>
        /// The empty week row will be added always.
        /// </summary>
        Always,
        /// <summary>
        /// The empty week row will be added depeneding on the Calendar layout.
        /// </summary>
        Auto
    }

    /// <summary>
    /// The Calendar component outputs data in monthly calendar format, best suited for presenting date-specific information, such as events, tasks, birthdays, etc. 
    /// </summary>
    [Designer(typeof(CalendarDesigner))]
    public class Calendar : DataBoundControl, IForm, ISpecialValueProvider, INamingContainer, IMTAttributeAccessor, IClientScriptHelper
    {
        [Serializable]
        private class EventInfo
        {
            public EventInfo(MTDate date, MTDate time)
            {
                if (!date.IsNull)
                    _date = (DateTime)date;
                if (!time.IsNull)
                    _time = ((DateTime)time).TimeOfDay;

            }
            public EventInfo(MTDate date, MTDate time, bool isEventTimeExists, object eventData)
                : this(date, time)
            {
                IsEventTimeExists = isEventTimeExists;
                EventData = eventData;
            }

            private object _data;

            public object EventData
            {
                get { return _data; }
                set { _data = value; }
            }

            MTDate _date;
            public MTDate Date
            {
                get
                {
                    if (IsEventTimeExists)
                        return _date.Date;
                    else
                        return _date;
                }
                set { _date = value; }
            }
            TimeSpan? _time;

            public TimeSpan? Time
            {
                get
                {
                    return _time;
                }
                set { _time = value; }
            }

            private bool _IsEventTimeExists;

            public bool IsEventTimeExists
            {
                get { return _IsEventTimeExists; }
                set { _IsEventTimeExists = value; }
            }
        }

        private class EventTimeComparer : IComparer<EventInfo>
        {

            public int Compare(EventInfo x, EventInfo y)
            {
                int result = 0;
                if (x.Date.IsNull && y.Date.IsNull) return 0;
                if (x.Date.IsNull && !y.Date.IsNull) return -1;
                if (!x.Date.IsNull && y.Date.IsNull) return 1;

                result = x.Date.Value.CompareTo(y.Date);
                if (result == 0 && x.IsEventTimeExists)
                {
                    if (x.Time == null && y.Time == null) return 0;
                    if (x.Time == null && y.Time != null) return -1;
                    if (x.Time != null && y.Time == null) return 1;
                    result = x.Time.Value.CompareTo(y.Time);
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private List<EventInfo> innerDataSource;
        private List<EventInfo> viewStateDataSource;
        private List<EventInfo> nextMonthDataSource;
        private DateTime _currentVerifiedDay;

        #region Templates Member variables

        private ITemplate footerTemplate;
        private ITemplate headerTemplate;
        private ITemplate monthHeaderTemplate;
        private ITemplate monthFooterTemplate;
        private ITemplate weekDaysTemplate;
        private ITemplate weekDaySeparatorTemplate;
        private ITemplate weekDaysFooterTemplate;
        private ITemplate weekHeaderTemplate;
        private ITemplate weekFooterTemplate;
        private ITemplate dayHeaderTemplate;
        private ITemplate dayFooterTemplate;
        private ITemplate eventSeparatorTemplate;
        private ITemplate daySeparatorTemplate;
        private ITemplate weekSeparatorTemplate;
        private ITemplate monthSeparatorTemplate;
        private ITemplate monthsRowSeparatorTemplate;
        private ITemplate noEventsTemplate;
        private ITemplate emptyDayTemplate;
        private ITemplate eventRowTemplate;
        #endregion

        #region Templates
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the header section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return headerTemplate;
            }
            set
            {
                headerTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the footer section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate FooterTemplate
        {
            get
            {
                return footerTemplate;
            }
            set
            {
                footerTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the month header section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate MonthHeaderTemplate
        {
            get
            {
                return monthHeaderTemplate;
            }
            set
            {
                monthHeaderTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the month footer section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate MonthFooterTemplate
        {
            get
            {
                return monthFooterTemplate;
            }
            set
            {
                monthFooterTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week header section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate WeekHeaderTemplate
        {
            get
            {
                return weekHeaderTemplate;
            }
            set
            {
                weekHeaderTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week footer section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate WeekFooterTemplate
        {
            get
            {
                return weekFooterTemplate;
            }
            set
            {
                weekFooterTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the day header section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate DayHeaderTemplate
        {
            get
            {
                return dayHeaderTemplate;
            }
            set
            {
                dayHeaderTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the day footer section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate DayFooterTemplate
        {
            get
            {
                return dayFooterTemplate;
            }
            set
            {
                dayFooterTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week days section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate WeekdaysTemplate
        {
            get
            {
                return weekDaysTemplate;
            }
            set
            {
                weekDaysTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week days footer section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate WeekdaysFooterTemplate
        {
            get
            {
                return weekDaysFooterTemplate;
            }
            set
            {
                weekDaysFooterTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week days separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate WeekdaySeparatorTemplate
        {
            get
            {
                return weekDaySeparatorTemplate;
            }
            set
            {
                weekDaySeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the event row section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate EventRowTemplate
        {
            get
            {
                return eventRowTemplate;
            }
            set
            {
                eventRowTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the event separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate EventSeparatorTemplate
        {
            get
            {
                return eventSeparatorTemplate;
            }
            set
            {
                eventSeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the day separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate DaySeparatorTemplate
        {
            get
            {
                return daySeparatorTemplate;
            }
            set
            {
                daySeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the week separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
       Browsable(false),
       DefaultValue(null),
       PersistenceMode(PersistenceMode.InnerProperty),
       TemplateContainer(typeof(CalendarItem))
       ]
        public virtual ITemplate WeekSeparatorTemplate
        {
            get
            {
                return weekSeparatorTemplate;
            }
            set
            {
                weekSeparatorTemplate = value;
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the month separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate MonthSeparatorTemplate
        {
            get
            {
                return monthSeparatorTemplate;
            }
            set
            {
                monthSeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the month row separator section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate MonthsRowSeparatorTemplate
        {
            get
            {
                return monthsRowSeparatorTemplate;
            }
            set
            {
                monthsRowSeparatorTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the no events section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate NoEventsTemplate
        {
            get
            {
                return noEventsTemplate;
            }
            set
            {
                noEventsTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the empty day section of <see cref="Calendar"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(CalendarItem))
        ]
        public virtual ITemplate EmptyDayTemplate
        {
            get
            {
                return emptyDayTemplate;
            }
            set
            {
                emptyDayTemplate = value;
            }
        }

        #endregion

        #region Style Properties

        /// <summary>
        /// Get the style for displaying the month header.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl MonthStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["MonthStyle"];
            }
            set
            {
                ViewState["MonthStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the current month header.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl CurrentMonthStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["CurrentMonthStyle"];
            }
            set
            {
                ViewState["CurrentMonthStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the name of the weekday.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl WeekdayNameStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["WeekdayNameStyle"];
            }
            set
            {
                ViewState["WeekdayNameStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the name of the weekend.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl WeekendNameStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["WeekendNameStyle"];
            }
            set
            {
                ViewState["WeekendNameStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the regular day.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl DayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["DayStyle"];
            }
            set
            {
                ViewState["DayStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the weekend day.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl WeekendStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["WeekendStyle"];
            }
            set
            {
                ViewState["WeekendStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the today.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl TodayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["TodayStyle"];
            }
            set
            {
                ViewState["TodayStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the today in case if it is weekend day.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl WeekendTodayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["WeekendTodayStyle"];
            }
            set
            {
                ViewState["WeekendTodayStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the day that belongs to other month than current.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl OtherMonthDayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["OtherMonthDayStyle"];
            }
            set
            {
                ViewState["OtherMonthDayStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the today date that belongs to other month than current.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl OtherMonthTodayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["OtherMonthTodayStyle"];
            }
            set
            {
                ViewState["OtherMonthTodayStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the day that belongs to other month than current and it is a weekend day.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl OtherMonthWeekendStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["OtherMonthWeekendStyle"];
            }
            set
            {
                ViewState["OtherMonthWeekendStyle"] = value;
            }
        }

        /// <summary>
        /// Get the style for displaying the today date that belongs to other month than current and it is weekend day.
        /// </summary>
        [Bindable(true),
        Category("Style"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public HtmlGenericControl OtherMonthWeekendTodayStyle
        {
            get
            {
                return (HtmlGenericControl)ViewState["OtherMonthWeekendTodayStyle"];
            }
            set
            {
                ViewState["OtherMonthWeekendTodayStyle"] = value;
            }

        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return ViewState["__se"] as string;
            }
            set
            {
                ViewState["__se"] = value;
                SelectArguments.SortExpression = value;
            }
        }
        /// <summary>
        /// Get or sets the <see cref="CalendarMode"/> mode of current component.
        /// </summary>
        /// <value>One of the <see cref="CalendarMode"/> values.</value>
        public CalendarMode Mode
        {
            get
            {
                return (CalendarMode)ViewState["_mode"];
            }
            set
            {
                ViewState["_mode"] = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the active date for the current calendar.
        /// </summary>
        public DateTime Date
        {
            get
            {
                return (DateTime)ViewState["_date"];
            }
            set
            {
                ViewState["_date"] = value;
            }
        }

        /// <summary>
        /// This property support framework infrastructure and is not 
        /// intended to be used directly from your code.
        /// </summary>
        public DateTime CurrentDate
        {
            get
            {
                if (ViewState["_currentDate"] == null)
                    ViewState["_currentDate"] = DateTime.Now;

                return (DateTime)ViewState["_currentDate"];
            }
            set
            {
                ViewState["_currentDate"] = value;
                this.OnInit(new EventArgs());
            }
        }

        /// <summary>
        /// Gets or sets the month part of active date.
        /// </summary>
        public int Month
        {
            get
            {
                return Date.Month;
            }
            set
            {
                Date = Date.AddMonths(value - Date.Month);
            }
        }

        /// <summary>
        /// Gets or sets the day part of active date.
        /// </summary>
        public int Day
        {
            get
            {
                return Date.Day;
            }
            set
            {
                Date = Date.AddDays(value - Date.Day);
            }
        }

        /// <summary>
        /// Gets or sets the month part of active date.
        /// </summary>
        public int Year
        {
            get
            {
                return Date.Year;
            }
            set
            {
                Date = Date.AddYears(value - Date.Year);
            }
        }

        /// <summary>
        /// Gets or sets the number of displaing months in one row.
        /// </summary>
        public int MonthsInRow
        {
            get
            {
                int result;
                if (CalendarMode.OneMonth == Mode) return 1;
                if (ViewState["_monthsInRow"] != null)
                    result = (int)ViewState["_monthsInRow"];
                else
                    result = 12;
                if ((CalendarMode.Quarter == Mode || CalendarMode.ThreeMonth == Mode) && result > 3)
                    result = 3;
                return result;
            }
            set
            {
                ViewState["_monthsInRow"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the showing of dates from other months for current month.
        /// </summary>
        public bool ShowOtherMonthsDays
        {
            get
            {
                if (ViewState["_showOtherMonthsDays"] != null)
                    return (bool)ViewState["_showOtherMonthsDays"];
                else
                    return false;
            }
            set
            {
                ViewState["_showOtherMonthsDays"] = value;
            }
        }

        private ExtendWeeksMode m_ExtendWeeks = ExtendWeeksMode.Auto;
        /// <summary>
        /// Specifies if an empty week row will be added to the month. 
        /// </summary>
        public ExtendWeeksMode ExtendWeeks
        {
            get
            {
                return m_ExtendWeeks;
            }
            set
            {
                m_ExtendWeeks = value;
            }
        }


        /// <summary>
        /// Gets or sets the format for displaying name of week days.
        /// </summary>
        public WeekdayFormat WeekdayFormat
        {
            get
            {
                if (ViewState["WeekdayFormat"] != null)
                    return (WeekdayFormat)ViewState["WeekdayFormat"];
                else
                    return WeekdayFormat.Full;
            }
            set
            {
                ViewState["WeekdayFormat"] = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the event date in the datasource.
        /// </summary>
        public string EventDateFieldName
        {
            get
            {
                if (ViewState["DFN"] == null)
                    ViewState["DFN"] = String.Empty;
                return (string)ViewState["DFN"];
            }
            set { ViewState["DFN"] = value; }
        }

        /// <summary>
        /// Gets or sets the format string of the event date in the datasource.
        /// </summary>
        public string EventDateFieldFormat
        {
            get
            {
                if (ViewState["DFF"] == null)
                    ViewState["DFF"] = String.Empty;
                return (string)ViewState["DFF"];
            }
            set { ViewState["DFF"] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the event time in the datasource, if time is stored in a different field than the date.
        /// </summary>
        public string EventTimeFieldName
        {
            get
            {
                if (ViewState["TFN"] == null)
                    ViewState["TFN"] = String.Empty;
                return (string)ViewState["TFN"];
            }
            set { ViewState["TFN"] = value; }
        }

        /// <summary>
        /// Gets or sets the format string of the event time in the datasource.
        /// </summary>
        public string EventTimeFieldFormat
        {
            get
            {
                if (ViewState["TFF"] == null)
                    ViewState["TFF"] = String.Empty;
                return (string)ViewState["TFF"];
            }
            set { ViewState["TFF"] = value; }
        }

        #endregion

        #region Events
        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowMonth"/> event.
        /// </summary>
        /// <param name="e">An <see cref="CalendarItemEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowMonth(CalendarItemEventArgs e)
        {
            if (BeforeShowMonth != null)
                BeforeShowMonth(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowDay"/> event.
        /// </summary>
        /// <param name="e">An <see cref="CalendarItemEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowDay(CalendarItemEventArgs e)
        {
            if (BeforeShowDay != null)
                BeforeShowDay(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowWeek"/> event.
        /// </summary>
        /// <param name="e">An <see cref="CalendarItemEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowWeek(CalendarItemEventArgs e)
        {
            if (BeforeShowWeek != null)
                BeforeShowWeek(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowEvent"/> event.
        /// </summary>
        /// <param name="e">An <see cref="CalendarItemEventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowEvent(CalendarItemEventArgs e)
        {
            if (BeforeShowEvent != null)
                BeforeShowEvent(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemCommand"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="CalendarCommandEventArgs"/> that contains event data. </param>
        protected virtual void OnItemCommand(CalendarCommandEventArgs e)
        {
            if (ItemCommand != null) ItemCommand(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemCreated"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="CalendarItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemCreated(CalendarItemEventArgs e)
        {
            if (ItemCreated != null) ItemCreated(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemDataBound"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="CalendarItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemDataBound(CalendarItemEventArgs e)
        {
            _CurrentItemIndex++;
            if (ItemDataBound != null) ItemDataBound(this, e);
        }

        /// <summary>
        /// Occurs before the Calendar send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;
        /// <summary>
        /// Occurs after the Calendar control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        /// <summary>
        /// Occurs before showing each month template. 
        /// </summary>
        public event EventHandler<CalendarItemEventArgs> BeforeShowMonth;
        /// <summary>
        /// Occurs before showing each day template. 
        /// </summary>
        public event EventHandler<CalendarItemEventArgs> BeforeShowDay;
        /// <summary>
        /// Occurs before showing each week template. 
        /// </summary>
        public event EventHandler<CalendarItemEventArgs> BeforeShowWeek;
        /// <summary>
        /// Occurs before showing each event template. 
        /// </summary>
        public event EventHandler<CalendarItemEventArgs> BeforeShowEvent;
        /// <summary>
        /// Occurs when a button is clicked in the Calendar control.
        /// </summary>
        public event EventHandler<CalendarCommandEventArgs> ItemCommand;

        /// <summary>
        /// Occurs when an item is created in the <see cref="Calendar"/> control. 
        /// </summary>
        [
        Category("Behavior"),
        Description("Raised when an item is created and is ready for customization.")
        ]
        public event EventHandler<CalendarItemEventArgs> ItemCreated;

        /// <summary>
        /// Occurs after an item in the <see cref="Calendar"/> is data-bound but before it is rendered on the page.
        /// </summary>
        [
        Category("Behavior"),
        Description("Raised when an item is data-bound.")
        ]
        public event EventHandler<CalendarItemEventArgs> ItemDataBound;

        #endregion

        #region Methods and Implementation
        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();

            if (ViewState["itemsCreated"] != null)
            {
                innerDataSource = (List<EventInfo>)ViewState["items"];
                if (innerDataSource != null)
                    viewStateDataSource = new List<EventInfo>(innerDataSource);
                nextMonthDataSource = new List<EventInfo>();
                CreateControlHierarchy(true, innerDataSource);
            }
        }

        private static int GetNumOfDay(DateTime val)
        {
            int i = (int)System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            if (i == 0)
                i = (int)val.DayOfWeek;
            else
            {
                i = (int)val.DayOfWeek - i;
                if (i < 0)
                    i += 7;
            }
            return i;
        }

        private DateTime __startDate;
        private DateTime _startDate
        {
            get
            {
                return __startDate;
            }
            set
            {
                __startDate = value;
                SpecialValues["StartDate"] = value;
                SpecialValues["DateRange"] = new DateTime[] { _startDate, _endDate };
            }
        }

        private DateTime __endDate;
        private DateTime _endDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
                SpecialValues["EndDate"] = value;
                SpecialValues["DateRange"] = new DateTime[] { _startDate, _endDate };
            }
        }

        private void CreateControlHierarchy(bool useDataSource, object data)
        {
            bool extendWeeks = ExtendWeeks == ExtendWeeksMode.Always;
            DateTime monthStart, monthEnd;
            switch (Mode)
            {
                case CalendarMode.Full:
                    monthStart = new DateTime(Year, 1, 1);
                    monthEnd = monthStart.AddMonths(12);
                    break;
                case CalendarMode.Quarter:
                    monthStart = new DateTime(Year, ((Month - 1) / 3) * 3 + 1, 1);
                    monthEnd = monthStart.AddMonths(3);
                    break;
                case CalendarMode.ThreeMonth:
                    monthStart = new DateTime(Year, Month, 1).AddMonths(-1);
                    monthEnd = monthStart.AddMonths(3);
                    break;
                case CalendarMode.OneMonth:
                    monthStart = new DateTime(Year, Month, 1);
                    monthEnd = monthStart.AddMonths(1);
                    break;
                default:
                    monthStart = new DateTime(Year, 1, 1);
                    monthEnd = monthStart.AddMonths(1);
                    break;
            }
            int index = 0;
            HtmlGenericControl style = null;
            DateTime currDate = monthStart;
            while (innerDataSource != null
                && innerDataSource.Count > 0
                && innerDataSource[0].Date.IsNull)
            {
                innerDataSource.RemoveAt(0);
            }

            CreateItem(ref index, CalendarItemType.Header, useDataSource, null, Date, null);
            int currMonth = 0;
            for (; currDate < monthEnd; currDate = currDate.AddMonths(1))
            {

                if (CalendarMode.OneMonth != Mode && EventRowTemplate == null && currMonth % MonthsInRow == 0)
                {
                    extendWeeks = ExtendWeeks == ExtendWeeksMode.Always;
                    DateTime monthStep = currDate.AddMonths(MonthsInRow);
                    for (DateTime checkDate = currDate; checkDate < monthStep && checkDate < monthEnd; checkDate = checkDate.AddMonths(1))
                        if (DateTime.DaysInMonth(checkDate.Year, checkDate.Month) + GetNumOfDay(checkDate) > 35)
                        {
                            extendWeeks = ExtendWeeks == ExtendWeeksMode.Always || ExtendWeeks == ExtendWeeksMode.Auto;
                            break;
                        }
                }
                if (CurrentDate.Month == currDate.Month)
                    style = CurrentMonthStyle;
                else
                    style = MonthStyle;
                CreateItem(ref index, CalendarItemType.MonthHeader, useDataSource, null, currDate, style);
                currMonth++;
                DateTime startDate = new DateTime(Year, currDate.Month, 1);
                startDate = startDate.AddDays(-GetNumOfDay(startDate));
                DateTime endDate = new DateTime(Year, currDate.Month, DateTime.DaysInMonth(Year, currDate.Month));
                endDate = endDate.AddDays(6 - GetNumOfDay(endDate));
                int offset = (endDate - startDate).Days;
                if (offset < 29)
                    endDate = endDate.AddDays(7);
                if (extendWeeks && offset < 36)
                    endDate = endDate.AddDays(7);
                DateTime dow = startDate;
                for (int w = 0; w < 7; w++)
                {
                    if (w == (int)DayOfWeek.Sunday || w == (int)DayOfWeek.Saturday)
                        style = WeekendNameStyle;
                    else
                        style = WeekdayNameStyle;
                    CalendarItem item = CreateItem(ref index, CalendarItemType.Weekdays, useDataSource, null, dow, style);
                    if (item != null)
                    {
                        Literal weekDay = (Literal)item.FindControl("WeekDay");

                        if (weekDay != null)
                        {
                            switch (WeekdayFormat)
                            {
                                case WeekdayFormat.Full:
                                    weekDay.Text = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetDayName((DayOfWeek)w);
                                    break;
                                case WeekdayFormat.Short:
                                    weekDay.Text = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedDayName((DayOfWeek)w);
                                    break;
                                case WeekdayFormat.Narrow:
                                    weekDay.Text = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetDayName((DayOfWeek)w).Substring(0, 1);
                                    break;
                            }
                        }
                    }
                    if (w != 6)
                        CreateItem(ref index, CalendarItemType.WeekdaySeparator, useDataSource, null, dow, style);
                    dow = dow.AddDays(1);
                }

                CreateItem(ref index, CalendarItemType.WeekdaysFooter, useDataSource, null, currDate, style);

                for (DateTime d = startDate; d <= endDate; d = d.AddDays(1))
                {
                    _currentVerifiedDay = d;
                    if ((int)d.DayOfWeek == (int)System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                        CreateItem(ref index, CalendarItemType.WeekHeader, useDataSource, null, d, style);
                    if (d.DayOfWeek == DayOfWeek.Sunday || d.DayOfWeek == DayOfWeek.Saturday)
                        if (d.Date == CurrentDate.Date && d.Month == currDate.Month)
                            style = WeekendTodayStyle;
                        else if (d.Month == currDate.Month)
                            style = WeekendStyle;
                        else if (d.Date == CurrentDate.Date && d.Month != currDate.Month)
                            style = OtherMonthWeekendTodayStyle;
                        else
                            style = OtherMonthWeekendStyle;
                    else
                        if (d.Date == CurrentDate.Date && d.Month == currDate.Month)
                            style = TodayStyle;
                        else if (d.Month == currDate.Month)
                            style = DayStyle;
                        else if (d.Date == CurrentDate.Date && d.Month != currDate.Month)
                            style = OtherMonthTodayStyle;
                        else
                            style = OtherMonthDayStyle;
                    if (d.Month == currDate.Month || ShowOtherMonthsDays)
                    {
                        bool hasEvents = false;
                        while (innerDataSource != null
                            && innerDataSource.Count > 0
                            && innerDataSource[0].Date.Value < d)
                            innerDataSource.RemoveAt(0);
                        if (nextMonthDataSource != null
                            && nextMonthDataSource.Count > 0
                            && nextMonthDataSource.Exists(IsCurrentVerifiedDay)
                            ||
                            innerDataSource != null
                            && innerDataSource.Count > 0
                            && innerDataSource.Exists(IsCurrentVerifiedDay))
                            hasEvents = true;
                        CreateItem(ref index, CalendarItemType.DayHeader, useDataSource, null, d, style, hasEvents);
                        bool eventsExists = false;
                        if (nextMonthDataSource != null
                            && nextMonthDataSource.Count > 0
                            && nextMonthDataSource.Exists(IsCurrentVerifiedDay))
                        {
                            System.Collections.Generic.List<EventInfo> eventList = nextMonthDataSource.FindAll(IsCurrentVerifiedDay);

                            for (int i = 0; i < eventList.Count; i++)
                            {
                                if (eventsExists)
                                    CreateItem(ref index, CalendarItemType.EventSeparator, useDataSource, eventList[i], d, style);
                                CreateItem(ref index, CalendarItemType.EventRow, useDataSource, eventList[i], d, style);
                                eventsExists = true;
                            }
                        }
                        if (d.Day == 15 && nextMonthDataSource != null)
                            nextMonthDataSource.Clear();
                        if (innerDataSource != null
                            && innerDataSource.Count > 0
                            && innerDataSource.Exists(IsCurrentVerifiedDay))
                        {
                            System.Collections.Generic.List<EventInfo> eventList = innerDataSource.FindAll(IsCurrentVerifiedDay);


                            for (int i = 0; i < eventList.Count; i++)
                            {
                                if (eventsExists)
                                    CreateItem(ref index, CalendarItemType.EventSeparator, useDataSource, eventList[i], d, style);
                                CreateItem(ref index, CalendarItemType.EventRow, useDataSource, eventList[i], d, style);
                                //nextMonthDataSource.Insert(nextMonthDataSource.Count, eventList[i]);
                                eventsExists = true;
                            }
                        }
                        if (!eventsExists)
                        {
                            CreateItem(ref index, CalendarItemType.NoEvents, useDataSource, null, d, style);
                        }
                        CreateItem(ref index, CalendarItemType.DayFooter, useDataSource, null, d, style, hasEvents);
                        style = null;
                    }
                    else
                    {
                        if (style == OtherMonthWeekendTodayStyle) style = OtherMonthWeekendStyle;
                        if (style == OtherMonthTodayStyle) style = OtherMonthDayStyle;
                        CreateItem(ref index, CalendarItemType.EmptyDay, useDataSource, null, d, style);
                    }
                    if (GetNumOfDay(d) == 6)
                    {
                        CreateItem(ref index, CalendarItemType.WeekFooter, useDataSource, null, d, style);
                        if (d != endDate)
                            CreateItem(ref index, CalendarItemType.WeekSeparator, useDataSource, null, d, style);
                    }
                    else if (d < endDate)
                        CreateItem(ref index, CalendarItemType.DaySeparator, useDataSource, null, d, style);
                }

                CreateItem(ref index, CalendarItemType.MonthFooter, useDataSource, null, new DateTime(Year, currDate.Month, 1), style);
                if (currMonth % MonthsInRow == 0 && currDate.AddMonths(1) < monthEnd)
                    CreateItem(ref index, CalendarItemType.MonthsRowSeparator, useDataSource, null, new DateTime(Year, currDate.Month, 1), style);
                else if (currDate.AddMonths(1) < monthEnd)
                    CreateItem(ref index, CalendarItemType.MonthSeparator, useDataSource, null, new DateTime(Year, currDate.Month, 1), style);
            }
            CreateItem(ref index, CalendarItemType.Footer, true, null, new DateTime(Year, 12, 31), style);

            if (useDataSource)
            {
                ViewState["items"] = viewStateDataSource;
                OnBeforeShow(EventArgs.Empty);
            }
            ViewState["itemsCreated"] = true;

        }

        private MTDate GetEventDate(object data)
        {
            if (string.IsNullOrEmpty(EventDateFieldName))
                return DateTime.MinValue;

            return MTDate.Parse(DataSourceHelper.GetFieldValue(EventDateFieldName, data), EventDateFieldFormat);
        }

        private MTDate GetEventTime(object data)
        {
            if (string.IsNullOrEmpty(EventTimeFieldName))
                return DateTime.MinValue;

            return MTDate.Parse(DataSourceHelper.GetFieldValue(EventTimeFieldName, data), EventTimeFieldFormat);
        }

        private bool IsEventDateNull(object data)
        {
            if (string.IsNullOrEmpty(EventDateFieldName))
                return true;

            return DataSourceHelper.GetFieldValue(EventDateFieldName, data) == null;
        }

        private bool IsEventTimeNull(object data)
        {
            if (string.IsNullOrEmpty(EventTimeFieldName))
                return true;

            return DataSourceHelper.GetFieldValue(EventTimeFieldName, data) == null;
        }

        private CalendarItem CreateItem(ref int itemIndex, CalendarItemType itemType, bool dataBind, EventInfo dataItem, DateTime date, HtmlGenericControl styleControl)
        {
            return CreateItem(ref itemIndex, itemType, dataBind, dataItem, date, styleControl, false);
        }

        private CalendarItem CreateItem(ref int itemIndex, CalendarItemType itemType, bool dataBind, EventInfo dataItem, DateTime date, HtmlGenericControl styleControl, bool hasEvents)
        {
            CalendarItem item = new CalendarItem(itemIndex, itemType, this, hasEvents);
            if (dataItem != null)
            {
                _dataItem = new DataItem(dataItem.EventData);
            }

            item.EnableViewState = false;
            if (styleControl != null)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(sb);
                HtmlTextWriter tw = new HtmlTextWriter(sw);
                styleControl.Attributes.Render(tw);
                item.StyleString = ControlsHelper.ReplaceResourcesAndStyles(sb.ToString(), Page);
            }
            switch (itemType)
            {
                case CalendarItemType.Header:
                    if (headerTemplate != null) { HeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.Footer:
                    if (FooterTemplate != null) { FooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.DayFooter:
                    if (DayFooterTemplate != null) { DayFooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.DayHeader:
                    if (DayHeaderTemplate != null) { DayHeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.EventRow:
                    if (EventRowTemplate != null) { EventRowTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.EventSeparator:
                    if (EventSeparatorTemplate != null) { EventSeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.MonthFooter:
                    if (MonthFooterTemplate != null) { MonthFooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.MonthHeader:
                    if (MonthHeaderTemplate != null) { MonthHeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.Weekdays:
                    if (WeekdaysTemplate != null) { WeekdaysTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.WeekdaysFooter:
                    if (WeekdaysFooterTemplate != null) { WeekdaysFooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.WeekdaySeparator:
                    if (WeekdaySeparatorTemplate != null) { WeekdaySeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.WeekFooter:
                    if (WeekFooterTemplate != null) { WeekFooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.WeekHeader:
                    if (WeekHeaderTemplate != null) { WeekHeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.DaySeparator:
                    if (DaySeparatorTemplate != null) { DaySeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.MonthSeparator:
                    if (MonthSeparatorTemplate != null) { MonthSeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.MonthsRowSeparator:
                    if (MonthsRowSeparatorTemplate != null) { MonthsRowSeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.WeekSeparator:
                    if (WeekSeparatorTemplate != null) { WeekSeparatorTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.NoEvents:
                    if (NoEventsTemplate != null) { NoEventsTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case CalendarItemType.EmptyDay:
                    if (EmptyDayTemplate != null) { EmptyDayTemplate.InstantiateIn(item); itemIndex++; }
                    break;
            }
            CalendarItemEventArgs e = new CalendarItemEventArgs(item);
            if (dataBind)
            {
                item.DataItem = _dataItem;
            }
            item.CurrentProcessingDate = date;
            SpecialValues["CurrentProcessingDate"] = date;
            switch (itemType)
            {
                case CalendarItemType.DayFooter:
                case CalendarItemType.DayHeader:
                case CalendarItemType.DaySeparator:
                case CalendarItemType.EmptyDay:
                case CalendarItemType.EventRow:
                case CalendarItemType.EventSeparator:
                case CalendarItemType.NoEvents:
                case CalendarItemType.WeekFooter:
                case CalendarItemType.WeekHeader:
                case CalendarItemType.WeekSeparator:
                    SpecialValues["NextProcessingDate"] = date.AddDays(1);
                    SpecialValues["PrevProcessingDate"] = date.AddDays(-1);
                    break;
                case CalendarItemType.MonthFooter:
                case CalendarItemType.MonthHeader:
                case CalendarItemType.MonthsRowSeparator:
                case CalendarItemType.MonthSeparator:
                    SpecialValues["NextProcessingDate"] = date.AddMonths(1);
                    SpecialValues["PrevProcessingDate"] = date.AddMonths(-1);
                    break;
                case CalendarItemType.Footer:
                case CalendarItemType.Header:
                    SpecialValues["NextProcessingDate"] = date.AddYears(1);
                    SpecialValues["PrevProcessingDate"] = date.AddYears(-1);
                    break;

            }
            OnItemCreated(e);
            Controls.Add(item);

            if (dataBind)
            {
                item.DataBind();
                OnItemDataBound(e);

                if (CalendarItemType.DayFooter == itemType || CalendarItemType.DayHeader == itemType || CalendarItemType.EmptyDay == itemType)
                    OnBeforeShowDay(new CalendarItemEventArgs(item));
                if (CalendarItemType.MonthFooter == itemType || CalendarItemType.MonthHeader == itemType && MonthFooterTemplate == null)
                    OnBeforeShowMonth(new CalendarItemEventArgs(item));
                if (CalendarItemType.WeekFooter == itemType || CalendarItemType.WeekHeader == itemType && WeekFooterTemplate == null)
                    OnBeforeShowWeek(new CalendarItemEventArgs(item));
                if (CalendarItemType.EventRow == itemType)
                    OnBeforeShowEvent(new CalendarItemEventArgs(item));
            }
            item.DataItem = null;
            if (item.FindControl("StyleString") != null)
                ((Literal)item.FindControl("StyleString")).Text = item.StyleString;
            return item;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;
            RenderContents(writer);
        }

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            if (DesignMode)
            {
                DataSourceID = "";
                DataSource = null;
            }
            else
            {
                if (!Visible || Restricted && !UserRights.AllowRead) return;
                OnBeforeSelect(EventArgs.Empty);

                IDataSource ds = GetDataSource() as IDataSource;
                if (ds != null)
                {
                    MTDataSourceView dv = ds.GetView("") as MTDataSourceView;
                    if (dv != null)
                    {
                        dv.Owner = this;
                    }
                }
            }
            base.PerformSelect();
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="Calendar"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="System.Collections.IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(System.Collections.IEnumerable data)
        {
            base.PerformDataBinding(data);
            if (data != null && data.GetEnumerator().MoveNext())
            {
                __hasData = true;
                innerDataSource = new List<EventInfo>();
                viewStateDataSource = new List<EventInfo>();
                nextMonthDataSource = new List<EventInfo>();
                bool timeExists = EventTimeFieldName.Length > 0;
                foreach (object item in data)
                {
                    innerDataSource.Add(new EventInfo(GetEventDate(item), GetEventTime(item), timeExists, item));
                    System.Collections.Hashtable ht = new System.Collections.Hashtable();
                    for (int i = 0; i < ((System.Data.DataRowView)item).Row.ItemArray.Length; i++)
                    {
                        ht.Add(((System.Data.DataRowView)item).Row.Table.Columns[i].Caption, ((System.Data.DataRowView)item).Row[i]);
                    }
                    viewStateDataSource.Add(new EventInfo(GetEventDate(item), GetEventTime(item), timeExists, ht));
                }
                //innerDataSource.Sort(new EventTimeComparer());
                //viewStateDataSource.Sort(new EventTimeComparer());
            }

            Controls.Clear();
            if (HasChildViewState)
                ClearChildViewState();

            CreateControlHierarchy(true, data);
            ChildControlsCreated = true;
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            string d = "";
            bool isFormPresent = false;
            if (!DesignMode)
            {
                isFormPresent = Page.Request.Form[ID + "Date"] != null || Page.Request.Form[ID + "Month"] != null || Page.Request.Form[ID + "Year"] != null;
                if (Page.Request.QueryString[ID + "Date"] != null && !isFormPresent)
                    d = Page.Request.QueryString[ID + "Date"];
                else if (Page.Request.Form[ID + "Date"] != null)
                    d = Page.Request.Form[ID + "Date"];
            }

            if (d.Length > 0)
            {
                string[] parts = d.Split('-');
                DateTime dummyDate;
                if (!DateTime.TryParseExact(parts[0], "yyyy", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out dummyDate))
                {
                    parts[0] = CurrentDate.Year.ToString("0000");
                }
                if (!DateTime.TryParseExact(parts[1], "MM", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out dummyDate))
                {
                    parts[1] = CurrentDate.Month.ToString("00");
                }
                if (!DateTime.TryParseExact(parts[0] + "-" + parts[1], "yyyy-MM", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out dummyDate))
                    Date = CurrentDate;
                else
                    Date = dummyDate;
            }
            else
            {
                Date = CurrentDate;
                if (!DesignMode)
                {
                    int val = 0;
                    if (Page.Request.QueryString[ID + "Month"] != null)
                        int.TryParse(Page.Request.QueryString[ID + "Month"], out val);
                    else if (Page.Request.Form[ID + "Month"] != null)
                        int.TryParse(Page.Request.Form[ID + "Month"], out val);
                    if (val > 0 && val < 13) Month = val;

                    if (Page.Request.QueryString[ID + "Year"] != null)
                        int.TryParse(Page.Request.QueryString[ID + "Year"], out val);
                    else if (Page.Request.Form[ID + "Year"] != null)
                        int.TryParse(Page.Request.Form[ID + "Year"], out val);
                    if (val > 0 && val < 10000) Year = val;
                }
            }

            switch (Mode)
            {
                case CalendarMode.Full:
                    _startDate = new DateTime(Year, 1, 1);
                    _endDate = _startDate.AddMonths(11);
                    break;
                case CalendarMode.Quarter:
                    _startDate = new DateTime(Year, ((Month - 1) / 3) * 3 + 1, 1);
                    _endDate = _startDate.AddMonths(2);
                    break;
                case CalendarMode.ThreeMonth:
                    _startDate = new DateTime(Year, Month, 1).AddMonths(-1);
                    _endDate = _startDate.AddMonths(2);
                    break;
                case CalendarMode.OneMonth:
                    _startDate = new DateTime(Year, Month, 1);
                    _endDate = _startDate;
                    break;
                default:
                    _startDate = new DateTime(Year, 1, 1);
                    _endDate = _startDate;
                    break;
            }
            _startDate = _startDate.Date;
            _endDate = new DateTime(_endDate.Year, _endDate.Month, DateTime.DaysInMonth(_endDate.Year, _endDate.Month), 23, 59, 59);

            if (ShowOtherMonthsDays)
            {
                bool extendWeeks = ExtendWeeks == ExtendWeeksMode.Always;
                if (CalendarMode.OneMonth != Mode && EventRowTemplate == null && _endDate.Month % MonthsInRow == 0)
                {
                    extendWeeks = ExtendWeeks == ExtendWeeksMode.Always;
                    DateTime monthStep = _endDate.AddMonths(MonthsInRow);
                    for (DateTime checkDate = _endDate; checkDate < monthStep && checkDate < _endDate.AddMonths(1); checkDate = checkDate.AddMonths(1))
                        if (DateTime.DaysInMonth(checkDate.Year, checkDate.Month) + GetNumOfDay(checkDate) > 35)
                        {
                            extendWeeks = ExtendWeeks == ExtendWeeksMode.Always || ExtendWeeks == ExtendWeeksMode.Auto;
                            break;
                        }
                }

                _startDate = _startDate.AddDays(-GetNumOfDay(_startDate));
                _endDate = _endDate.AddDays(6 - GetNumOfDay(_endDate));
                if (extendWeeks)
                    _endDate = _endDate.AddDays(7);
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent control, if the EventArgs parameter is an instance of <see cref="CalendarCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            bool handled = false;

            if (args is CalendarCommandEventArgs)
            {
                CalendarCommandEventArgs ce = (CalendarCommandEventArgs)args;

                OnItemCommand(ce);
                handled = true;

            }
            return handled;
        }

        #endregion

        #region IForm Members

        private int _CurrentItemIndex = -1;
        /// <summary>
        /// Gets the index of the current processed <see cref="CalendarItem"/>.
        /// </summary>
        public int CurrentItemIndex
        {
            get { return _CurrentItemIndex; }
        }

        /// <summary>
        /// Gets the current processed <see cref="CalendarItem"/>.
        /// </summary>
        public CalendarItem CurrentItem
        {
            get
            {
                if (_CurrentItemIndex < 0 || _CurrentItemIndex > Controls.Count - 1)
                    return null;
                return (CalendarItem)Controls[_CurrentItemIndex];
            }
        }

        private DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (ViewState["__UserRights"] == null)
                    ViewState["__UserRights"] = new FormSupportedOperations();
                return (FormSupportedOperations)ViewState["__UserRights"];
            }
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1) rows.Insert(0, _CurrentItemIndex);
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                ControlsHelper.PopulateControlCollection<T>(result, Controls[i].Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            List<int> rows = new List<int>(new int[] { 0, Controls.Count - 1 });
            if (_CurrentItemIndex != 0 && _CurrentItemIndex != Controls.Count - 1) rows.Insert(0, _CurrentItemIndex);
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)Controls[rows[i]].FindControl(id);
                if (ctrl != null) return ctrl;
            }
            for (int i = 0; i < rows.Count && rows[i] >= 0; i++)
            {
                T ctrl = (T)InMotion.Common.Controls.FindControlIterative(Controls[rows[i]], id);
                if (ctrl != null) return ctrl;
            }
            return (T)InMotion.Common.Controls.FindControlIterative(this, id);
        }
        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>The instance of the <i>Control</i> if any; otherwise the <b>null</b> reference.</returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }
        #endregion

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
        }

        private static CalendarItem GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is CalendarItem) && c != null)
                c = c.NamingContainer;
            return c as CalendarItem;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            CalendarItem ri = GetControlContainer(control);
            string arrayName = "";
            switch (ri.ItemType)
            {
                case CalendarItemType.Footer:
                    arrayName = ID + "Controls_Footer";
                    break;
                case CalendarItemType.Header:
                    arrayName = ID + "Controls_Header";
                    break;
                default:
                    arrayName = ID + "Controls_Row" + ri.ItemIndex.ToString();
                    break;
            }
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }

        Dictionary<string, object> _specialValues = new Dictionary<string, object>();
        /// <summary>
        /// Get the dictionary that contain the calculated special values for current component.
        /// </summary>
        public Dictionary<string, object> SpecialValues
        {
            get { return _specialValues; }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = this;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }

        private bool IsCurrentVerifiedDay(EventInfo eventInfo)
        {
            return (eventInfo.Date.Day == _currentVerifiedDay.Day
                    && eventInfo.Date.Month == _currentVerifiedDay.Month
                    && eventInfo.Date.Year == _currentVerifiedDay.Year);
        }
    }

    /// <summary>
    /// Designer for <see cref="Calendar"/> control.
    /// </summary>
    class CalendarDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Calendar ctrl = (Calendar)Component;
                    __TemplateGroups = base.TemplateGroups;
                    TemplateDefinition tempDef;
                    TemplateGroup tempGroup;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "HeaderTemplate", ctrl, "HeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "FooterTemplate", ctrl, "FooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Month Templates");
                    tempDef = new TemplateDefinition(this, "MonthHeaderTemplate", ctrl, "MonthHeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "MonthFooterTemplate", ctrl, "MonthFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "MonthSeparatorTemplate", ctrl, "MonthSeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "MonthsRowSeparatorTemplate", ctrl, "MonthsRowSeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Week Days Templates");
                    tempDef = new TemplateDefinition(this, "WeekdaysTemplate", ctrl, "WeekdaysTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "WeekdaySeparatorTemplate", ctrl, "WeekdaySeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "WeekdaysFooterTemplate", ctrl, "WeekdaysFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Week Templates");
                    tempDef = new TemplateDefinition(this, "WeekHeaderTemplate", ctrl, "WeekHeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "WeekFooterTemplate", ctrl, "WeekFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "WeekSeparatorTemplate", ctrl, "WeekSeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Day Templates");
                    tempDef = new TemplateDefinition(this, "DayHeaderTemplate", ctrl, "DayHeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "DayFooterTemplate", ctrl, "DayFooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "DaySeparatorTemplate", ctrl, "DaySeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Events Templates");
                    tempDef = new TemplateDefinition(this, "EventSeparatorTemplate", ctrl, "EventSeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "NoEventsTemplate", ctrl, "NoEventsTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    tempDef = new TemplateDefinition(this, "EventRowTemplate", ctrl, "EventRowTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}