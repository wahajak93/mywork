//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a report label control, which displays text in a <see cref="Report"/> form.
    /// </summary>
    public class MTReportLabel : MTLabel
    {
        private string _emptyText = "";
        /// <summary>
        /// Gets or sets text to display when the label's value is empty. The text can be in HTML format if <see cref="MTLabel.ContentType"/> property is set to <see cref="ContentType.Html"/>.
        /// </summary>
        public string EmptyText
        {
            get
            {
                return _emptyText;
            }
            set
            {
                _emptyText = value;
            }
        }

        private bool _hideDuplicates;
        /// <summary>
        /// Gets or sets value that indicating whether to hide duplicate values, if you do not want to show the same value in multiple consecutive rows.
        /// </summary>
        public bool HideDuplicates
        {
            get
            {
                return _hideDuplicates;
            }
            set
            {
                _hideDuplicates = value;
            }
        }

        private TotalFunction _function;
        /// <summary>
        /// Gets or sets <see cref="TotalFunction"/> to be used if you want the value to be auto-calculated. The following functions are available: Sum, Count, Min, Max, and Average.
        /// </summary>
        public TotalFunction Function
        {
            get
            {
                return _function;
            }
            set
            {
                _function = value;
            }
        }

        private string _percentOf = "";
        /// <summary>
        /// Gets or sets the name of <see cref="ReportGroup"/> that indicating whether the label value should be shown as percentage of a higher-level group or the report.
        /// </summary>
        public string PercentOf
        {
            get
            {
                return _percentOf;
            }
            set
            {
                _percentOf = value;
            }
        }

        private string _resetAt = "";
        /// <summary>
        /// Specify when the function-based calculations should be reset - never, at page level, or at group level.
        /// </summary>
        public string ResetAt
        {
            get
            {
                return _resetAt;
            }
            set
            {
                _resetAt = value;
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="MTLabel.BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="System.Web.UI.HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (!DesignMode)
            {
                if (String.IsNullOrEmpty(BaseText))
                {
                    BaseText = EmptyText;
                    if (ContentType.Text == ContentType)
                        BaseText = Page.Server.HtmlEncode(BaseText);
                }
            }
            base.Render(writer);
        }
    }
}