//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for the ItemCreated and ItemDataBound events of a <see cref="Menu"/>.
    /// </summary>
    public sealed class MenuItemEventArgs : EventArgs
    {
        private MenuItemControl item;
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemEventArgs"/> class.
        /// </summary>
        /// <param name="item">The <see cref="MenuItemControl"/> associated with the event. The Item property is set to this value.</param>
        public MenuItemEventArgs(MenuItemControl item)
        {
            this.item = item;
        }

        /// <summary>
        /// Gets the <see cref="MenuItemControl"/> associated with the event.
        /// </summary>
        public MenuItemControl Item
        {
            get
            {
                return item;
            }
        }
    }
}