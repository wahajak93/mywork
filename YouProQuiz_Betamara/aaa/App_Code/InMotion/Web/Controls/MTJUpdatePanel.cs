//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Enables sections of a page to be partially rendered without a postback.
    /// </summary>
    public class MTJUpdatePanel : UpdatePanel, IAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTJUpdatePanel"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region IMTAttributeAccessor Members
        /// <summary>
        /// Set value of attribute by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <param name="value">Value of attribute.</param>
        public void SetAttribute(String name, String value)
        {
            Attributes[name] = value;
        }

        /// <summary>
        /// Retrieve attribute value by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <returns>Value of attribute by the name.</returns>
        public String GetAttribute(String name)
        {
            return (String)Attributes[name];
        }

        System.Web.UI.AttributeCollection _attributes = null;
        StateBag _attributeState;
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        public System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    if (_attributeState == null)
                        _attributeState = new StateBag(true);
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                    _attributes = new System.Web.UI.AttributeCollection(_attributeState);
                }
                return _attributes;
            }
        }
        #endregion

        #region Methods
        private string _dataId = "";
        /// <summary>
        /// Gets or sets client selector.
        /// </summary>
        public string DataId
        {
            get { return _dataId; }
            set { 
                _dataId = value;
                Attributes["data-Id"] = _dataId;
            }
        }
        /// <summary>
        /// Saves any state that was modified after the TrackViewState method was invoked.
        /// </summary>
        /// <returns>An object that contains the current view state of the control; otherwise, if there is no view state associated with the control, a null reference (Nothing in Visual Basic).</returns>
        protected override object SaveViewState()
        {
            object view_state;
            object attr_view_state = null;

            view_state = base.SaveViewState();
            if (_attributeState != null)
                attr_view_state = ((IStateManager)_attributeState).SaveViewState();

            if (view_state == null && attr_view_state == null)
                return null;
            return new Pair(view_state, attr_view_state);
        }

        /// <summary>
        /// Restores view-state information from a previous request that was saved with the <see cref="SaveViewState"/> method.
        /// </summary>
        /// <param name="savedState">An object that represents the control state to restore.</param>
        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }

            Pair pair = (Pair)savedState;
            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                if (_attributeState == null)
                {
                    _attributeState = new StateBag();
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                }
                ((IStateManager)_attributeState).LoadViewState(pair.Second);
                _attributes = new System.Web.UI.AttributeCollection(_attributeState);
            }
        }

        private string GetFormatedPath(string path)
        {
            char[] arrSplitValue = new char[] { '=' };
            string result = (new Uri(Context.Request.Url, path)).Query;
            result = result.TrimStart(new char[] { '?' });
            string[] arrValues = result.Split(new char[] { '&' });
            List<string> al = new List<string>();

            foreach (string sValue in arrValues)
            {
                string[] arrKeyValue = sValue.Split(arrSplitValue);
                if (arrKeyValue.Length > 0 && arrKeyValue.Length < 3)
                    al.Add(System.Web.HttpUtility.UrlDecode(arrKeyValue[0].Trim()));
            }

            result = path;
            foreach (string sKey in Context.Request.QueryString.AllKeys)
            {
                if (!al.Contains(sKey))
                    result += (result.Contains("?") ? "&" : "?") + sKey + "=";
            }
            return result;
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Page.IsPostBack && Page.Request.Form["__EVENTARGUMENT"] != null && FindControl(Page.Request.Form["__EVENTTARGET"]) is HyperLink)
            {
                Context.RewritePath(GetFormatedPath(Page.Request.Form["__EVENTARGUMENT"]));
                DataBind();
            }
        }
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Determines whether the event for the server control is passed up the page's UI server control hierarchy.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">An EventArgs object that contains the event data.</param>
        /// <returns>true if the event has been canceled; otherwise, false. The default is false.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
//            if (!(source is Button))
                this.DataBind();
            return base.OnBubbleEvent(source, args);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="Control"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            return (T)FindControl(id);
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        /// <summary>
        /// Renders the control to the specified HTML writer. 
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PageScript", Page.Items["js"].ToString(), true);
            Page.ClientScript.RegisterStartupScript(typeof(MTJUpdatePanel), "UpdatePanelOnBeforeRefresh", "Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(function(s, e) {\n  jQuery.fn.ccsUpdatePanel('invoke', jQuery.fn.ccsUpdatePanel('getPanelByPostback', e), 'beforerefresh');\n});\n", true);
            //Page.ClientScript.RegisterStartupScript(typeof(MTJUpdatePanel), "UpdatePanelOnAfterRequest", "Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function(s, e) {\n  jQuery.fn.ccsUpdatePanel('invoke', jQuery.fn.ccsUpdatePanel('getPanelId', e), 'afterrequest');\n});\n", true);
            //Page.ClientScript.RegisterStartupScript(typeof(MTJUpdatePanel), "UpdatePanelOnAfterRefresh", "Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function(s, e) {\n  var panels = e.get_panelsUpdated();\n  if (panels.length == 1) jQuery.fn.ccsUpdatePanel('invoke', panels[0], 'afterrefresh');\n});\n", true);
            Page.ClientScript.RegisterStartupScript(typeof(MTJUpdatePanel), "replacing_updatePanel", "Sys.WebForms.PageRequestManager.prototype._updatePanel = jQuery.fn.ccsUpdatePanel('getReplacementFunction', '_updatePanel'); \n", true);
            

            if (_attributeState != null)
            {
                IEnumerator ie = Attributes.Keys.GetEnumerator();
                while (ie.MoveNext())
                {
                    string key = (string)ie.Current;
                    writer.AddAttribute(key, Attributes[key]);
                }
            }
            base.Render(writer);
        }
        #endregion
    }
}