//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents the parameter of stored procedure command.
    /// </summary>
    public class SPParameter : DataParameter
    {
        /// <summary>
        /// Initializes the new instance of the <see cref="SPParameter"/> class.
        /// </summary>
        public SPParameter()
        {
            
        }

        /// <summary>
        /// Gets the <see cref="DataType"/> of the parameter.
        /// </summary>
        public override DataType DataType
        {
            get
            {
                return InMotion.Data.DataUtility.MapDbType(this.DbType);
            }
            set
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The property Set method is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The property Set method is not implemented.");
            }
        }

        private ParameterDirection _direction;
        /// <summary>
        /// Gets or sets a value that indicates whether the parameter is input-only, 
        /// output-only, bidirectional, or a stored procedure return value parameter.
        /// </summary>
        public ParameterDirection Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
            }
        }


        private DbType _dbType;
        /// <summary>
        /// Gets or sets the <see cref="DbType"/> of the parameter.
        /// </summary>
        public DbType DbType
        {
            get
            {
                return _dbType;
            }
            set
            {
                _dbType = value;
            }
        }

        private int _size;
        /// <summary>
        /// Gets or sets the maximum size, in bytes, of the data within the column.
        /// </summary>
        public int Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }
    
    }
}
