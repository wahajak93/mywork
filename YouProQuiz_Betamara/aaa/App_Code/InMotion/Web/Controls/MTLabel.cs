//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a label control, which displays text on a Web page.
    /// </summary>
    [ParseChildren(false, "Text")]
    public class MTLabel : Literal, IMTControlWithValue, IMTAttributeAccessor
    {
        #region Events

        /// <summary>
        /// Occurs after the <see cref="MTLabel"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (_value != null) _value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        private ContentType _content_Type = ContentType.Text;

        /// <summary>
        /// Gets or sets whether the content of the control is plain text or HTML code.
        /// </summary>
        public ContentType ContentType
        {
            get { return _content_Type; }
            set { _content_Type = value; }
        }

        private string _format = String.Empty;

        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;

        /// <summary>
        /// Format that will be used to extract value from the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private bool _valUpdated;

        private bool _textUpdated
        {
            get
            {
                return ViewState["_textUpdated"] != null && (bool)ViewState["_textUpdated"];
            }
            set
            {
                ViewState["_textUpdated"] = value;
            }
        }

        private object _value;
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                if (_value == null) return DefaultValue;
                return _value;
            }
            set
            {
                object val = value;
                val = TypeFactory.CreateTypedField(DataType, val, Format);
                _value = val;
                _valUpdated = true;

            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;

                val = TypeFactory.CreateTypedField(DataType, val, Format);
                __DefaultValue = val;

            }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }

        private string DisplayedText;
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public new string Text
        {
            get
            {
                if (DisplayedText != null)
                {
                    return DisplayedText;
                }
                if (DesignMode && Value == null && String.IsNullOrEmpty(base.Text))
                {
                    return ID;
                }
                if (!_textUpdated && _valUpdated)
                {
                    return ((IMTField)Value).ToString(Format);
                }
                else
                {
                    return base.Text;
                }
            }
            set
            {
                _textUpdated = true;
                base.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of base text.
        /// </summary>
        protected internal string BaseText
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
        #endregion


        #region Methods
        private bool isBeforeShowRaised = false;
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
            isBeforeShowRaised = true;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode)
            {
                IMTField val = (IMTField)Value;
                if (!_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                }
                else
                {
                    DisplayedText = Text;
                }

                DisplayedText = DisplayedText.Replace("{0}", ControlsHelper.EncodeText(val.ToString(Format), ContentType));
                base.Text = DisplayedText = Text;
                if (String.IsNullOrEmpty(base.Text))
                    base.Text = ((System.Web.UI.Control)this).ID;

                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");
                writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "#AAAAAA");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                base.Render(writer);
                writer.RenderEndTag();
            }
            else
            {
                base.Render(writer);
            }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!isBeforeShowRaised && OwnerForm == null)
                OnBeforeShow(EventArgs.Empty);
            if (!Page.IsPostBack || _valUpdated)
            {
                IMTField val = (IMTField)Value;
                if (!_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                }
                else
                {
                    DisplayedText = Text;
                }
                DisplayedText = DisplayedText.Replace("{0}", ControlsHelper.EncodeText(val.ToString(Format), ContentType));
                base.Text = DisplayedText = ControlsHelper.ReplaceResource(Text);
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (!DesignMode || SourceType == InMotion.Common.SourceType.SpecialValue)
            {
                object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
                if (val != null || val is IMTField && !((IMTField)val).IsNull)
                    Value = val;
            }
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!_valUpdated)
            {
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Value = Page.Request.QueryString[ID];
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                    Value = Page.Request.Form[ID];
                /*if (Page.IsPostBack && Text .Length>0)
                {
                    if (Text.IndexOf("{0}") == -1) Value = Text; else DisplayedText = Text;
                    _textUpdated = false;
                }*/
            }
        }
        #endregion

        #region IMTAttributeAccessor Members
        /// <summary>
        /// Set value of attribute by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <param name="value">Value of attribute.</param>
        public void SetAttribute(String name, String value)
        {
            Attributes[name] = value;
        }

        /// <summary>
        /// Retrieve attribute value by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <returns>Value of attribute by the name.</returns>
        public String GetAttribute(String name)
        {
            return (String)Attributes[name];
        }

        System.Web.UI.AttributeCollection _attributes = null;
        StateBag _attributeState;
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        public System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    if (_attributeState == null)
                        _attributeState = new StateBag(true);
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                    _attributes = new System.Web.UI.AttributeCollection(_attributeState);
                }
                return _attributes;
            }
        }
        #endregion

        /// <summary>
        /// Saves any state that was modified after the TrackViewState method was invoked.
        /// </summary>
        /// <returns>An object that contains the current view state of the control; otherwise, if there is no view state associated with the control, a null reference (Nothing in Visual Basic).</returns>
        protected override object SaveViewState()
        {
            object view_state;
            object attr_view_state = null;

            view_state = base.SaveViewState();
            if (_attributeState != null)
                attr_view_state = ((IStateManager)_attributeState).SaveViewState();

            if (view_state == null && attr_view_state == null)
                return null;
            return new Pair(view_state, attr_view_state);
        }

        /// <summary>
        /// Restores view-state information from a previous request that was saved with the <see cref="SaveViewState"/> method.
        /// </summary>
        /// <param name="savedState">An object that represents the control state to restore.</param>
        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }

            Pair pair = (Pair)savedState;
            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                if (_attributeState == null)
                {
                    _attributeState = new StateBag();
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                }
                ((IStateManager)_attributeState).LoadViewState(pair.Second);
                _attributes = new System.Web.UI.AttributeCollection(_attributeState);
            }
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }
    }
}