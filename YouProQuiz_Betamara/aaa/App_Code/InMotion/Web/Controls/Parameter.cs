//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Text;
using System.Web.UI;
using System.Collections;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides a mechanism that data source controls use to bind to 
    /// application variables, user identities and choices, and other data. 
    /// Serves as the base class for all InMotion parameter types. 
    /// </summary>
    public class Parameter : IStateManager
    {
        #region Properties
        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the <see cref="InMotion.Common.DataType"/> of the parameter.
        /// </summary>
        public virtual DataType DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }

        /// <summary>
        /// Gets or sets the value of the parameter or first value in case of multiple values.
        /// </summary>
        public virtual object Value
        {
            get { return Values[0]; }
            set
            {
                SetValue(value, true);
                if (value != null)
                    IsValueChanged = true;
            }
        }

        private bool _IsValueChanged = false;
        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Value"/> property is changed.
        /// </summary>
        protected internal virtual bool IsValueChanged
        {
            get
            {
                return _IsValueChanged;
            }
            set
            {
                _IsValueChanged = value;
            }
        }

        /// <summary>
        /// Sets a value to the parameter.
        /// </summary>
        /// <param name="val">The new parameter value.</param>
        protected internal virtual void SetValue(object val)
        {
            SetValue(val, false);
        }

        /// <summary>
        /// Sets a value to the parameter.
        /// </summary>
        /// <param name="val">The new parameter value.</param>
        /// <param name="ignoreChanges">Indicates whethere need to ignore value changes.</param>
        protected internal virtual void SetValue(object val, bool ignoreChanges)
        {
            if (IsValueChanged && !ignoreChanges) return;
            ICollection vals = val as ICollection;
            if (vals != null)
            {
                object[] _tvalues = new object[vals.Count];
                vals.CopyTo(_tvalues, 0);
                SetValues(_tvalues, ignoreChanges);
            }
            else
                Values[0] = (IMTField)TypeFactory.CreateTypedField(this.DataType, val, Format);
        }

        /// <summary>
        /// Sets a values to the parameter.
        /// </summary>
        /// <param name="val">The new parameter value.</param>
        protected internal virtual void SetValues(object[] val)
        {
            SetValues(val, false);
        }

        /// <summary>
        /// Sets a values to the parameter.
        /// </summary>
        /// <param name="val">The new parameter value.</param>
        /// <param name="ignoreChanges">Indicates whethere need to ignore value changes.</param>
        protected internal virtual void SetValues(object[] val, bool ignoreChanges)
        {
            if (IsValueChanged && !ignoreChanges) return;
            if (val != null)
            {
                _values = new object[val.Length];
                for (int i = 0; i < val.Length; i++)
                {
                    _values[i] = (IMTField)TypeFactory.CreateTypedField(this.DataType, val[i], Format);
                }
            }
        }

        private object[] _values = new object[1];
        /// <summary>
        /// Gets or sets the array containing values of the parameter.
        /// </summary>
        public object[] Values
        {
            get { return _values; }
            set
            {
                SetValues(value, true);
                IsValueChanged = true;
            }
        }

        /// <summary>
        /// Gets the value indicating whether parameter contain multiple values.
        /// </summary>
        public bool IsMultiple
        {
            get { return _values.Length > 1; }
        }

        private string _Name;
        /// <summary>
        /// Gets or sets the name of the parameter.
        /// </summary>
        public virtual string Name
        {
            get
            {
                if (_Name == null) _Name = String.Empty;
                return _Name;
            }
            set { _Name = value; }
        }

        private string _Source;
        /// <summary>
        /// Gets or sets the source of the parameter.
        /// </summary>
        public virtual string Source
        {
            get
            {
                if (_Source == null) _Source = String.Empty;
                return _Source;
            }
            set { _Source = value; }
        }

        private object _DefaultValue;
        /// <summary>
        /// Specifies a default value for the parameter, 
        /// should the value that the parameter is bound to be uninitialized.
        /// </summary>
        public virtual object DefaultValue
        {
            get { return _DefaultValue; }
            set { _DefaultValue = value; }
        }

        private string _Format;
        /// <summary>
        /// Gets or sets the format string that will be used for format <see cref="Value"/> of parameter.
        /// </summary>
        public virtual string Format
        {
            get { return _Format; }
            set { _Format = value; }
        }

        /// <summary>
        /// Gets the string representation of <see cref="Value"/>.
        /// </summary>
        public virtual string Text
        {
            get
            {
                StringBuilder result = new StringBuilder();
                foreach (object val in _values)
                {
                    if (val is IMTField)
                    {
                        result.Append(((IMTField)val).ToString(Format));
                        result.Append(',');
                    }
                    else
                    {
                        IMTField v = (IMTField)TypeFactory.CreateTypedField(this.DataType, Value, Format);
                        result.Append(v.ToString(Format));
                        result.Append(',');
                    }
                }
                return result.ToString().TrimEnd(',');
            }
        }
        #endregion

        /// <summary>
        /// Initializes the new instance of the <b>Parameter</b> class.
        /// </summary>
        public Parameter() { }

        /// <summary>
        /// Initializes the new instance of the <b>Parameter</b> class using supplied name and value.
        /// </summary>
        /// <param name="name">The <see cref="Name"/> of new parameter.</param>
        /// <param name="value">The <see cref="Value"/> of parameter.</param>
        public Parameter(string name, object value)
        {
            this.Name = name;
            if (value is ICollection)
            {
                object[] _tvalues = new object[((ICollection)value).Count];
                ((ICollection)value).CopyTo(_tvalues, 0);
                SetValues(_tvalues, false);
            }
            else
                SetValue(value, false);
        }

        internal Control _owner;
        /// <summary>
        /// Updates and returns the value of the Parameter object.
        /// </summary>
        /// <param name="owner">The <see cref="Control"/> that the parameter is bound to.</param>
        /// <returns>An object that represents the updated and current value of the parameter.</returns>
        public object Evaluate(Control owner)
        {
            _owner = owner;
            return Evaluate();
        }

        /// <summary>
        /// Updates and returns the value of the Parameter object.
        /// </summary>
        /// <returns>An object that represents the updated and current value of the parameter.</returns>
        public virtual object Evaluate()
        {
            return Value;
        }

        #region IStateManager Members
        private bool _isTrackingViewState;
        /// <summary>
        /// Gets a value indicating whether a parameter is tracking its view state changes.
        /// </summary>
        public bool IsTrackingViewState
        {
            get { return _isTrackingViewState; }
        }

        /// <summary>
        /// Loads the server control's previously saved view state to the control.
        /// </summary>
        /// <param name="state">An Object that contains the saved view state values for the control.</param>
        public virtual void LoadViewState(object state)
        {
            if (state == null) return;
            object[] states = (object[])state;
            if (states.Length != 1)
            {
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Invalid Parameter View State.\n{0}", Environment.StackTrace);
                throw new ArgumentException("Invalid Parameter View State");
            }
            ((IStateManager)ViewState).LoadViewState(states[0]);
        }

        /// <summary>
        /// Saves the changes to a server control's view state to an Object.
        /// </summary>
        /// <returns>The Object that contains the view state changes.</returns>
        public virtual object SaveViewState()
        {
            object[] state = new object[1];
            if (_ViewState != null)
            {
                state[0] = ((IStateManager)_ViewState).SaveViewState();
            }
            return state;
        }

        /// <summary>
        /// Instructs the server control to track changes to its view state.
        /// </summary>
        public virtual void TrackViewState()
        {
            _isTrackingViewState = true;
            if (_ViewState != null)
            {
                ((IStateManager)_ViewState).TrackViewState();
            }
        }
        #endregion

        private bool _required;
        /// <summary>
        /// Gets or sets the value that indicating whether parameter must have non-empty value.
        /// </summary>
        /// <remarks>The <see cref="MTDataSourceView"/> checks the values for each required parameter. If its value is empty the command will not be executed.</remarks>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        /// <summary>
        /// Returns a <see cref="String"/> that represents the parameter value.
        /// </summary>
        /// <returns>A <see cref="String"/> that represents the parameter value.</returns>
        public override string ToString()
        {
            return Text;
        }

        private bool _IsValueNotPassed;
        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        public bool IsValueNotPassed
        {
            get { return _IsValueNotPassed; }
            set { _IsValueNotPassed = value; }
        }

        private bool _IsOmitEmptyValue;
        /// <summary>
        /// Indicates whethere parameter value will be excluded from data update operation when <see cref="IsValueNotPassed"/> is true;
        /// </summary>
        public bool IsOmitEmptyValue
        {
            get
            {
                return _IsOmitEmptyValue;
            }
            set
            {
                _IsOmitEmptyValue = value;
            }
        }

        private StateBag _ViewState;
        /// <summary>
        /// Gets a dictionary of state information that allows you to save and restore the view state of a parameter across multiple requests for the same page.
        /// </summary>
        protected StateBag ViewState
        {
            get
            {
                if (_ViewState == null)
                {
                    _ViewState = new StateBag(false);
                    if (_isTrackingViewState)
                    {
                        ((IStateManager)_ViewState).TrackViewState();
                    }
                }
                return _ViewState;
            }
        }

        internal void SetDirty(bool p)
        {
            ViewState.SetDirty(true);
        }

        /// <summary>
        /// Serves as a hash function. GetHashCode is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>A hash code for the current Parameter.</returns>
        public override int GetHashCode()
        {
            if (Value != null)
                return Value.GetHashCode();
            else
                return int.MinValue;
        }
    }
}