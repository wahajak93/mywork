//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a collection of <see cref="DataParameter"/> that are used by data source controls in advanced 
    /// data-binding scenarios.
    /// </summary>
    public class DataParameterCollection : InMotion.Web.Controls.ParameterCollection
    {
        /// <summary>
        /// Creates a DataParameter object with the specified name, 
        /// data type, source type, source and default value, 
        /// and appends it to the end of the collection.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="type">The <see cref="InMotion.Common.DataType"/> of the parameter.</param>
        /// <param name="sourceType">The <see cref="DataParameterSourceType"/> of the parameter.</param>
        /// <param name="source">The source of the parameter.</param>
        /// <param name="defaultValue">A object that serves as a default value for the parameter.</param>
        /// <returns>The index value of the added item.</returns>
        public int Add(string name, InMotion.Common.DataType type, DataParameterSourceType sourceType, string source, object defaultValue)
        {
            DataParameter p = new DataParameter();
            p.Name = name;
            p.DataType = type;
            p.Source = source;
            p.SourceType = sourceType;
            p.DefaultValue = defaultValue;
            return ((IList)this).Add(p);
        }

        private static readonly Type[] _typeOfParameters =
            new Type[] { typeof(SqlParameter) , typeof(WhereParameter), typeof(SPParameter) };

        /// <summary>
        /// Gets an array of Parameter types that the ParameterCollection collection can contain.
        /// </summary>
        /// <returns>An array of Parameter types that the ParameterCollection collection can contain.</returns>
        protected override Type[] GetKnownTypes()
        {
            return _typeOfParameters;
        }

        /// <summary>
        /// Creates an instance of a default Parameter object.
        /// </summary>
        /// <param name="index">The index of the type of Parameter to create from the ordered list of types returned by GetKnownTypes.</param>
        /// <returns>A default instance of a Parameter.</returns>
        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new SqlParameter();
                case 1:
                    return new WhereParameter();
                case 2:
                    return new SPParameter();
                default:
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        Trace.TraceError("Unknown Type.\n{0}", Environment.StackTrace);
                    throw new ArgumentOutOfRangeException("Unknown Type");
            }
        }
    }
}