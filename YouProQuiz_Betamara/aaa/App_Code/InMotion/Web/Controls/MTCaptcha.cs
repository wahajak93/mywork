using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.ComponentModel;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Security;

namespace InMotion.Web.Controls
{
    public class MTCaptcha : DataBoundControl, IMTControl, IErrorProducer, IMTAttributeAccessor
    {

        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTTextBox"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Occurs after the control performed the value validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;
        #endregion

        private Collection<MTParameter> _paremeters = null;
        /// <summary>
        /// Gets collection of flashchart rendering parameters.
        /// </summary>
        [
        DefaultValue(null),
        Editor("System.Drawing.Design.UITypeEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor)),
        PersistenceMode(PersistenceMode.InnerProperty),
        MergableProperty(false)
        ]
        public Collection<MTParameter> Parameters
        {
            get
            {
                if (_paremeters == null)
                    _paremeters = new Collection<MTParameter>();
                return _paremeters;
            }
        }

        string _caption;
        /// <summary>
        /// Gets or sets the caption of the control that will be used in validation messages.
        /// </summary>
        public string Caption
        {
            get
            {
                if (String.IsNullOrEmpty(_caption)) _caption = ID;
                return _caption;
            }
            set { _caption = value; }
        }

        private string __ErrorControl;
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                if (__ErrorControl == null)
                    __ErrorControl = "";
                return __ErrorControl;
            }
            set { __ErrorControl = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private string _text;

        public string Text
        {
            get 
            {
                return _text; 
            }
            set 
            {
               _text = value;
            }
        }

        private int _imageWidth;

        public int ImageWidth
        {
            get { return _imageWidth; }
            set { _imageWidth = value; }
        }
        private int _imageHeight;

        public int ImageHeight
        {
            get { return _imageHeight; }
            set { _imageHeight = value; }
        }
        private int _length;

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }
        private int _rotation;

        public int Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }
        private int _broke;

        public int Broke
        {
            get { return _broke; }
            set { _broke = value; }
        }
        private double _vAmplitude;

        public double VAmplitude
        {
            get { return _vAmplitude; }
            set { _vAmplitude = value; }
        }
        private double _hAmplitude;

        public double HAmplitude
        {
            get { return _hAmplitude; }
            set { _hAmplitude = value; }
        }
        private int _noise;

        public int Noise
        {
            get { return _noise; }
            set { _noise = value; }
        }

        private string _servicePath;

        public string ServicePath
        {
            get { return _servicePath; }
            set { _servicePath = value; }
        }
        
        private bool _caseSensitive;
        public bool CaseSensitive
        {
            get { return _caseSensitive; }
            set { _caseSensitive = value; }
        }

        private string _bColor;

        public string BColor
        {
            get { return _bColor; }
            set { _bColor = value; }
        }

        private string _fColor;

        public string FColor
        {
            get { return _fColor; }
            set { _fColor = value; }
        }
        
        private string _callbackControlName;

        public string CallbackControlName
        {
            get { return _callbackControlName; }
            set { _callbackControlName = value; }
        }
        /// <summary>
        /// Raises the <see cref="Validating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (Validating != null)
                Validating(this, e);
        }

        public bool Validate()
        {
            /*OnValidating(new ValidatingEventArgs());
            if (HttpContext.Current.Request.Form[UniqueID] == "")
            {
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_RequiredField"), ControlsHelper.ReplaceResource(Caption)));
                HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID] = "";
                return false;
            }

            if (Convert.ToString(HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID]) == "" 
                || string.Compare(HttpContext.Current.Request.Form[UniqueID], Convert.ToString(HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID]), !_caseSensitive) != 0)
            {
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_Captcha_ControlValidation"), ControlsHelper.ReplaceResource(Caption)));
                HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID] = "";
                return false;
            }
            HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID] = "";*/
            return true;
            
        }

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the Collection&lt;string&gt; which is used to collect error messages generated during the controls execution.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new ControlErrorCollection();
                    _errors.ErrorAdded += new EventHandler<EventArgs>(OnErrorAdded);
                    _errors.ErrorRemoved += new EventHandler<EventArgs>(OnErrorRemoved);
                }
                return _errors;
            }
        }

        private void OnErrorAdded(object sender, EventArgs e)
        {
            if (OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RegisterError(this, null);
        }

        private void OnErrorRemoved(object sender, EventArgs e)
        {
            if (Errors.Count == 0 && OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RemoveError(this);
        }

        private void OwnerForm_Validating(object sender, ValidatingEventArgs e)
        {
            //e.HasErrors = !Validate();
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }
        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (!DesignMode && Page.Request["callbackControl"] == _callbackControlName)
            {
                GenerateCaptchaCode();
            }
            OnBeforeShow(EventArgs.Empty);
            
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            //Page.Response.AddHeader("X-UA-Compatible", "IE=7");
            if (AppConfig.HTMLTemplateType != "HTML")
            {
                writer.AddAttribute(HtmlTextWriterAttribute.For, ClientID);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");

                writer.RenderBeginTag(HtmlTextWriterTag.Label);
                writer.Write(Caption);
                writer.RenderEndTag();
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Width, _imageWidth.ToString());
            writer.AddAttribute(HtmlTextWriterAttribute.Height, _imageHeight.ToString());
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _callbackControlName + "Picture");
            writer.RenderBeginTag("canvas");
            writer.RenderEndTag();
        }

        protected void GenerateCaptchaCode()
        {
            string[] letters = System.IO.File.ReadAllLines(System.IO.Path.Combine(System.IO.Path.Combine(Page.Request.PhysicalApplicationPath, AppRelativeTemplateSourceDirectory.Substring(2).Replace("/", "\\")), ServicePath));
            string restricted = "|cp|cb|ck|c6|c9|rn|rm|mm|co|do|cl|db|qp|qb|dp|";
            QuadraticPaths res = new QuadraticPaths();
            QuadraticPaths t;
            string code = " ";
            int i;
            int r;
            Random rand = new Random();
            for (i = 0; i < Length; i++)
            {
                r = rand.Next(0,letters.Length);
                while (restricted.IndexOf("|" + code.Substring(code.Length-1) + letters[r][0] + "|") != -1)
                    r = rand.Next(0, letters.Length);
                code = code + letters[r][0];
                t = new QuadraticPaths();
                t.LoadFromString(letters[r]);
                t.Wave(2 * _hAmplitude * rand.NextDouble() - _hAmplitude);
                t.Rotate(rand.Next(-_rotation, _rotation));
                t.Normalize(0, 100);
                if (t.MaxX - t.MinX > 100)
                    t.Normalize(100, 100);
                t.Addition((i) * t.MaxY, 0);
                res.AddPaths(t);
            }
            res.Rotate(90);
            res.Wave(2 * _vAmplitude * rand.NextDouble() - _vAmplitude);
            res.Rotate(-90);
            res.Broke(_broke, _broke);
            res.Normalize(_imageWidth - 12, _imageHeight - 12);
            res.Addition(6, 6);
            res.Noises(_noise);
            res.Mix();
            if (OwnerForm == null)
                HttpContext.Current.Session["Captcha" + ID] = code.Substring(1);
            else
                HttpContext.Current.Session["Captcha" + OwnerForm.ID + ID] = code.Substring(1);
            Page.Response.Clear();
            Page.Response.Write(res.ToString());
            Page.Response.End();
        }
    }
}