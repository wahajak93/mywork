//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.Design;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides a base control designer class for extending the design-mode behavior of a InMotion Web server control.
    /// </summary>
    class MTFormDesigner : ControlDesigner
    {
        protected TemplateGroupCollection __TemplateGroups = null;

        /// <summary>
        /// Initializes the <see cref="MTFormDesigner"/> designer and loads the specified component. 
        /// </summary>
        /// <param name="component">The InMotion Web server control being designed.</param>
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        /// <summary>
        /// Gets a value indicating whether the InMotion Web Server can be resized in the design-time environment.
        /// </summary>
        public override bool AllowResize
        {
            get
            {
                if (this.InTemplateMode)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Occurs before the control binding.
        /// </summary>
        public event EventHandler<EventArgs> BeforeDataBind;

        /// <summary>
        /// Raises the <see cref="BeforeDataBind"/> event.
        /// </summary>
        /// <param name="e">An <see cref="BeforeDataBind"/> object that contains the event data.</param>
        protected virtual void OnBeforeDataBind(EventArgs e)
        {
            if (BeforeDataBind != null)
                BeforeDataBind(this, e);
        }

        /// <summary>
        /// Retrieves the HTML markup that is used to represent the InMotion Web Server at design time.
        /// </summary>
        /// <returns>The HTML markup used to represent the InMotion Web Server at design time.</returns>
        public override string GetDesignTimeHtml()
        {
            string[][] TemplateContent = new string[TemplateGroups.Count][];
            Regex res = new Regex("<%\\s*=\\s*ResManager.GetString\\s*\\(\\s*\"([^\"]*)\"\\s*\\)\\s*%>");
            Regex dynamicStyle = new Regex("<img[\\w\\W]*?\\s*src=\"[\\./]*?Styles/<%=StyleName%>/Images/Spacer.gif\"[\\w\\W]*?/>");
            bool isDynamicStyle = false;
            for (int i = 0; i < TemplateGroups.Count; i++)
            {
                TemplateContent[i] = new string[TemplateGroups[i].Templates.Length];
                for (int j = 0; j < TemplateGroups[i].Templates.Length; j++)
                {
                    TemplateContent[i][j] = TemplateGroups[i].Templates[j].Content;
                    MatchCollection MC = res.Matches(TemplateGroups[i].Templates[j].Content);
                    for (int k = 0; k < MC.Count; k++)
                        TemplateGroups[i].Templates[j].Content = TemplateGroups[i].Templates[j].Content.Replace(MC[k].Groups[0].Value, "{res:" + MC[k].Groups[1].Value + "}");
                    isDynamicStyle = isDynamicStyle || dynamicStyle.IsMatch(TemplateGroups[i].Templates[j].Content);
                }
            }

            OnBeforeDataBind(new EventArgs());
            ((IForm)Component).DataBind();
            ControlCollection Controls = ((Control)Component).Controls;
            for (int i = 0; i < Controls.Count; i++)
            {
                for (int j = 0; j < Controls[i].Controls.Count; j++)
                    if (!(Controls[i].Controls[j] is DataSourceControl) && !Controls[i].Controls[j].Visible)
                        Controls[i].Controls[j].Visible = true;
            }

            System.IO.StringWriter sw = new System.IO.StringWriter();
            if (isDynamicStyle)
                RenderDefaultStyle(sw);

            HtmlTextWriter htw = new HtmlTextWriter(sw);
            ((Control)Component).RenderControl(htw);
            for (int i = 0; i < TemplateGroups.Count; i++)
            {
                for (int j = 0; j < TemplateGroups[i].Templates.Length; j++)
                    TemplateGroups[i].Templates[j].Content = TemplateContent[i][j];
            }
            return sw.ToString();
        }

        /// <summary>
        /// Outputs styles for control representation at design time.
        /// </summary>
        /// <param name="writer">The <see cref="StringWriter"/> object that receives the styles content.</param>
        public void RenderDefaultStyle(StringWriter writer)
        {
            writer.WriteLine("<style>");
            // Common
            writer.WriteLine("ul, ol, blockquote { margin-top: 5px; margin-bottom: 10px; margin-left: 20px; }");
            writer.WriteLine("h1 { color: #787878; font-size: 150%; margin-top: 5px; margin-bottom: 5px; }");
            writer.WriteLine("h2 { color: #787878; font-size: 130%; margin-top: 15px; margin-bottom: 5px; }");
            writer.WriteLine("h3 { color: #787878; font-size: 110%; font-style: italic; margin-top: 15px; margin-bottom: 5px; }");
            writer.WriteLine("h4 { color: #787878; font-size: 100%; margin-top: 15px; margin-bottom: 5px; }");
            writer.WriteLine("h5 { color: #787878; font-size: 100%; font-style: italic; margin-top: 15px; margin-bottom: 5px; }");
            writer.WriteLine("h6 { color: #787878; font-size: 90%; margin-top: 15px; margin-bottom: 5px; }");
            writer.WriteLine(".Header { width: 100%; }");
            writer.WriteLine(".Header .th, .Header th { font-size: 110%; font-weight: bold; text-align: left; padding: 3px; background-color: #f7f7f7; color: #000000; width: 100%; white-space: nowrap; padding-left: 8px; border-top: 1px solid #787878; }");
            writer.WriteLine(".HeaderLeft { border-left: 1px solid #787878; }");
            writer.WriteLine(".HeaderRight { border-right: 1px solid #787878; }");
            // Grid
            writer.WriteLine(".Grid { border-left: 1px solid #787878; border-bottom: 1px solid #787878; width: 100%; }");
            writer.WriteLine(".Caption th, .Caption td, .Caption .th { font-size: 80%; text-align: left; vertical-align: top; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #f7f7f7; color: #000000; white-space: nowrap; }");
            writer.WriteLine(".Caption a:link, .Caption a:visited { color: #000000; }");
            writer.WriteLine(".Caption img, .Caption input, .Footer img, .Footer input { margin: 2px; vertical-align: middle; }");
            writer.WriteLine(" { vertical-align: middle; }");
            writer.WriteLine(".Row th, .Row td, .Row .th { font-size: 80%; font-weight: normal; text-align: left; vertical-align: top; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #f7f7f7; color: #000000; white-space: nowrap; color: #000000; }");
            writer.WriteLine("caption.Row { font-size: 80%; padding: 3px; border: 1px solid #787878; border-bottom: 0px; text-align: left; vertical-align: top; background-color: #f7f7f7; color: #000000; }");
            writer.WriteLine(".Row a:link, .AltRow a:link, .Footer a:link, .Footer a:visited { color: #0033cc; }");
            writer.WriteLine(".Row a:visited, .AltRow a:visited { color: #830083; }");
            writer.WriteLine(".Separator{ color: #787878; }");
            writer.WriteLine(".Separator td{ height: 1px; }");
            writer.WriteLine(".AltRow th, .AltRow td, .AltRow .th { font-size: 80%; font-weight: normal; text-align: left; vertical-align: top; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #ededed; color: #000000; white-space: nowrap; color: #000000; }");
            writer.WriteLine(".NoRecords td { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; background-color: #f7f7f7; color: #000000; vertical-align: top; }");
            writer.WriteLine(".Footer td { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #f7f7f7; color: #000000; text-align: center; vertical-align: middle; white-space: nowrap; color: #000000; }");
            // Record
            writer.WriteLine(".Record { border-left: 1px solid #787878; border-bottom: 1px solid #787878; width: 100%; }");
            writer.WriteLine(".Error td { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; color: #FF0000; vertical-align: top; background-color: #f7f7f7; }");
            writer.WriteLine(".Controls th, .Controls .th { font-size: 80%; font-weight: normal; text-align: left; vertical-align: top; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #f7f7f7; color: #000000; white-space: nowrap; color: #000000; }");
            writer.WriteLine(".Controls td { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; vertical-align: top; background-color: #f7f7f7; color: #000000; }");
            writer.WriteLine(".Bottom td { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #f7f7f7; color: #000000; text-align: right; vertical-align: middle; white-space: nowrap; color: #000000; }");
            writer.WriteLine(".Bottom a:link { color: #0033cc; }");
            writer.WriteLine(".Bottom a:visited { color: #830083; }");
            writer.WriteLine(".Bottom img { margin: 2px; vertical-align: middle; }");
            writer.WriteLine(".Bottom input { margin: 2px; vertical-align: middle; }");
            // Report
            writer.WriteLine(".GroupCaption th, .GroupCaption .th{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; white-space: nowrap; text-align: left; vertical-align: top; font-weight: bold; background-color: #ededed; color: #000000; }");
            writer.WriteLine(".GroupCaption td{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; vertical-align: top; font-weight: bold; background-color: #f7f7f7; color: #000000; }");
            writer.WriteLine(".GroupCaption a:link, .GroupCaption a:visited { color: #000000; }");
            writer.WriteLine(".GroupFooter td{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; vertical-align: middle; white-space: nowrap; background-color: #d7d7d7; color: #000000; }");
            writer.WriteLine(".GroupFooter a:link, .SubTotal a:link, .Total a:link { color: #0033cc; }");
            writer.WriteLine(".GroupFooter a:visited, .SubTotal a:visited, .Total a:visited { color: #830083; }");
            writer.WriteLine(".GroupFooter img, .GroupFooter input { margin: 2px; vertical-align: middle; }");
            writer.WriteLine(".SubTotal td{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; vertical-align: top; font-weight: bold; background-color: #ededed; color: #000000; border-bottom: 2px solid #787878; }");
            writer.WriteLine(".Total td{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: left; vertical-align: top; white-space: nowrap; font-weight: bold; background-color: #ededed; color: #000000; }");
            // Calendar
            writer.WriteLine(".Calendar { border: 1px solid #787878; width: 100%; }");
            writer.WriteLine(".CalendarWeekdayName, .CalendarWeekendName { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; vertical-align: top; white-space: nowrap; background-color: #ededed; color: #000000; font-weight: normal; }");
            writer.WriteLine(".CalendarWeekdayName a:link, .CalendarDay a:link, .CalendarWeekend a:link, .CalendarToday a:link, .CalendarWeekendToday a:link, .CalendarNavigator a:link, .CalendarEvent a:link { color: #0033cc; }");
            writer.WriteLine(".CalendarWeekdayName a:visited, .CalendarDay a:visited, .CalendarWeekend a:visited, .CalendarToday a:visited, .CalendarWeekendToday a:visited, .CalendarNavigator a:visited, .CalendarEvent a:visited { color: #830083; }");
            writer.WriteLine(".CalendarWeekendName a:link, .CalendarWeekendName a:visited, .CalendarSelectedDay a:link, .CalendarSelectedDay a:visited { color: #000000; }");
            writer.WriteLine(".CalendarDay { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; background-color: #f7f7f7; color: #000000; }");
            writer.WriteLine(".CalendarWeekend { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; background-color: #ededed; color: #000000; }");
            writer.WriteLine(".CalendarToday { font-size: 80%; padding: 1px; border-top: 3px solid #787878; border-right: 3px solid #787878; border-left: 2px solid #787878; border-bottom: 2px solid #787878; text-align: center; background-color: #f7f7f7; color: #000000; }");
            writer.WriteLine(".CalendarWeekendToday { font-size: 80%; padding: 1px; border-top: 3px solid #787878; border-right: 3px solid #787878; border-left: 2px solid #787878; border-bottom: 2px solid #787878; text-align: center; background-color: #ededed; color: #000000; }");
            writer.WriteLine(".CalendarOtherMonthDay { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; background-color: #f7f7f7; color: #787878; }");
            writer.WriteLine(".CalendarOtherMonthDay a:link, .CalendarOtherMonthToday a:link, .CalendarOtherMonthWeekend a:link, .CalendarOtherMonthWeekendToday a:link { color: #787878; }");
            writer.WriteLine(".CalendarOtherMonthDay a:visited, .CalendarOtherMonthToday a:visited, .CalendarOtherMonthWeekend a:visited, .CalendarOtherMonthWeekendToday a:visited { color: #787878; }");
            writer.WriteLine(".CalendarOtherMonthToday { font-size: 80%; padding: 1px; border-top: 3px solid #787878; border-right: 3px solid #787878; border-left: 2px solid #787878; border-bottom: 2px solid #787878; text-align: center; background-color: #f7f7f7; color: #787878; }");
            writer.WriteLine(".CalendarOtherMonthWeekend { font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; text-align: center; background-color: #ededed; color: #787878; }");
            writer.WriteLine(".CalendarOtherMonthWeekendToday { font-size: 80%; padding: 1px; border-top: 3px solid #787878; border-right: 3px solid #787878; border-left: 2px solid #787878; border-bottom: 2px solid #787878; text-align: center; color: #787878; background-color: #ededed; }");
            writer.WriteLine(".CalendarSelectedDay{ font-size: 80%; padding: 3px; border-top: 1px solid #787878; border-right: 1px solid #787878; background-color: #d7d7d7; color: #000000; text-align: center; font-weight: bold; }");
            writer.WriteLine(".CalendarNavigator { font-size: 80%; padding: 3px; background-color: #f7f7f7; color: #000000; text-align: center; vertical-align: middle; white-space: nowrap; color: #000000; }");
            writer.WriteLine(".CalendarNavigator img { margin: 2px; vertical-align: middle; }");
            writer.WriteLine(".CalendarNavigator td, .CalendarNavigator th { font-size: 80%; }");
            writer.WriteLine(".CalendarEvent { font-size: 80%; text-align: left; }");
            // Menu
            writer.WriteLine(".menu_htb .right2 { display: none; }");
            writer.WriteLine(".menu_htb .content { background-color: #787878; height: auto; width: auto; }");
            writer.WriteLine(".menu_htb a:hover .content, .menu_htb li:hover>a .content { background-color: #545454; }");
            writer.WriteLine(".menu_htb .text { position: relative; color: #ffffff; margin-left: 1em; margin-right: 1em; padding-top: .2em; padding-bottom: .2em; width: auto; height: auto; display: inline; }");
            writer.WriteLine(".menu_htb a:hover .text, .menu_htb li:hover>a .text { color: #ffffff; }");
            writer.WriteLine(".menu_htb a:hover .text, .menu_htb .adxmhoverA .text { color: #ffffff; }");
            writer.WriteLine(".menu_htb a { border: 0; }");
            writer.WriteLine(".menu_htb a:hover, .menu_htb li:hover>a { border: 0; }");
            writer.WriteLine(".menu_htb, .menu_htb ul li { background: transparent; }");
            writer.WriteLine(".menu_htb li { border: 0; }");
            writer.WriteLine(".menu_vlr .right2 { display: none; }");
            writer.WriteLine(".menu_vlr .content { width: 100%; height: auto; background-color: #787878; }");
            writer.WriteLine(".menu_vlr a:hover .content, .menu_vlr li:hover>a .content { background-color: #545454; }");
            writer.WriteLine(".menu_vlr .text { position: relative; height: auto; width: 100%; text-align: center; color: #ffffff; padding-top: .2em; padding-bottom: .2em; }");
            writer.WriteLine(".menu_vlr a:hover .text, .menu_vlr li:hover>a .text { color: #ffffff; }");
            writer.WriteLine(".menu_vlr a { border: 0; }");
            writer.WriteLine(".menu_vlr a:hover, .menu_vlr li:hover>a { border: 0; }");
            writer.WriteLine(".menu_vlr, .menu_vlr ul li { background: transparent; }");
            writer.WriteLine(".menu_vlr li { border: 0; }");
            writer.WriteLine(".menu_vlr_tree .menu_vlr_tree_openedA .text { font-weight: bold; }");
            writer.WriteLine(".menu_vlr_tree .menu_vlr_tree_openedUL { display: block; visibility: visible; }");
            writer.WriteLine(".menu_vlr_tree .menu_vlr_tree_closedUL { display: none; visibility: hidden; }");
            writer.WriteLine(".menu_vlr_tree .right2 { display: none; }");
            writer.WriteLine(".menu_vlr_tree .content { width: 100%; height: auto; background-color: #545454; }");
            writer.WriteLine(".menu_vlr_tree ul .content { background-color: #787878; }");
            writer.WriteLine(".menu_vlr_tree a:hover .content, .menu_vlr_tree li:hover>a .content { background-color: #545454; }");
            writer.WriteLine(".menu_vlr_tree a:hover .content, .menu_vlr_tree .adxmhoverA .content { background-color: #545454; }");
            writer.WriteLine(".menu_vlr_tree ul a:hover .content, .menu_vlr_tree ul li:hover>a .content { }");
            writer.WriteLine(".menu_vlr_tree .menu_vlr_tree_openedA .content { background-color: #545454; }");
            writer.WriteLine(".menu_vlr_tree .text { position: relative; height: auto; text-align: center; color: #ffffff; width: 100%; padding-top: .2em; padding-bottom: .2em; }");
            writer.WriteLine(".menu_vlr_tree ul .text { width: auto; text-align: left; margin-left: .3em; margin-right: .3em; display: inline; }");
            writer.WriteLine(".menu_vlr_tree a:hover .text, .menu_vlr_tree li:hover>a .text { color: #ffffff; }");
            writer.WriteLine(".menu_vlr_tree .menu_vlr_tree_openedA .text { color: #ffffff; }");
            writer.WriteLine(".menu_vlr_tree a, .menu_vlr_tree ul a, .menu_vlr_tree a:hover, .menu_vlr_tree li:hover>a, .menu_vlr_tree li { border: 0; }");
            writer.WriteLine(".menu_vlr_tree, .menu_vlr_tree ul li { background: transparent; }");

            writer.WriteLine("</style>");
        }
    }
}