//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a reporrt section, such as Header or Detail.
    /// </summary>
    [ParseChildren(true)]
    public class ReportSection : Control, INamingContainer
    {
        private int itemIndex;
        private string _name = "";
        private float _height = 1;
        private ITemplate _template;

        /// <summary>
        /// Occurs when the report perform the total controls calculation for the current section.
        /// </summary>
        public event EventHandler<OnCalculateEventArgs> Calculate;
        /// <summary>
        /// Occurs after the current <see cref="ReportSection"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<BeforeShowSectionEventArgs> BeforeShow;
        /// <summary>
        /// Initializes the new instance of a <see cref="ReportSection"/>
        /// </summary>
        /// <param name="itemIndex">The index of the section.</param>
        public ReportSection(int itemIndex)
        {
            this.itemIndex = itemIndex;
        }

        /// <summary>
        /// Initializes the new instance of a <see cref="ReportSection"/>
        /// </summary>
        public ReportSection()
        {
            
        }

        internal void RaiseCalculate(OnCalculateEventArgs e)
        {
            if (Calculate != null)
                Calculate(this, e);
        }

        internal void RaiseBeforeShow(BeforeShowSectionEventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets or sets the name of the section.
        /// </summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the section in abstract "line" units.
        /// </summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public float Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the Section are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(ReportSectionTemplate))
        ]
        public ITemplate Template
        {
            get
            {
                return _template;
            }
            set
            {
                _template = value;
            }
        }

    }

}
