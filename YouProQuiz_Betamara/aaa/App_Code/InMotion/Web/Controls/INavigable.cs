//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to interact with <see cref="MTNavigator"/> control.
    /// </summary>
    public interface INavigable
    {
        /// <summary>
        /// Gets or sets the number of a current page.
        /// </summary>
        int CurrentPage { get; set; }
        /// <summary>
        /// Gets the total nuber of pages.
        /// </summary>
        int TotalPages { get;}
    }
}