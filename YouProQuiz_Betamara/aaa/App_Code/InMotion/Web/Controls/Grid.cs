//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.ComponentModel;
using System.Text.RegularExpressions;
using InMotion.Security;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// A grid form is used to display multiple rows of data within an HTML table.
    /// </summary>
    [Designer(typeof(GridDesigner))]
    public class Grid : Repeater, IForm, ISortable, INavigable, IMTAttributeAccessor, IClientScriptHelper
    {
        /// <summary>
        /// Occurs before the Grid send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;
        /// <summary>
        /// Occurs after the Grid control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        /// <summary>
        /// Occurs before showing (populating template with data) each grid's row. 
        /// </summary>
        public event EventHandler<EventArgs> BeforeShowRow;

        #region IMTAttributeAccessor Members
        /// <summary>
        /// Set value of attribute by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <param name="value">Value of attribute.</param>
        public void SetAttribute(String name, String value)
        {
            Attributes[name] = value;
        }

        /// <summary>
        /// Retrieve attribute value by the name.
        /// </summary>
        /// <param name="name">Name of attribute.</param>
        /// <returns>Value of attribute by the name.</returns>
        public String GetAttribute(String name)
        {
            return (String)Attributes[name];
        }

        System.Web.UI.AttributeCollection _attributes = null;
        StateBag _attributeState;
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        public System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    if (_attributeState == null)
                        _attributeState = new StateBag(true);
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                    _attributes = new System.Web.UI.AttributeCollection(_attributeState);
                }
                return _attributes;
            }
        }
        #endregion

        /// <summary>
        /// Saves any state that was modified after the TrackViewState method was invoked.
        /// </summary>
        /// <returns>An object that contains the current view state of the control; otherwise, if there is no view state associated with the control, a null reference (Nothing in Visual Basic).</returns>
        protected override object SaveViewState()
        {
            object view_state;
            object attr_view_state = null;

            view_state = base.SaveViewState();
            if (_attributeState != null)
                attr_view_state = ((IStateManager)_attributeState).SaveViewState();

            if (view_state == null && attr_view_state == null)
                return null;
            return new Pair(view_state, attr_view_state);
        }

        /// <summary>
        /// Restores view-state information from a previous request that was saved with the <see cref="SaveViewState"/> method.
        /// </summary>
        /// <param name="savedState">An object that represents the control state to restore.</param>
        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }

            Pair pair = (Pair)savedState;
            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                if (_attributeState == null)
                {
                    _attributeState = new StateBag();
                    if (IsTrackingViewState)
                        ((IStateManager)_attributeState).TrackViewState();
                }
                ((IStateManager)_attributeState).LoadViewState(pair.Second);
                _attributes = new System.Web.UI.AttributeCollection(_attributeState);
            }
        }

        /// <summary>
        /// The current processed <see cref="RepeaterItem"/>.
        /// </summary>
        protected internal RepeaterItem __currentItem;
        /// <summary>
        /// Gets the current processed <see cref="RepeaterItem"/>.
        /// </summary>
        public RepeaterItem CurrentItem
        {
            get
            {
                return __currentItem;
            }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        /// <summary>
        /// Gets or sets the numbers of records to be displayed per page by default.
        /// </summary>
        public int RecordsPerPage
        {
            get
            {
                int result = SelectArguments.MaximumRows;
                if (result <= 0 && PageSizeLimit > 0)
                    result = PageSizeLimit;
                return result;
            }
            set
            {
                if (value > PageSizeLimit && PageSizeLimit > 0) value = PageSizeLimit;
                SelectArguments.MaximumRows = value;
                ViewState["RecordsPerPage"] = value;
            }
        }

        int _numberOfColumns = 1;
        /// <summary>
        /// Gets or sets the numbers of columns to be displayed.
        /// </summary>
        public int NumberOfColumns
        {
            get
            {
                if (_numberOfColumns < 1)
                    _numberOfColumns = 1;
                return _numberOfColumns;
            }
            set
            {
                _numberOfColumns = value;
            }
        }

        /// <summary>
        /// Gets or sets the numbers of rows to be displayed per page by default.
        /// </summary>
        public int RowsPerPage
        {
            get
            {
                return (RecordsPerPage + NumberOfColumns - 1) / NumberOfColumns;
            }
        }

        private bool _allowRead = true;
        /// <summary>
        /// Gets or sets the value that indicating whether current user have Read permission.
        /// </summary>
        public bool AllowRead
        {
            get { return _allowRead; }
            set { _allowRead = value; }
        }

        private int _pageSizeLimit;
        /// <summary>
        /// Gets or sets the maximum number of records a user may opt to display per page if using a search form that allows control over the number of records displayed.
        /// </summary>
        public int PageSizeLimit
        {
            get { return _pageSizeLimit; }
            set { _pageSizeLimit = value; }
        }

        /// <summary>
        /// Gets or sets the number of current page.
        /// </summary>
        public int CurrentPage
        {
            get
            {
                if (ViewState["__cp"] != null)
                    return (int)ViewState["__cp"];
                else
                    return 1;
            }
            set
            {
                if (value < 1) value = 1;
                ViewState["__cp"] = value;
            }
        }

        /// <summary>
        /// Gets the total number of pages.
        /// </summary>
        public int TotalPages
        {
            get
            {
                if (ViewState["tp"] == null) return 1;
                return (int)ViewState["tp"];
            }
        }

        /// <summary>
        /// Gets or sets the total number of records.
        /// </summary>
        public int TotalRecords
        {
            get
            {
                if (ViewState["tr"] == null) return SelectArguments.TotalRowCount;
                return (int)ViewState["tr"];
            }
        }


        private int __rowCount;
        /// <summary>
        /// Gets or sets the number of currently proccesed records.
        /// </summary>
        public int RowCount
        {
            get
            {
                return __rowCount;
            }
        }

        /// <summary>
        /// Gets value indicates whethere grid datasource is empty, 
        /// </summary>
        public bool IsEmpty
        {
            get { return TotalRecords == 0; }
        }

        private string _noRecordsControl = "NoRecords";
        /// <summary>
        /// Gets or sets the ID of control that used for displaing "no records" message.
        /// </summary>
        public string NoRecordsControl
        {
            get { return _noRecordsControl; }
            set { _noRecordsControl = value; }
        }

        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return ViewState["__se"] as string;
            }
            set
            {
                ViewState["__se"] = value;
                SelectArguments.SortExpression = value;
            }
        }

        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowRow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowRow(EventArgs e)
        {
            if (BeforeShowRow != null)
                BeforeShowRow(this, e);
        }

        /// <summary>
        /// Populates the Controls collection based on data source or view state.
        /// </summary>
        /// <param name="useDataSource">Indicates whether to use the specified data source.</param>
        protected override void CreateControlHierarchy(bool useDataSource)
        {
            base.CreateControlHierarchy(useDataSource);
            if (useDataSource)
            {
                DataSourceView dv = GetDataSourceView();
                if (dv is MTDataSourceView) ((MTDataSourceView)dv).Owner = this;
            }

            ShowNoRecords();
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// Returns an <see cref="System.Collections.IEnumerable"/> interface from the data source.
        /// </summary>
        /// <returns>An IEnumerable that represents the data from the data source.</returns>
        protected override System.Collections.IEnumerable GetData()
        {
            System.Collections.IEnumerable data = null;
            if (!DesignMode)
            {
                OnBeforeSelect(EventArgs.Empty);
                if (!AllowRead) return null;
                if (Restricted && !UserRights.AllowRead)
                    return new ArrayList();

                SelectArguments.StartRowIndex = RecordsPerPage * (CurrentPage - 1);
                SelectArguments.MaximumRows = RecordsPerPage;
                SelectArguments.RetrieveTotalRowCount = true;
                if (DataSourceID.Length > 0)
                {
                    data = base.GetData();
                }

                ViewState["tr"] = SelectArguments.TotalRowCount;
                if (SelectArguments.TotalRowCount < 1 || RecordsPerPage == 0)
                    ViewState["tp"] = 1;
                else
                {
                    int result = 0;
                    if (RecordsPerPage != 0)
                        result = SelectArguments.TotalRowCount / RecordsPerPage;
                    if (result * RecordsPerPage < TotalRecords) result++;
                    ViewState["tp"] = result;
                }
                if (data != null && data.GetEnumerator().MoveNext()) __hasData = true;
            }
            else
            {
                data = new Collection<string>();
                ((Collection<string>)data).Add("");
            }

            if (data == null) data = new ArrayList();

            return data;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;

            Dictionary<string, string> TmpAttrs = new Dictionary<string, string>();
            foreach (string attr in Attributes.Keys)
                TmpAttrs.Add(attr, Attributes[attr]);

            for (int i = 0; i < Controls.Count; i++)
            {
                foreach (string attr in TmpAttrs.Keys)
                    if (attr.StartsWith(Controls[i].UniqueID))
                        Attributes[attr.Substring(Controls[i].UniqueID.Length + 1)] = TmpAttrs[attr];

                Controls[i].RenderControl(writer);
                foreach (string attr in TmpAttrs.Keys)
                    Attributes[attr] = TmpAttrs[attr];
            }
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            if (!DesignMode && !Visible) return;
            if (ViewState["RecordsPerPage"] != null && ViewState["RecordsPerPage"] is int)
                SelectArguments.MaximumRows = (int)ViewState["RecordsPerPage"];
            base.OnDataBinding(e);
        }

        /// <summary>
        /// Raises the ItemCreated event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="RepeaterItemEventArgs"/> that contains event data.</param>
        protected override void OnItemCreated(RepeaterItemEventArgs e)
        {
            _dataItem = new DataItem(e.Item.DataItem);
            __currentItem = e.Item;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                __rowCount++;
            base.OnItemCreated(e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowRow"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="RepeaterItemEventArgs"/> that contains event data.</param>
        protected override void OnItemDataBound(RepeaterItemEventArgs e)
        {
            base.OnItemDataBound(e);
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                if (NumberOfColumns > 1)
                {
                    if (RowCount % NumberOfColumns != 1 && e.Item.FindControl("RowOpenTag") != null)
                        e.Item.FindControl("RowOpenTag").Visible = false;
                    if (RowCount % NumberOfColumns != 0 && e.Item.FindControl("RowCloseTag") != null)
                        e.Item.FindControl("RowCloseTag").Visible = false;
                }
                OnBeforeShowRow(EventArgs.Empty);

                if (NumberOfColumns > 1 && DataItem.Value is System.Data.DataRowView && RowCount == ((System.Data.DataRowView)(DataItem.Value)).DataView.Count)
                {
                    int numNeedRows = (NumberOfColumns - RowCount % NumberOfColumns) % NumberOfColumns;
                    for (int i = 0; i < numNeedRows; i++)
                    {
                        RepeaterItem ri = new RepeaterItem(RowCount + i + 1, ListItemType.Item);
                        ItemTemplate.InstantiateIn(ri);
                        for (int j = ri.Controls.Count - 1; j >= 0; j--)
                            if (!(ri.Controls[j] is LiteralControl) && (i < numNeedRows - 1 || ri.Controls[j].ID != "RowCloseTag"))
                                ri.Controls.RemoveAt(j);
                        Controls.Add(ri);
                    }
                }
                Dictionary<string, string> TmpAttrs = new Dictionary<string, string>();
                foreach (string attr in Attributes.Keys)
                    if (!attr.StartsWith(UniqueID + "$ctl"))
                        TmpAttrs.Add(e.Item.UniqueID + "$" + attr, Attributes[attr]);
                foreach (string attr in TmpAttrs.Keys)
                    Attributes.Add(attr, TmpAttrs[attr]);
            }
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Items.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Items.Count, "", ""));
            }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Manages the visibility of the "No Records" panel.
        /// </summary>
        protected virtual void ShowNoRecords()
        {
            if (TotalRecords <= 0 && Controls.Count > 0)
            {
                Control c = Controls[Controls.Count - 1].FindControl(NoRecordsControl);
                if (c != null) c.Visible = true;
            }
        }

        #region IForm Members

        /// <summary>
        /// The current <see cref="DataItem"/>.
        /// </summary>
        protected internal DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        #endregion

        private static RepeaterItem GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is RepeaterItem))
                c = c.NamingContainer;
            return c as RepeaterItem;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            RepeaterItem ri = GetControlContainer(control);
            string arrayName = "";
            switch (ri.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    arrayName = ID + "Controls_Row" + ri.ItemIndex.ToString();
                    break;
                case ListItemType.Footer:
                    arrayName = ID + "Controls_Footer";
                    break;
                case ListItemType.Header:
                    arrayName = ID + "Controls_Header";
                    break;

                default:
                    break;
            }
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Page.IsPostBack && !DesignMode)
            {
                if (Page.Request.QueryString[ID + "Page"] != null)
                {
                    try
                    {
                        CurrentPage = Int32.Parse(Page.Request.QueryString[ID + "Page"]);
                    }
                    catch (Exception)
                    {
                        if (AppConfig.IsMTErrorHandlerUse("all"))
                            Trace.TraceError("Unable to parse page number value from QueryString parameter {0}Page. Grid {0}\n{1}", ID, Environment.StackTrace);
                    }
                }

                if (Page.Request.QueryString[ID + "PageSize"] != null)
                {
                    try
                    {

                        int val = Int32.Parse(Page.Request.QueryString[ID + "PageSize"]);
                        if (val > 0) RecordsPerPage = val;
                    }
                    catch (Exception)
                    {
                        if (AppConfig.IsMTErrorHandlerUse("all"))
                            Trace.TraceError("Unable to parse page size value from QueryString parameter {0}PageSize. Grid {0}\n{1}", ID, Environment.StackTrace);
                    }
                }

                if (Page.Request.QueryString[ID + "Order"] != null)
                {
                    RepeaterItem ri = new RepeaterItem(0, ListItemType.Header);
                    if (HeaderTemplate != null)
                    {
                        HeaderTemplate.InstantiateIn(ri);
                        Control c = ri.FindControl(Page.Request.QueryString[ID + "Order"]);
                        if (c == null && FooterTemplate != null)
                        {
                            FooterTemplate.InstantiateIn(ri);
                            c = ri.FindControl(Page.Request.QueryString[ID + "Order"]);
                        }
                        if (c != null && c is MTSorter)
                            if (Page.Request.QueryString[ID + "Dir"] != null
                             && String.Compare(Page.Request.QueryString[ID + "Dir"], "desc", StringComparison.OrdinalIgnoreCase) == 0)
                                SortExpression = ((MTSorter)c).ReverseOrder;
                            else
                                SortExpression = ((MTSorter)c).SortOrder;
                    }
                }
            }
        }

        #region IForm Members
        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<RepeaterItem> rows = new List<RepeaterItem>();
            if (Controls[0] != null && ((RepeaterItem)Controls[0]).ItemType == ListItemType.Header)
                rows.Add((RepeaterItem)Controls[0]);
            if (Controls[Controls.Count - 1] != null && ((RepeaterItem)Controls[Controls.Count - 1]).ItemType == ListItemType.Footer)
                rows.Add((RepeaterItem)Controls[Controls.Count - 1]);
            if (__currentItem != null) rows.Add(__currentItem);
            foreach (RepeaterItem ri in rows)
            {
                ControlsHelper.PopulateControlCollection<T>(result, ri.Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            List<RepeaterItem> rows = new List<RepeaterItem>();
            if (Controls[0] != null && ((RepeaterItem)Controls[0]).ItemType == ListItemType.Header)
                rows.Add((RepeaterItem)Controls[0]);
            if (Controls[Controls.Count - 1] != null && ((RepeaterItem)Controls[Controls.Count - 1]).ItemType == ListItemType.Footer)
                rows.Add((RepeaterItem)Controls[Controls.Count - 1]);
            if (__currentItem != null) rows.Add(__currentItem);
            foreach (RepeaterItem ri in rows)
            {
                T ctrl = (T)ri.FindControl(id);
                if (ctrl != null) return ctrl;
            }
            foreach (RepeaterItem ri in rows)
            {
                T ctrl = (T)InMotion.Common.Controls.FindControlIterative(ri, id);
                if (ctrl != null) return ctrl;
            }
            return (T)InMotion.Common.Controls.FindControlIterative(this, id);
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private FormSupportedOperations _userRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (_userRights == null)
                    _userRights = new FormSupportedOperations();
                return _userRights;
            }
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }
        #endregion
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Grid"/> control.
    /// </summary>
    class GridDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Grid ctrl = (Grid)Component;
                    __TemplateGroups = base.TemplateGroups;

                    TemplateGroup tempGroup;
                    TemplateDefinition tempDef;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "FooterTemplate", ctrl, "FooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "HeaderTemplate", ctrl, "HeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Item Templates");
                    tempDef = new TemplateDefinition(this, "AlternatingItemTemplate", ctrl, "AlternatingItemTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "ItemTemplate", ctrl, "ItemTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "SeparatorTemplate", ctrl, "SeparatorTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}