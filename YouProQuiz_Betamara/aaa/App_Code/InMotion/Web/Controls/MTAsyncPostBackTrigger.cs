//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a control and optional event of the control as an asynchronous postback control trigger that causes the <see cref="MTUpdatePanel"/> control to refresh.
    /// </summary>
    public class MTAsyncPostBackTrigger : AsyncPostBackTrigger
    {
        #region Properties
        private string _formID;
        /// <summary>
        /// Gets or sets the name of the control form that is an <see cref="MTAsyncPostBackTrigger"/> control for an <see cref="MTUpdatePanel"/> control.
        /// </summary>
        public string FormID
        {
            get
            {
                return _formID;
            }
            set
            {
                _formID = value;
            }
        }
        #endregion

        #region Methods
        private bool IsServerEvent(Control ctrl)
        {
            if (ctrl != null && System.ComponentModel.TypeDescriptor.GetEvents(ctrl)[EventName] != null)
            {
                ControlID = ctrl.UniqueID;
                return true;
            }
            else if (ctrl is IAttributeAccessor)
            {
                ((IAttributeAccessor)ctrl).SetAttribute("on" + EventName, Owner.Page.ClientScript.GetPostBackClientHyperlink(Owner, ""));
            }
            return false;
        }

        /// <summary>
        /// Initializes the <see cref="MTAsyncPostBackTrigger"/> object.
        /// </summary>
        protected override void Initialize()
        {
            if (string.IsNullOrEmpty(FormID))
            {
                Control ctrl = Owner.FindControl(ControlID);
                if (ctrl == null)
                    ctrl = Owner.Page.FindControl(ControlID);
                if (IsServerEvent(ctrl))
                    base.Initialize();
            }
            else
            {
                Control ctrl = Owner.Page.FindControl(FormID);
                if (ctrl != null)
                {
                    ControlCollection CtrlCollect = ctrl.Controls;
                    for (int i = 0; i < CtrlCollect.Count; i++)
                    {
                        ctrl = CtrlCollect[i].FindControl(ControlID);
                        if (IsServerEvent(ctrl))
                            base.Initialize();
                    }
                }
            }
        }
        #endregion
    }
}
