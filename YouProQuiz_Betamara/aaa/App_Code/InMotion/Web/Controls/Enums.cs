//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the type of URL conversion.
    /// </summary>
    public enum UrlType
    {
        /// <summary>
        /// The none conversion will be performed.
        /// </summary>
        None,
        /// <summary>
        /// The URL will be converted to absolute form.
        /// </summary>
        Absolute,
        /// <summary>
        /// The URL will be converted to SSL form.
        /// </summary>
        SSL
    }

    /// <summary>
    /// Specifies the type of request parameters that will be preserved by the URL.
    /// </summary>
    public enum PreserveParameterType
    {
        /// <summary>
        /// The parameters will not be preserved.
        /// </summary>
        None,
        /// <summary>
        /// The GET parameters of request will be preserved.
        /// </summary>
        Get,
        /// <summary>
        /// The POST parameters of request will be preserved.
        /// </summary>
        Post,
        /// <summary>
        /// The both GET and POST parameters of request will be preserved.
        /// </summary>
        GetAndPost
    }

    /// <summary>
    /// Specifies the type of data operation.
    /// </summary>
    public enum DataOperationType
    {
        /// <summary>
        /// Specify the SELECT data operation.
        /// </summary>
        Select,
        /// <summary>
        /// Specify the INSERT data operation.
        /// </summary>
        Insert,
        /// <summary>
        /// Specify the UPDATE data operation.
        /// </summary>
        Update,
        /// <summary>
        /// Specify the DELETE data operation.
        /// </summary>
        Delete,
        /// <summary>
        /// Specify the COUNT rows operation.
        /// </summary>
        Count,
        /// <summary>
        /// Specify the batch command operation.
        /// </summary>
        Batch
    }

    /// <summary>
    /// Specify the source type for Href property.
    /// </summary>
    public enum HrefType
    {
        /// <summary>
        /// The Href is static URL.
        /// </summary>
        Page,
        /// <summary>
        /// The Href will be initialized from datasource field.
        /// </summary>
        Database,
        /// <summary>
        /// The Href will be initialized from value of datasource parameter.
        /// </summary>
        DatasourceParameter
    }

    /// <summary>
    /// Specify the content type of HTML control
    /// </summary>
    public enum ContentType
    {
        /// <summary>
        /// The result text will be HTML encoded.
        /// </summary>
        Text,
        /// <summary>
        /// The text will be shown as is, without any conversion.
        /// </summary>
        Html
    }

    /// <summary>
    /// Specify the operation mode of the <see cref="MTSorter"/> and <see cref="MTNavigator"/>
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// The control is use postback mechanism 
        /// for notifying of the state changes.
        /// </summary>
        Postback,
        /// <summary>
        /// The control is use Url parameter with 
        /// page redirecting for notifying of the state changes.
        /// </summary>
        Url
    }

    /// <summary>
    /// Specify the format of week day name.
    /// </summary>
    public enum WeekdayFormat
    {
        /// <summary>
        /// The name will be shown in full form, i.e. Friday.
        /// </summary>
        Full,
        /// <summary>
        /// The only first 3 letters of name will be shown, i.e. Fri.
        /// </summary>
        Short,
        /// <summary>
        /// The name will be shown in narrow form, i.e. F.
        /// </summary>
        Narrow
    }

    /// <summary>
    /// Specifies the calendar view mode.
    /// </summary>
    public enum CalendarMode 
    { 
        /// <summary>
        /// The calendar is display full year.
        /// </summary>
        Full, 
        /// <summary>
        /// The calendar is display quarter that contain selected month.
        /// </summary>
        Quarter, 
        /// <summary>
        /// The calendar is display previous, selected and next months.
        /// </summary>
        ThreeMonth, 
        /// <summary>
        /// The calendar is display only selected month.
        /// </summary>
        OneMonth 
    }

    /// <summary>
    /// Specify the total function for calculated value.
    /// </summary>
    public enum TotalFunction 
    { 
        /// <summary>
        /// The no calculation will be performed.
        /// </summary>
        None, 
        /// <summary>
        /// The sum of values will be calculated.
        /// </summary>
        Sum, 
        /// <summary>
        /// The count of values(or rows, depending on Source property of control) will be calculated.
        /// </summary>
        Count, 
        /// <summary>
        /// The minimum value will be calculated.
        /// </summary>
        Min,
        /// <summary>
        /// The maximum value will be calculated.
        /// </summary>
        Max, 
        /// <summary>
        /// The average value will be calculated.
        /// </summary>
        Avg 
    }

    /// <summary>
    /// Specifies the view mode of Report.
    /// </summary>
    public enum ReportViewMode 
    { 
        /// <summary>
        /// The report will be shown in Print (all pages in the same time) mode.
        /// </summary>
        Print, 
        /// <summary>
        /// The report will be shown in Web (only current page) mode.
        /// </summary>
        Web 
    }

    /// <summary>
    /// Specifies the sorting direction.
    /// </summary>
    public enum SortDirection
    {
        /// <summary>
        /// The ascending sorting will be applied.
        /// </summary>
        Asc,
        /// <summary>
        /// The descending sorting will be applied.
        /// </summary>
        Desc
    }

}
