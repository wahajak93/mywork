//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Text.RegularExpressions;
using System.Diagnostics;
using InMotion.Data;
using InMotion.Configuration;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace InMotion.Web.Controls
{
    static internal class ControlsHelper
    {
        static Regex res;

        public enum AttributeFields { Field = 0, Datatype = 1, DataBindField = 2 }
        private static String[] MTAttributes = new String[] { "mtField", "mtDataType", "mtDataBindField" };

        static ControlsHelper()
        {
            res = new Regex("{res:([^}]+)}", RegexOptions.Compiled | RegexOptions.Multiline);
        }

        internal static void PopulateControlCollection<T>(List<T> res, ControlCollection col)
            where T : IMTControl
        {
            foreach (object c in col)
            {
                if (((Control)c).Controls.Count > 0)
                    PopulateControlCollection<T>(res, ((Control)c).Controls);
                if (c is T)
                {
                    res.Add((T)c);
                }
            }
        }

        internal static void PopulateControlCollection(List<Control> list, ControlCollection col)
        {
            foreach (Control c in col)
            {
                if (c.Controls.Count > 0)
                    PopulateControlCollection(list, c.Controls);
                if (c is IAttributeAccessor && !(c is IMTControl))
                {
                    IAttributeAccessor iattr = c as IAttributeAccessor;
                    foreach (string attr in MTAttributes)
                    {
                        string attrValue = iattr.GetAttribute(attr);
                        if (attrValue != null)
                        {
                            list.Add((Control)c);
                            break;
                        }
                    }
                }
            }
        }

        internal static List<Control> GetExternalControls(ControlCollection col)
        {
            List<Control> Controls = new List<Control>();
            PopulateControlCollection(Controls, col);
            return Controls;
        }

        internal static void RemoveMTAttributes(ControlCollection col)
        {
            List<Control> controls = new List<Control>();
            PopulateControlCollection(controls, col);
            foreach (Control c in controls)
                RemoveMTAttributes(c);
        }

        private static void RemoveMTAttributes(Control c)
        {
            if (c is WebControl)
                foreach (string attr in MTAttributes)
                    (c as WebControl).Attributes.Remove(attr);
            else if (c is HtmlControl)
                foreach (string attr in MTAttributes)
                    (c as HtmlControl).Attributes.Remove(attr);
        }

        internal static string GetMTField(Control c)
        {
            string attributeName = MTAttributes[(int)AttributeFields.Field];
            if (c is IAttributeAccessor)
                return (c as IAttributeAccessor).GetAttribute(attributeName);
            else
                return null;
        }

        internal static string GetAttribute(Control c, AttributeFields attr)
        {
            string attributeName = MTAttributes[(int)attr];
            if (c is IAttributeAccessor)
                return (c as IAttributeAccessor).GetAttribute(attributeName);
            else
                return null;
        }

        internal static TypedValue EvaluateTypedValue(Control c)
        {
            TypedValue result = new TypedValue();
            string dataType = GetAttribute(c, AttributeFields.Datatype);
            try
            {
                result.DataType = (DataType)Enum.Parse(typeof(DataType), GetAttribute(c, AttributeFields.Datatype), true);
            }
            catch
            {
                result.DataType = DataType.Text;
            }
            result.Value = GetValue(c);
            return result;
        }

        /// <summary>
        /// Retrive Value of generic control.
        /// </summary>
        /// <param name="c"></param>
        /// <returns>Value assigned whis current control</returns>
        internal static object GetValue(Control c)
        {
            Type tp = c.GetType();
            PropertyInfo info = null;
            
            ControlValuePropertyAttribute[] cvp = (ControlValuePropertyAttribute[])tp.GetCustomAttributes(typeof(ControlValuePropertyAttribute), true);
            if (cvp.Length == 1)
            {
                string propertyName = cvp[0].Name;
                if (propertyName.Length > 0)
                {
                    info = tp.GetProperty(propertyName);
                    if (info != null)
                        return (object)info.GetValue(c, null);
                }
            }
            
            if (c is ITextControl)
            {
                info = tp.GetProperty("Value");
                if (info != null)
                    return (object)info.GetValue(c, null);
                else
                    return (object)(c as ITextControl).Text;
            }
            else if (c is ICheckBoxControl)
                return (object)(c as ICheckBoxControl).Checked;
            else
            {
                string dataBuildField = GetAttribute(c, AttributeFields.DataBindField);
                if (dataBuildField != null)
                    info = tp.GetProperty(MTAttributes[(int)AttributeFields.DataBindField]);
                else
                    info = tp.GetProperty("Text");
                if (info != null)
                    return info.GetValue(c, null);
                else
                    return null;
            }
        }

        internal static string EvaluateDbFormat(string DbFormat, DataType type, Connection conn)
        {
            if (conn != null && String.IsNullOrEmpty(DbFormat))
            {
                switch (type)
                {
                    case DataType.Boolean:
                        DbFormat = conn.BooleanFormat;
                        break;
                    case DataType.Date:
                        DbFormat = conn.DateFormat;
                        break;
                }
            }
            return DbFormat;
        }

        internal static void PerformDataBinding(ListControl ctrl, IEnumerable dataSource, InMotion.Data.Connection conn)
        {
            if (dataSource != null)
            {
                bool IsSourceColumnNameSet = false;
                string textColumn = ctrl.DataTextField;
                string textColumnFormat = ctrl.DataTextFormatString;
                string boundColumn = ctrl.DataValueField;
                if (!ctrl.AppendDataBoundItems)
                {
                    ctrl.Items.Clear();
                }
                ICollection collection1 = dataSource as ICollection;
                if (collection1 != null)
                {
                    ctrl.Items.Capacity = collection1.Count + ctrl.Items.Count;
                }
                if ((textColumn.Length != 0) || (boundColumn.Length != 0))
                {
                    IsSourceColumnNameSet = true;
                }

                string DBFormat = "";
                DBFormat = EvaluateDbFormat(DBFormat, ((IMTControlWithValue)ctrl).DataType, conn);
                foreach (object dsItem in dataSource)
                {
                    ListItem itm = new ListItem();
                    if (IsSourceColumnNameSet)
                    {
                        if (textColumn.Length > 0)
                        {
                            itm.Text = DataBinder.GetPropertyValue(dsItem, textColumn, textColumnFormat);
                        }
                        if (boundColumn.Length > 0)
                        {
                            object val = DataBinder.GetPropertyValue(dsItem, boundColumn);
                            val = TypeFactory.CreateTypedField(((IMTControlWithValue)ctrl).DataType, val, DBFormat);
                            itm.Value = val.ToString();
                        }
                    }
                    else
                    {
                        if (dsItem is System.Data.DataRowView)
                        {
                            DataRow dr = ((DataRowView)dsItem).Row;
                            if (dr.ItemArray.Length > 0) itm.Value = TypeFactory.CreateTypedField(((IMTControlWithValue)ctrl).DataType, dr.ItemArray[0], DBFormat).ToString();
                            if (dr.ItemArray.Length > 1) itm.Text = TypeFactory.CreateTypedField(DataType.Text, dr.ItemArray[1], textColumnFormat).ToString();
                        }
                    }
                    ctrl.Items.Add(itm);
                }
            }
        }

        internal static string EncodeText(string str, ContentType ContentType)
        {
            if (Web.Controls.ContentType.Text == ContentType)
            {
                str = HttpUtility.HtmlEncode(str);
                str = str.Replace("\r", "").Replace("\n", "<br>");
            }
            return str;
        }

        private static Regex ResourceRegex = new Regex("(?:{res:([^}]*)})", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
        internal static string ReplaceResource(string text)
        {
            return ResourceRegex.Replace(text, new MatchEvaluator(ResourceEvalutor));
        }

        private static string ResourceEvalutor(Match m)
        {
            return Common.Resources.ResManager.GetString(m.Groups[1].ToString());
        }

        internal static TypedValue CreateTypedValue(IMTField Value, DataType type, string DBText, string DBFormat, string Format)
        {
            TypedValue result = new TypedValue();
            result.Format = DBFormat;
            result.DataType = type;
            if (DBText == Value.ToString(DBFormat))
            {
                result.Value = Value;
            }
            else
            {
                try
                {
                    result.Value = TypeFactory.CreateTypedField(type, DBText, Format);
                }
                catch (ArgumentNullException)
                {
                    result.Value = DBText;
                }
                catch (OverflowException)
                {
                    result.Value = DBText;
                }
                catch (FormatException)
                {
                    result.Value = DBText;
                }
            }
            return result;
        }

        internal static TypedValue EvaluateTypedValue(TypedValue value, Connection conn)
        {
            TypedValue result = new TypedValue();
            result.DataType = value.DataType;
            result.Format = EvaluateDbFormat(value.Format, value.DataType, conn);
            result.Value = value.Value;
            return result;
        }

        internal static string ReplaceResourcesAndStyles(string value, Page p)
        {
            if (string.IsNullOrEmpty(value)) return value;
            if (p is MTPage)
                value = value.Replace("{CCS_Style}", ((MTPage)p).StyleName);
            else
                value = value.Replace("{CCS_Style}", "Simple");



            Match m = res.Match(value);
            while (m.Success)
            {
                if (p is MTPage)
                    value = value.Replace(m.Value, ((MTPage)p).ResManager.GetString(m.Groups[1].Value));
                m = m.NextMatch();
            }
            return value;
        }

        internal static string ReplaceResourcesAndStyles(string value, Page p, Control c)
        {
            string res = ReplaceResourcesAndStyles(value, p);
            if (res.IndexOf("{CCS_PathToMasterPage}") != -1)
            {
                Control parent = c.Parent;
                while (parent != null)
                {
                    if (parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)parent).MasterPageFile))
                    {
                        res = res.Replace("{CCS_PathToMasterPage}", ((MTPanel)parent).PathToMasterPage);
                        break;
                    }
                    else if (parent is MTPage && !string.IsNullOrEmpty(((MTPage)parent).MasterPageFile))
                    {
                        res = res.Replace("{CCS_PathToMasterPage}", ((MTPage)parent).PathToMasterPage);
                        break;
                    }
                    parent = parent.Parent;
                }
            }

            if (res.IndexOf("{page:pathToRoot}") != -1)
                res = res.Replace("{page:pathToRoot}", p.ResolveClientUrl(("~/")));
            return res;
        }

        internal static string GetMTFormat(string format)
        {
            switch (format)
            {
                case "D":
                    return "LongDate";
                case "G":
                    return "GeneralDate";
                case "d":
                    return "ShortDate";
                case "T":
                    return "LongTime";
                case "t":
                    return "ShortTime";
            }

            format = format.Replace("m", "n");
            format = format.Replace("M", "m");
            format = format.Replace("tt", "AM/PM");
            format = format.Replace("t", "M/P");
            format = format.Replace("\\/", "/");
            format = format.Replace("\\:", ":");

            return format;
        }

        internal static object DataBindControl(SourceType sourceType, string source, IForm owner,DataType type, string DBFormat)
        {
            object val = null;
            if (sourceType == SourceType.DatabaseColumn && source.Length > 0 && owner != null && owner.DataItem != null && owner.DataItem.Value != null)
            {
                val = owner.DataItem[source];
                return TypeFactory.CreateTypedField(type, val, DBFormat);
            }
            if (sourceType == SourceType.DBParameter && source.Length > 0 && owner != null)
            {
                MTDataSourceView dv = owner.GetDataSourceView() as MTDataSourceView;
                if (dv != null)
                {
                    val = dv.SelectParameters[source].Value;
                    return TypeFactory.CreateTypedField(type, val, DBFormat);
                }
            }
            if (sourceType == SourceType.SpecialValue && source.Length > 0 && owner != null && owner is ISpecialValueProvider)
            {
                if (((ISpecialValueProvider)owner).SpecialValues.ContainsKey(source))
                {
                    val = ((ISpecialValueProvider)owner).SpecialValues[source];
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("The specified special value does not exist in parent form collection.\n{0}", Environment.StackTrace);
                    throw new InvalidControlConfigurationException("The specified special value does not exist in parent form collection");
                }
                return TypeFactory.CreateTypedField(type, val, DBFormat);
            }
            return null;
        }

    }
}
