//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Creates a multi selection check box group that can be dynamically created by binding the control to a data source.
    /// </summary>
    public class MTCheckBoxList : System.Web.UI.WebControls.CheckBoxList, IMTMultipleEditableControl, IErrorProducer, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTCheckBoxList"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }
        /// <summary>
        /// Gets or sets the value indicating whether control contains multiple values.
        /// </summary>
        public bool IsMultiple
        {
            get { return false; }
        }
        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (!((IMTField)Value).IsNull) Value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        string _caption;
        /// <summary>
        /// Gets or sets the caption of the control that will be used in validation messages.
        /// </summary>
        public string Caption
        {
            get
            {
                if (String.IsNullOrEmpty(_caption)) _caption = ID;
                return _caption;
            }
            set { _caption = value; }
        }

        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return SelectArguments.SortExpression;
            }
            set
            {
                SelectArguments.SortExpression = value;
            }
        }

        private bool _required;
        /// <summary>
        /// Gets or sets the value that indicating whether control must have non-empty value.
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        private bool _Unique;
        /// <summary>
        /// Gets or sets the value that indicating whether value of control must be unique.
        /// </summary>
        public bool Unique
        {
            get { return _Unique; }
            set { _Unique = value; }
        }

        private string __ValidationMask;
        /// <summary>
        /// Gets or sets regular expression pattern for validating data entry.
        /// </summary>
        public string ValidationMask
        {
            get
            {
                if (__ValidationMask == null)
                    __ValidationMask = "";
                return __ValidationMask;
            }
            set { __ValidationMask = value; }
        }

        private string __ErrorControl;
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                if (__ErrorControl == null)
                    __ErrorControl = "";
                return __ErrorControl;
            }
            set { __ErrorControl = value; }
        }

        private ContentType _content_Type = ContentType.Text;

        /// <summary>
        /// Gets or sets whether the content of the control is plain text or HTML code.
        /// </summary>
        public ContentType ContentType
        {
            get { return _content_Type; }
            set { _content_Type = value; }
        }

        private string _format = String.Empty;

        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;

        /// <summary>
        /// Format that will be used to extract as well as place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        /// <summary>
        /// Gets the value of the selected item in the list control, or selects the item in the list control that contains the specified value.
        /// </summary>
        public override string SelectedValue
        {
            get { return Text; }
            set { Text = value; }
        }

        private bool _valUpdated;

        
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                return TypeFactory.CreateTypedField(DataType, base.SelectedValue, Format);
            }
            set
            {

                try
                {
                    IMTField val;
                    SelectedIndex = -1;
                    if (value is ICollection)
                    {
                        foreach (object v in (ICollection)value)
                        {
                            val = TypeFactory.CreateTypedField(DataType, v, Format);
                            ListItem li = Items.FindByValue(val.ToString(Format));
                            if (li != null)
                                li.Selected = true;
                        }
                    }
                    else
                    {
                        val = TypeFactory.CreateTypedField(DataType, value, Format);
                        base.SelectedValue = val.ToString(Format);
                    }
                    _valUpdated = true;
                }
                catch (FormatException e)
                {
                    if (OwnerForm is IErrorHandler)
                    {
                        if (String.IsNullOrEmpty(Format))
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectValue"), ControlsHelper.ReplaceResource(Caption)));
                        else
                        {
                            string _format = Format;
                            if (DataType == DataType.Date)
                                _format = ControlsHelper.GetMTFormat(_format);
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectFormat"), ControlsHelper.ReplaceResource(Caption), _format));
                        }
                        ((IErrorHandler)OwnerForm).RegisterError(this, e);
                    }
                    else
                    {
                        if (AppConfig.IsMTErrorHandlerUse("critical"))
                            Trace.TraceError(e.ToString());
                        throw e;
                    }
                }
            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;
                try
                {
                    val = TypeFactory.CreateTypedField(DataType, val, Format);
                    __DefaultValue = val;
                }
                catch (FormatException e)
                {
                    if (OwnerForm is IErrorHandler)
                    {
                        if (String.IsNullOrEmpty(Format))
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectValue"), ControlsHelper.ReplaceResource(Caption)));
                        else
                        {
                            string _format = Format;
                            if (DataType == DataType.Date)
                                _format = ControlsHelper.GetMTFormat(_format);
                            Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_IncorrectFormat"), ControlsHelper.ReplaceResource(Caption), _format));
                        }
                        ((IErrorHandler)OwnerForm).RegisterError(this, e);
                    }
                    else
                    {
                        if (AppConfig.IsMTErrorHandlerUse("critical"))
                            Trace.TraceError(e.ToString());
                        throw e;
                    }
                }
            }
        }

        private bool _AppendDataBoundItems = true;
        /// <summary>
        /// Gets or sets a value that indicates whether list items are cleared before data binding.
        /// </summary>
        public override bool AppendDataBoundItems
        {
            get { return _AppendDataBoundItems; }
            set { _AppendDataBoundItems = value; }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public override string Text
        {
            get
            {
                return GetText(Format);
            }
            set
            {
                base.SelectedValue = value;
            }
        }

        /// <summary>
        /// Gets a formatted <see cref="Value"/> representation to be placed into the database.
        /// </summary>
        public TypedValue DBValue
        {
            get
            {
                return ControlsHelper.CreateTypedValue((IMTField)this.Value, this.DataType, GetText(DBFormat), DBFormat, this.Format);
            }
        }


        private string GetText(string _format)
        {
            return base.SelectedValue;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }
        #endregion

        /// <summary>
        /// Raises the <see cref="Validating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (Validating != null)
                Validating(this, e);
        }

        /// <summary>
        /// Perform the value validation.
        /// </summary>
        /// <returns><b>true</b> if the <see cref="Value"/> is valid; otherwise <b>false</b>.</returns>
        public bool Validate()
        {
            ValidatingEventArgs args = new ValidatingEventArgs();
            if (Required && String.IsNullOrEmpty(Text))
            {
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_RequiredField"), ControlsHelper.ReplaceResource(Caption)));
                args.HasErrors = true;
            }
            if (!ValidationHelper.ValidateMask(ValidationMask, Text))
            {
                args.HasErrors = true;
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_MaskValidation"), ControlsHelper.ReplaceResource(Caption)));
            }
            if (Unique && SourceType == SourceType.DatabaseColumn && Source.Length > 0 && Text.Length > 0)
            {
                if (OwnerForm is IValidator)
                    if (!((IValidator)OwnerForm).ValidateUnique(this))
                    {
                        args.HasErrors = true;
                        Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_UniqueValue"), ControlsHelper.ReplaceResource(Caption)));
                    }
            }
            OnValidating(args);
            if (args.HasErrors)
            {
                if (OwnerForm is IErrorHandler)
                    ((IErrorHandler)OwnerForm).RegisterError(this, null);
                return false;
            }
            return true;
        }

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the Collection&lt;string&gt; which is used to collect error messages generated during the controls execution.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new ControlErrorCollection();
                    _errors.ErrorAdded += new EventHandler<EventArgs>(OnErrorAdded);
                    _errors.ErrorRemoved += new EventHandler<EventArgs>(OnErrorRemoved);
                }
                return _errors;
            }
        }

        private void OnErrorAdded(object sender, EventArgs e)
        {
            if (OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RegisterError(this, null);
        }

        private void OnErrorRemoved(object sender, EventArgs e)
        {
            if (Errors.Count == 0 && OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RemoveError(this);
        }

        /// <summary>
        /// Occurs after the control performed the value validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;

        private void OwnerForm_Validating(object sender, ValidatingEventArgs e)
        {
            e.HasErrors = !Validate();
        }

        private InMotion.Data.Connection __conn;
        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            DataSourceView dsv = this.GetData();
            if (dsv is MTDataSourceView)
            {
                ((MTDataSourceView)dsv).Owner = this;
                IEnumerable data = ((MTDataSourceView)dsv).Select(SelectArguments);
                string connectionName = ((MTDataSourceView)dsv).ConnectionString;
                if (connectionName.StartsWith("InMotion:"))
                {
                    __conn = InMotion.Configuration.AppConfig.GetConnection(connectionName.Substring(9));
                }
                this.PerformDataBinding(data);
            }
            else base.OnDataBinding(e);

            object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
            if (val != null || val is IMTField && !((IMTField)val).IsNull)
                Value = val;
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="MTCheckBoxList"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {
            ControlsHelper.PerformDataBinding(this, data, __conn);
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (OwnerForm is IValidator)
                (OwnerForm as IValidator).ControlsValidating += new EventHandler<ValidatingEventArgs>(OwnerForm_Validating);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!_valUpdated)
            {
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Value = Page.Request.QueryString.GetValues(ID);
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                    Value = Page.Request.Form.GetValues(ID);
            }
        }

        /// <summary>
        /// This method support framework infrastructure and is not 
        /// intended to be used directly from your code. 
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
            _IsValueChanged = true;
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);

            ControlsHelper.EncodeText(Text, ContentType);
            if (Web.Controls.ContentType.Text == this.ContentType)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Text = ControlsHelper.EncodeText(this.Items[i].Text, ContentType);
                }
            }
            if (String.IsNullOrEmpty(base.SelectedValue) && !((IMTField)DefaultValue).IsNull) Value = DefaultValue;
            foreach (ListItem li in Items)
            {
                li.Value = ControlsHelper.ReplaceResourcesAndStyles(li.Value, Page);
                li.Text = ControlsHelper.ReplaceResourcesAndStyles(li.Text, Page);
            }
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode && Items.Count == 0)
                Items.Add(new ListItem(Caption));
            base.Render(writer);
        }

        private bool _IsValueChanged;
        /// <summary>
        /// Gets or sets the value indicating whether value of control changed by the user.
        /// </summary>
        public bool IsValueChanged
        {
            get { return _IsValueChanged; }
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }

        /// <summary>
        /// Returns a string representation of value at the specified index.
        /// </summary>
        /// <param name="index">The index of value.</param>
        /// <returns>A string representation of value at the specified index.</returns>
        public string ToString(int index)
        {
            return ((IMTField)SelectedValues[index]).ToString(Format);
        }

        /// <summary>
        /// Gets the array of the selected values; 
        /// </summary>
        public object[] SelectedValues
        {
            get
            {
                ArrayList res = new ArrayList();
                foreach (ListItem li in Items)
                {
                    if (li.Selected)
                        res.Add(TypeFactory.CreateTypedField(DataType, li.Value, Format));
                }
                return res.ToArray();
            }
        }

        private bool _IsValueNotPassed;
        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        public bool IsValueNotPassed
        {
            get { return _IsValueNotPassed; }
            set { _IsValueNotPassed = value; }
        }

        private bool _IsOmitEmptyValue;
        /// <summary>
        /// Indicates whethere control value will be excluded from data update operation when <see cref="IsValueNotPassed"/> is true;
        /// </summary>
        public bool IsOmitEmptyValue
        {
            get
            {
                return _IsOmitEmptyValue;
            }
            set
            {
                _IsOmitEmptyValue = value;
            }
        }
    }
}