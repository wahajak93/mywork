//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InMotion.Security;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to all InMotion form components.
    /// </summary>
    public interface IForm: IMTControlContainer
    {
        /// <summary>
        /// Gets or sets the unique ID of the control.
        /// </summary>
        string ID { get;set;}
        /// <summary>
        /// Gets or sets the data source of the control.
        /// </summary>
        object DataSource { get;set;}
        /// <summary>
        /// Gets the current processed <see cref="DataItem"/>.
        /// </summary>
        DataItem DataItem { get;}
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> object.
        /// </summary>
        FormSupportedOperations UserRights { get;}
        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        void DataBind();
        /// <summary>
        ///  Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        bool Restricted { get;set;}
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        bool HasData { get;}
        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        DataSourceView GetDataSourceView();
    }
}
