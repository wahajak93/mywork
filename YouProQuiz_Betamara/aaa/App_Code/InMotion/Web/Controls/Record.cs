//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Security;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// A record form that is used to add, edit or delete database content. 
    /// </summary>
    [Designer(typeof(RecordDesigner))]
    public class Record : DataBoundControl, IForm, INamingContainer, IErrorHandler, IErrorProducer, IValidator, IClientScriptHelper, IMTAttributeAccessor
    {
        /// <summary>
        /// Occurs before the Record send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;
        /// <summary>
        /// Occurs at the onset of the operations performed to insert a new record into the database. 
        /// </summary>
        public event EventHandler<EventArgs> BeforeInsert;
        /// <summary>
        /// Occurs after inserting a new row into data source, but before error processing. 
        /// </summary>
        public event EventHandler<DataOperationCompletingEventArgs> AfterInsert;
        /// <summary>
        /// Occurs before preparing to update an existing record. 
        /// </summary>
        public event EventHandler<EventArgs> BeforeUpdate;
        /// <summary>
        /// Occurs after updating an existing row in the data source, but before error processing.
        /// </summary>
        public event EventHandler<DataOperationCompletingEventArgs> AfterUpdate;
        /// <summary>
        /// Occurs before preparing for a row deletion.
        /// </summary>
        public event EventHandler<EventArgs> BeforeDelete;
        /// <summary>
        /// Occurs after deleting a row from the data source, but before error processing.
        /// </summary>
        public event EventHandler<DataOperationCompletingEventArgs> AfterDelete;
        /// <summary>
        /// Occurs after the Record control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        private bool itemsDataBindingComplete;

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        #region IForm Members

        private ITemplate _item;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the <see cref="Record"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateContainer(typeof(RecordItem))
        ]
        public virtual ITemplate ItemTemplate
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }

        private string[] _dataKeyNames;
        /// <summary>
        /// Gets or sets an array that contains the names of the primary key fields. Used for support nonInMotion DataSources
        /// </summary>
        [DefaultValue((string)null), TypeConverter(typeof(StringArrayConverter))]
        public virtual string[] DataKeyNames
        {
            get
            {
                object obj2 = this._dataKeyNames;

                if (obj2 != null)
                {
                    return (string[])((string[])obj2).Clone();
                }
                return new string[0];
            }
            set
            {
                if (value != null)
                {
                    this._dataKeyNames = (string[])value.Clone();
                }
                else
                {
                    this._dataKeyNames = null;
                }
                if (base.Initialized)
                {
                    base.RequiresDataBinding = true;
                }
            }
        }

        /// <summary>
        /// Contains the key field values. Used for support nonInMotion DataSources
        /// </summary>
        private OrderedDictionary KeyTable
        {
            get
            {
                if (ViewState["__keyTable"] == null)
                    ViewState["__keyTable"] = new OrderedDictionary();
                return (OrderedDictionary)ViewState["__keyTable"];
            }
            set
            {
                ViewState["__keyTable"] = value;
            }
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;
            RenderContents(writer);
        }

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;

            OnBeforeSelect(EventArgs.Empty);
            if (this.DataSourceID.Length == 0)
            {
                this.OnDataBinding(EventArgs.Empty);
            }
            if (!DesignMode)
            {
                DataSourceView ds = this.GetData();
                if (ds is MTDataSourceView)
                    ((MTDataSourceView)ds).Owner = this;
                base.RequiresDataBinding = false;
                this.MarkAsDataBound();
                try
                {
                    if (!AllowRead)
                    {
                        if (AppConfig.IsMTErrorHandlerUse("critical"))
                            Trace.TraceError("Error in the parameters initialization.\n{0}", Environment.StackTrace);
                        throw new ParametersInitializationException("");
                    }
                    ds.Select(SelectArguments, new DataSourceViewSelectCallback(this.OnDataSourceViewSelectCallback));
                }
                catch (System.Threading.ThreadAbortException) { }
                catch (ParametersInitializationException)
                {
                    //Parameters are not passed - is Insert Mode.
                    this.OnDataSourceViewSelectCallback(null);
                }
                catch (Exception e)
                {
                    Errors.Add("Data source error. " + e.Message);
                    if (!itemsDataBindingComplete) OnDataSourceViewSelectCallback(null);
                }
            }
            else
            {
                OnDataSourceViewSelectCallback(null);
            }
            if (Errors.Count == 0) OnBeforeShow(EventArgs.Empty);
        }

        private void OnDataSourceViewSelectCallback(IEnumerable data)
        {
            if (this.DataSourceID.Length > 0)
            {
                this.OnDataBinding(EventArgs.Empty);
            }

            if (data != null && data.GetEnumerator().MoveNext()) __hasData = true;
            this.PerformDataBinding(data);

            this.OnDataBound(EventArgs.Empty);
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="Record"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(System.Collections.IEnumerable data)
        {
            Controls.Clear();

            object DataItemValue = null;
            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();
                if (en.MoveNext())
                    DataItemValue = en.Current;
            }
            _dataItem = new DataItem(DataItemValue);
            RecordItem item = new RecordItem(DataItemValue);
            if (ItemTemplate != null)
                ItemTemplate.InstantiateIn(item);
            Controls.Add(item);
            item.DataBind();
            if (item.DataItem is System.Data.DataRowView)
            {
                System.Data.DataRowView drv = item.DataItem as System.Data.DataRowView;
                if (drv != null && DataKeyNames.Length > 0)
                {
                    KeyTable.Clear();
                    foreach (string text in DataKeyNames)
                    {
                        if (!KeyTable.Contains(text)) KeyTable.Add(text, drv.Row[text]);
                    }
                }
            }
            this.itemsDataBindingComplete = true;
            ProcessErrors();
            ChildControlsCreated = true;
            ViewState["created"] = true;
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Controls.Clear();
            if (ViewState["created"] != null)
            {
                RecordItem item = new RecordItem(null);
                if (ItemTemplate != null)
                    ItemTemplate.InstantiateIn(item);
                Controls.Add(item);
            }
        }

        private bool _PerformRedirect = true;
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        public bool PerformRedirect
        {
            get
            {
                return _PerformRedirect;
            }
            set
            {
                _PerformRedirect = value;
            }
        }

        private bool _allowRead = true;
        /// <summary>
        /// Gets or sets the value that indicating whether current user have Read permission.
        /// </summary>
        public bool AllowRead
        {
            get
            {
                return _allowRead;
            }
            set
            {
                _allowRead = value;
            }
        }

        private bool _allowInsert = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Insert operation for current user.
        /// </summary>
        public bool AllowInsert
        {
            get
            {
                return _allowInsert;
            }
            set
            {
                _allowInsert = value;
            }
        }

        private bool _allowUpdate = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Update operation for current user.
        /// </summary>
        public bool AllowUpdate
        {
            get
            {
                return _allowUpdate;
            }
            set
            {
                _allowUpdate = value;
            }
        }

        private bool _allowDelete = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Delete operation for current user.
        /// </summary>
        public bool AllowDelete
        {
            get
            {
                return _allowDelete;
            }
            set
            {
                _allowDelete = value;
            }
        }

        private string __ErrorControl = "ErrorLabel";
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                return __ErrorControl;
            }
            set
            {
                __ErrorControl = value;
            }
        }

        private string __ErrorPanel = "Error";
        /// <summary>
        /// Gets or sets the ID of the <see cref="MTPanel"/> to be contain error message block.
        /// </summary>
        public string ErrorPanel
        {
            get
            {
                return __ErrorPanel;
            }
            set
            {
                __ErrorPanel = value;
            }
        }

        private DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }
        #endregion

        private bool _isInsertMode = false;
        /// <summary>
        /// Gets a value indicating whether <see cref="Record"/> form is in Insert mode. 
        /// </summary>
        public bool IsInsertMode
        {
            get
            {
                if (IsSubmitted)
                    return _isInsertMode;
                else
                    return DataItem == null || DataItem.Value == null;
            }
        }

        private bool __IsSubmitted;
        /// <summary>
        /// Gets the value indicating whether Record is submitted by user.
        /// </summary>
        public bool IsSubmitted
        {
            get
            {
                return __IsSubmitted;
            }
        }

        private Url __RedirectUrl = new Url();
        /// <summary>
        /// Gets or sets the url to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public Url RedirectUrl
        {
            get
            {
                return __RedirectUrl;
            }
            set
            {
                __RedirectUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the page name to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public string ReturnPage
        {
            get
            {
                return base.ResolveClientUrl(RedirectUrl.ToString());
            }
            set
            {
                RedirectUrl.Parameters.Clear();
                RedirectUrl.Address = this.ResolveUrl(value);
            }
        }

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get
            {
                return RedirectUrl.RemoveParameters;
            }
            set
            {
                RedirectUrl.RemoveParameters = value;
            }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get
            {
                return RedirectUrl.Type;
            }
            set
            {
                RedirectUrl.Type = value;
            }
        }

        private bool __EnableValidation = true;
        /// <summary>
        /// Gets or sets the value indicating whether form should validate user input.
        /// </summary>
        public bool EnableValidation
        {
            get
            {
                return __EnableValidation;
            }
            set
            {
                __EnableValidation = value;
            }
        }

        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get
            {
                return RedirectUrl.PreserveParameters;
            }
            set
            {
                RedirectUrl.PreserveParameters = value;
            }
        }

        private FormSupportedOperations __UserRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (__UserRights == null)
                    __UserRights = new FormSupportedOperations();
                return __UserRights;
            }
        }

        /// <summary>
        /// Executes an Insert operation.
        /// </summary>
        public virtual void Insert()
        {
            _isInsertMode = true;
            if (!AllowInsert || Restricted && !UserRights.AllowInsert) return;
            if (!Validate()) return;
            DataSourceView ds = GetData();
            if (ds is MTDataSourceView)
                ((MTDataSourceView)ds).Owner = this;
            if (ds.CanInsert)
            {
                OnBeforeInsert(EventArgs.Empty);
                Dictionary<string, object> values = new Dictionary<string, object>();
                foreach (IMTEditableControl c in GetControls<IMTEditableControl>())
                {
                    if (c.SourceType == SourceType.DatabaseColumn && c.Source.Length > 0)
                        if (ds is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c.DBValue, ((MTDataSourceView)ds).GetConnection());
                            val.IsEmpty = c.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || c.IsOmitEmptyValue);
                            values.Add(c.Source, val);
                        }
                        else
                        {
                            values.Add(c.Source, c.Value);
                        }
                }

                foreach (Control c in ControlsHelper.GetExternalControls(this.Controls))
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (ds is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }

                ds.Insert(values, InsertOperationCallback);
            }
            else
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Insert operation does not supported the Record {0}.\n{1}", ID, Environment.StackTrace);
                ProduceRedirect(RedirectUrl);
            }
        }

        private bool InsertOperationCallback(int affectedRecords, Exception ex)
        {
            DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Insert, affectedRecords, RedirectUrl, ex);
            OnAfterInsert(args);
            if (ex != null)
            {
                Errors.Add(args.OperationExceptionMessage);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Insert operation fails for the Record {0}.\n{1}", ID, ex);
            }
            Control p = this.Parent;
            while (!(p is MTPage))
            {
                if (p.GetType().Name == "MTUpdatePanel")
                    return true;
                p = p.Parent;
            }

            if (ProcessErrors())
                ProduceRedirect(RedirectUrl, args.PerformRedirect);
            return true;
        }

        /// <summary>
        /// Executes an Update operation.
        /// </summary>
        public virtual void Update()
        {
            if (!AllowUpdate || Restricted && !UserRights.AllowUpdate) return;
            if (!Validate()) return;
            DataSourceView ds = GetData();
            if (ds is MTDataSourceView)
                ((MTDataSourceView)ds).Owner = this;
            if (ds.CanUpdate)
            {
                OnBeforeUpdate(EventArgs.Empty);
                Dictionary<string, object> values = new Dictionary<string, object>();
                foreach (IMTEditableControl c in GetControls<IMTEditableControl>())
                {
                    if (c.SourceType == SourceType.DatabaseColumn && c.Source.Length > 0)
                        if (ds is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c.DBValue, ((MTDataSourceView)ds).GetConnection());
                            val.IsEmpty = c.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || c.IsOmitEmptyValue);
                            values.Add(c.Source, val);
                        }
                        else
                        {
                            values.Add(c.Source, c.Value);
                        }
                }
                foreach (Control c in ControlsHelper.GetExternalControls(this.Controls))
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (ds is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }
                ds.Update(KeyTable.Count > 0 ? KeyTable : null, values, null, UpdateOperationCallback);
            }
            else
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Update operation does not supported the Record {0}.\n{1}", ID, Environment.StackTrace);
                ProduceRedirect(RedirectUrl);
            }
        }

        private bool UpdateOperationCallback(int affectedRecords, Exception ex)
        {
            DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Update, affectedRecords, RedirectUrl, ex);
            OnAfterUpdate(args);
            if (ex != null)
            {
                Errors.Add(args.OperationExceptionMessage);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Update operation fails for the Record {0}.\n{1}", ID, ex);
            }
            if (ProcessErrors() && PerformRedirect)
                ProduceRedirect(RedirectUrl, args.PerformRedirect);
            return true;
        }

        /// <summary>
        /// Executes a Delete operation.
        /// </summary>
        public virtual void Delete()
        {
            if (!AllowDelete || Restricted && !UserRights.AllowDelete) return;
            if (!Validate()) return;
            DataSourceView ds = GetData();
            if (ds is MTDataSourceView)
                ((MTDataSourceView)ds).Owner = this;
            if (ds.CanDelete)
            {
                OnBeforeDelete(EventArgs.Empty);
                Dictionary<string, object> values = new Dictionary<string, object>();
                foreach (IMTEditableControl c in GetControls<IMTEditableControl>())
                {
                    if (c.SourceType == SourceType.DatabaseColumn && c.Source.Length > 0)
                        if (ds is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c.DBValue, ((MTDataSourceView)ds).GetConnection());
                            val.IsEmpty = c.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || c.IsOmitEmptyValue);
                            values.Add(c.Source, val);
                        }
                        else
                        {
                            values.Add(c.Source, c.Value);
                        }

                }
                foreach (Control c in ControlsHelper.GetExternalControls(this.Controls))
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (ds is MTDataSourceView)
                        {
                            MTDataSourceView dv = GetDataSourceView() as MTDataSourceView;

                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }
                ds.Delete(KeyTable.Count > 0 ? KeyTable : null, null, DeleteOperationCallback);
            }
            else
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Delete operation does not supported the Record {0}.\n{1}", ID);
                ProduceRedirect(RedirectUrl);
            }
        }

        private bool DeleteOperationCallback(int affectedRecords, Exception ex)
        {
            DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Delete, affectedRecords, RedirectUrl, ex);
            OnAfterDelete(args);
            if (ex != null)
            {
                Errors.Add(args.OperationExceptionMessage);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Delete operation fails for the Record {0}\n{1}", ID, ex);
            }
            if (ProcessErrors())
                ProduceRedirect(RedirectUrl, args.PerformRedirect);
            return true;
        }

        /// <summary>
        /// Performs the validating uniqueness of supplied <see cref="IMTEditableControl"/> value against the database.
        /// </summary>
        /// <param name="control">The <see cref="IMTEditableControl"/> that value should be validated.</param>
        /// <returns>true if the validation was successfull; otherwise false.</returns>
        public bool ValidateUnique(IMTEditableControl control)
        {
            DataSourceView ds = GetData();
            if (!(ds is MTDataSourceView)) return true;
            MTDataSourceView dv = ds as MTDataSourceView;
            dv.Owner = this;
            if (!dv.CanValidateUnique) return true;
            TypedValue val = ControlsHelper.EvaluateTypedValue(control.DBValue, ((MTDataSourceView)ds).GetConnection());
            return dv.ValidateUnique(null, control.Source, val);
        }

        /// <summary>
        /// Executes an Cancel operation.
        /// </summary>
        public virtual void Cancel()
        {
            ProduceRedirect(RedirectUrl);
        }

        /// <summary>
        /// Executes a Search operation.
        /// </summary>
        public virtual void Search()
        {
            if (!Validate()) return;
            ReadOnlyCollection<IMTEditableControl> col = GetControls<IMTEditableControl>();
            Url parsedUrl = new Url(RedirectUrl.ToString());
            foreach (IMTEditableControl c in col)
            {
                while (parsedUrl.Parameters[((Control)c).ID] != null)
                    parsedUrl.Parameters.Remove(parsedUrl.Parameters[((Control)c).ID]);
                if (c is IMTMultipleEditableControl && ((IMTMultipleEditableControl)c).SelectedValues != null)
                    for (int i = 0; i < ((IMTMultipleEditableControl)c).SelectedValues.Length; i++)
                    {
                        parsedUrl.Parameters.Add(((Control)c).ID, ((IMTMultipleEditableControl)c).ToString(i));
                    }
                else
                    if (!c.IsEmpty)
                    {
                        if (c is MTCheckBox)
                            parsedUrl.Parameters.Add(((Control)c).ID, ((MTCheckBox)c).GetText());
                        else
                            parsedUrl.Parameters.Add(((Control)c).ID, c.Text);
                    }
            }
            ProduceRedirect(parsedUrl);
        }

        internal void ProduceRedirect(Url RedirectUrl)
        {
            ProduceRedirect(RedirectUrl, true);
        }

        internal void ProduceRedirect(Url RedirectUrl, bool PerfRedirectArgs)
        {
            if (TemplateControl.Parent != null && TemplateControl.Parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)this.TemplateControl.Parent).MasterPageFile))
                AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            if (PerformRedirect && PerfRedirectArgs ||
                 (String.Compare(base.ResolveUrl(RedirectUrl.Address), Context.Request.FilePath, true) != 0 &&
                  String.Compare(base.ResolveUrl(RedirectUrl.Address), Context.Request.UrlReferrer.AbsoluteUri, true) != 0))
            {
                Page.Response.Redirect(base.ResolveClientUrl(RedirectUrl.ToString()), false);
            }
            else
            {
                Context.Request.RequestType = "GET";
                Context.RewritePath(Context.Request.FilePath, Context.Request.PathInfo, RedirectUrl.Parameters.ToString());
                __IsSubmitted = false;
                DataBind();
            }
        }

        /// <summary>
        /// Perform the value validation of child controls.
        /// </summary>
        /// <returns><b>true</b> if the validation is success; otherwise <b>false</b>.</returns>
        public virtual bool Validate()
        {
            if (EnableValidation)
            {
                ValidatingEventArgs e = new ValidatingEventArgs();
                OnValidating(e);
            }
            return ProcessErrors();
        }

        /// <summary>
        /// Process the errors collection of child controls and form itself.
        /// </summary>
        /// <returns>false if any error found, otherwise true.</returns>
        protected virtual bool ProcessErrors()
        {
            bool flag = false;
            MTLabel inst = GetControl<MTLabel>(ErrorControl);
            MTPanel panel = GetControl<MTPanel>(ErrorPanel);
            if (_errorControls.Count > 0 || Errors.Count > 0)
            {
                List<string> clearedIds = new List<string>();
                foreach (IErrorProducer ep in _errorControls)
                {
                    if (ep.Errors.Count > 0)
                    {
                        string id;
                        if (ep.ErrorControl.Length > 0)
                            id = ep.ErrorControl;
                        else if (ErrorControl.Length > 0)
                            id = ErrorControl;
                        else
                            continue;
                        MTLabel instance = Controls.Count > 0 ? (MTLabel)Controls[0].FindControl(id) : null;
                        if (instance == null) continue;
                        instance.ContentType = ContentType.Html;
                        instance.EnableViewState = false;
                        if (!clearedIds.Contains(id))
                        {
                            instance.Text = "";
                            clearedIds.Add(id);
                        }
                        flag = true;
                        foreach (string s in ep.Errors)
                        {
                            if (instance.Text.Length > 0) instance.Text += "<br/>";
                            instance.Text += s;
                        }
                    }
                }
                foreach (string s in Errors)
                {
                    flag = true;
                    if (inst == null) break;
                    inst.ContentType = ContentType.Html;
                    inst.EnableViewState = false;
                    if (inst.Text.Length > 0) inst.Text += "<br/>";
                    inst.Text += s;
                }
            }
            if (panel != null) panel.Visible = flag;
            return !flag;
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent control, if the source is an instance of <see cref="IMTButtonControl"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            __IsSubmitted = true;
            if (source is IButtonControl)
            {
                if (source is IMTButtonControl)
                {
                    IMTButtonControl b = (IMTButtonControl)source;
                    this.EnableValidation = b.EnableValidation;
                    PerformRedirect = PerformRedirect && b.PerformRedirect;
                    if (PerformRedirect)
                    {
                        if (b.ReturnPage.Length > 0)
                        {
                            RedirectUrl.Reset();
                            RedirectUrl.Address = b.ReturnPage;
                        }
                        if (b.ConvertUrl != UrlType.None)
                            RedirectUrl.Type = b.ConvertUrl;
                        if (b.RemoveParameters.Length > 0)
                            RedirectUrl.RemoveParameters = b.RemoveParameters;
                        if (RedirectUrl.Address.Length == 0)
                            RedirectUrl.Address = Page.Request.CurrentExecutionFilePath;
                    }
                }

                string cName = ((IButtonControl)source).CommandName.ToLower(CultureInfo.CurrentCulture);
                switch (cName)
                {
                    case "insert":
                        Insert();
                        break;
                    case "update":
                        Update();
                        break;
                    case "delete":
                        Delete();
                        break;
                    case "cancel":
                        Cancel();
                        break;
                    case "search":
                        Search();
                        break;
                    default:
                        if (Validate())
                        {
                            if (PerformRedirect)
                                Page.Response.Redirect(base.ResolveClientUrl(RedirectUrl.ToString()));
                            else if (Uri.IsWellFormedUriString(base.ResolveUrl(RedirectUrl.ToString()), UriKind.Absolute))
                                Context.RewritePath(new Uri(base.ResolveUrl(RedirectUrl.ToString())).PathAndQuery);
                            else
                                Context.RewritePath(base.ResolveUrl(RedirectUrl.ToString()));
                        }
                        break;
                }
            }

            return base.OnBubbleEvent(source, args);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeInsert"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeInsert(EventArgs e)
        {
            if (BeforeInsert != null)
                BeforeInsert(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterInsert"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterInsert(DataOperationCompletingEventArgs e)
        {
            if (AfterInsert != null)
                AfterInsert(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeUpdate"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeUpdate(EventArgs e)
        {
            if (BeforeUpdate != null)
                BeforeUpdate(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterUpdate"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterUpdate(DataOperationCompletingEventArgs e)
        {
            if (AfterUpdate != null)
                AfterUpdate(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeDelete"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeDelete(EventArgs e)
        {
            if (BeforeDelete != null)
                BeforeDelete(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterDelete"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterDelete(DataOperationCompletingEventArgs e)
        {
            if (AfterDelete != null)
                AfterDelete(this, e);
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (TemplateControl.Parent != null && TemplateControl.Parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)this.TemplateControl.Parent).MasterPageFile))
                AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            if (Visible)
            {
                string clientOnLoad = "";
                if (Attributes["clientOnLoad"] != null)
                {
                    clientOnLoad = Attributes["clientOnLoad"];
                    Attributes.Remove("clientOnLoad");
                }

                string clientOnSubmit = "";
                if (Attributes["clientOnSubmit"] != null)
                {
                    clientOnSubmit = Attributes["clientOnSubmit"];
                    Attributes.Remove("clientOnSubmit");
                }

                if (Page is MTPage)
                {
                    ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','Single','{1}','{2}'", ID, clientOnLoad, clientOnSubmit));
                }
                else
                {
                    Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','Single','{1}','{2}'", ID, clientOnLoad, clientOnSubmit));
                }
            }
        }


        #region IForm Members

        /// <summary>
        /// Raises the <see cref="ControlsValidating"/> and <see cref="Validating"/> events.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (ControlsValidating != null)
                ControlsValidating(this, e);
            if (Validating != null)
                Validating(this, e);
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            ControlsHelper.PopulateControlCollection<T>(result, Controls);
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Occurs after the control performs the child controls values validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> ControlsValidating;

        /// <summary>
        /// Occurs when the control performs a validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            T cnt = (T)Controls[0].FindControl(id);
            if (cnt == null)
                cnt = (T)InMotion.Common.Controls.FindControlIterative(this, id);
            return cnt;
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>The instance of the <i>Control</i> if any; otherwise the <b>null</b> reference.</returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }
        #endregion

        #region IErrorHandler Members
        private List<IErrorProducer> _errorControls = new List<IErrorProducer>();
        /// <summary>
        /// Performs an error registration.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> of error.</param>
        /// <param name="innerException">The initial exception, if any.</param>
        public void RegisterError(IErrorProducer control, Exception innerException)
        {
            if (!_errorControls.Contains(control))
                _errorControls.Add(control);
        }
        /// <summary>
        /// Performs an error removal.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> of error.</param>
        public void RemoveError(IErrorProducer control)
        {
            if (_errorControls.Contains(control))
                _errorControls.Remove(control);
        }
        #endregion

        #region IErrorProducer Members

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the <see cref="Collection&lt;T&gt;"/> that contain list of form level errors messages.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new ControlErrorCollection();
                }
                return _errors;
            }
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the current sort expression.
        /// </summary>
        public string SortExpression
        {
            get
            {
                return SelectArguments.SortExpression;
            }
            set
            {
                SelectArguments.SortExpression = value;
            }
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(ID + "Controls", String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(ID + "Controls", String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get
            {
                return __hasData;
            }
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Record"/> control.
    /// </summary>
    class RecordDesigner : MTFormDesigner
    {
        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Record ctrl = (Record)Component;
                    __TemplateGroups = base.TemplateGroups;
                    TemplateGroup tempGroup = new TemplateGroup("ItemTemplate");
                    TemplateDefinition tempDef = new TemplateDefinition(this, "ItemTemplate", ctrl, "ItemTemplate", false);

                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }

    }
}