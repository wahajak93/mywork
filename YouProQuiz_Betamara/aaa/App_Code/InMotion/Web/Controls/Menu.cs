//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;
using System.Web.UI.HtmlControls;
using InMotion.Security;
using InMotion.Configuration;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specify type of menu displaying.
    /// </summary>
    public enum MenuTypes
    {
        /// <summary>
        /// Specify horizontal menu type.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Specify vertical menu type.
        /// </summary>
        Vertical,
        /// <summary>
        /// Specify menu type as vertical tree.
        /// </summary>
        VerticalTree,
        /// <summary>
        /// Specify menu type as vertical right to left.
        /// </summary>
        VerticalRight
    }

    /// <summary>
    /// Specifies the type of an item in a <see cref="Menu"/> control.
    /// </summary>
    public enum MenuItemType
    {
        /// <summary>
        /// A header for the <see cref="Menu"/> control.
        /// </summary>
        Header,
        /// <summary>
        /// A footer for the <see cref="Menu"/> control.
        /// </summary>
        Footer,
        /// <summary>
        /// A item for the <see cref="Menu"/> control.
        /// </summary>
        Item,
        /// <summary>
        /// A close item for the <see cref="Menu"/> control.
        /// </summary>
        CloseItem,
        /// <summary>
        /// A open level for the <see cref="Menu"/> control.
        /// </summary>
        OpenLevel,
        /// <summary>
        /// A close item for the <see cref="Menu"/> control.
        /// </summary>
        CloseLevel
    }

    /// <summary>
    /// A Menu form is used to display site menu.
    /// </summary>
    [Designer(typeof(MenuDesigner))]
    public class Menu : DataBoundControl, IForm, INamingContainer, IMTAttributeAccessor, IClientScriptHelper
    {
        #region Events
        /// <summary>
        /// Occurs before the <see cref="Menu"/> send request to the datasource control for data.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSelect;

        /// <summary>
        /// Occurs after the <see cref="Menu"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Occurs after the <see cref="Menu"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<MenuItemEventArgs> BeforeShowRow;

        /// <summary>
        /// Occurs when the <see cref="Menu"/> item is initialized, which is the first step in its lifecycle.
        /// </summary>
        public event EventHandler<EventArgs> ItemCreated;

        /// <summary>
        /// Occurs after an item in the <see cref="Menu"/> is data-bound but before it is rendered on the page.
        /// </summary>
        public event EventHandler<MenuItemEventArgs> ItemDataBound;

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShowRow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShowRow(MenuItemEventArgs e)
        {
            if (BeforeShowRow != null)
                BeforeShowRow(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemDataBound"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="MenuItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemDataBound(MenuItemEventArgs e)
        {
            __itemCount++;
            if (ItemDataBound != null) ItemDataBound(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ItemCreated"/> event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="MenuItemEventArgs"/> that contains event data.</param>
        protected virtual void OnItemCreated(EventArgs e)
        {
            if (ItemCreated != null) ItemCreated(this, e);
        }

        /// <summary>
        /// Raises the <see cref="BeforeSelect"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs e)
        {
            if (BeforeSelect != null)
                BeforeSelect(this, e);
        }
        #endregion

        #region Properties
        private MenuTypes __menuType = MenuTypes.Horizontal;
        /// <summary>
        /// Gets or sets the type of menu displaying.
        /// </summary>
        public MenuTypes MenuType
        {
            get
            {
                return __menuType;
            }
            set
            {
                __menuType = value;
            }
        }

        private bool _generateDiv = true;
        public bool GenerateDiv
        {
            get { return _generateDiv; }
            set { _generateDiv = value; }
        }

        private bool _allowRead = true;
        /// <summary>
        /// Gets or sets the value that indicating whether current user have Read permission.
        /// </summary>
        public bool AllowRead
        {
            get
            {
                return _allowRead;
            }
            set
            {
                _allowRead = value;
            }
        }

        private string __rootItemId = "";
        /// <summary>
        /// Gets or sets the menu root item identifier.
        /// </summary>
        public string RootItemId
        {
            get
            {
                return __rootItemId;
            }
            set
            {
                __rootItemId = value;
            }
        }

        private string __parentItemIDFieldName = "";
        /// <summary>
        /// Gets or sets the name of  the field with primary key values of the parent menu items.
        /// </summary>
        public string ParentItemIDFieldName
        {
            get
            {
                return __parentItemIDFieldName;
            }
            set
            {
                __parentItemIDFieldName = value;
            }
        }

        private string __itemIDFieldName = "";
        /// <summary>
        /// Gets or sets the name of the field containing the unique ID that identifies the menu items.
        /// </summary>
        public string ItemIDFieldName
        {
            get
            {
                return __itemIDFieldName;
            }
            set
            {
                __itemIDFieldName = value;
            }
        }

        /// <summary>
        /// Gets the current processed <see cref="RepeaterItem"/>.
        /// </summary>
        protected internal MenuItemControl __currentItem;
        /// <summary>
        /// Gets the current processed <see cref="RepeaterItem"/>.
        /// </summary>
        public MenuItemControl CurrentItem
        {
            get
            {
                return __currentItem;
            }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        /// <summary>
        /// Gets or sets the total number of records.
        /// </summary>
        public int TotalRecords
        {
            get
            {
                if (ViewState["tr"] == null) return SelectArguments.TotalRowCount;
                return (int)ViewState["tr"];
            }
        }

        private int __itemCount;
        /// <summary>
        /// Gets or sets the number of currently proccesed records.
        /// </summary>
        public int ItemCount
        {
            get
            {
                return __itemCount;
            }
        }

        /// <summary>
        /// Gets value indicates whethere <see cref="Menu"/> datasource is empty, 
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return TotalRecords == 0;
            }
        }

        private int __itemLevel = 1;
        /// <summary>
        /// Gets level of current item.
        /// </summary>
        public int ItemLevel
        {
            get
            {
                return __itemLevel;
            }
        }

        private MenuItemList _items = new MenuItemList();
        /// <summary>
        /// Gets the collection of items in the list control.
        /// </summary>
        [
        DefaultValue(""),
        MergableProperty(false),
        PersistenceMode(PersistenceMode.InnerProperty),
        Editor("System.Web.UI.Design.WebControls.MenuItemCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))
        ]
        public MenuItemList Items
        {
            get
            {
                return _items;
            }
        }

        private MenuItem _currentMenuItem;
        public MenuItem CurrentMenuItem
        {
            get { return _currentMenuItem; }
            set { _currentMenuItem = value; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();
        }

        private MenuItemControl CreateItem(ref int itemIndex, MenuItemType templateType, System.Data.DataRowView dataItem)
        {
            MenuItemControl item = new MenuItemControl(itemIndex, this, templateType);
            __currentItem = item;
            item.EnableViewState = false;

            MenuItemEventArgs e = new MenuItemEventArgs(item);
            item.DataItem = _dataItem = new DataItem(dataItem);
            OnItemCreated(e);
            
            switch (templateType)
            {
                case MenuItemType.Header:
                    if (HeaderTemplate != null) { HeaderTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case MenuItemType.Footer:
                    if (FooterTemplate != null) { FooterTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case MenuItemType.Item:
                    if (ItemTemplate != null) { ItemTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case MenuItemType.CloseItem:
                    if (CloseItemTemplate != null) { CloseItemTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case MenuItemType.OpenLevel:
                    if (OpenLevelTemplate != null) { OpenLevelTemplate.InstantiateIn(item); itemIndex++; }
                    break;
                case MenuItemType.CloseLevel:
                    if (CloseLevelTemplate != null) { CloseLevelTemplate.InstantiateIn(item); itemIndex++; }
                    break;
            }
            Controls.Add(item);

            item.DataBind();
            OnItemDataBound(e);
            if (MenuItemType.Item == templateType)
                OnBeforeShowRow(e);
            item.DataItem = null;
            return item;
        }

        internal List<System.Data.DataRowView> innerDataSource;

        /// <summary>
        /// Retrieves data from the associated data source.
        /// </summary>
        protected override void PerformSelect()
        {
            if (DesignMode)
            {
                DataSourceID = "";
                DataSource = null;
            }
            base.PerformSelect();
        }

        /// <summary>
        /// Binds the specified data source to the <see cref="Menu"/> control.
        /// </summary>
        /// <param name="data">An object that represents the data source; the object must implement the <see cref="System.Collections.IEnumerable"/> interface.</param>
        protected override void PerformDataBinding(System.Collections.IEnumerable data)
        {
            base.PerformDataBinding(data);
            if (data != null && data.GetEnumerator().MoveNext())
            {
                __hasData = true;
                innerDataSource = new List<System.Data.DataRowView>();
                foreach (object item in data)
                    innerDataSource.Add((System.Data.DataRowView)item);
            }

            Controls.Clear();
            if (HasChildViewState)
                ClearChildViewState();

            CreateControlHierarchy();
            ChildControlsCreated = true;
        }

        /// <summary>
        /// Retrieve value indicated is item has child.
        /// </summary>
        /// <param name="ItemId">The ID value of item.</param>
        /// <returns>true, if item has childs; otherwise false.</returns>
        private bool IsHasChild(string ItemId)
        {
            if (innerDataSource != null)
            {
                string parrentId;
                for (int i = 0; i < innerDataSource.Count; i++)
                {
                    if (String.IsNullOrEmpty(ParentItemIDFieldName))
                        parrentId = innerDataSource[i][1].ToString();
                    else
                        parrentId = innerDataSource[i][ParentItemIDFieldName].ToString();
                    if (parrentId == ItemId || String.IsNullOrEmpty(ItemId) && String.IsNullOrEmpty(parrentId))
                        return true;
                }
            }
            else
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].Parent == ItemId || String.IsNullOrEmpty(ItemId) && String.IsNullOrEmpty(Items[i].Parent))
                        return true;
                }
            }
            return false;
        }

        internal void CreateOpenLevelItem(ref int index, ref bool isLevelOpen)
        {
            if (!isLevelOpen)
            {
                __itemLevel++;
                Attributes["Item_Level"] = __itemLevel.ToString();
                CreateItem(ref index, MenuItemType.OpenLevel, null);
                isLevelOpen = true;
            }
        }

        internal void CreateCloseLevelItem(ref int index, bool isLevelOpen)
        {
            if (isLevelOpen)
            {
                CreateItem(ref index, MenuItemType.CloseLevel, null);
                __itemLevel--;
                Attributes["Item_Level"] = __itemLevel.ToString();
            }
        }

        internal bool IsCurrentItemSelected(Url url)
        {
            return !String.IsNullOrEmpty(url.Address)
                && ResolveUrl(url.ToString()) == Page.Request.RawUrl
                || ResolveUrl(url.ToString()) == Page.Request.Url.ToString();
        }

        internal void SetItemParameters(MenuItemControl currentItemControl, MenuItem currentItem, ref int index, string level, bool isFirstLevelItem)
        {
            for (int j = 0; j < currentItemControl.Controls.Count; j++)
            {
                if (currentItemControl.Controls[j] is MTLink)
                {
                    MTLink ItemLink = (MTLink)(currentItemControl.Controls[j]);
                    if (currentItem != null)
                    {
                        if (!String.IsNullOrEmpty(currentItem.HrefSource))
                        {
                            ItemLink.HrefSource = Page.ResolveUrl(currentItem.HrefSource);
                        }
                        ItemLink.Value = currentItem.Caption;
                    }
                }
            }

            if (DesignMode
                || Items.Count == 0 && currentItem == null && !CreateLevel(level, ref index)
                || Items.Count > 0 && !CreateStaticLevel(level, ref index))
                CreateItem(ref index, MenuItemType.CloseItem, null);
        }

        internal bool CreateLevel(string Level, ref int index)
        {
            string parrentId;
            bool result = false;
            bool IsLevelOpen = (Level == RootItemId);

            for (int i = 0; i < innerDataSource.Count; i++)
            {
                if (String.IsNullOrEmpty(ParentItemIDFieldName))
                    parrentId = innerDataSource[i][1].ToString();
                else
                    parrentId = innerDataSource[i][ParentItemIDFieldName].ToString();

                if (string.IsNullOrEmpty(Level) || parrentId == Level || String.IsNullOrEmpty(Level) && String.IsNullOrEmpty(parrentId))
                {
                    result = true;
                    if (!String.IsNullOrEmpty(ParentItemIDFieldName))
                        CreateOpenLevelItem(ref index, ref IsLevelOpen);

                    string lvl;
                    if (String.IsNullOrEmpty(ItemIDFieldName))
                        lvl = innerDataSource[i][0].ToString();
                    else
                        lvl = innerDataSource[i][ItemIDFieldName].ToString();

                    Attributes["Submenu"] = (IsHasChild(lvl) ? "submenu" : null);

                    MenuItemControl CurrentItem = CreateItem(ref index, MenuItemType.Item, innerDataSource[i]);
                    innerDataSource.RemoveAt(i);
                    i--;
                    SetItemParameters(CurrentItem, null, ref index, lvl, Level == RootItemId);
                }
            }

            CreateCloseLevelItem(ref index, IsLevelOpen && Level != RootItemId);
            return result;
        }

        internal bool CreateStaticLevel(string Level, ref int index)
        {
            bool result = false;
            bool IsLevelOpen = (Level == RootItemId);
            string parrentId;

            for (int i = 0; i < Items.Count; i++)
            {
                CurrentMenuItem = Items[i];
                parrentId = Items[i].Parent;
                if (parrentId == Level || String.IsNullOrEmpty(Level) && String.IsNullOrEmpty(parrentId))
                {
                    result = true;
                    CreateOpenLevelItem(ref index, ref IsLevelOpen);

                    string lvl = Items[i].Name;
                    Attributes["Submenu"] = (IsHasChild(lvl) ? "submenu" : null);
                    Attributes["Target"] = Items[i].Target;
                    Attributes["Title"] = ControlsHelper.ReplaceResourcesAndStyles(Items[i].Title, Page);

                    MenuItemControl CurrentItem = CreateItem(ref index, MenuItemType.Item, null);
                    SetItemParameters(CurrentItem, Items[i], ref index, lvl, Level == RootItemId);
                }
            }

            CreateCloseLevelItem(ref index, IsLevelOpen && Level != RootItemId);
            return result;
        }

        /// <summary>
        /// Populates the Controls collection based on data source or view state.
        /// </summary>
        protected void CreateControlHierarchy()
        {
            switch (MenuType)
            {
                case MenuTypes.Horizontal: Attributes["MenuType"] = "menu_htb"; break;
                case MenuTypes.Vertical: Attributes["MenuType"] = "menu_vlr"; break;
                case MenuTypes.VerticalTree: Attributes["MenuType"] = "menu_vlr_tree"; break;
                case MenuTypes.VerticalRight: Attributes["MenuType"] = "menu_vrl"; break;
            }

            int index = 0;
            CreateItem(ref index, MenuItemType.Header, null);

            if (innerDataSource != null)
            {
                CreateLevel(RootItemId, ref index);
            }
            else if (Items.Count > 0)
            {
                CreateStaticLevel(RootItemId, ref index);
            }
            else if (DesignMode)
            {
                for (int i = 1; i < 4; i++)
                {
                    MenuItemControl CurrentItem = CreateItem(ref index, MenuItemType.Item, null);
                    for (int j = 0; j < CurrentItem.Controls.Count; j++)
                        if (CurrentItem.Controls[j] is MTLink)
                            ((MTLink)CurrentItem.Controls[j]).Value = "Menu Item " + i;
                    CreateItem(ref index, MenuItemType.CloseItem, null);
                }
            }

            CreateItem(ref index, MenuItemType.Footer, null);
            ViewState["itemsCreated"] = true;
        }

        /// <summary>
        /// Retrieves the <see cref="DataSourceView"/> object that is associated with the current form.
        /// </summary>
        /// <returns>The <see cref="DataSourceView"/> object that is associated with the current form.</returns>
        public DataSourceView GetDataSourceView()
        {
            Control c1 = Parent;
            IDataSource dataSource = null;
            while (dataSource == null && c1 != Page)
            {
                dataSource = c1.FindControl(DataSourceID) as IDataSource;
                c1 = c1.Parent;
            }
            return dataSource != null ? dataSource.GetView(DataMember) : null;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if (!DesignMode && (!Visible || Restricted && !UserRights.AllowRead)) return;

            Dictionary<string, string> TmpAttrs = new Dictionary<string, string>();
            foreach (string attr in Attributes.Keys)
                TmpAttrs.Add(attr, Attributes[attr]);
            if (_generateDiv)
            {
                writer.WriteBeginTag("div");
                writer.WriteAttribute("id", ClientID);
                writer.Write(HtmlTextWriter.TagRightChar);
            }
            for (int i = 0; i < Controls.Count; i++)
            {
                if (Controls[i] is MenuItemControl)
                    foreach (string attr in ((MenuItemControl)Controls[i]).Attributes.Keys)
                        Attributes[attr] = ((MenuItemControl)Controls[i]).Attributes[attr];

                Controls[i].RenderControl(writer);
                foreach (string attr in TmpAttrs.Keys)
                    Attributes[attr] = TmpAttrs[attr];
            }
            if (_generateDiv)
                writer.WriteEndTag("div");
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            if (!Visible) return;
            base.OnDataBinding(e);
        }
        #endregion

        #region IForm Members
        /// <summary>
        /// Current processed <see cref="DataItem"/>.
        /// </summary>
        protected internal DataItem _dataItem;
        /// <summary>
        /// Gets the current <see cref="DataItem"/>.
        /// </summary>
        public DataItem DataItem
        {
            get
            {
                return _dataItem;
            }
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            List<MenuItemControl> rows = new List<MenuItemControl>();
            if (Controls[0] != null && ((MenuItemControl)Controls[0]).ItemType == MenuItemType.Header)
                rows.Add((MenuItemControl)Controls[0]);
            if (Controls[Controls.Count - 1] != null && ((MenuItemControl)Controls[Controls.Count - 1]).ItemType == MenuItemType.Footer)
                rows.Add((MenuItemControl)Controls[Controls.Count - 1]);
            if (__currentItem != null) rows.Add(__currentItem);
            foreach (MenuItemControl ri in rows)
            {
                ControlsHelper.PopulateControlCollection<T>(result, ri.Controls);
            }
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            if (Controls.Count == 0) return null;
            List<MenuItemControl> rows = new List<MenuItemControl>();
            if (Controls[0] != null && ((MenuItemControl)Controls[0]).ItemType == MenuItemType.Header)
                rows.Add((MenuItemControl)Controls[0]);
            if (Controls[Controls.Count - 1] != null && ((MenuItemControl)Controls[Controls.Count - 1]).ItemType == MenuItemType.Footer)
                rows.Add((MenuItemControl)Controls[Controls.Count - 1]);
            if (__currentItem != null) rows.Add(__currentItem);
            foreach (MenuItemControl ri in rows)
            {
                T ctrl = (T)ri.FindControl(id);
                if (ctrl != null) return ctrl;
            }
            return null;
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        private FormSupportedOperations _userRights;
        /// <summary>
        /// Gets the <see cref="FormSupportedOperations"/> for the current form.
        /// </summary>
        public FormSupportedOperations UserRights
        {
            get
            {
                if (_userRights == null)
                    _userRights = new FormSupportedOperations();
                return _userRights;
            }
        }

        private bool _restricted;
        /// <summary>
        /// Gets or sets the value indicating whether form will restrict the user access according to the <see cref="UserRights"/>.
        /// </summary>
        public bool Restricted
        {
            get
            {
                return _restricted;
            }
            set
            {
                _restricted = value;
            }
        }

        private bool __hasData;
        /// <summary>
        /// Gets value indicating whether form has non-empty data object.
        /// </summary>
        public virtual bool HasData
        {
            get { return __hasData; }
        }
        #endregion

        #region Templates
        private ITemplate _headerTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the header section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return _headerTemplate;
            }
            set
            {
                _headerTemplate = value;
            }
        }

        private ITemplate _footerTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the footer section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate FooterTemplate
        {
            get
            {
                return _footerTemplate;
            }
            set
            {
                _footerTemplate = value;
            }
        }

        private ITemplate _itemTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the item section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate ItemTemplate
        {
            get
            {
                return _itemTemplate;
            }
            set
            {
                _itemTemplate = value;
            }
        }

        private ITemplate _closeItemTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the ClodeItem section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate CloseItemTemplate
        {
            get
            {
                return _closeItemTemplate;
            }
            set
            {
                _closeItemTemplate = value;
            }
        }

        private ITemplate _openLevelTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the OpenLevel section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate OpenLevelTemplate
        {
            get
            {
                return _openLevelTemplate;
            }
            set
            {
                _openLevelTemplate = value;
            }
        }

        private ITemplate _closeLevelTemplate;
        /// <summary>
        /// Gets or sets the <see cref="ITemplate"/> that defines how the CloseLevel section of <see cref="Menu"/> control are displayed. 
        /// </summary>
        [
        Browsable(false),
        DefaultValue(null),
        PersistenceMode(PersistenceMode.InnerProperty),
        ]
        public virtual ITemplate CloseLevelTemplate
        {
            get
            {
                return _closeLevelTemplate;
            }
            set
            {
                _closeLevelTemplate = value;
            }
        }


        #endregion

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterArrayDeclaration();
        }

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected virtual void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Controls.Count, "", ""));
            }
        }

        private static MenuItemControl GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is MenuItemControl) && c != null)
                c = c.NamingContainer;
            return c as MenuItemControl;
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            MenuItemControl ri = GetControlContainer(control);
            string arrayName = "";
            switch (ri.ItemType)
            {
                case MenuItemType.Footer:
                    arrayName = ID + "Controls_Footer";
                    break;
                case MenuItemType.Header:
                    arrayName = ID + "Controls_Header";
                    break;
                default:
                    arrayName = ID + "Controls_Row" + ri.ItemIndex.ToString();
                    break;
            }
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration(arrayName, String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="Menu"/> control.
    /// </summary>
    class MenuDesigner : MTFormDesigner
    {
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            BeforeDataBind += new EventHandler<EventArgs>(MenuDesigner_BeforeDataBind);
        }

        void MenuDesigner_BeforeDataBind(object sender, EventArgs e)
        {
            string menuType = "";
            switch (((Menu)Component).MenuType)
            {
                case MenuTypes.Horizontal: menuType = "menu_htb"; break;
                case MenuTypes.Vertical: menuType = "menu_vlr"; break;
                case MenuTypes.VerticalTree: menuType = "menu_vlr_tree"; break;
            }

            for (int i = 0; i < TemplateGroups.Count; i++)
            {
                for (int j = 0; j < TemplateGroups[i].Templates.Length; j++)
                {
                    TemplateGroups[i].Templates[j].Content = Regex.Replace(TemplateGroups[i].Templates[j].Content, "<%=\\s*" + ((Control)Component).ClientID + "\\.Attributes\\s*\\[\\s*\"MenuType\"\\s*\\]\\s*%>", menuType);
                    TemplateGroups[i].Templates[j].Content = Regex.Replace(TemplateGroups[i].Templates[j].Content, "<%=\\s*" + ((Control)Component).ClientID + "\\.Attributes\\s*\\[\\s*\"Item_Level\"\\s*\\]\\s*%>", "1");
                }
            }
        }

        /// <summary>
        /// Gets a collection of template groups, each containing one or more template definitions.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                if (__TemplateGroups == null)
                {
                    Menu ctrl = (Menu)Component;
                    __TemplateGroups = base.TemplateGroups;

                    TemplateGroup tempGroup;
                    TemplateDefinition tempDef;

                    tempGroup = new TemplateGroup("Common Templates");
                    tempDef = new TemplateDefinition(this, "FooterTemplate", ctrl, "FooterTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "HeaderTemplate", ctrl, "HeaderTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Item Templates");
                    tempDef = new TemplateDefinition(this, "ItemTemplate", ctrl, "ItemTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "CloseItemTemplate", ctrl, "CloseItemTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);

                    tempGroup = new TemplateGroup("Level Templates");
                    tempDef = new TemplateDefinition(this, "OpenLevelTemplate", ctrl, "OpenLevelTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);

                    tempDef = new TemplateDefinition(this, "CloseLevelTemplate", ctrl, "CloseLevelTemplate", false);
                    tempGroup.AddTemplateDefinition(tempDef);
                    __TemplateGroups.Add(tempGroup);
                }
                return __TemplateGroups;
            }
        }
    }
}