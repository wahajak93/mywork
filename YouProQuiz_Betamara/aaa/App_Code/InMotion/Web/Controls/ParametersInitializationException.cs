//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// The exception that is thrown when parameters of DataCommand are not initialized properly. For example required parameters are not passed.
    /// </summary>
    public class ParametersInitializationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the ArgumentException class with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public ParametersInitializationException(string message)
        {
            _Message = message;
        }

        string _Message = "";

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public override string Message
        {
            get
            {
                return _Message;
            }
        }
    }
}
