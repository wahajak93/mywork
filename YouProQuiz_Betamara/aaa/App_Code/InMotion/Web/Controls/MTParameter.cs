//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides a mechanism that controls use to bind parameters.
    /// </summary>
    public class MTParameter : IStateManager
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MTParameter"/> class.
        /// </summary>
        /// <param name="name">The name of parameter.</param>
        /// <param name="value">The value of parameter.</param>
        public MTParameter(string name, string value)
        {
            this._name = name;
            this._value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTParameter"/> class.
        /// </summary>
        /// <param name="name">The name of parameter.</param>
        public MTParameter(string name)
            : this(name, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTParameter"/> class.
        /// </summary>
        public MTParameter()
            : this(null, null)
        {
        }
        #endregion

        #region Properties
        private string _name;
        /// <summary>
        /// Gets or sets parameter name.
        /// </summary>
        [DefaultValue("")]
        public string Name
        {
            get
            {
                if (_name == null)
                    _name = String.Empty;
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _value;
        /// <summary>
        /// Gets or sets parameter value.
        /// </summary>
        [DefaultValue("")]
        [PersistenceMode(PersistenceMode.EncodedInnerDefaultProperty)]
        public string Value
        {
            get
            {
                if (_value == null)
                    _value = String.Empty;
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a <see cref="String"/> that represents value of current parameter.
        /// </summary>
        /// <returns>A <see cref="String"/> that represents value of current parameter.</returns>
        public override string ToString()
        {
            return Value;
        }
        #endregion

        #region IStateManager Members
        private bool _isTrackingViewState = false;
        bool IStateManager.IsTrackingViewState
        {
            get
            {
                return _isTrackingViewState;
            }
        }

        void IStateManager.TrackViewState()
        {
            _isTrackingViewState = true;
        }

        object IStateManager.SaveViewState()
        {
            return new Pair(Name, Value);
        }

        void IStateManager.LoadViewState(object state)
        {
            if (state is Pair)
            {
                Pair tv = (Pair)state;
                if (tv.First != null)
                    Name = (string)tv.First;

                if (tv.Second != null)
                    Value = (string)tv.Second;
            }
        }
        #endregion
    }
}
