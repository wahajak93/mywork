//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Interface provide InMotion control with attributes collection.
    /// </summary>
    public interface IMTAttributeAccessor : IAttributeAccessor
    {
        /// <summary>
        /// Gets the collection of arbitrary attributes (for rendering only) that do not correspond to properties on the control.
        /// </summary>
        AttributeCollection Attributes { get; }
    }
}