//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Displays a push button control on the Web page.
    /// </summary>
    public class MTButton : Button, IMTButtonControl, IMTAttributeAccessor
    {
        #region IMTControl Members

        /// <summary>
        /// Occurs after the <see cref="MTButton"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        #endregion


        private Url __RedirectUrl = new Url("");
        /// <summary>
        /// Gets or sets the url to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Url RedirectUrl
        {
            get
            {
                return __RedirectUrl;
            }
            set { __RedirectUrl = value; }
        }

        private bool _PerformRedirect = true;
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        public bool PerformRedirect
        {
            get { return _PerformRedirect; }
            set { _PerformRedirect = value; }
        }

        /// <summary>
        /// Gets or sets the page name to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public string ReturnPage
        {
            get
            {
                return RedirectUrl.ToString();
            }
            set
            {
                RedirectUrl.Reset();
                RedirectUrl.Address = this.ResolveUrl(value);
            }
        }

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get
            {
                return RedirectUrl.RemoveParameters;
            }
            set { RedirectUrl.RemoveParameters = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get
            {
                return RedirectUrl.Type;
            }
            set { RedirectUrl.Type = value; }
        }
        private bool __EnableValidation = true;
        /// <summary>
        /// Gets or sets the value indicating whether form should validate user input when this button is pressed.
        /// </summary>
        public bool EnableValidation
        {
            get
            {
                return __EnableValidation;
            }
            set { __EnableValidation = value; }
        }

        private PreserveParameterType __PreserveParameters = PreserveParameterType.None;
        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get
            {
                return __PreserveParameters;
            }
            set { __PreserveParameters = value; }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);
            else if (Parent is MTPanel)
                ((MTPanel)Parent).RegisterClientControl(this);
            else if (Page is MTPage)
                ((MTPage)Page).RegisterClientControl(this);
            Text = ControlsHelper.ReplaceResourcesAndStyles(Text, Page);
            OnClientClick = ControlsHelper.ReplaceResourcesAndStyles(OnClientClick, Page);
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (TemplateControl.Parent != null && TemplateControl.Parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)this.TemplateControl.Parent).MasterPageFile))
                AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            base.Render(writer);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (DesignMode) return;
            if (OwnerForm != null && OwnerForm is Record)
            {

                Record r = (Record)OwnerForm;
                if (String.Compare(CommandName, "insert", StringComparison.OrdinalIgnoreCase) == 0 && !r.IsInsertMode)
                    Visible = false;
                if ((String.Compare(CommandName, "update", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(CommandName, "delete", StringComparison.OrdinalIgnoreCase) == 0) && r.IsInsertMode)
                    Visible = false;
                if (String.Compare(CommandName, "insert", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowInsert && r.Restricted || !r.AllowInsert))
                    Visible = false;
                if (String.Compare(CommandName, "delete", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowDelete && r.Restricted || !r.AllowDelete))
                    Visible = false;
                if (String.Compare(CommandName, "update", StringComparison.OrdinalIgnoreCase) == 0 && (!r.UserRights.AllowUpdate && r.Restricted || !r.AllowUpdate))
                    Visible = false;
            }
            else if (OwnerForm != null && OwnerForm is EditableGrid)
            {
                EditableGrid r = (EditableGrid)OwnerForm;
                if (String.Compare(CommandName, "submit", StringComparison.OrdinalIgnoreCase) == 0 &&
                    (!(r.UserRights.AllowUpdate || r.UserRights.AllowInsert || r.UserRights.AllowDelete) && r.Restricted || !r.AllowUpdate && !r.AllowDelete && !r.AllowInsert))
                    Visible = false;

            }
            OnBeforeShow(EventArgs.Empty);
        }
    }
}