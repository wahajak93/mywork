//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines a service implemented by objects to provide errors information.
    /// </summary>
    public interface IErrorProducer
    {
        /// <summary>
        /// Gets or sets the ID of control that used for displaying errors text.
        /// </summary>
        string ErrorControl { get;set;}
        /// <summary>
        /// Gets the <see cref="Collection&lt;T&gt;"/> that contain list of errors messages.
        /// </summary>
        ControlErrorCollection Errors { get;}
    }
}
