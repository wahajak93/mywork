//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace InMotion.Web.Controls
{
    internal sealed class DummyDataSource:ICollection,IEnumerable
    {
        
        private class DummyEnumerator:IEnumerator
        {
            private int count;
            private int index = -1;
            public DummyEnumerator(int count)
            {
                this.count = count;    
            }

            public object Current
            {
                get { return null; }
            }

            public bool MoveNext()
            {
                index++;
                return index < count;
            }

            public void Reset()
            {
                index = -1;
            }
        }
        private int count;
        public DummyDataSource(int count)
        {
            this.count = count;
        }
        public void CopyTo(Array array, int index)
        {
            IEnumerator ie = GetEnumerator();
            while (ie.MoveNext())
            {
                index++;
                array.SetValue(ie.Current, index);
                
            }
        }

        public int Count
        {
            get { return count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }


        public IEnumerator GetEnumerator()
        {
            return new DummyEnumerator(count);
        }

        

        
    }
}
