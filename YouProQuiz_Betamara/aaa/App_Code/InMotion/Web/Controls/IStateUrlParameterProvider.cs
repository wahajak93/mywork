//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    internal interface IStateUrlParameterProvider:IMTControl
    {
        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> of state parameters for preserving in query string.
        /// </summary>
        UrlParameterCollection PreserveStateParameters { get;}
        /// <summary>
        /// Gets the name of state parameter for remove from query string.
        /// </summary>
        string RemoveStateParameters { get; }
    }
}
