using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Summary description for QuadraticPaths
    /// </summary>
    class QuadraticPaths
    {
        private int _maxX;
        private int _maxY;
        private int _minX;
        private int _minY;
        private List<QuadraticPath> _paths;
        private bool _needNewMetrics;
        public int MaxX
        {
            get { return _maxX; }
            set { _maxX = value; }
        }
        public int MaxY
        {
            get { return _maxY; }
            set { _maxY = value; }
        }
        public int MinX
        {
            get { return _minX; }
            set { _minX = value; }
        }
        public int MinY
        {
            get { return _minY; }
            set { _minY = value; }
        }
        public List<QuadraticPath> Paths
        {
            get { return _paths; }
        }
        public QuadraticPaths()
        {
            _paths = new List<QuadraticPath>();
            _needNewMetrics = true;
        }
        public void LoadFromString(string str)
        {
            string[] c = str.Split(',');
            for (int i = 1; i < c.Length; i += 6)
                AddPath(int.Parse(c[i]), int.Parse(c[i + 1]), int.Parse(c[i + 2]), int.Parse(c[i + 3]), int.Parse(c[i + 4]), int.Parse(c[i + 5]));
        }
        public override string ToString()
        {
            int i;
            string res = "";
            for (i = 0; i < _paths.Count; i++)
            {
                res += _paths[i].ToString() + ",";
            }
            return res.Substring(0, res.Length - 1);            
        }
        public void AddPath(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            _paths.Add(new QuadraticPath(x1, y1, x2, y2, x3, y3));

            if (_needNewMetrics)
            {
                _maxX = Math.Max(x1, x3);
                _maxY = Math.Max(y1, y3);
                _minX = Math.Min(x1, x3);
                _minY = Math.Min(y1, y3);
                _needNewMetrics = false;
            }
            else
            {
                _maxX = Math.Max(x1, Math.Max(x3, _maxX));
                _maxY = Math.Max(y1, Math.Max(y3, _maxY));
                _minX = Math.Min(x1, Math.Min(x3, _minX));
                _minY = Math.Min(y1, Math.Min(y3, _minY));
            }
        }
        public void AddPaths(QuadraticPaths p)
        {
            for (int i = 0; i < p._paths.Count; i++)
                AddPath(p._paths[i].X1, p._paths[i].Y1, p._paths[i].X2, p._paths[i].Y2, p._paths[i].X3, p._paths[i].Y3);
        }
        public void Normalize(int mx, int my)
        {
            int mix = _minX, miy = _minY;
            float kx = mx / ((float)(_maxX - _minX));
            float ky = my / ((float)(_maxY - _minY));
            if (mx == 0)
                kx = ky;
            if (my == 0)
                ky = kx;
            _needNewMetrics = true;
            for (int i = 0; i < _paths.Count; i++)
            {
                _paths[i].X1 = (int)((_paths[i].X1 - mix) * kx);
                _paths[i].Y1 = (int)((_paths[i].Y1 - miy) * ky);
                _paths[i].X2 = (int)((_paths[i].X2 - mix) * kx);
                _paths[i].Y2 = (int)((_paths[i].Y2 - miy) * ky);
                _paths[i].X3 = (int)((_paths[i].X3 - mix) * kx);
                _paths[i].Y3 = (int)((_paths[i].Y3 - miy) * ky);

                if (_needNewMetrics)
                {
                    MaxX = Math.Max(_paths[i].X1, _paths[i].X3);
                    MaxY = Math.Max(_paths[i].Y1, _paths[i].Y3);
                    MinX = Math.Min(_paths[i].X1, _paths[i].X3);
                    MinY = Math.Min(_paths[i].Y1, _paths[i].Y3);
                    _needNewMetrics = false;
                }
                else
                {
                    MaxX = Math.Max(_paths[i].X1, Math.Max(_paths[i].X3, MaxX));
                    MaxY = Math.Max(_paths[i].Y1, Math.Max(_paths[i].Y3, MaxY));
                    MinX = Math.Min(_paths[i].X1, Math.Min(_paths[i].X3, MinX));
                    MinY = Math.Min(_paths[i].Y1, Math.Min(_paths[i].Y3, MinY));
                }
            }
        }
        public void Multiply(int mx, int my)
        {
            int t, i;
            for (i = 0; i < _paths.Count; i++)
            {
                _paths[i].X1 = _paths[i].X1 * mx;
                _paths[i].Y1 = _paths[i].Y1 * my;
                _paths[i].X2 = _paths[i].X2 * mx;
                _paths[i].Y2 = _paths[i].Y2 * my;
                _paths[i].X3 = _paths[i].X3 * mx;
                _paths[i].Y3 = _paths[i].Y3 * my;
            }
            _maxX = _maxX * mx;
            _maxY = _maxY * my;
            _minX = _minX * mx;
            _minY = _minY * my;
            if (_maxX < _minX)
            {
                t = _maxX;
                _maxX = _minX;
                _minX = t;
            }
            if (_maxY < _minY)
            {
                t = _maxY;
                _maxY = _minY;
                _minY = t;
            }
        }
        public void Rotate(int deg)
        {
            _needNewMetrics = true;
            double ang = deg * Math.PI / 180;
            double angSin = Math.Sin(ang), angCos = Math.Cos(ang);
            int i, tx, ty;
            for (i = 0; i < _paths.Count; i++)
            {
                tx = (int)(_paths[i].X1 * angCos - _paths[i].Y1 * angSin);
                ty = (int)(_paths[i].X1 * angSin + _paths[i].Y1 * angCos);
                _paths[i].X1 = tx;
                _paths[i].Y1 = ty;
                tx = (int)(_paths[i].X2 * angCos - _paths[i].Y2 * angSin);
                ty = (int)(_paths[i].X2 * angSin + _paths[i].Y2 * angCos);
                _paths[i].X2 = tx;
                _paths[i].Y2 = ty;
                tx = (int)(_paths[i].X3 * angCos - _paths[i].Y3 * angSin);
                ty = (int)(_paths[i].X3 * angSin + _paths[i].Y3 * angCos);
                _paths[i].X3 = tx;
                _paths[i].Y3 = ty;

                if (_needNewMetrics)
                {
                    MaxX = Math.Max(_paths[i].X1, _paths[i].X3);
                    MaxY = Math.Max(_paths[i].Y1, _paths[i].Y3);
                    MinX = Math.Min(_paths[i].X1, _paths[i].X3);
                    MinY = Math.Min(_paths[i].Y1, _paths[i].Y3);
                    _needNewMetrics = false;
                }
                else
                {
                    MaxX = Math.Max(_paths[i].X1, Math.Max(_paths[i].X3, MaxX));
                    MaxY = Math.Max(_paths[i].Y1, Math.Max(_paths[i].Y3, MaxY));
                    MinX = Math.Min(_paths[i].X1, Math.Min(_paths[i].X3, MinX));
                    MinY = Math.Min(_paths[i].Y1, Math.Min(_paths[i].Y3, MinY));
                }
            }
        }
        public void Wave(double w)
        {
            int i;
            double dx = (_maxX - _minX) * w;
            double dy = (_maxY - _minY) / 1;
            double omega = _minX;
            _needNewMetrics = false;
            for (i = 0; i < _paths.Count; i++)
            {
                _paths[i].X1 += (int)(dx * Math.Cos(Math.PI * (_paths[i].Y1 - omega) / dy));
                _paths[i].X2 += (int)(dx * Math.Cos(Math.PI * (_paths[i].Y2 - omega) / dy));
                _paths[i].X3 += (int)(dx * Math.Cos(Math.PI * (_paths[i].Y3 - omega) / dy));
                if (_needNewMetrics)
                {
                    MaxX = Math.Max(_paths[i].X1, _paths[i].X3);
                    MaxY = Math.Max(_paths[i].Y1, _paths[i].Y3);
                    MinX = Math.Min(_paths[i].X1, _paths[i].X3);
                    MinY = Math.Min(_paths[i].Y1, _paths[i].Y3);
                    _needNewMetrics = false;
                }
                else
                {
                    MaxX = Math.Max(_paths[i].X1, Math.Max(_paths[i].X3, MaxX));
                    MaxY = Math.Max(_paths[i].Y1, Math.Max(_paths[i].Y3, MaxY));
                    MinX = Math.Min(_paths[i].X1, Math.Min(_paths[i].X3, MinX));
                    MinY = Math.Min(_paths[i].Y1, Math.Min(_paths[i].Y3, MinY));
                }
            }
        }
        public void Centralize()
        {
            int cx, cy, i;
            cx = (_minX + _maxX) / 2;
            cy = (_minY + _maxY) / 2;
            for (i = 0; i < _paths.Count; i++)
            {
                _paths[i].X1 = _paths[i].X1 - cx;
                _paths[i].Y1 = _paths[i].Y1 - cy;
                _paths[i].X2 = _paths[i].X2 - cx;
                _paths[i].Y2 = _paths[i].Y2 - cy;
                _paths[i].X3 = _paths[i].X3 - cx;
                _paths[i].Y3 = _paths[i].Y3 - cy;
            }
            _maxX -= cx;
            _maxY -= cy;
            _minX -= cx;
            _minY -= cy;
        }
        public void Addition(int cx, int cy)
        {
            for (int i = 0; i < _paths.Count; i++)
            {
                _paths[i].X1 += cx;
                _paths[i].Y1 += cy;
                _paths[i].X2 += cx;
                _paths[i].Y2 += cy;
                _paths[i].X3 += cx;
                _paths[i].Y3 += cy;
            }
            _maxX += cx;
            _maxY += cy;
            _minX += cx;
            _minY += cy;
        }
        public void Broke(int dx, int dy)
        {
            _needNewMetrics = true;
            Random r = new Random();
            int rx, ry, i;
            for (i = 0; i < _paths.Count; i++)
            {
                rx = r.Next(-dx, dx);
                ry = r.Next(-dy, dy);
                _paths[i].X1 += rx;
                _paths[i].Y1 += ry;
                rx = r.Next(-dx, dx);
                ry = r.Next(-dy, dy);
                _paths[i].X3 += rx;
                _paths[i].Y3 += ry;
                if (_needNewMetrics)
                {
                    MaxX = Math.Max(_paths[i].X1, _paths[i].X3);
                    MaxY = Math.Max(_paths[i].Y1, _paths[i].Y3);
                    MinX = Math.Min(_paths[i].X1, _paths[i].X3);
                    MinY = Math.Min(_paths[i].Y1, _paths[i].Y3);
                    _needNewMetrics = false;
                }
                else
                {
                    MaxX = Math.Max(_paths[i].X1, Math.Max(_paths[i].X3, MaxX));
                    MaxY = Math.Max(_paths[i].Y1, Math.Max(_paths[i].Y3, MaxY));
                    MinX = Math.Min(_paths[i].X1, Math.Min(_paths[i].X3, MinX));
                    MinY = Math.Min(_paths[i].Y1, Math.Min(_paths[i].Y3, MinY));
                }
            }
        }
        public void Noises(int n)
        {
            Random r = new Random();
            int x1, y1, x2, y2, x3, y3;
            for (int i = 0; i < n; i++)
            {
                x1 = r.Next(_minX, _maxX);
                y1 = r.Next(_minY, _maxY);
                x3 = r.Next(_minX, _maxX);
                y3 = r.Next(_minY, _maxY);
                x2 = r.Next(Math.Min(x1, x3), Math.Max(x1, x3));
                y2 = r.Next(Math.Min(y1, y3), Math.Max(y1, y3));
                AddPath(x1, y1, x2, y2, x3, y3);
            }
        }
        public void Mix()
        {
            Random r = new Random();
            int i, j;
            QuadraticPath t;
            for (i = 0; i < _paths.Count; i++)
            {
                j = r.Next(_paths.Count);
                t = _paths[i];
                _paths[i] = _paths[j];
                _paths[j] = t;
            }
        }
    }
}