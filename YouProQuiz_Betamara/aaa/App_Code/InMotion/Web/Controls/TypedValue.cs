//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Common;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Repersent a value with associated <see cref="DataType"/> and format string.
    /// </summary>
    public class TypedValue
    {
        private object _Value;
        /// <summary>
        /// The value.
        /// </summary>
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        private DataType _DataType;
        /// <summary>
        /// The <see cref="DataType"/> of value.
        /// </summary>
        public DataType DataType
        {
            get { return _DataType; }
            set { _DataType = value; }
        }
        private string _Format;
        /// <summary>
        /// The format string for the value.
        /// </summary>
        public string Format
        {
            get { return _Format; }
            set { _Format = value; }
        }

        private bool _isEmpty;

        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        public bool IsEmpty
        {
            get { return _isEmpty; }
            set { _isEmpty = value; }
        }
	

        /// <summary>
        /// Returns a string representation of current value.
        /// </summary>
        /// <returns>A string that represent current value.</returns>
        public override string ToString()
        {
            if (Value is IMTField) return ((IMTField)Value).ToString(Format);
            return Value.ToString();
        }
    }
}