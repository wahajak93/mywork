//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Text;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a collection of Url parameters. 
    /// Provides methods for persists collection into the string.
    /// </summary>
    public class UrlParameterCollection : InMotion.Web.Controls.ParameterCollection
    {
        /// <summary>
        /// Creates a UrlParameter object with the specified name and value, 
        /// and appends it to the end of the collection.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter.</param>
        /// <param name="isPreserved">Shows that parameter is preserved and RemoveParameters filter will be applied.</param>
        /// <returns>The index value of the added item.</returns>
        public int Add(string name, string value, bool isPreserved)
        {
            int idx = this.Add(name, value);
            UrlParameter p = (UrlParameter)this[idx];
            p.IsPreserved = isPreserved;
            return idx;
        }

        /// <summary>
        /// Copies the entries in the specified <see cref="UrlParameterCollection"/> to the current <see cref="UrlParameterCollection"/>. 
        /// </summary>
        /// <param name="parameters">The UrlParameterCollection to copy to the current UrlParameterCollection.</param>
        public void Add(UrlParameterCollection parameters)
        {
            foreach (UrlParameter up in parameters)
                this[up.Name] = up;
        }

        /// <summary>
        /// Creates a UrlParameter object with the specified name and value, 
        /// and appends it to the end of the collection.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter.</param>
        /// <returns>The index value of the added item.</returns>
        public int Add(string name, string value)
        {
            UrlParameter p = new UrlParameter();
            p.DataType = InMotion.Common.DataType.Text;
            p.Name = name;
            p.Source = value;
            p.SourceType = UrlParameterSourceType.Expression;
            p.DefaultValue = "";
            p.Evaluate();
            return ((IList)this).Add(p);
        }

        /// <summary>
        /// Creates a UrlParameter object with the specified name, 
        /// data type, source type, source and default value, 
        /// and appends it to the end of the collection.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="type">The <see cref="InMotion.Common.DataType"/> of the parameter.</param>
        /// <param name="sourceType">The <see cref="UrlParameterSourceType"/> of the parameter.</param>
        /// <param name="source">The source of the parameter.</param>
        /// <param name="defaultValue">A object that serves as a default value for the parameter.</param>
        /// <returns>The index value of the added item.</returns>
        public int Add(string name, InMotion.Common.DataType type, UrlParameterSourceType sourceType, string source, object defaultValue)
        {
            UrlParameter p = new UrlParameter();
            p.Name = name;
            p.DataType = type;
            p.Source = source;
            p.SourceType = sourceType;
            p.DefaultValue = defaultValue;
            return ((IList)this).Add(p);
        }

        private static readonly Type[] _typeOfParameters =
            new Type[] { typeof(UrlParameter) };

        /// <summary>
        /// Gets an array of Parameter types that the ParameterCollection collection can contain.
        /// </summary>
        /// <returns>An array of Parameter types that the ParameterCollection collection can contain.</returns>
        protected override Type[] GetKnownTypes()
        {
            return _typeOfParameters;
        }

        /// <summary>
        /// Creates an instance of a default Parameter object.
        /// </summary>
        /// <param name="index">The index of the type of Parameter to create from the ordered list of types returned by GetKnownTypes.</param>
        /// <returns>A default instance of a Parameter.</returns>
        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new UrlParameter();
                default:
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        Trace.TraceError("Unknown Type.\n{0}", Environment.StackTrace);
                    throw new ArgumentOutOfRangeException("Unknown Type");
            }
        }

        /// <summary>
        /// Returns a <see cref="string"/> representation of collection.
        /// </summary>
        /// <returns>A <see cref="string"/> representation of collection.</returns>
        public override string ToString()
        {
            return ToString(PreserveParameterType.None, "");
        }

        /// <summary>
        /// Returns a <see cref="string"/> representation of collection.
        /// </summary>
        /// <param name="preserveParameters">The type of parameters to preserve.</param>
        /// <returns>A <see cref="string"/> representation of collection.</returns>
        public string ToString(PreserveParameterType preserveParameters)
        {
            return ToString(preserveParameters, "");
        }
        
        /// <summary>
        /// Returns a value indicating whether parameter with this name exists in collection.
        /// </summary>
        /// <param name="name">The name of parameter.</param>
        /// <returns>true if the parameter exists in collection; otherwise false.</returns>
        private bool ParameterExists(string name)
        {
            if (name == null) name = "";
            name = name.ToUpper(CultureInfo.CurrentCulture);
            for (int i = 0; i < this.Count; i++)
            {
                UrlParameter p = (UrlParameter)this[i];
                if (String.Compare(p.Name, name, StringComparison.OrdinalIgnoreCase) == 0) return true;
            }
            return false;
        }

        /// <summary>
        /// Return a string representation of collection.
        /// </summary>
        /// <param name="preserveParameters">The type of parameters to preserve.</param>
        /// <param name="removeParameters">Semicolon-separated list of parameters that should be removed.</param>
        /// <returns>A <see cref="string"/> representation of collection.</returns>
        public string ToString(PreserveParameterType preserveParameters, string removeParameters)
        {
            if (HttpContext.Current != null)
            {
                HttpRequest Request = HttpContext.Current.Request;
                HttpServerUtility Server = HttpContext.Current.Server;

                StringBuilder sb = new StringBuilder();
                bool PreserveGet = (preserveParameters == PreserveParameterType.Get) || (preserveParameters == PreserveParameterType.GetAndPost);
                bool PreservePost = (preserveParameters == PreserveParameterType.Post) || (preserveParameters == PreserveParameterType.GetAndPost);

                string[] List;
                if (String.IsNullOrEmpty(removeParameters))
                    List = new string[1];
                else
                    List = removeParameters.Split(new Char[] { ';' });

                for (int i = 0; i < this.Count; i++)
                {
                    UrlParameter p = (UrlParameter)this[i];
                    if (!p.IsPreserved || Array.IndexOf(List, p.Name) == -1)
                    {
                        p.Evaluate();
                        sb.Append(Server.UrlEncode(p.Name));
                        sb.Append("=");
                        sb.Append(Server.UrlEncode(p.ToString()));
                        sb.Append("&");
                    }
                }

                if (PreserveGet)
                {
                    for (int i = 0; i < Request.QueryString.Count; i++)
                    {
                        if (Array.IndexOf(List, Request.QueryString.AllKeys[i]) < 0 && !ParameterExists(Request.QueryString.AllKeys[i]))
                            foreach (string val in Request.QueryString.GetValues(i))
                            {
                                sb.Append(Server.UrlEncode(Request.QueryString.AllKeys[i]));
                                sb.Append("=");
                                sb.Append(Server.UrlEncode(val));
                                sb.Append("&");
                            }
                    }
                }
                if (PreservePost)
                {
                    for (int i = 0; i < Request.Form.Count; i++)
                    {
                        if (Array.IndexOf(List, Request.Form.AllKeys[i]) < 0
                            && Request.Form.AllKeys[i] != "__EVENTTARGET"
                            && Request.Form.AllKeys[i] != "__EVENTARGUMENT"
                            && Request.Form.AllKeys[i] != "__EVENTVALIDATION"
                            && Request.Form.AllKeys[i] != "__VIEWSTATE"
                            && !ParameterExists(Request.Form.AllKeys[i]))
                            foreach (string val in Request.Form.GetValues(i))
                            {
                                sb.Append(Server.UrlEncode(Request.Form.AllKeys[i]));
                                sb.Append("=");
                                sb.Append(Server.UrlEncode(val));
                                sb.Append("&");
                            }
                    }
                }
                return sb.ToString().TrimEnd('&');
            }
            return "";
        }
    }
}