//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the type of an item in a <see cref="Path"/> control.
    /// </summary>
    public enum PathItemType 
    {
        /// <summary>
        /// A header for the <see cref="Path"/> control.
        /// </summary>
        Header,
        /// <summary>
        /// A footer for the <see cref="Path"/> control.
        /// </summary>
        Footer,
        /// <summary>
        /// A path component for the <see cref="Path"/> control.
        /// </summary>
        PathComponent,
        /// <summary>
        /// A current category for the <see cref="Path"/> control.
        /// </summary>
        CurrentCategory 
    }

    /// <summary>
    /// Represents an item in the <see cref="Path"/> control.
    /// </summary>
    public class PathItem : Control, INamingContainer
    {
        private int itemIndex;
        private PathItemType itemType;
        private object dataItem;

        /// <summary>
        /// Initialize the new instance of <see cref="PathItem"/> class.
        /// </summary>
        /// <param name="itemIndex">The index of the item in the <see cref="Path"/> control.</param>
        /// <param name="itemType">One of the <see cref="PathItemType"/> values.</param>
        public PathItem(int itemIndex, PathItemType itemType)
        {
            this.itemIndex = itemIndex;
            this.itemType = itemType;
        }

        /// <summary>
        /// Gets or sets a data item associated with the <see cref="PathItem"/> object in the <see cref="Path"/> control.
        /// </summary>
        public virtual object DataItem
        {
            get
            {
                return dataItem;
            }
            set
            {
                dataItem = value;
            }
        }

        /// <summary>
        /// Gets the index of the item in the <see cref="Path"/> control.
        /// </summary>
        public virtual int ItemIndex
        {
            get
            {
                return itemIndex;
            }
        }

        /// <summary>
        /// Gets the type of the item in the Path control.
        /// </summary>
        public virtual PathItemType ItemType
        {
            get
            {
                return itemType;
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent <see cref="Path"/> control, if the EventArgs parameter is an instance of <see cref="PathCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is CommandEventArgs)
            {
                PathCommandEventArgs pargs =
                    new PathCommandEventArgs(this, source, (CommandEventArgs)args);

                RaiseBubbleEvent(this, pargs);
                return true;
            }
            return false;
        }

        internal void SetItemType(PathItemType itemType)
        {
            this.itemType = itemType;
        }
    }
}