//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Web;
using System.Web.Security;

namespace InMotion.Security
{
    /// <summary>
    /// Encapsulate the list of supported operation by the InMotion form.
    /// </summary>
    [Serializable]
    public class FormSupportedOperations : SortedDictionary<string, AccessRights>
    {

        private bool? _allowReadInternal;
        /// <summary>
        /// Gets or sets the value indicating whether form allow Read operation for current user.
        /// </summary>
        public bool AllowRead
        {
            get
            {
                if (_allowReadInternal.HasValue) return _allowReadInternal.Value;
                if (Roles.Enabled)
                {
                    //if (this.Count == 0) return true;
                    bool result = false;
                    foreach (KeyValuePair<string, AccessRights> e in this)
                    {
                        result = result ||
                            HttpContext.Current.User.IsInRole(e.Key) &&
                            e.Value.Read;
                    }
                    return result;
                }
                else
                    return HttpContext.Current.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(InMotion.Security.Utility.UserId);
            }
            set
            {
                _allowReadInternal = value;
            }
        }
        private bool? _allowInsertInternal;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Insert operation for current user.
        /// </summary>
        public bool AllowInsert
        {
            get
            {
                if (_allowInsertInternal.HasValue) return _allowInsertInternal.Value;
                if (Roles.Enabled)
                {
                    bool result = false;
                    foreach (KeyValuePair<string, AccessRights> e in this)
                    {
                        result = result ||
                            HttpContext.Current.User.IsInRole(e.Key) &&
                            e.Value.Insert;
                    }
                    return result;
                }
                else
                    return HttpContext.Current.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(InMotion.Security.Utility.UserId);
            }
            set
            {
                _allowInsertInternal = value;
            }
        }
        private bool? _allowUpdateInternal;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Update operation for current user.
        /// </summary>
        public bool AllowUpdate
        {
            get
            {
                if (_allowUpdateInternal.HasValue) return _allowUpdateInternal.Value;
                if (Roles.Enabled)
                {
                    bool result = false;
                    foreach (KeyValuePair<string, AccessRights> e in this)
                    {
                        result = result ||
                            HttpContext.Current.User.IsInRole(e.Key) &&
                            e.Value.Update;
                    }
                    return result;
                }
                else
                    return HttpContext.Current.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(InMotion.Security.Utility.UserId);

            }
            set
            {
                _allowUpdateInternal = value;
            }
        }
        private bool? _allowDeleteInternal;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Delete operation for current user.
        /// </summary>
        public bool AllowDelete
        {
            get
            {
                if (_allowDeleteInternal.HasValue) return _allowDeleteInternal.Value;
                if (Roles.Enabled)
                {
                    bool result = false;
                    foreach (KeyValuePair<string, AccessRights> e in this)
                    {
                        result = result ||
                            HttpContext.Current.User.IsInRole(e.Key) &&
                            e.Value.Delete;
                    }
                    return result;
                }
                else
                    return HttpContext.Current.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(InMotion.Security.Utility.UserId);
            }
            set
            {
                _allowDeleteInternal = value;
            }
        }
        /// <summary>
        /// Gets or sets the value indicating whether form allow all operations for current user.
        /// </summary>
        public bool FullControl
        {
            get
            {
                return AllowRead && AllowInsert && AllowUpdate && AllowDelete;
            }
        }
        /// <summary>
        /// Gets or sets the value indicating whether form does not allow any operation for current user.
        /// </summary>
        public bool None
        {
            get
            {
                return !(AllowRead || AllowInsert || AllowUpdate || AllowDelete);
            }
        }

        /// <summary>
        /// Gets or sets the value indicating whether form allow any of Insert, Update or Delete operations for current user.
        /// </summary>
        public bool Editable
        {
            get
            {
                return AllowInsert || AllowUpdate || AllowDelete;
            }
        }

        /// <summary>
        /// Adds a new <see cref="AccessRights"/> for a given group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="allowRead">The value that indicating whether group will have Read permission.</param>
        public virtual void Add(string groupName, bool allowRead)
        {
            Add(groupName, new AccessRights(allowRead));
        }

        /// <summary>
        /// Adds a new <see cref="AccessRights"/> for a given group.
        /// </summary>
        /// <param name="groupName">The name of the group.</param>
        /// <param name="allowRead">The value that indicating whether group will have Read permission.</param>
        /// <param name="allowInsert">The value that indicating whether group will have Insert permission.</param>
        /// <param name="allowUpdate">The value that indicating whether group will have Update permission.</param>
        /// <param name="allowDelete">The value that indicating whether group will have Delete permission.</param>
        public virtual void Add(string groupName, bool allowRead, bool allowInsert, bool allowUpdate, bool allowDelete)
        {
            Add(groupName, new AccessRights(allowRead, allowInsert, allowUpdate, allowDelete));
        }
    }
}