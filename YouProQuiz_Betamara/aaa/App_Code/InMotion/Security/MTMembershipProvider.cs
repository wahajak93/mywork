//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using InMotion.Data;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Security
{
    /// <summary>
    /// Manages storage of membership information for an InMotion application in a database. 
    /// </summary>
    public class MTMembershipProvider : MembershipProvider
    {
        private string connName;
        /// <summary>
        /// Gets the name of the connection which used for reading security information from database.
        /// </summary>
        public string ConnName
        {
            get { return connName; }
        }
        private string tableName;
        /// <summary>
        /// Gets the table name which storing security information.
        /// </summary>
        public string TableName
        {
            get { return tableName; }
        }
        private string userIdField;
        /// <summary>
        /// Gets the name of the id field in database table.
        /// </summary>
        public string UserIdField
        {
            get { return userIdField; }
        }
        private string userLoginField;
        /// <summary>
        /// Gets the name of the login field in database table.
        /// </summary>
        public string UserLoginField
        {
            get { return userLoginField; }
        }
        private string userPasswordField;
        /// <summary>
        /// Gets the name of the password field in database table.
        /// </summary>
        public string UserPasswordField
        {
            get { return userPasswordField; }
        }
        private string userGroupField;
        /// <summary>
        /// Gets the name of the group id field in database table.
        /// </summary>
        public string UserGroupField
        {
            get { return userGroupField; }
        }
        private string userIdSessionVariable;
        /// <summary>
        /// Gets the name of the session variable which stores user id of the current user.
        /// </summary>
        public string UserIdSessionVariable
        {
            get { return userIdSessionVariable; }
        }
        private string userGroupSessionVariable;
        /// <summary>
        /// Gets the name of the session variable which stores user group id of the current user.
        /// </summary>
        public string UserGroupSessionVariable
        {
            get { return userGroupSessionVariable; }
        }
        private string userLoginSessionVariable;
        /// <summary>
        /// Gets the name of the session variable which stores user login of the current user.
        /// </summary>
        public string UserLoginSessionVariable
        {
            get { return userLoginSessionVariable; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTMembershipProvider"/> class.
        /// </summary>
        public MTMembershipProvider()
        {
        }

        /// <summary>
        /// Initializes the InMotion membership provider with the property values specified in the InMotion application's configuration file. This method is not intended to be used directly from your code.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null) return;
            connName = config["connectionString"];
            tableName = config["tableName"];
            userIdField = config["userIdField"];
            userLoginField = config["userLoginField"];
            userPasswordField = config["userPasswordField"];
            userGroupField = config["userGroupField"];
            userIdSessionVariable = config["userIdSessionVariable"];
            userGroupSessionVariable = config["userGroupSessionVariable"];
            userLoginSessionVariable = config["userLoginSessionVariable"];

            base.Initialize(name, config);
            if (config["applicationName"] == null || config["applicationName"].Trim() == "")
            {
                pApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                pApplicationName = config["applicationName"];
            }
        }

        private string pApplicationName;
        /// <summary>
        /// Gets or sets the name of the application to store and retrieve membership information for.
        /// </summary>
        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        /// <summary>
        /// Modifies a user's password.
        /// </summary>
        /// <param name="username">The user to update the password for.</param>
        /// <param name="oldPassword">The current password for the specified user.</param>
        /// <param name="newPassword">The new password for the specified user.</param>
        /// <returns>true if the password was updated successfully. false if the supplied old password is invalid, the user is locked out, or the user does not exist in the database.</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            string Sql = String.Format("UPDATE {0} SET {2} = {4} WHERE {1} = {3} AND {2} = {5})",
                tableName,
                userLoginField,
                userPasswordField,
                conn.EncodeSqlValue(username, DataType.Text, true),
                conn.EncodeSqlValue(newPassword, DataType.Text, true),
                conn.EncodeSqlValue(oldPassword, DataType.Text, true)
                );
            dc.CommandText = Sql;
            try
            {
                return dc.ExecuteNonQuery() > 0;
            }
            catch (DbException)
            {
                return false;
            }

        }

        /// <summary>
        /// Processes a request to update the password question and answer for a membership user.
        /// </summary>
        /// <param name="username">The user to change the password question and answer for.</param>
        /// <param name="password">The password for the specified user.</param>
        /// <param name="newPasswordQuestion">The new password question for the specified user.</param>
        /// <param name="newPasswordAnswer">The new password answer for the specified user.</param>
        /// <returns>true if the password question and answer are updated successfully; otherwise, false.</returns>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }


        /// <summary>
        /// Adds a new user to the InMotion membership database. 
        /// </summary>
        /// <param name="username">The user name for the new user.</param>
        /// <param name="password">The password for the new user.</param>
        /// <param name="email">Ignored</param>
        /// <param name="passwordQuestion">Ignored</param>
        /// <param name="passwordAnswer">Ignored</param>
        /// <param name="isApproved">Ignored</param>
        /// <param name="providerUserKey">Ignored</param>
        /// <param name="status">One of the <see cref="MembershipCreateStatus"/> values, indicating whether the user was created successfully.</param>
        /// <returns>A <see cref="MembershipUser"/> object for the newly created user. If no user was created, this method returns a null reference (Nothing in Visual Basic).</returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            string Sql = String.Format("insert into {0} ({1},{2}) values({3},{4})",
                tableName,
                userLoginField,
                userPasswordField,
                conn.EncodeSqlValue(username, DataType.Text, true),
                conn.EncodeSqlValue(password, DataType.Text, true));
            dc.CommandText = Sql;
            dc.ExecuteNonQuery();
            dc.CommandText = String.Format("SELECT {0} FROM {1} WHERE {2}={3} AND {4}={5}",
                userIdField,
                tableName,
                conn.EncodeSqlValue(username, DataType.Text, true),
                userLoginField,
                conn.EncodeSqlValue(password, DataType.Text, true));

            int id = (int)dc.ExecuteScalar();
            MembershipUser user = new MembershipUser("MTMembershipProvider", username, id, null, null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
            status = MembershipCreateStatus.Success;
            return user;
        }

        private void CreateDataCommand(out Connection conn, out DataCommand dc)
        {
            conn = AppConfig.GetConnection(connName);
            dc = (DataCommand)conn.CreateCommand();
            dc.MTCommandType = MTCommandType.Sql;
        }
        /// <summary>
        /// Removes a user's membership information from the SQL Server membership database.
        /// </summary>
        /// <param name="username">The name of the user to delete.</param>
        /// <param name="deleteAllRelatedData">Ignored.</param>
        /// <returns>true if the user was deleted; otherwise, false. A value of false is also returned if the user does not exist in the database.</returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("DELETE FROM {0} WHERE {1}={2}",
                tableName,
                userLoginField,
                conn.EncodeSqlValue(username, DataType.Text, true));
            return dc.ExecuteNonQuery() > 0;
        }

        /// <summary>
        /// Indicates whether the membership provider is configured to allow users to reset their passwords.
        /// </summary>
        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        /// <summary>
        /// Indicates whether the membership provider is configured to allow users to retrieve their passwords.
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a collection of membership users where the e-mail address contains the specified e-mail address to match.
        /// </summary>
        /// <param name="emailToMatch">The e-mail address to search for.</param>
        /// <param name="pageIndex">The index of the page of results to return. pageIndex is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of matched users.</param>
        /// <returns>A MembershipUserCollection collection that contains a page of pageSize MembershipUser objects beginning at the page specified by pageIndex.</returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets a collection of membership users where the user name contains the specified user name to match.
        /// </summary>
        /// <param name="usernameToMatch">The user name to search for.</param>
        /// <param name="pageIndex">The index of the page of results to return. pageIndex is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of matched users.</param>
        /// <returns>A MembershipUserCollection collection that contains a page of pageSize MembershipUser objects beginning at the page specified by pageIndex.</returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets a collection of all the users in the SQL Server membership database. 
        /// </summary>
        /// <param name="pageIndex">The index of the page of results to return. pageIndex is zero-based.</param>
        /// <param name="pageSize">The size of the page of results to return.</param>
        /// <param name="totalRecords">The total number of users.</param>
        /// <returns> <see cref="MembershipUserCollection"/> of <see cref="MembershipUser"/> objects representing all the users in the database for the configured <see cref="ApplicationName"/>.</returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            dc.CommandText = String.Format("SELECT * FROM {0}", tableName);
            MembershipUserCollection col = new MembershipUserCollection();

            DataSet ds = dc.Execute(pageIndex * pageSize, pageSize);
            totalRecords = ds.Tables[0].Rows.Count;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                MembershipUser user = new MembershipUser("MTMembershipProvider", ds.Tables[0].Rows[i][userLoginField].ToString(), ds.Tables[0].Rows[0][userIdField].ToString(), null, null, null, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
                col.Add(user);
            }
            return col;
        }

        /// <summary>
        /// Gets the number of users currently accessing the application.
        /// </summary>
        /// <returns>The number of users currently accessing the application.</returns>
        public override int GetNumberOfUsersOnline()
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }
        /// <summary>
        /// Gets the password for the specified user name from the data source.
        /// </summary>
        /// <param name="username">The user to retrieve the password for.</param>
        /// <param name="answer">The password answer for the user.</param>
        /// <returns>The password for the specified user name.</returns>
        public override string GetPassword(string username, string answer)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets information from the data source for a user based on the unique identifier for the membership user. Provides an option to update the last-activity date/time stamp for the user.
        /// </summary>
        /// <param name="username">The name of the user to get information for.</param>
        /// <param name="userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        /// <returns>A MembershipUser object populated with the specified user's information from the data source.</returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Gets information from the data source for a user based on the unique identifier for the membership user. Provides an option to update the last-activity date/time stamp for the user.
        /// </summary>
        /// <param name="providerUserKey">The unique identifier for the membership user to get information for.</param>
        /// <param name="userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        /// <returns>A MembershipUser object populated with the specified user's information from the data source.</returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }
        /// <summary>
        /// Gets the user name associated with the specified e-mail address.
        /// </summary>
        /// <param name="email">The e-mail address to search for.</param>
        /// <returns>The user name associated with the specified e-mail address. If no match is found, return a null reference.</returns>
        public override string GetUserNameByEmail(string email)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }
        /// <summary>
        /// Gets the number of invalid password or password-answer attempts allowed before the membership user is locked out.
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets the minimum number of special characters that must be present in a valid password.
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets the minimum length required for a password.
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets the number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets a value indicating the format for storing passwords in the membership data store.
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets the regular expression used to evaluate a password.
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets a value indicating whether the membership provider is configured to require the user to answer a password question for password reset and retrieval.
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Gets a value indicating whether the membership provider is configured to require a unique e-mail address for each user name.
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
                throw new NotImplementedException("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Resets a user's password to a new, automatically generated password.
        /// </summary>
        /// <param name="username">The user to reset the password for.</param>
        /// <param name="answer">The password answer for the specified user.</param>
        /// <returns>The new password for the specified user.</returns>
        public override string ResetPassword(string username, string answer)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }
        /// <summary>
        /// Clears a lock so that the membership user can be validated.
        /// </summary>
        /// <param name="userName">The membership user to clear the lock status for.</param>
        /// <returns>true if the membership user was successfully unlocked; otherwise, false.</returns>
        public override bool UnlockUser(string userName)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Updates information about a user in the data source.
        /// </summary>
        /// <param name="user">A MembershipUser object that represents the user to update and the updated information for the user.</param>
        public override void UpdateUser(MembershipUser user)
        {
            if (AppConfig.IsMTErrorHandlerUse("all"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Verifies that the specified user name and password exist in the SQL Server membership database.
        /// </summary>
        /// <param name="username">The name of the user to validate.</param>
        /// <param name="password">The password for the specified user.</param>
        /// <returns>true if the specified username and password are valid; otherwise, false. A value of false is also returned if the user does not exist in the database.</returns>
        public override bool ValidateUser(string username, string password)
        {
            Connection conn;
            DataCommand dc;
            CreateDataCommand(out conn, out dc);
            string passValue;
            if (AppConfig.ProtectPasswordsMethod == "DBFunction")
                passValue = AppConfig.ProtectPasswordsExpression.Replace("{password}", conn.EncodeSqlValue(password, DataType.Text, true));
            else
                passValue = conn.EncodeSqlValue(password, DataType.Text, true);
            dc.CommandText = String.Format("SELECT * FROM {0} WHERE {1}={2} AND {3}={4}",
                tableName,
                userLoginField,
                conn.EncodeSqlValue(username, DataType.Text, true),
                userPasswordField,
                passValue);

            bool result = false;
            conn.Open();
            try
            {
                DbDataReader reader = dc.ExecuteReader();

                if (reader.Read())
                {
                    HttpContext.Current.Session[userIdSessionVariable] = reader[userIdField];
                    if (!String.IsNullOrEmpty(userGroupField))
                        HttpContext.Current.Session[userGroupSessionVariable] = Convert.ToString(reader[userGroupField]);
                    HttpContext.Current.Session[userLoginSessionVariable] = username;
                    return true;
                }
            }
            finally
            {
                conn.Close();
            }

            return result;
        }
    }
}
