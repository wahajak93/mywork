//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Common
{
    /// <summary>
    /// Represents a string. 
    /// </summary>
    [Serializable]
    public struct MTMemo : IMTField
    {
        private bool _isNull;
        private string _value;

        /// <summary>
        /// Returns a <see cref="DataType"/> of the specified <see cref="IMTField"/> object.
        /// </summary>
        public DataType DataType
        {
            get { return DataType.Memo; }
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="MTMemo"/> structure's value.
        /// </summary>
        public string Value
        {
            get { return _value; }
        }

        /// <summary>
        /// Indicates whether a structure is null. This property is read-only.
        /// </summary>
        public bool IsNull
        {
            get { return _isNull; }
        }

        /// <summary>
        /// Represents a null value that can be assigned to the Value property of an instance of the <see cref="MTMemo"/> structure.
        /// </summary>
        public static MTMemo Null
        {
            get { return new MTMemo(true); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="MTMemo"/> structure.
        /// </summary>
        /// <param name="IsNullable">The supplied <see cref="bool"/> IsNullable indicates that the value of the new <see cref="MTMemo"/> structure is null.</param>
        private MTMemo(bool IsNullable)
            : this("")
        {
            _isNull = IsNullable;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MTMemo"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="string"/> value that will be used as the value of the new <see cref="MTMemo"/> structure.</param>
        public MTMemo(string value)
        {
            _isNull = String.IsNullOrEmpty(value);
            _value = _isNull ? "" : value;
        }

        /// <summary>
        /// Converts an object to its <see cref="MTMemo"/> equivalent.
        /// </summary>
        /// <param name="value">An object containing the value to convert.</param>
        /// <returns>The <see cref="MTMemo"/> value equivalent to the string value representation.</returns>
        public static MTMemo Parse(object value)
        {
            if (value is MTMemo) return (MTMemo)value;
            if (value == null || value.Equals(DBNull.Value) || ((value is INullable) && ((INullable)value).IsNull))
                return MTMemo.Null;
            try
            {
                if (value is IMTField) return (MTMemo)((IMTField)value).ToString("");
                return (MTMemo)value.ToString();
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Unable to parse Memo.\n{0}", e);
                throw (new FormatException("Unable to parse Memo: " + e.Message));
            }
        }

        private static MTMemo ToMTMemo(object obj)
        {
            MTMemo result;
            try
            {
                result = (MTMemo)obj;
            }
            catch (Exception e)
            {
                if (obj is MTText) result = (MTMemo)(MTText)obj;
                else if (obj is string) result = (MTMemo)(string)obj;
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw new InvalidCastException();
                }
            }
            return result;
        }

        /// <summary>
        /// Tests for equality between two <see cref="MTMemo"/> objects.
        /// </summary>
        /// <param name="obj">An <see cref="object"/> value to compare to this instance.</param>
        /// <returns>true if obj has the same value as this instance; otherwise, false</returns>
        public override bool Equals(object obj)
        {
            MTMemo i = null;
            try
            {
                i = ToMTMemo(obj);
            }
            catch { return false; }
            if (i.IsNull || this.IsNull)
                return this.IsNull == i.IsNull;
            return i.Value == this.Value;
        }

        /// <summary>
        /// Serves as a hash function for a particular type. GetHashCode is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>A hash code for the current <see cref="MTMemo"/>.</returns>
        public override int GetHashCode()
        {
            if (this.IsNull) return 0;
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Converts the boolean value of this instance to its equivalent string representation.
        /// </summary>
        /// <returns>The string representation of the value of this instance.</returns>
        public override string ToString()
        {
            return this.ToString("");
        }

        /// <summary>
        /// Converts the text value of this instance to its equivalent string representation, using the specified format. 
        /// </summary>
        /// <param name="format">A format string. </param>
        /// <returns>The string representation of the value of this instance as specified by format. </returns>
        public string ToString(string format)
        {
            return this.ToString(format, null);
        }

        /// <summary>
        /// Converts the text value of this instance to its equivalent string representation using the specified format and culture-specific format information. 
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <param name="formatProvider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (this.IsNull) return "";
            return Value.ToString(formatProvider);
        }
        #endregion

        #region IComparable Members
        /// <summary>
        /// Compares this instance to a specified <see cref="MTMemo"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(MTMemo op1)
        {
            if (op1.IsNull && this.IsNull) return 0;
            if (op1.IsNull) return 1;
            if (this.IsNull) return -1;
            return this.Value.CompareTo(op1.Value);
        }

        /// <summary>
        /// Compares this instance to an <see cref="object"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="object"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(object op1)
        {
            if (op1 is MTMemo)
                return CompareTo((MTMemo)op1);
            else
                return CompareTo(Parse(op1));
        }
        #endregion

        #region Static operators
        /// <summary>
        /// Determines whether two specified instances of MTMemo are equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator ==(MTMemo op1, MTMemo op2)
        {
            return op1.Equals(op2);
        }
        /// <summary>
        /// Determines whether two specified instances of MTMemo are not equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> do not represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator !=(MTMemo op1, MTMemo op2)
        {
            return !op1.Equals(op2);
        }
        /// <summary>
        /// Concatenate a specified string with another specified string.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns>A <b>MTMemo</b> whose value is the result of concatenation value of <i>op1</i> and the value of <i>op2</i>.</returns>
        public static MTMemo operator +(MTMemo op1, MTMemo op2)
        {
            if (op1.IsNull) return op2;
            if (op2.IsNull) return op1;
            return new MTMemo(op1.Value + op2.Value);
        }
        /// <summary>
        /// Concatenate a string representation of a specified object with another specified string.
        /// </summary>
        /// <param name="op1">A <see cref="Object"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns>A <b>MTMemo</b> whose value is the result of concatenation value of <i>op1</i> and the value of <i>op2</i>.</returns>
        public static MTMemo operator +(object op1, MTMemo op2)
        {
            if (op1 == null) return op2;
            if (op2.IsNull) return new MTMemo(op1.ToString());
            return new MTMemo(op1.ToString() + op2.Value);
        }

        /// <summary>
        /// Determines whether first specified instances of MTMemo are greater than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> greater than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >(MTMemo op1, MTMemo op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value.CompareTo(op2.Value) > 0);
        }
        /// <summary>
        /// Determines whether first specified instances of MTMemo are greater than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> greater than or equal to <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >=(MTMemo op1, MTMemo op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(!(op1.Value.CompareTo(op2.Value) < 0));
        }
        /// <summary>
        /// Determines whether first specified instances of MTMemo are less than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <(MTMemo op1, MTMemo op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value.CompareTo(op2.Value) < 0);
        }
        /// <summary>
        /// Determines whether first specified instances of MTMemo are less than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <param name="op2">A <b>MTMemo</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than or equal to<i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <=(MTMemo op1, MTMemo op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(!(op1.Value.CompareTo(op2.Value) > 0));
        }
        /// <summary>
        /// Implicitly creates a <see cref="MTMemo"/> instance that value represent the value of specified <see cref="string"/>.
        /// </summary>
        /// <param name="op1">A <see cref="string"/>.</param>
        /// <returns>A <see cref="MTMemo"/> instance.</returns>
        public static implicit operator MTMemo(string op1)
        {
            return new MTMemo(op1);
        }
        /// <summary>
        /// Implicitly creates a <see cref="string"/> instance that value represent the value of specified <see cref="MTMemo"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <returns>A <see cref="string"/> instance.</returns>
        public static implicit operator string(MTMemo op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be null.");
            }
            return op1.Value;
        }
        /// <summary>
        /// Implicitly creates a <see cref="MTMemo"/> instance that value represent the value of specified <see cref="MTText"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTText"/>.</param>
        /// <returns>A <see cref="MTMemo"/> instance.</returns>
        public static implicit operator MTMemo(MTText op1)
        {
            if (op1.IsNull) return MTMemo.Null;
            return new MTMemo(op1.Value);
        }
        /// <summary>
        /// Implicitly creates a <see cref="MTText"/> instance that value represent the value of specified <see cref="MTMemo"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTMemo"/>.</param>
        /// <returns>A <see cref="MTText"/> instance.</returns>
        public static implicit operator MTText(MTMemo op1)
        {
            if (op1.IsNull) return MTText.Null;
            return new MTText(op1.Value);
        }
        #endregion

        /// <summary>
        /// Returns the <see cref="TypeCode"/> for this instance.
        /// </summary>
        /// <returns>The enumerated constant that is the <see cref="TypeCode"/> of the class or value type that implements this interface.</returns>
        public TypeCode GetTypeCode()
        {
            return ((IConvertible)Value).GetTypeCode();
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToBoolean"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToBoolean(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToChar"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        char IConvertible.ToChar(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToChar(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDateTime"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDateTime(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDecimal"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDecimal(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDouble"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDouble(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt64(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSingle"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSingle(provider);
        }

        /// <summary>
        /// Converts the string value of this instance to its equivalent string representation using the specified culture-specific format information. 
        /// </summary>
        /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by provider.</returns>
        public string ToString(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToString(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToType"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="conversionType"></param>
        /// <returns></returns>
        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return ((IConvertible)Value).ToType(conversionType, provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt64(provider);
        }
    }
}