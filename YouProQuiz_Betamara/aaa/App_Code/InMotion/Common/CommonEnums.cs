//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Common
{
    /// <summary>
    /// Specifies the data type of a control or parameter
    /// </summary>
    public enum DataType 
    { 
        /// <summary>
        /// The value represented by <see cref="MTInteger"/> type.
        /// </summary>
        Integer, 
        /// <summary>
        /// The value represented by <see cref="MTFloat"/> type.
        /// </summary>
        Float, 
        /// <summary>
        /// The value represented by <see cref="MTText"/> type.
        /// </summary>
        Text, 
        /// <summary>
        /// The value represented by <see cref="MTMemo"/> type.
        /// </summary>
        Memo, 
        /// <summary>
        /// The value represented by <see cref="MTBoolean"/> type.
        /// </summary>
        Boolean, 
        /// <summary>
        /// The value represented by <see cref="MTDate"/> type.
        /// </summary>
        Date, 
        /// <summary>
        /// The value represented by <see cref="MTSingle"/> type.
        /// </summary>
        Single 
    }

    /// <summary>
    /// Specifies the source type of control or parameter
    /// </summary>
    public enum SourceType
    {
        /// <summary>
        /// The value will be read from database field.
        /// </summary>
        DatabaseColumn,
        /// <summary>
        /// The value is initialized by code expression.
        /// </summary>
        CodeExpression,
        /// <summary>
        /// The value is initialized by some special value from parent form.
        /// </summary>
        SpecialValue,
        /// <summary>
        /// The value will be read from output parameter of the database command.
        /// </summary>
        DBParameter
    }
}