//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-4E4856AC
public partial class Home_Page : MTPage
{
//End Page class

//Page Home Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page Home Event Init

//Page Events @1-C192B29D
        this.AfterInitialize += new EventHandler<EventArgs>(Page_AfterInitialize);
//End Page Events

//Page Home Init event tail @1-FCB6E20C
    }
//End Page Home Init event tail

//Page Home Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) 
    {
//End Page Home Event Load
        if (Session["UserID"] != null)
        {
            Response.Redirect("~/Dashboard.aspx");
        }
//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page Home Load event tail @1-FCB6E20C
    }
//End Page Home Load event tail

//Page Home Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page Home Event On PreInit
       
//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page Home On PreInit event tail @1-FCB6E20C
    }
//End Page Home On PreInit event tail

//Page Home Event After Initialize @1-1ECBEF72
    protected void Page_AfterInitialize(object sender, EventArgs e) {
//End Page Home Event After Initialize

//Page Home Event After Initialize. Action Logout @4-12AD234D
        if (HttpContext.Current.Request["Logout"] != null) 
        {
            HttpContext.Current.Session.Remove("UserID");
            HttpContext.Current.Session.Remove("GroupID");
            HttpContext.Current.Session.Remove("UserLogin");
            HttpContext.Current.Response.Cookies.Remove(AppConfig.AutoLoginCookieName);
            HttpContext.Current.Request.Cookies.Remove(AppConfig.AutoLoginCookieName);
            InMotion.Security.Utility.NeedAutologin = false;
            FormsAuthentication.SignOut();
            Response.Redirect("Home.aspx");
        }
//End Page Home Event After Initialize. Action Logout

//Page Home After Initialize event tail @1-FCB6E20C
    }
//End Page Home After Initialize event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

