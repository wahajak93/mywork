<!--ASCX page header @1-A28F2B91-->
<%@ Control language="C#" CodeFile="UC_Quiz_ListsEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC_Quiz_Lists_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-7501D2EE-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:Record DataSourceID="TBL_QuizSearchDataSource" AllowInsert="False" AllowDelete="False" AllowUpdate="False" ID="TBL_QuizSearch" runat="server" OnValidating="TBL_QuizSearch_Validating"><ItemTemplate><div data-emulate-form="UC_Quiz_ListsTBL_QuizSearch" id="UC_Quiz_ListsTBL_QuizSearch">







  <h2>Search </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").ClientID %>">Quiz Id</label></td> 
      <td><mt:MTTextBox  Caption="Quiz Id" maxlength="10" Columns="10" ID="s_Quiz_id" data-id="UC_Quiz_ListsTBL_QuizSearchs_Quiz_id" runat="server" type="number" ErrorControl="MTLabel1" /><mt:MTLabel id="MTLabel1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_Name").ClientID %>">Quiz Name</label></td> 
      <td><mt:MTTextBox Caption="Quiz Name" maxlength="50" Columns="50" ID="s_Quiz_Name" data-id="UC_Quiz_ListsTBL_QuizSearchs_Quiz_Name" runat="server" ErrorControl="MTLabel2" /><mt:MTLabel id="MTLabel2" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="UC_Quiz_ListsTBL_QuizSearchButton_DoSearch" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="TBL_QuizSearchDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
</mt:MTDataSource><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="TBL_QuizDataSource" ID="TBL_Quiz" runat="server">
<HeaderTemplate>
<h2>List of Quiz</h2>
<table>
  <tr>
    <th scope="col">Quiz Id</th>
 
    <th scope="col">Quiz Name</th>
 
    <th scope="col">Quiz Category</th>
 
    <th scope="col">&nbsp;Quiz Question Mode</th>
 
    <th scope="col">&nbsp;Attempt Limits</th>
 
    <th scope="col">&nbsp;*</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_id" DataType="Integer" ID="Quiz_id" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Title" ID="Quiz_Name" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Category_Name" ID="Quiz_Category" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="TBL_Quiz_Mode_Quiz_Mode" ID="Label1" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_User_Attempt_Limit" ID="Label2" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLink Text="View" ID="Link1" data-id="UC_Quiz_ListsTBL_QuizLink1_{TBL_Quiz:rowNumber}" runat="server" HrefSource="~/Pg_QuizHomePage.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="Quiz_id" SourceType="DataSourceColumn" Source="Quiz_id" Format="yyyy-MM-dd"/>
    </Parameters></mt:MTLink>&nbsp;</td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="6">No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td colspan="6">
      <mt:MTNavigator FirstOnValue="&lt;img alt=&quot;{First_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/First.gif&quot;/&gt; " FirstOffValue="&lt;img alt=&quot;{First_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/FirstOff.gif&quot;/&gt;" PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Prev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/PrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Next.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/NextOff.gif&quot;/&gt;" LastOnValue="&lt;img alt=&quot;{Last_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Last.gif&quot;/&gt; " LastOffValue="&lt;img alt=&quot;{Last_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/LastOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageOnly" TotalPagesText="of" PostPageNumberCaption="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/>&nbsp;</td> 
  </tr>
</table>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="TBL_QuizDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} TBL_Quiz.*, TBL_Quiz_Mode.Quiz_Mode AS TBL_Quiz_Mode_Quiz_Mode, user_name, Category_Name 
FROM ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_Quiz_id" SourceType="URL" Source="s_Quiz_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="TBL_Quiz.Quiz_id"/>
     <mt:WhereParameter Name="Urls_Quiz_Name" SourceType="URL" Source="s_Quiz_Name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="Quiz_Title"/>
   </SelectParameters>
</mt:MTDataSource><br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">



<!--End ASCX page-->

