//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-8FD157D9
public partial class KMSAddQuestionToQuiz_Page : MTPage
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page KMSAddQuestionToQuiz Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page KMSAddQuestionToQuiz Event Load


        string quest = Request.QueryString["QuestionID"].ToString();
        string quiz = Request.QueryString["Quiz_id"].ToString();
        DataTable dt1 = clsdb.readdataKMS("SELECT * from tbl_Quiz_Question where Quiz_Quest_ID="+quest+" AND Quiz_ID="+quiz+"");
        if (dt1.Rows.Count > 0)
        {
            Button1.Visible = false;
            //Response.Write("<script>alert('Already inserted this question in existing quiz');</script>");
        }



      //  Response.Write("<script>alert('asdasdaskdka');</script>");
        DataTable dt2 = clsdb.readdataKMS("select * from tbl_Quiz_Question where Quiz_ID=" + quiz + " and Quiz_Quest_ID=" + quest + "");

        if (dt2.Rows.Count < 1)
        {
            tbl_Quiz_Question_Answers.Visible = false;
            tbl_Quiz_Question_Choice1.Visible = false;
            tbl_Quiz_Question1.Visible = false;
        }







//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page KMSAddQuestionToQuiz Load event tail @1-FCB6E20C
    }
//End Page KMSAddQuestionToQuiz Load event tail

//Page KMSAddQuestionToQuiz Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page KMSAddQuestionToQuiz Event On PreInit
        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page Dashboard access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", false); this.UserRights.Add("Premium", true);

        //this
//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page KMSAddQuestionToQuiz On PreInit event tail @1-FCB6E20C
    }
//End Page KMSAddQuestionToQuiz On PreInit event tail

//Button Button1 Event On Click @61-67A57B05
    protected void Button1_Click(object sender, EventArgs e) {
//End Button Button1 Event On Click
try
{
string quiz_id=Request.QueryString["Quiz_id"].ToString();
string quest_id=Request.QueryString["QuestionID"].ToString();

 Connection conn = (Connection)DataUtility.GetConnectionObject("InMotion:KMS");
       DataCommand select=(DataCommand)conn.CreateCommand();
  select.MTCommandType = MTCommandType.Table;
  select.CommandText = "INSERT INTO tbl_Quiz_Question(Quiz_ID,Quiz_Quest_ID) VALUES("+quiz_id+","+quest_id+")";
  
  select.ExecuteNonQuery();


       DataCommand select1=(DataCommand)conn.CreateCommand();
  select1.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) SELECT "+quiz_id+","+quest_id+",tblChoice.ChoiceID FROM tblChoice WHERE tblChoice.QuestionID="+quest_id+"";


select1.ExecuteNonQuery();



       DataCommand select2=(DataCommand)conn.CreateCommand();
  select2.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
select2.CommandText = "INSERT INTO tbl_Quiz_Question_Answers(Quiz_ID,QuestionID,ChoiceID) SELECT "+quiz_id+","+quest_id+",tblAnswer.ChoiceID FROM tblAnswer WHERE tblAnswer.QuestionID="+quest_id+"";


select2.ExecuteNonQuery();





Response.Redirect(Request.RawUrl);



}
catch(Exception ex)
{
	Response.Write("<script>alert('You have already inserted this question');</script>");
	
}



//Button Button1 Event On Click. Action Custom Code @75-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Button Button1 Event On Click. Action Custom Code

//Button Button1 On Click event tail @61-FCB6E20C
    }
//End Button Button1 On Click event tail

//DEL      

//CheckBox CheckBox_Delete Event Init @73-402FBBF6
    protected void tbl_Quiz_Question1CheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @73-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @73-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @73-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @73-BD104E47
    protected void tbl_Quiz_Question1CheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @73-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @73-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C
    protected void Button1_BeforeShow(object sender, EventArgs e)
    {
        string quiz = Request.QueryString["Quiz_id"];
        string quest = Request.QueryString["QuestionID"];

        DataTable dt1 = clsdb.readdataKMS("select * from tbl_Quiz_Question where Quiz_ID=" + quiz + " and Quiz_Quest_ID=" + quest + "");

        if (dt1.Rows.Count > 1)
        {
            Button1.Visible = false;

        }

    }

    protected void tblChoice_BeforeShow(object sender, EventArgs e)
    {
     
    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

