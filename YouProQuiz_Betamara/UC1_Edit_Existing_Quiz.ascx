<!--ASCX page header @1-B82E97C5-->
<%@ Control language="C#" CodeFile="UC1_Edit_Existing_QuizEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC1_Edit_Existing_Quiz_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-1E13F3B5-->

<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<p>&nbsp;</p>
<mt:Record ID="TBL_QuizSearch" runat="server" OnValidating="TBL_QuizSearch_Validating" OnControlsValidating="TBL_QuizSearch_Validating"><ItemTemplate><div data-emulate-form="UC1_Edit_Existing_QuizTBL_QuizSearch" id="UC1_Edit_Existing_QuizTBL_QuizSearch">







  <h2>Existing Quizes </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").ClientID %>">Id</label></td> 
      <td><mt:MTTextBox Source="Quiz_id" DataType="text" Caption="Quiz Id" maxlength="12" Columns="12" ID="s_Quiz_id" data-id="UC1_Edit_Existing_QuizTBL_QuizSearchs_Quiz_id" runat="server"  CausesValidation="True" ErrorControl="Label1" /><mt:MTLabel ID="Label1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_Title").ClientID %>">Title</label></td> 
      <td><mt:MTTextBox Source="Quiz_Title" ErrorControl="Label2" Caption="Quiz Title" maxlength="100" Columns="50" ID="s_Quiz_Title" data-id="UC1_Edit_Existing_QuizTBL_QuizSearchs_Quiz_Title" runat="server"/><mt:MTLabel ID="Label2" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="UC1_Edit_Existing_QuizTBL_QuizSearchButton_DoSearch" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="TBL_QuizDataSource" ID="TBL_Quiz" runat="server">
<HeaderTemplate>
<p>&nbsp;</p>
<p>
<table>
  <tr>
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="ID" ID="Sorter_Quiz_id" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_id" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Title" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Title" ID="Sorter_Quiz_Title" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_Title" runat="server"/></th>
 
    <th scope="col">Quiz Descp</th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Time_limit_secs" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Time Limit(Secs)" ID="Sorter_Time_limit_secs" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Time_limit_secs" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Mode" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Mode" ID="Sorter_Quiz_Mode" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_Mode" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Added_on" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Added On" ID="Sorter_Added_on" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Added_on" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Modified_on" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Modified On" ID="Sorter_Modified_on" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Modified_on" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Added_By" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Added By" ID="Sorter_Added_By" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Added_By" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Modified_By" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Modified By" ID="Sorter_Modified_By" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Modified_By" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Total_Points" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Total Points" ID="Sorter_Quiz_Total_Points" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_Total_Points" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_Category" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz Category" ID="Sorter_Quiz_Category" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_Category" runat="server"/></th>
 
    <th scope="col"></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Quiz_User_Attempt_Limit" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Quiz User Attempt Limit" ID="Sorter_Quiz_User_Attempt_Limit" data-id="UC1_Edit_Existing_QuizTBL_QuizSorter_Quiz_User_Attempt_Limit" runat="server"/></th>
 
    <th scope="col">Edit Details&nbsp;</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTLabel Source="Quiz_id" DataType="Float" ID="Quiz_id" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Title" ID="Quiz_Title" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Descp" DataType="Memo" ID="Quiz_Descp" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Time_limit_secs" DataType="Integer" ID="Time_limit_secs" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="TBL_Quiz_Mode_Quiz_Mode" ID="Quiz_Mode" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Added_on" DataType="Date" ID="Added_on" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Modified_on" DataType="Date" ID="Modified_on" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="TBL_users_user_name" ID="Added_By" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="TBL_users1_user_name" ID="Modified_By" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_Total_Points" DataType="Integer" ID="Quiz_Total_Points" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Category_Name" ID="Quiz_Category" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right">&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Quiz_User_Attempt_Limit" DataType="Integer" ID="Quiz_User_Attempt_Limit" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLink Text="Edit" ID="Link1" data-id="UC1_Edit_Existing_QuizTBL_QuizLink1_{TBL_Quiz:rowNumber}" runat="server" HrefSource="~/KMS_EditQuiz.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="Quiz_id" SourceType="DataSourceColumn" Source="Quiz_id" Format="yyyy-MM-dd"/>
    </Parameters></mt:MTLink>&nbsp;</td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="14">No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td colspan="14">
      <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/>&nbsp;</td> 
  </tr>
</table>
&nbsp;</p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="TBL_QuizDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} TBL_Quiz_Mode.Quiz_Mode AS TBL_Quiz_Mode_Quiz_Mode, Quiz_id, Quiz_Title, Time_limit_secs, Quiz_Descp, Added_on, Modified_on,
Added_By, Modified_By, Quiz_Total_Points, Quiz_Category, Quiz_Value_Type, Quiz_User_Attempt_Limit, TBL_users.user_name AS TBL_users_user_name,
TBL_users1.user_name AS TBL_users1_user_name, Category_Name 
FROM (((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN TBL_users TBL_users1 ON
TBL_Quiz.Modified_By = TBL_users1.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM (((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN TBL_users TBL_users1 ON
TBL_Quiz.Modified_By = TBL_users1.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_Quiz_id" SourceType="URL" Source="s_Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="TBL_Quiz.Quiz_id"/>
     <mt:WhereParameter Name="Urls_Quiz_Title" SourceType="URL" Source="s_Quiz_Title" DataType="Text" Operation="And" Condition="Contains" SourceColumn="TBL_Quiz.Quiz_Title"/>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Integer" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="TBL_Quiz.Added_By"/>
   </SelectParameters>
</mt:MTDataSource>
<p><br>
<br>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
</p>




<!--End ASCX page-->

