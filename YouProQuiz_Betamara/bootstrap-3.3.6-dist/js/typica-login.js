﻿jQuery(document).ready(function ($) {

    $.backstretch([
      "bootstrap-3.3.6-dist/css/bg1.png",
      "bootstrap-3.3.6-dist/css/bg2.png"
    ], { duration: 3000, fade: 750 });

});
