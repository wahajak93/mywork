﻿<%--<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-499931D3-->--%>
<%@ Page language="C#" CodeFile="Pg_QuizHomePageEvents.aspx.cs" Inherits="YouProQuiz_Beta.Pg_QuizHomePage_Page" MasterPageFile="~/Dashboard.master" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/UC_Quiz_Intro.ascx" TagPrefix="uc1" TagName="UC_Quiz_Intro" %>
<%@ Register Src="~/UC_LeaderBoard.ascx" TagPrefix="uc1" TagName="UC_LeaderBoard" %>

<%--
<!--End ASPX page header-->

<!--ASPX page @1-263A6001-->
<html>
<head>--%>
<asp:Content ContentPlaceHolderID="head" runat="server">

<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">


<title>Pg_QuizHomePage</title>

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script src='ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>' type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>
<%--</head>
<body>
    <h1>Quiz Overview</h1>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">--%>
 <asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
     <uc1:UC_Quiz_Intro runat="server" ID="UC_Quiz_Intro" />
     
     <div class="container">
      <div class="col-lg-12">
    <div class ="text-right">
        <asp:Button ID="Button1" runat="server" Text="Attempt Quiz" OnClick="Button1_Click"  CssClass="btn btn-primary"/> 
    </div>

    </div></div>

    <uc1:UC_LeaderBoard runat="server" ID="UC_LeaderBoard" />


  
     </asp:Content>
<%--</form>
<center><font face="Arial"><small>Ge&#110;e&#114;a&#116;e&#100; <!-- CCS -->&#119;&#105;&#116;h <!-- SCC -->C&#111;d&#101;&#67;ha&#114;g&#101; <!-- CCS -->&#83;&#116;&#117;&#100;io.</small></font></center></body>
</html>

<!--End ASPX page-->--%>

