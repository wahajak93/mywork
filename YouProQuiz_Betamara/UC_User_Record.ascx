<!--ASCX page header @1-4C15F136-->
<%@ Control language="C#" CodeFile="UC_User_RecordEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC_User_Record_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-760561E8-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<mt:Record DataSourceID="View_for_User_Quiz_Record1DataSource" AllowInsert="False" AllowDelete="False" AllowUpdate="False" ID="View_for_User_Quiz_Record1" runat="server" OnValidating="View_for_User_Quiz_Record1_Validating"><ItemTemplate><div data-emulate-form="UC_User_RecordView_for_User_Quiz_Record1" id="UC_User_RecordView_for_User_Quiz_Record1">







  <h2>Search </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# View_for_User_Quiz_Record1.GetControl<InMotion.Web.Controls.MTTextBox>("s_TBL_Quiz_Quiz_id").ClientID %>">Quiz ID</label></td> 
      <td><mt:MTTextBox Caption="TBL Quiz Quiz Id" maxlength="10" type="number" Columns="10" ID="s_TBL_Quiz_Quiz_id" data-id="UC_User_RecordView_for_User_Quiz_Record1s_TBL_Quiz_Quiz_id" runat="server" ErrorControl="MTLabel1" /><mt:MTLabel id="MTLabel1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# View_for_User_Quiz_Record1.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_Name").ClientID %>">Quiz Name</label></td> 
      <td><mt:MTTextBox Caption="Quiz Name" maxlength="50" Columns="50" ID="s_Quiz_Name" data-id="UC_User_RecordView_for_User_Quiz_Record1s_Quiz_Name" runat="server" ErrorControl="MTLabel2" /><mt:MTLabel id="MTLabel2" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="UC_User_RecordView_for_User_Quiz_Record1Button_DoSearch" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="View_for_User_Quiz_Record1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
</mt:MTDataSource><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="5" DataSourceID="View_for_User_Quiz_RecordDataSource" ID="View_for_User_Quiz_Record" runat="server">
<HeaderTemplate>
<p>&nbsp;</p>
<table>
  <tr>
    <th scope="col">Quiz Id</th>
 
    <th scope="col">Quiz Name</th>
 
    <th scope="col">Category Name</th>
 
    <th scope="col">User Attempt No</th>
 
    <th scope="col">Date Attempted Last</th>
 
    <th scope="col">Result In Percent</th>
 
    <th scope="col">Details</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="QuizID" DataType="Integer" ID="TBL_Quiz_Quiz_id" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Quiz_Title" ID="Quiz_Name" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Category_Name" ID="Category_Name" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="AttemptID" DataType="Integer" ID="user_attempt_no" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="DateAttempted" DataType="Date" ID="Date_attempted_last" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="ResultInPercent" DataType="Float" ID="Result_in_percent" runat="server"/>&nbsp;</td> 
    <td>&nbsp;<mt:MTLink Text="View Details" ID="Link1" data-id="UC_User_RecordView_for_User_Quiz_RecordLink1_{View_for_User_Quiz_Record:rowNumber}" runat="server" HrefSource="~/Pg_UserRecordDetails.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="QuizID" SourceType="DataSourceColumn" Source="QuizID" Format="yyyy-MM-dd"/>
      <mt:UrlParameter Name="AttemptID" SourceType="DataSourceColumn" Source="AttemptID"/>
    </Parameters></mt:MTLink></td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="7">No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td colspan="7">
      <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" ID="Navigator" runat="server"/>&nbsp;</td> 
  </tr>
</table>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="View_for_User_Quiz_RecordDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} tblUserQuizResultData.*, Quiz_Title, TBL_Quiz_Mode.Quiz_Mode AS TBL_Quiz_Mode_Quiz_Mode, Category_Name, Quiz_User_Attempt_Limit 
FROM tblUserQuizResultData LEFT JOIN ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id) ON
tblUserQuizResultData.QuizID = TBL_Quiz.Quiz_id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblUserQuizResultData LEFT JOIN ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id) ON
tblUserQuizResultData.QuizID = TBL_Quiz.Quiz_id
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="UserID"/>
     <mt:WhereParameter Name="Urls_TBL_Quiz_Quiz_id" SourceType="URL" Source="s_TBL_Quiz_Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="Quiz_id"/>
     <mt:WhereParameter Name="Urls_Quiz_Name" SourceType="URL" Source="s_Quiz_Name" DataType="Text" Operation="And" Condition="Contains" SourceColumn="Quiz_Title"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<p><br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>




<!--End ASCX page-->

