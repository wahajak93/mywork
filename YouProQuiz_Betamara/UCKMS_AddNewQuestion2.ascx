<!--ASCX page header @1-B1EC49D9-->
<%@ Control language="C#" CodeFile="UCKMS_AddNewQuestion2Events.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_AddNewQuestion2_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-F457A175-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-BEF39FE0
</script>
<mt:MTPanel ID="___link_panel_09065192582141535" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/jquery-ui.css">
</mt:MTPanel>
<mt:MTPanel ID="___link_panel_019850426675590238" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/ccs-jquery-ui-calendar.css">
</mt:MTPanel>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-493B6C2A
    $('body').ccsBind(function() {
        features["UCKMS_AddNewQuestion2tblQuestionQuestionInsertedOnInlineDatePicker1"] = $('*:ccsControl(UCKMS_AddNewQuestion2, tblQuestion, QuestionInsertedOn)');
        features["UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedOnInlineDatePicker2"] = $('*:ccsControl(UCKMS_AddNewQuestion2, tblQuestion, QuestionUpdatedOn)');
    });
//End Controls Selecting

//Feature Parameters @1-DA1A2371
    params["UCKMS_AddNewQuestion2tblQuestionQuestionInsertedOnInlineDatePicker1"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
    params["UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedOnInlineDatePicker2"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
//End Feature Parameters

//Feature Init @1-3FAC8080
    features["UCKMS_AddNewQuestion2tblQuestionQuestionInsertedOnInlineDatePicker1"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_AddNewQuestion2tblQuestionQuestionInsertedOnInlineDatePicker1"]);
    });
    features["UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedOnInlineDatePicker2"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedOnInlineDatePicker2"]);
    });
//End Feature Init

//UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad Event Start @-6775AC84
    actions["UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad Event Start

//Attach CKeditor @179-64E0EBFD
        try {
            var UCKMS_AddNewQuestion2tblQuestionQuestion_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "250",
                width: "350",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad Event End

//UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad Event Start @-2A434795
    actions["UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad Event Start

//Attach CKeditor @49-6D53B2BD
        try {
            var UCKMS_AddNewQuestion2tblChoiceChoice_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "300",
                width: "350",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad Event End

//Event Binding @1-292CC64C
    $('*:ccsControl(UCKMS_AddNewQuestion2, tblQuestion, Question)').ccsBind(function() {
        this.each(function(){ actions["UCKMS_AddNewQuestion2tblQuestionQuestionOnLoad"].call(this); });
    });
    $('*:ccsControl(UCKMS_AddNewQuestion2, tblChoice, Choice)').ccsBind(function() {
        this.each(function(){ actions["UCKMS_AddNewQuestion2tblChoiceChoiceOnLoad"].call(this); });
    });
//End Event Binding

//Plugin Calls @1-BB1602F3
    $('*:ccsControl(UCKMS_AddNewQuestion2, tblQuestion, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<p>&nbsp;</p>
<br>
<mt:Record PreserveParameters="Get" DataSourceID="tblQuestionDataSource" OnBeforeInsert="tblQuestion_BeforeInsert" OnAfterInsert="tblQuestion_AfterInsert" OnBeforeUpdate="tblQuestion_BeforeUpdate" OnAfterDelete="tblQuestion_AfterDelete" ID="tblQuestion" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion2tblQuestion" id="UCKMS_AddNewQuestion2tblQuestion">







  <h2>Add/Edit Tbl Question </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="4"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").ClientID %>">Question Name</label></td> 
      <td colspan="3"><mt:MTTextBox Source="Question_Name" Caption="Question Name" maxlength="50" Columns="50" ID="Question_Name" data-id="UCKMS_AddNewQuestion2tblQuestionQuestion_Name" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question").ClientID %>">Question</label></td> 
      <td colspan="3">
        <p>&nbsp;</p>
 
        <p>&nbsp; 
<mt:MTTextBox TextMode="Multiline" Source="Question" DataType="Memo" Caption="Question" Rows="3" Columns="10" ID="Question" data-id="UCKMS_AddNewQuestion2tblQuestionQuestion" runat="server"/>
</p>
 </td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionInsertedOn" DataType="Date" Format="d" Caption="Question Inserted On" ID="QuestionInsertedOn" data-id="UCKMS_AddNewQuestion2tblQuestionQuestionInsertedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionInsertedBy" DataType="Float" Caption="Question Inserted By" ID="QuestionInsertedBy" data-id="UCKMS_AddNewQuestion2tblQuestionQuestionInsertedBy" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionUpdatedOn" DataType="Date" Format="d" Caption="Question Updated On" OnLoad="tblQuestionQuestionUpdatedOn_Load" ID="QuestionUpdatedOn" data-id="UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td></td> 
      <td colspan="3"><mt:MTHidden Source="QuestionUpdatedBy" DataType="Float" Caption="Question Updated By" ID="QuestionUpdatedBy" data-id="UCKMS_AddNewQuestion2tblQuestionQuestionUpdatedBy" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionStatusID").ClientID %>">Question Status ID</label></td> 
      <td colspan="3">
        <mt:MTListBox Rows="1" Source="QuestionStatusID" DataType="Float" Caption="Question Status ID" DataSourceID="QuestionStatusIDDataSource" DataValueField="StatusID" DataTextField="Status" ID="QuestionStatusID" data-id="UCKMS_AddNewQuestion2tblQuestionQuestionStatusID" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="QuestionStatusIDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblRefStatus {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td>&nbsp;Question Type For Current Quiz </td> 
      <td>&nbsp; 
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Type_ID" DataSourceID="ListBox1DataSource" DataValueField="Quest_Type_ID" DataTextField="Quest_Type" ID="ListBox1" data-id="UCKMS_AddNewQuestion2tblQuestionListBox1" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionType {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
      <td></td> 
      <td>&nbsp;</td> 
    </tr>
 
    <tr>
      <td>&nbsp;&nbsp;Question Category For Current Quiz</td> 
      <td>&nbsp; 
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Category_ID" DataSourceID="ListBox2DataSource" DataValueField="CategoryID" DataTextField="Category" ID="ListBox2" data-id="UCKMS_AddNewQuestion2tblQuestionListBox2" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox2DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionCategory {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 
        <td>&nbsp;</td> 
        <td>&nbsp;</td></td> 
    </tr>
 
    <tr>
      <td>&nbsp;Question Time For Current Quiz</td> 
      <td>&nbsp;<mt:MTTextBox Source="Quiz_Quest_Time" ID="TextBox1" data-id="UCKMS_AddNewQuestion2tblQuestionTextBox1" runat="server"/></td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="4">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion2tblQuestionButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="UCKMS_AddNewQuestion2tblQuestionButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="UCKMS_AddNewQuestion2tblQuestionButton_Delete" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblQuestionDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="SQL" DeleteCommandType="SQL" OnInitializeUpdateParameters="tblQuestion_InitializeUpdateParameters" runat="server">
   <SelectCommand>
SELECT * 
FROM tblQuestion LEFT JOIN tbl_Quiz_Question ON
tblQuestion.QuestionID = tbl_Quiz_Question.Quiz_Quest_ID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblQuestion([Question_Name], [Question], [QuestionInsertedOn], [QuestionInsertedBy], [QuestionUpdatedOn], [QuestionUpdatedBy], [QuestionStatusID]) VALUES ({Question_Name}, {Question}, {QuestionInsertedOn}, {QuestionInsertedBy}, {QuestionUpdatedOn}, {QuestionUpdatedBy}, {QuestionStatusID})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblQuestion SET Question_Name='{Question_Name}', QuestionUpdatedOn='{QuestionUpdatedOn}', QuestionUpdatedBy={QuestionUpdatedBy}, QuestionStatusID=1, Question='{TextArea1}' WHERE  QuestionID = {QuestionID}
UPDATE tbl_Quiz_Question SET Quiz_Quest_Category_ID={catid},Quiz_Quest_Type_ID={typid},Quiz_Quest_Time={time} WHERE  Quiz_Quest_ID= {QuestionID} AND Quiz_ID={quiz_id}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tbl_Quiz_Question WHERE  Quiz_Quest_ID = {QuestionID} AND Quiz_ID={quiz_id}
DELETE FROM tbl_Quiz_Question_Answers WHERE QuestionID={QuestionID} AND Quiz_ID={quiz_id}
DELETE FROM tbl_Quiz_Question_Choice WHERE Quest_ID={QuestionID} AND Quiz_ID={quiz_id}
  </DeleteCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="tblQuestion.QuestionID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="Question_Name" SourceType="Control" Source="Question_Name" DataType="Text" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Question" SourceType="Control" Source="Question" DataType="Memo" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionInsertedOn" SourceType="Control" Source="QuestionInsertedOn" DataType="Date" Format="d" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionInsertedBy" SourceType="Control" Source="QuestionInsertedBy" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionUpdatedOn" SourceType="Control" Source="QuestionUpdatedOn" DataType="Date" Format="d" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionUpdatedBy" SourceType="Control" Source="QuestionUpdatedBy" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="QuestionStatusID" SourceType="Control" Source="QuestionStatusID" DataType="Float" IsOmitEmptyValue="True"/>
   </InsertParameters>
   <UpdateParameters>
     <mt:SqlParameter Name="Question_Name" SourceType="Control" Source="Question_Name" DataType="Text"/>
     <mt:SqlParameter Name="QuestionUpdatedOn" SourceType="Control" Source="QuestionUpdatedOn" DataType="Text" DBFormat="yyyy-mm-dd HH:nn:ss.S" Format="GeneralDate"/>
     <mt:SqlParameter Name="QuestionUpdatedBy" SourceType="Session" Source="UserID" DataType="Text"/>
     <mt:SqlParameter Name="QuestionStatusID" SourceType="Control" Source="QuestionStatusID" DataType="Text"/>
     <mt:SqlParameter Name="TextArea1" SourceType="Control" Source="Question" DataType="Text"/>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="catid" SourceType="Control" Source="ListBox2" DataType="Text"/>
     <mt:SqlParameter Name="typid" SourceType="Control" Source="ListBox1" DataType="Text"/>
     <mt:SqlParameter Name="time" SourceType="Control" Source="TextBox1" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
   </UpdateParameters>
   <DeleteParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
   </DeleteParameters>
</mt:MTDataSource>
<p><br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>
<p>
<mt:Grid PageSizeLimit="100" RecordsPerPage="15" DataSourceID="View_2DataSource" ID="View_2" runat="server">
<HeaderTemplate>
<h2>List of View 2</h2>
<p>
<table>
  <tr>
    <th scope="col"></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Choice_ID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice ID" ID="Sorter_Choice_ID" data-id="UCKMS_AddNewQuestion2View_2Sorter_Choice_ID" runat="server"/></th>
 
    <th scope="col"></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Choice_Score" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice Score" ID="Sorter_Choice_Score" data-id="UCKMS_AddNewQuestion2View_2Sorter_Choice_Score" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Choice" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice" ID="Sorter_Choice" data-id="UCKMS_AddNewQuestion2View_2Sorter_Choice" runat="server"/></th>
 
    <th scope="col"></th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTHidden Source="Quiz_ID" DataType="Float" ID="Quiz_ID" data-id="UCKMS_AddNewQuestion2View_2Quiz_ID_{View_2:rowNumber}" runat="server"/>&nbsp;</td> 
    <td>
      <p><mt:MTLabel Source="Choice_ID" DataType="Float" ID="Choice_ID" runat="server"/>&nbsp;</p>
 
      <p>&nbsp;</p>
 </td> 
    <td><mt:MTHidden Source="Quest_ID" DataType="Float" ID="Quest_ID" data-id="UCKMS_AddNewQuestion2View_2Quest_ID_{View_2:rowNumber}" runat="server"/>&nbsp;</td> 
    <td style="TEXT-ALIGN: right"><mt:MTLabel Source="Choice_Score" DataType="Integer" ID="Choice_Score" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Choice" ID="Choice" runat="server" Mode="PassThrough" ContentType="Html" />&nbsp;</td> 
    <td><mt:MTLink Text="edit" ID="Link1" data-id="UCKMS_AddNewQuestion2View_2Link1_{View_2:rowNumber}" runat="server" HrefSource="~/KMS_EditChoice.aspx" PreserveParameters="Get"/></td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="6">No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td colspan="6">
      <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/></td> 
  </tr>
</table>
</p>
<p></p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="View_2DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} Choice, tbl_Quiz_Question_Choice.* 
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
   </SelectParameters>
</mt:MTDataSource>
<p>&nbsp;</p>
<p></p>
<p>&nbsp;</p>
&nbsp; 
<mt:Record PreserveParameters="Get" DataSourceID="tblChoiceDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" OnBeforeInsert="tblChoice_BeforeInsert" OnAfterInsert="tblChoice_AfterInsert" ID="tblChoice" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion2tblChoice" id="UCKMS_AddNewQuestion2tblChoice">


  <h2>Add/Edit Tbl Choice </h2>
 
  <p>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td></td> 
      <td><mt:MTHidden Source="QuestionID" DataType="Float" Required="True" Caption="Question ID" ID="QuestionID" data-id="UCKMS_AddNewQuestion2tblChoiceQuestionID" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("Choice").ClientID %>">Choice</label></td> 
      <td>
        <p></p>
 
        <p>
<mt:MTTextBox TextMode="Multiline" Source="Choice" Caption="Choice" Rows="3" Columns="10" ID="Choice" data-id="UCKMS_AddNewQuestion2tblChoiceChoice" runat="server"/>
&nbsp; </p>
 </td> 
    </tr>
 
    <tr>
      <td>Score</td> 
      <td><mt:MTTextBox ID="TextBox1" data-id="UCKMS_AddNewQuestion2tblChoiceTextBox1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2"><mt:MTHidden ID="Hidden1" data-id="UCKMS_AddNewQuestion2tblChoiceHidden1" runat="server"/>
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion2tblChoiceButton_Insert" runat="server"/></td> 
    </tr>
 
  </table>
 </p>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblChoiceDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="SQL" UpdateCommandType="Table" OnInitializeInsertParameters="tblChoice_InitializeInsertParameters" runat="server">
   <SelectCommand>
SELECT * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblChoice(QuestionID, Choice) VALUES({QuestionID}, '{Choice}')
INSERT INTO tbl_Quiz_Question_Choice SELECT {quiz_id},MAX(tblChoice.ChoiceID),{QuestionID},'{score}' FROM tblChoice where tblChoice.QuestionID={QuestionID}
  </InsertCommand>
   <UpdateCommand>
UPDATE tblChoice SET [QuestionID]={QuestionID}, [Choice]={Choice}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlChoice" SourceType="URL" Source="Choice" DataType="Text" Operation="And" Condition="Equal" SourceColumn="Choice" Required="true"/>
     <mt:WhereParameter Name="UrlChoiceID" SourceType="URL" Source="ChoiceID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="ChoiceID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="Control" Source="QuestionID" DataType="Float"/>
     <mt:SqlParameter Name="Choice" SourceType="Control" Source="Choice" DataType="Text"/>
     <mt:SqlParameter Name="choiceid" SourceType="Control" Source="Hidden1" DataType="Text"/>
     <mt:SqlParameter Name="quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
     <mt:SqlParameter Name="score" SourceType="Control" Source="TextBox1" DataType="Text"/>
   </InsertParameters>
</mt:MTDataSource>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tblChoice_tblAnswer_tbl_QDataSource" ID="tblChoice_tblAnswer_tbl_Q" runat="server">
<HeaderTemplate>
<h2>List of Tbl Quiz Question Answers, Tbl Answer, Tbl Choice</h2>
<p>
<table>
  <tr>
    <th scope="col"></th>
 
    <th scope="col"></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="tbl_Quiz_Question_Answers_ChoiceID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Tbl Quiz Question Answers Choice ID" ID="Sorter_tbl_Quiz_Question_Answers_ChoiceID" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QSorter_tbl_Quiz_Question_Answers_ChoiceID" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Score" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Score" ID="Sorter_Score" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QSorter_Score" runat="server"/></th>
 
    <th scope="col"></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Choice" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice" ID="Sorter_Choice" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QSorter_Choice" runat="server"/></th>
 
    <th scope="col"></th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTHidden Source="Quiz_ID" DataType="Float" ID="Quiz_ID" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QQuiz_ID_{tblChoice_tblAnswer_tbl_Q:rowNumber}" runat="server"/>&nbsp;</td> 
    <td><mt:MTHidden Source="tbl_Quiz_Question_Answers_QuestionID" DataType="Float" ID="tbl_Quiz_Question_Answers_QuestionID" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_Qtbl_Quiz_Question_Answers_QuestionID_{tblChoice_tblAnswer_tbl_Q:rowNumber}" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="tbl_Quiz_Question_Answers_ChoiceID" DataType="Float" ID="tbl_Quiz_Question_Answers_ChoiceID" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Score" DataType="Float" ID="Score" runat="server"/>&nbsp;</td> 
    <td><mt:MTHidden Source="AnswerID" DataType="Float" ID="AnswerID" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QAnswerID_{tblChoice_tblAnswer_tbl_Q:rowNumber}" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Choice" ID="Choice" runat="server" ContentType="Html" Mode="PassThrough" />&nbsp;</td> 
    <td><mt:MTLink Text="Edit" ID="Link1" data-id="UCKMS_AddNewQuestion2tblChoice_tblAnswer_tbl_QLink1_{tblChoice_tblAnswer_tbl_Q:rowNumber}" runat="server" PreserveParameters="Get"/></td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="7">No records</td> 
  </tr>
 </mt:MTPanel>
</table>
</p>
<p></p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="tblChoice_tblAnswer_tbl_QDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} Quiz_ID, tbl_Quiz_Question_Answers.QuestionID AS tbl_Quiz_Question_Answers_QuestionID, tbl_Quiz_Question_Answers.ChoiceID AS tbl_Quiz_Question_Answers_ChoiceID,
Score, AnswerID, Choice 
FROM (tbl_Quiz_Question_Answers LEFT JOIN tblAnswer ON
tbl_Quiz_Question_Answers.QuestionID = tblAnswer.QuestionID AND tbl_Quiz_Question_Answers.ChoiceID = tblAnswer.ChoiceID) LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.QuestionID = tblChoice.QuestionID AND tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM (tbl_Quiz_Question_Answers LEFT JOIN tblAnswer ON
tbl_Quiz_Question_Answers.QuestionID = tblAnswer.QuestionID AND tbl_Quiz_Question_Answers.ChoiceID = tblAnswer.ChoiceID) LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.QuestionID = tblChoice.QuestionID AND tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tbl_Quiz_Question_Answers.Quiz_ID"/>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tblAnswer.QuestionID"/>
   </SelectParameters>
</mt:MTDataSource>
<p>&nbsp;</p>
<p></p>
<p>&nbsp;</p>
<p>
<mt:Record PreserveParameters="Get" DataSourceID="tblAnswerDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" ID="tblAnswer" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AddNewQuestion2tblAnswer" id="UCKMS_AddNewQuestion2tblAnswer">


  <h2>Add/Edit Tbl Answer </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# tblAnswer.GetControl<InMotion.Web.Controls.MTListBox>("ChoiceID").ClientID %>">Choice ID</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="ChoiceID" DataType="Float" Required="True" Caption="Choice ID" DataSourceID="ChoiceIDDataSource" DataValueField="ChoiceID" DataTextField="ChoiceID" ID="ChoiceID" data-id="UCKMS_AddNewQuestion2tblAnswerChoiceID" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ChoiceIDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
           <SelectParameters>
             <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
           </SelectParameters>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td>Score</td> 
      <td><mt:MTTextBox ID="TextBox1" data-id="UCKMS_AddNewQuestion2tblAnswerTextBox1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_AddNewQuestion2tblAnswerButton_Insert" runat="server"/><mt:MTHidden Source="QuestionID" DataType="Float" Required="True" Caption="Question ID" ID="QuestionID" data-id="UCKMS_AddNewQuestion2tblAnswerQuestionID" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblAnswerDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="SQL" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM tblAnswer {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO tblAnswer(QuestionID, ChoiceID) VALUES({QuestionID}, {ChoiceID})
INSERT INTO tbl_Quiz_Question_Answers(Quiz_ID,QuestionID,ChoiceID,Score) VALUES({Quiz_id},{QuestionID},{ChoiceID},{score})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblAnswer SET [QuestionID]={QuestionID}, [ChoiceID]={ChoiceID}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlAnswerID" SourceType="URL" Source="AnswerID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="AnswerID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="Control" Source="QuestionID" DataType="Float"/>
     <mt:SqlParameter Name="ChoiceID" SourceType="Control" Source="ChoiceID" DataType="Float"/>
     <mt:SqlParameter Name="Quiz_id" SourceType="URL" Source="Quiz_id" DataType="Text"/>
     <mt:SqlParameter Name="score" SourceType="Control" Source="TextBox1" DataType="Text"/>
   </InsertParameters>
</mt:MTDataSource>



<!--End ASCX page-->

