﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;


public partial class UC_UserOverallQuizChart : System.Web.UI.UserControl
{
    
        public string user_id = "0";
        public string quiz_id = "1";
        public string[] questions = new string[5];
       // public float[] points = new float[5];

        public string[] questions1 = new string[5];
        //public float[] points1 = new float[5];




        protected void Page_Init(Object sender, EventArgs e)
        {


        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {


                user_id = Session["UserID"].ToString();
                DataTable dtx1 = clsdb.readdata("SELECT user_name from TBL_Users where id="+user_id+"");
                string name1 = dtx1.Rows[0][0].ToString();
                //Response.Write(user_id);
                float[] points = new float[4];
                float[] points1 = new float[4];
                string[] questions = new string[5];

                DataTable dt1 = clsdb.readdata("select user_id,quiz_id,MAX(user_attempt_no) as no_of_attempts ,avg(Result_in_percent) as Avg_Percent from TBL_User_Quiz_Stats where quiz_id IN (select distinct quiz_id from TBL_User_Quiz_Stats where user_id=" + user_id + ") AND user_id=" + user_id + " GROUP BY  user_id,quiz_id order by quiz_id");

                if (dt1.Rows.Count > 0)
                {
                    points = new float[dt1.Rows.Count];
                    questions = new string[dt1.Rows.Count];
                    for (int x = 0; x < dt1.Rows.Count; x++)
                    {
                        questions[x] = dt1.Rows[x]["quiz_id"].ToString();
                        string a = dt1.Rows[x]["Avg_Percent"].ToString();
                        points[x] = float.Parse(a);
                    }


                }


                DataTable dt2 = clsdb.readdata("select quiz_id,avg(Result_in_percent) as Avg_Percent from TBL_User_Quiz_Stats where quiz_id IN (select distinct quiz_id from TBL_User_Quiz_Stats where user_id=" + user_id + ") GROUP BY quiz_id order by quiz_id");
                if (dt2.Rows.Count > 0)
                {
                    points1 = new float[dt2.Rows.Count];


                    for (int x = 0; x < dt2.Rows.Count; x++)
                    {
                        questions1[x] = dt2.Rows[x]["quiz_id"].ToString();

                        string a = dt2.Rows[x]["Avg_Percent"].ToString();


                        points1[x] = float.Parse(a);

                    }

                    List<string> tempString = new List<string>();
                    tempString.Add("Hello");
                    tempString.Add("World");

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<script>");
                    sb.Append("var questions = new Array;var points=new Array;points1=new Array;var name1;");
                    foreach (string str in questions)
                    {
                        sb.Append("questions.push('" + str + "');");

                    }
                    foreach (float a in points)
                    {

                        sb.Append("points.push(" + a + ");");
                    }
                    foreach (float a in points1)
                    {

                        double xx = Math.Round(a, 2);

                        sb.Append("points1.push(" + xx + ");");
                    }
                    sb.Append("name1='"+name1+"';");

                    sb.Append("</script>");

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "strBuilderJS", sb.ToString(), false);

                }
            }
        }
    }

