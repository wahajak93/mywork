﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testing.aspx.cs" Inherits="testing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script>

        function btnCallWCF_onclick()
        {
            var MCN1 = new Service();
            MCN1.MCNUSER($get("txtUNm").Value, OnServiceComplete, onerror);
        }
        function OnServiceComplete(result) {
            $get("dispService").innerHTML = result;
        }
        function onerror(result) {
            alert(result.get_message());
        }
</script>

 
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/Service.svc" />
            </Services>
        </asp:ScriptManager>
    </div>

        <div id ="dispService">
    <input id="btnCallWCF" type="button" value="MCN User" onclick="return btnCallWCF_onclick()"  />
               <input id="txtUNm" type="text" />  
            </div>
    </form>
</body>
</html>
