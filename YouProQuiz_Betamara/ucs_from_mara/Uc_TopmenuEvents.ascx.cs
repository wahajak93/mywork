//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-E15A59F8
public partial class Uc_Topmenu_Page : MTUserControl
{
//End Page class

//Attributes constants @1-B958E248
    public const string Attribute_MenuType = "MenuType";
    public const string Attribute_Item_Level = "Item_Level";
    public const string Attribute_Submenu = "Submenu";
    public const string Attribute_Target = "Target";
    public const string Attribute_Title = "Title";
//End Attributes constants

//Page Uc_Topmenu Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page Uc_Topmenu Event Init

//ScriptIncludesInit @1-B2B60718
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/menu/ccs-menu.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page Uc_Topmenu Init event tail @1-FCB6E20C
    }
//End Page Uc_Topmenu Init event tail

//Menu Menu1 Event Init @2-F233BD07
    protected void Menu1_Init(object sender, EventArgs e) {
//End Menu Menu1 Event Init

//Set Attributes @2-937EB4DB
        ((InMotion.Web.Controls.Menu)sender).Attributes.Add("Target", Convert.ToString(""));
        ((InMotion.Web.Controls.Menu)sender).Attributes.Add("Title", Convert.ToString(""));
//End Set Attributes

//Menu Menu1 Init event tail @2-FCB6E20C
    }
//End Menu Menu1 Init event tail

//Link ItemLink Event Init @6-C3ED2B6E
    protected void Menu1ItemLink_Init(object sender, EventArgs e) {
//End Link ItemLink Event Init

//Set Attributes @6-08B41A24
        ((InMotion.Web.Controls.MTLink)sender).Attributes.Add("class", Menu1.Attributes["Submenu"]);
        ((InMotion.Web.Controls.MTLink)sender).Attributes.Add("target", Menu1.Attributes["Target"]);
        ((InMotion.Web.Controls.MTLink)sender).Attributes.Add("title", Menu1.Attributes["Title"]);
//End Set Attributes

//Link ItemLink Init event tail @6-FCB6E20C
    }
//End Link ItemLink Init event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

