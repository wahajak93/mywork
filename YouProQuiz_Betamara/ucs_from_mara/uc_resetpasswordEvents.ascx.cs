//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-B12BB90D
public partial class uc_resetpassword_Page : MTUserControl
{
//End Page class

//Page uc_resetpassword Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page uc_resetpassword Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page uc_resetpassword Init event tail @1-FCB6E20C
    }
//End Page uc_resetpassword Init event tail

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          // -------------------------
//DEL          NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").Value = "";

//Record NewRecord1 Event Before Show @2-9FB2BE54
    protected void NewRecord1_BeforeShow(object sender, EventArgs e) {
//End Record NewRecord1 Event Before Show

//Record NewRecord1 Event Before Show. Action Preserve Password @4-349A8264
        NewRecord1.GetControl<MTHidden>("user_password_Shadow").Text = InMotion.Security.Cryptography.EncodeString(NewRecord1.GetControl<MTTextBox>("user_password").Text, ConfigurationManager.AppSettings["RC4EncryptionKey"]);
        NewRecord1.GetControl<MTTextBox>("user_password").Text = "";
//End Record NewRecord1 Event Before Show. Action Preserve Password

//Record NewRecord1 Before Show event tail @2-FCB6E20C
    }
//End Record NewRecord1 Before Show event tail

//Record NewRecord1 Event On Validate @2-DAB8D08D
    protected void NewRecord1_Validating(object sender, ValidatingEventArgs e) {
//End Record NewRecord1 Event On Validate

//Record NewRecord1 Event On Validate. Action Custom Code @21-2A29BDB7
        // -------------------------
        // Write your own code here.
          MTTextBox tbPwd = NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password");
        MTTextBox tbCnfPwd = NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("Confirm_password");
        
        if(!(tbPwd.IsEmpty && tbCnfPwd.IsEmpty))
        {
        	if(!(tbPwd.Text).Equals(tbCnfPwd.Text))
        	{
        		tbPwd.Errors.Add("Password and Confirm Password not match");
        	}
        }
        // -------------------------
//End Record NewRecord1 Event On Validate. Action Custom Code

//Record NewRecord1 On Validate event tail @2-FCB6E20C
    }
//End Record NewRecord1 On Validate event tail

//Record NewRecord1 Event Before Build Update @2-F1833024
    protected void NewRecord1_BeforeBuildUpdate(object sender, DataOperationEventArgs e) {
//End Record NewRecord1 Event Before Build Update

//Record NewRecord1 Event Before Build Update. Action Encrypt Password @6-A3F8C3A8
        if (e.Command.Parameters["user_password"].Value.ToString() != "")
            e.Command.Parameters["user_password"].Value = InMotion.Security.Cryptography.MD5(e.Command.Parameters["user_password"].Value.ToString());
        else
            e.Command.Parameters["user_password"].Value = InMotion.Security.Cryptography.DecodeString(NewRecord1.GetControl<MTHidden>("user_password_Shadow").Text, ConfigurationManager.AppSettings["RC4EncryptionKey"]);
//End Record NewRecord1 Event Before Build Update. Action Encrypt Password

//Record NewRecord1 Event Before Build Update. Action Custom Code @20-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record NewRecord1 Event Before Build Update. Action Custom Code

//Record NewRecord1 Before Build Update event tail @2-FCB6E20C
    }
//End Record NewRecord1 Before Build Update event tail

//TextBox user_password Event On Validate @8-D87C4CC0
    protected void NewRecord1user_password_Validating(object sender, ValidatingEventArgs e) {
//End TextBox user_password Event On Validate

//TextBox user_password Event On Validate. Action Reset Password Validation @9-2330E411
        if (!NewRecord1.IsInsertMode && NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").IsEmpty)
        {
            NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").Errors.Clear();
            NewRecord1.RemoveError(NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password"));
            e.HasErrors = false;
        }
//End TextBox user_password Event On Validate. Action Reset Password Validation

//TextBox user_password On Validate event tail @8-FCB6E20C
    }
//End TextBox user_password On Validate event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

