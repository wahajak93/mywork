<!--ASCX page header @1-78DA6DD3-->
<%@ Control language="C#" CodeFile="Uc_TopmenuEvents.ascx.cs" Inherits="YouProQuiz_Beta.Uc_Topmenu_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-E2956484-->






<%-- <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li><a href="../navbar-static-top/">Static top</a></li>
            <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>--%>

<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">

<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         
          <a class="navbar-brand" href="#">Learners Quiz</a>
        </div>
          
<mt:Menu MenuType="Horizontal" ParentItemIDFieldName="ParentID" ItemIDFieldName="MenuID" DataSourceID="Menu1DataSource" OnInit="Menu1_Init" ID="Menu1" runat="server"  >
<HeaderTemplate>

<div id="navbar" class="navbar-collapse collapse"  >
  <ul class="nav navbar-nav <%= Menu1.Attributes[Attribute_MenuType] %> navbar-right ">
    
</HeaderTemplate>
<ItemTemplate>
    
      <li  class="dropdown" >





<mt:MTLink Source="MenuName" OnInit="Menu1ItemLink_Init" ID="ItemLink" runat="server" HrefType="Database" HrefSource="MenuLink" PreserveParameters="Get"  /> 
      
</ItemTemplate>
<CloseItemTemplate></li>
 
</CloseItemTemplate>
<OpenLevelTemplate >
    <ul class="dropdown-menu"<%= Menu1.Attributes[Attribute_Item_Level] %>" >
      
</OpenLevelTemplate>
<CloseLevelTemplate>
    </ul>
 </li>
 
</CloseLevelTemplate>
<FooterTemplate>
  </ul>
</div>

</FooterTemplate>
</mt:Menu>
          </div>
    </nav>
<mt:MTDataSource ID="Menu1DataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM top_Menu {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
</mt:MTDataSource>



<!--End ASCX page-->

