<!--ASCX page header @1-34312A1E-->
<%@ Control language="C#" CodeFile="uc_resetpasswordEvents.ascx.cs" Inherits="YouProQuiz_Beta.uc_resetpassword_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-E859515A-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<br>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<br>
<br>
<mt:Record PreserveParameters="Get" DataSourceID="NewRecord1DataSource" AllowInsert="False" AllowDelete="False" OnBeforeShow="NewRecord1_BeforeShow" OnValidating="NewRecord1_Validating" ID="NewRecord1" runat="server"><ItemTemplate><div data-emulate-form="uc_resetpasswordNewRecord1" id="uc_resetpasswordNewRecord1">







  <h2>Add/Edit TBL Users </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").ClientID %>">User Password</label></td> 
      <td><mt:MTTextBox TextMode="Password" Source="user_password" Required="True" Caption="User Password" maxlength="50" Columns="50" OnValidating="NewRecord1user_password_Validating" ID="user_password" data-id="uc_resetpasswordNewRecord1user_password" runat="server"/></td> 
    </tr>
 
    <tr>
      <td>&nbsp;confirm password</td> 
      <td><mt:MTTextBox TextMode="Password" Required="True" ID="Confirm_password" data-id="uc_resetpasswordNewRecord1Confirm_password" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="uc_resetpasswordNewRecord1Button_Update" runat="server"/><mt:MTHidden ID="user_password_Shadow" data-id="uc_resetpasswordNewRecord1user_password_Shadow" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="NewRecord1DataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" UpdateCommandType="Table" OnBeforeBuildUpdate="NewRecord1_BeforeBuildUpdate" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_users {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <UpdateCommand>
UPDATE TBL_users SET user_password={user_password}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>




<!--End ASCX page-->

