//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-EABE8434
public partial class UC_LogOut_Page : MTUserControl
{
//End Page class

//Attributes constants @1-93D58DF7
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page UC_LogOut Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_LogOut Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Access Denied Page Url @1-2E5EF9F8
        this.AccessDeniedPage = "Home.aspx";
//End Access Denied Page Url

//Page UC_LogOut Init event tail @1-FCB6E20C
    }
//End Page UC_LogOut Init event tail

//Link Logout Event Load @2-7637EA15
    protected void Logout_Load(object sender, EventArgs e) {
//End Link Logout Event Load

//Set Link Parameter Code Expression @3-E96F9BE8
        ((InMotion.Web.Controls.MTLink)sender).Parameters.Add("Logout", Convert.ToString("True"));
//End Set Link Parameter Code Expression

//Link Logout Load event tail @2-FCB6E20C
    }
//End Link Logout Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

