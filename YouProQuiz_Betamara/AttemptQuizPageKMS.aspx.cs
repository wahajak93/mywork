﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;

public partial class AttemptQuizPage : MTPage
{

    public string user_id;

    protected void Page_Init(object sender, EventArgs e)
    {
        //End Page Dashboard Event Init

        if (Session["UserID"] != null)
        {
            user_id = Session["UserID"].ToString();
        }

        //Access Denied Page Url @1-2E5EF9F8
        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page Dashboard access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", true);
        //End Set Page Dashboard access rights.

        //Page Dashboard Init event tail @1-FCB6E20C
    }
    protected void Page_Load(object sender, EventArgs e)
    {

       
    }
}