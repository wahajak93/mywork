<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-B77742E2-->
<%@ Page language="C#" CodeFile="KMS_QuestionEditEvents.aspx.cs" Inherits="YouProQuiz_Beta.KMS_QuestionEdit_Page"  %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UCKMS_QuestionEdit" Src="UCKMS_QuestionEdit.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-3DC3FE42-->
<html>
<head>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>KMS_QuestionEdit</title>


<link rel="stylesheet" type="text/css" href="Styles/Basic/Style_doctype.css">
<link rel="stylesheet" type="text/css" href="Styles/Basic/jquery-ui.css">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">


<YouProQuiz_Beta:UCKMS_QuestionEdit ID="UCKMS_QuestionEdit" runat="server"/> 

</form>
<center><font face="Arial"><small>&#71;&#101;&#110;era&#116;&#101;&#100; <!-- CCS -->&#119;i&#116;h <!-- CCS -->C&#111;de&#67;h&#97;&#114;ge <!-- CCS -->&#83;&#116;&#117;&#100;i&#111;.</small></font></center></body>
</html>

<!--End ASPX page-->

