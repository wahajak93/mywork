<!--ASCX page header @1-C6C783E8-->
<%@ Control language="C#" CodeFile="UC_Quiz_IntroEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC_Quiz_Intro_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-81B98172-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<mt:Grid PageSizeLimit="100" RecordsPerPage="5" DataSourceID="ViewForQuizDetailsDataSource" ID="ViewForQuizDetails" runat="server">
<HeaderTemplate>
<h2>List of View For Quiz Details</h2>
<table>
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <th scope="row">Quiz Id</th>
 
    <td><mt:MTLabel Source="Quiz_id" DataType="Integer" ID="Quiz_id" runat="server"/>&nbsp;</td> 
  </tr>
 

  <tr>
    <th scope="row">Description</th>
 
    <td><mt:MTLabel Source="Quiz_Descp" DataType="Memo" ID="Quiz_Descp" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Title</th>
 
    <td><mt:MTLabel Source="Quiz_Title" ID="Quiz_Title" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Time Limit (Secs)</th>
 
    <td><mt:MTLabel Source="Time_limit_secs" DataType="Integer" ID="Time_limit_secs" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Quiz Mode</th>
 
    <td><mt:MTLabel Source="TBL_Quiz_Mode_Quiz_Mode" ID="Quiz_Mode" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Quiz Mode Description</th>
 
    <td><%= Variables["Quiz_Mode_Descp"]%>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Added On</th>
 
    <td><mt:MTLabel Source="Added_on" DataType="Date" ID="Added_on" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Modified On</th>
 
    <td><mt:MTLabel Source="Modified_on" DataType="Date" ID="Modified_on" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Total Points</th>
 
    <td><mt:MTLabel Source="Quiz_Total_Points" DataType="Integer" ID="Quiz_Total_Points" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Category Name</th>
 
    <td><mt:MTLabel Source="Category_Name" ID="Category_Name" runat="server"/>&nbsp;</td> 
  </tr>
 
  <tr>
    <th scope="row">Description</th>
 
    <td><%= Variables["Category_Descp"]%>&nbsp;</td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="2">No records</td> 
  </tr>
 </mt:MTPanel>
</table>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="ViewForQuizDetailsDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} TBL_Quiz.*, TBL_Quiz_Mode.Quiz_Mode AS TBL_Quiz_Mode_Quiz_Mode, user_name, Category_Name 
FROM ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM ((TBL_Quiz LEFT JOIN TBL_Quiz_Mode ON
TBL_Quiz.Quiz_Mode = TBL_Quiz_Mode.id) LEFT JOIN TBL_users ON
TBL_Quiz.Added_By = TBL_users.id) LEFT JOIN tblQuizCategory ON
TBL_Quiz.Quiz_Category = tblQuizCategory.id
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="TBL_Quiz.Quiz_id"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">








<!--End ASCX page-->

