﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;



    public partial class UC1_UserDashBoardAttemptDetails : System.Web.UI.UserControl
    {
    
               public string user_id = "2";
        public string quiz_id = "1";
        protected void Page_Init(Object sender, EventArgs e)
        {


        }
        protected void Page_Load(object sender, EventArgs e)
        {

            func1();


        }
        public void func1()
        {


            string[] a = new string[1];
            string[] attempt = new string[1];
            string[] questions = new string[1];
            string[] attempt_time = new string[1];
            string[] attempt_points = new string[1];

            StringBuilder sb = new StringBuilder();




            DataTable dtx = clsdb.readdata("select distinct(user_attempt_no) as attempt_no from TBL_User_Stats where quiz_id=" + quiz_id + " AND user_id=" + user_id + "");
            if (dtx.Rows.Count > 0)
            {
                a = new string[dtx.Rows.Count];
                attempt = new string[dtx.Rows.Count];
                attempt_time = new string[(dtx.Rows.Count)+1];
                attempt_points = new string[(dtx.Rows.Count) + 1];

                for (int x = 0; x < dtx.Rows.Count; x++)
                {

                    a[x] = dtx.Rows[x]["attempt_no"].ToString();

                    attempt[x] = "attempt no:" + a[x];
                }
            }

            for (int x = 0; x < dtx.Rows.Count; x++)
            {

                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb4 = new StringBuilder();

                DataTable dt1 = clsdb.readdata("select * from TBL_User_Stats where quiz_id=" + quiz_id + " AND user_id=" + user_id + " AND user_attempt_no=" + a[x] + "");
              


                if (dt1.Rows.Count > 0)
                {


                    questions = new string[dt1.Rows.Count];



                    for (int y = 0; y < dt1.Rows.Count; y++)
                    {
                        questions[y] = dt1.Rows[y]["quiz_quest_id"].ToString();

                        if (sb1.Length == 0)
                        {
                            sb1.Append("[");
                            sb1.Append(dt1.Rows[y]["quest_time_taken"]);
                            sb4.Append("[");
                            sb4.Append(dt1.Rows[y]["quiz_quest_points_achieved"]);
                        }
                        else
                        {
                            sb1.Append(",");
                            sb1.Append(dt1.Rows[y]["quest_time_taken"]);
                            sb4.Append(",");
                            sb4.Append(dt1.Rows[y]["quiz_quest_points_achieved"]);
                        }

                    }

                    //filling x-axis for questions

                    sb1.Append("]");
                    sb4.Append("]");

                }

                attempt_time[x] = sb1.ToString();
                attempt_points[x] = sb4.ToString();
            }

            
            
            
            
            //for filling average


           DataTable dt3 = clsdb.readdata("select quiz_id,quiz_quest_id,avg(quest_time_taken) as time_taken,avg(quest_time_taken) as time_taken,avg(quiz_quest_points_achieved) as avg_points from TBL_User_Stats GROUP BY quiz_id,quiz_quest_id");
           // DataTable dt4 = clsdb.readdata("select quiz_id,quiz_quest_id,avg(quest_time_taken) as time_taken,avg(quiz_quest_points_achieved) as avg_points from TBL_User_Stats GROUP BY quiz_id,quiz_quest_id");

            StringBuilder sb3 = new StringBuilder();
            StringBuilder sb5 = new StringBuilder();

            if(dt3.Rows.Count>0)
            {
                for(int w=0;w<dt3.Rows.Count;w++)
                {
                    if (sb3.Length == 0)
                        {
                        sb3.Append("[");

                        double z = double.Parse(dt3.Rows[w]["time_taken"].ToString());
                        z=Math.Round(z,2);
                            sb3.Append(z);



                            sb5.Append("[");

                            double y = double.Parse(dt3.Rows[w]["avg_points"].ToString());
                            y = Math.Round(z, 2);
                            sb5.Append(z);





                        }
                        else
                        {
                            sb3.Append(",");
                            double z = double.Parse(dt3.Rows[w]["time_taken"].ToString());
                            z = Math.Round(z, 2);
                            sb3.Append(z);

                            sb5.Append(",");
                            double y = double.Parse(dt3.Rows[w]["avg_points"].ToString());
                            y = Math.Round(z, 2);
                            sb5.Append(z);



                        }
                }
                   sb3.Append("]");
                   sb5.Append("]");    

                   attempt_time[(dtx.Rows.Count)] = sb3.ToString();
                   attempt_points[(dtx.Rows.Count)] = sb5.ToString();

            }
            
            
            


           





            StringBuilder sb2 = new StringBuilder();
            sb2.Append("<script>");
            sb2.Append("var questions = new Array;");

            foreach (string xa in questions)
            {

                sb2.Append("questions.push('" + xa + "');");
            }


            sb2.Append("series_maker=[");


            for (int b = 0; b < dtx.Rows.Count; b++)
            {
                if (b == 0)
                {
                    sb2.Append("{name:\" Average \",");
                    sb2.Append("data:" + attempt_time[dtx.Rows.Count] + "},");


                    sb2.Append("{name:\"" + attempt[b] + "\",");
                    sb2.Append("data:" + attempt_time[b] + "}");

                   
                  
                }
                else
                {
                    sb2.Append(",{name:\"" + attempt[b] + "\",");
                    sb2.Append("data:" + attempt_time[b] + "}");

                }

            }




            sb2.Append("];");




            //for points 


            sb2.Append("series_maker2=[");


            for (int b = 0; b < dtx.Rows.Count; b++)
            {
                if (b == 0)
                {
                    sb2.Append("{name:\" Average \",");
                    sb2.Append("data:" + attempt_points[dtx.Rows.Count] + "},");


                    sb2.Append("{name:\"" + attempt[b] + "\",");
                    sb2.Append("data:" + attempt_points[b] + "}");



                }
                else
                {
                    sb2.Append(",{name:\"" + attempt[b] + "\",");
                    sb2.Append("data:" + attempt_points[b] + "}");

                }

            }




            sb2.Append("];");













            sb2.Append("abc=series_maker;</script>");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "strBuilderJS", sb2.ToString(), false);


            //   name: 'Tokyo',
            // data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]

        
        
        
        }

            

        

              //  List<string> tempString = new List<string>();
                //tempString.Add("Hello");
                //tempString.Add("World");

                //StringBuilder sb = new StringBuilder();
                //sb.Append("<script>");
                //sb.Append("var questions = new Array;var points=new Array;points1=new Array;");
                //foreach (string str in questions)
                //{
                //    sb.Append("questions.push('" + str + "');");

                //}
                //foreach (int a in points)
                //{

                //    sb.Append("points.push(" + a + ");");
                //}
                //foreach (int a in points1)
                //{

                //    sb.Append("points1.push(" + a + ");");
                //}

                //sb.Append("</script>");

             //   ScriptManager.RegisterStartupScript(this, this.GetType(), "strBuilderJS", sb.ToString(), false);


            }

        


