﻿

<%@ Page language="C#" CodeFile="KMS_SectionQuestionsEvents.aspx.cs" Inherits="YouProQuiz_Beta.KMS_SectionQuestions_Page" MasterPageFile="~/Dashboard.master"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Dashboard</title>


<script language="JavaScript" type="text/javascript">
    //Begin CCS script
    //Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
    //End Include Common JSFunctions

    //Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
    //End Include User Scripts

    //End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


&nbsp;&nbsp;&nbsp;&nbsp; 
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" DataSourceID="uc_recSkilldepartmentsDataSource" AllowUpdate="False" EmptyRows="50" OnBeforeShow="uc_recSkilldepartments_BeforeShow" OnBeforeShowRow="uc_recSkilldepartments_BeforeShowRow" OnRowValidating="uc_recSkilldepartments_RowValidating" OnBeforeSubmit="uc_recSkilldepartments_BeforeSubmit" ID="uc_recSkilldepartments" runat="server" RowErrorControl="MTLabel1">
<HeaderTemplate><div data-emulate-form="" id="">


   
  <table>
    <tr>
      <td style="VERTICAL-ALIGN: top">
        <table class="Header">
          <tr>
            <td class="HeaderLeft"><img alt="" src="../../UC/Styles/EXPack/Images/Spacer.gif"/></td> 
            <td class="th">Add Question To the Section</td> 
            <td class="HeaderRight"><img alt="" src="../../UC/Styles/EXPack/Images/Spacer.gif"/></td>
          </tr>
        </table>
 
        <table class="Grid">
          <mt:MTPanel id="Error" visible="False" runat="server">
          <tr class="Error">
            <td colspan="5"><mt:MTLabel id="ErrorLabel" runat="server"/></td>
          </tr>
          </mt:MTPanel>
          <tr class="Caption" colspan="2">
            <th>Question</th>
 
            <th>Section </th>
 
            <th>Delete</th>
          </tr>
 
          
</HeaderTemplate>
<ItemTemplate>
          <mt:MTPanel id="RowError" visible="False" runat="server">
          <tr class="Error">
            <td colspan="5"><mt:MTLabel id="RowErrorLabel" runat="server"/></td>
          </tr>
          </mt:MTPanel>
          <!--  title="<mt:MTLabel ID="RowNameAttribute" runat="server"/>" <mt:MTLabel ContentType="Html" ID="RowStyleAttribute" runat="server"/>   -->
          <tr id="uc_recSkilldepartmentsrow" class="Row" runat="server">
         
            <td><label style="display: none;" for="<%# uc_recSkilldepartments.GetControl("manager_id").ClientID %>">ListBox1</label> 

                <mt:MTListBox Rows="1" Source="QuestionID" DataType="Integer" ErrorControl="errSkill" Caption="Skill" DataSourceID="manager_idDataSource" DataValueField="Quiz_Quest_ID" DataTextField="Question_Name" ID="manager_id" data-id="uc_recSkilldepartmentsmanager_id" runat="server">
                <asp:ListItem Value="" Selected="True" Text="Select Value"/>
              </mt:MTListBox>
              <mt:MTDataSource ID="manager_idDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
                 <SelectCommand>
SELECT tbl_Quiz_Question.*, Question_Name 
FROM tbl_Quiz_Question LEFT JOIN tblQuestion ON
tbl_Quiz_Question.Quiz_Quest_ID = tblQuestion.QuestionID {SQL_Where} {SQL_OrderBy}
                 </SelectCommand>
                 <SelectParameters>
                   <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="Quiz_ID"/>
                 </SelectParameters>
              </mt:MTDataSource>
 &nbsp;<mt:MTLabel SourceType="CodeExpression" ID="errSkill" runat="server"/> </td> 
            <td><label style="display: none;" for="<%# uc_recSkilldepartments.GetControl("ListBox1").ClientID %>">ListBox1</label> 
              <mt:MTListBox Rows="1" Source="SectionID" DataSourceID="ListBox1DataSource" DataValueField="SectionID" DataTextField="SectionTitle" ID="ListBox1" data-id="uc_recSkilldepartmentsListBox1" runat="server">
                <asp:ListItem Value="" Selected="True" Text="Select Value"/>
              </mt:MTListBox>
                <mt:MTLabel id="MTLabel1" runat="server"/>
              <mt:MTDataSource ID="ListBox1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
                 <SelectCommand>
SELECT * 
FROM tblSection {SQL_Where} {SQL_OrderBy}
                 </SelectCommand>
                 <SelectParameters>
                   <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="QuizID"/>
                 </SelectParameters>
              </mt:MTDataSource>
 &nbsp;</td> 
            <td colspan="3">
              <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label style="display: none;" for="<%# uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>">Delete</label> 
              <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="uc_recSkilldepartmentsCheckBox_Delete_Init" OnLoad="uc_recSkilldepartmentsCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="uc_recSkilluc_recSkilldepartmentsCheckBox_Delete_{uc_recSkilldepartments:rowNumber}" runat="server"/></mt:MTPanel></td>
          </tr>
 
</ItemTemplate>
<FooterTemplate>
          <mt:MTPanel id="NoRecords" visible="False" runat="server">
          <tr class="NoRecords">
            <td colspan="5">No records&nbsp;</td>
          </tr>
          </mt:MTPanel>
          <tr class="Footer">
            <td style="TEXT-ALIGN: right" colspan="5"><label style="display: none;" for="<%# uc_recSkilldepartments.GetControl("AddRowBtn").ClientID %>">Add Row</label> 
              <mt:MTButton EnableValidation="False" Text="Add Row" ID="AddRowBtn" data-id="uc_recSkilldepartmentsAddRowBtn" runat="server"/>
              <mt:MTButton CommandName="Submit" Text="Submit" ID="Button_Submit" runat="server"/></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="uc_recSkilldepartmentsDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" OnBeforeExecuteDelete="uc_recSkilldepartments_BeforeExecuteDelete" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tblSectionQuestions {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblSectionQuestions
   </CountCommand>
   <InsertCommand>
INSERT INTO tblSectionQuestions([QuestionID], [SectionID], [QuizID]) VALUES ({QuestionID}, {SectionID}, {QuizID})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblSectionQuestions SET [QuestionID]={QuestionID}, [SectionID]={SectionID}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tblSectionQuestions
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="QuestionID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="QuizID"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="Control" Source="manager_id" DataType="Integer"/>
     <mt:SqlParameter Name="SectionID" SourceType="Control" Source="ListBox1" DataType="Text"/>
     <mt:SqlParameter Name="QuizID" SourceType="URL" Source="Quiz_id" DataType="Float" IsOmitEmptyValue="True"/>
   </InsertParameters>
</mt:MTDataSource>

</asp:Content>


