﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UC_UserOverallQuizChart.ascx.cs" Inherits="UC_UserOverallQuizChart" %>



      <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>--%>

    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--%>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
      <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


    <script>

        var params = {};

        if (location.search) {
            var parts = location.search.substring(1).split('&');

            for (var i = 0; i < parts.length; i++) {
                var nv = parts[i].split('=');
                if (!nv[0]) continue;
                params[nv[0]] = nv[1] || true;
            }
        }


        var us_id = params.user_id;
        var quiz_id=params.quiz_id;


    
 
    

    </script>



    <script>
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Performance Overview'
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 150,
                    y: 100,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'white'
                },
                xAxis: {
                    

                    //categories: [
                    //    'Monday',
                    //    'Tuesday',
                    //    'Wednesday',
                    //    'Thursday',
                    //    'Friday',
                    //    'Saturday',
                    //    'Sunday'
                    //],
                    categories: questions,


                    plotBands: [{ // visualize the weekend
                        from: 4.5,
                        to: 6.5,
                        color: 'rgba(120, 170, 213, .6)'
                    }]
                },
                yAxis: {
                    title: {
                        text: 'Grade Points'
                    }
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' %'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    areaspline: {
                        fillOpacity: 0.5
                    }
                },
                series: [{
                    name: name1,
                    data: points
                }, {
                    name: 'Overall Average',
                    data: points1
                }]

            });
        });</script>


             <div class="container">
        <div class="col-lg-12">

<h3>User Overall Record</h3>

<div id="container" style="min-width: 200px; height: 300px; margin: 0 auto;"></div>
            </div>

        </div>
    

