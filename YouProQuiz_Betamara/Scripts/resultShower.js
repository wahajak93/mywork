﻿var xxx = new Object();

//points for current setup;



function GetResult() {
   // var xid = 23;
    $.ajax({


        type: 'POST',
        url: 'service_provider_page.aspx/Result_Maker',
        dataType: 'json',

        //filhal aesai 
        //will get the quiz_id from url after checking

        data: "{quiz_id:" + xid + ",attempt:" + attempt_no + ",ptmnd:'101110110111110110111010011111'}",
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            // dget(response.d);
            xxx = response.d;
            //xxx.bb = response.d.bb;

            res();

            USRpoints = xxx.bb;
            TOTquests = xxx.aa;

            avg_points_for_curr_usr_arr = USRpoints;
            avg_points_overall_arr = USRpoints;
            arr1 = TOTquests;

        },

        error: function (error) {
            //  alert("asdqwerqwdsadf3qwqwdsad");

            console.log(error);
        }

    });

}

function res() {

   // alert("this is res()");

    ////arr1 is the question number of the current quiz attempted by the user
    // avg_points_overall_arr = new Array();
    Users_Avg_Score = new Array();
    arr1 = new Array();
    Overall_Avg_Score = new Array();
    Best_Avg_Score = new Array();
    Curr_Attempt_Score = new Array();
    Curr_Attempt_Time = new Array();
    average_time_taken_curr_usr = new Array();
    average_time_taken_all_usr = new Array();

    var best_score = xxx.Best_Overall_Percentage_of_Current_Quiz;
    var avg_score = xxx.Avg_Overall_Percentage_of_Current_Quiz;
    var curr_user_score = xxx.User_Overall_Percentage_of_Current_Quiz;
    var zz = xxx.User_Overall_Percentage_of_Current_Attempt;



    $('#User_Overall_Percentage_of_Attempt').html(zz + '%');
    $('#User_Overall_Percentage_of_quiz').html(curr_user_score + '%');

    $('#Best_Score').html(best_score + '% <br> by' + xxx.Best_scorer_Name);
    $('#Avg_Score').html(avg_score + '%');

    //  arr1 = xxx.aa;
    for (var x = 0; x < xxx.Questions.length; x++) {
        arr1.push(xxx.Questions[x]);
        Users_Avg_Score.push(xxx.User_Scores[x]);
        Overall_Avg_Score.push(xxx.Avg_Scores[x]);
        Best_Avg_Score.push(xxx.Best_Scores[x]);
        Curr_Attempt_Score.push(xxx.User_Scores_For_Current_Attempt[x]);
        Curr_Attempt_Time.push(xxx.time_taken_for_last_attempt_curr_user[x]);
        average_time_taken_curr_usr.push(xxx.average_time_taken_curr_usr[x]);
        average_time_taken_all_usr.push(xxx.average_time_taken_all_usr[x]);
    }


    if (xxx.curr_quiz_section.length > 0) {
        curr_quiz_section = new Array();
        curr_quiz_section_curr_user_avg = new Array();
        for (var y = 0; y < xxx.curr_quiz_section.length; y++) {
            curr_quiz_section.push(xxx.curr_quiz_section[y]);
            curr_quiz_section_curr_user_avg.push(xxx.curr_quiz_section_curr_user_avg[y]);

        }



    }
    else {

        $("#container3").hide();
    }

    //alert(arr1);
    // alert(avg_points_overall_arr);

    //arr1.push('a');
    //arr1.push('b');

    //arr1.push('a');
    //arr1.push('b');
    //arr1.push('a');

    //avg_points_overall_arr is the avg of all the users attemted this question;
    //avg_points_overall_arr = new Array();

    //for (var x = 0; x < xxx.bb.length; x++) {
    //    avg_points_overall_arr.push(xxx.bb[x]);
    //}
    //alert(avg_points_overall_arr);

    ////  avg_points_overall_arr = xxx.bb;
    //avg_points_overall_arr.push(10);
    //avg_points_overall_arr.push(12);
    //avg_points_overall_arr.push(15);
    //avg_points_overall_arr.push(12);
    //avg_points_overall_arr.push(11);


    //avg_points_for_curr_usr_arr is the avg of the curr users for this question
    //  avg_points_for_curr_usr_arr = new Array();
    //avg_points_for_curr_usr_arr = xxx.aa;
    //avg_points_for_curr_usr_arr.push(13);
    //avg_points_for_curr_usr_arr.push(12);
    //avg_points_for_curr_usr_arr.push(16);
    //avg_points_for_curr_usr_arr.push(18);
    //avg_points_for_curr_usr_arr.push(9);



    //  var arrString = xxx.aa.join("");
    //  $(function () {

    $('#container1').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Performance Details'
        },
        subtitle: {
            text: 'Quiz.Learners.Club'
        },
        xAxis: {
            categories: arr1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Points (pts)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Best Scores',
            //data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
            data: Best_Avg_Score
        }, {
            name: 'Average Scores',
            data: Overall_Avg_Score
        },



        {
            name: 'Current User Average Scores',
            // data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
            data: Users_Avg_Score
        },
           {
               name: 'Current User\'s Last Attempt Scores',
               // data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
               data: Curr_Attempt_Score
           }
        ]

    });
    //});










    $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Time taken to attempt'
        },
        subtitle: {
            text: 'Quiz.Learners.Club'
        },
        xAxis: {
            categories: arr1,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Time (seconds)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} seconds</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Current Attempt Time of Current User',
            //data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
            data: Curr_Attempt_Time
        }, 
        
           {
               name: 'Average Attempt Time of Current User',
               // data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
               data: average_time_taken_curr_usr
           },
           {
               name: 'Overall Average Attempt Time',
               data: average_time_taken_all_usr

           }
        ]

    });









    $('#container3').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Section-wise Performance'
        },
        subtitle: {
            text: 'Quiz.Learners.Club'
        },
        xAxis: {
            categories: curr_quiz_section,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Points (pts)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{black};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Current User\'s Section Performance',
            //data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
            data: curr_quiz_section_curr_user_avg
        }
        ]

    });







}



