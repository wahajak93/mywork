<!--ASCX page header @1-F1991186-->
<%@ Control language="C#" CodeFile="UC2_Quiz_QueriesEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC2_Quiz_Queries_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-C904CFD6-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>' src="ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<mt:Record ID="View_For_QueriesSearch" runat="server"><ItemTemplate><div data-emulate-form="UC2_AdminDashboardView_For_QueriesSearch" id="UC2_AdminDashboardView_For_QueriesSearch">



    <h1>Queries</h1>


       <div class="container">    
     <div class="col-lg-12">
 
 

    <mt:MTPanel id="Error" visible="False" runat="server">
    <span id="<%= Variables["@error-block"]%>">
     <mt:MTLabel id="ErrorLabel" runat="server"/> 
    </span>
 </mt:MTPanel>
    <div class="col-md-3 container">
    <label for="<%# View_For_QueriesSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Query_ID").ClientID %>">Query ID</label>
      <mt:MTTextBox Source="Query_ID" DataType="Integer" Caption="Query ID" maxlength="10" CssClass="form-control" ID="s_Query_ID" data-id="UC2_AdminDashboardView_For_QueriesSearchs_Query_ID" runat="server"/>
    </div>

    <div class="col-md-3 container">
 <br/>
           <mt:MTButton CommandName="Search" Text="Search" CssClass="btn btn-primary" ID="Button_DoSearch" data-id="UC2_AdminDashboardView_For_QueriesSearchButton_DoSearch" runat="server"/></td> 
    </div>

         </div>
           </div>
</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="View_For_QueriesDataSource" ID="View_For_Queries" runat="server">
<HeaderTemplate>
  <div class="container">    
     <div class="col-lg-12">

        <div class="table-responsive">
<table class="table">
  <tr>
    <th >Query ID</th>
 
    <th >Query Descp</th>
 
    <th >From User</th>
 
    <th >To User</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td ><mt:MTLabel Source="Query_ID" DataType="Integer" ID="Query_ID" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Query_Descp" ID="Query_Descp" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="From_User" ID="From_User" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="To_User" ID="To_User" runat="server"/>&nbsp;</td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td >No records</td> 
  </tr>
 </mt:MTPanel>
  <tr>
    <td >
      <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/en/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" ID="Navigator" runat="server"/>&nbsp;</td> 
  </tr>
</table>
    </div>
    </div>
    </div>
</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="View_For_QueriesDataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM View_For_Queries {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM View_For_Queries
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_Query_ID" SourceType="URL" Source="s_Query_ID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Query_ID"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>
<br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">



<!--End ASCX page-->

