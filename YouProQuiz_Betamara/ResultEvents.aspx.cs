//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-025EE8F4
public partial class Result_Page : MTPage
{
//End Page class
int user_id;
//Page Dashboard Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page Dashboard Event Init
//Access Denied Page Url @1-2E5EF9F8
        this.AccessDeniedPage = "Dashboard.aspx";
//End Access Denied Page Url

//Set Page Dashboard access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", true);
//End Set Page Dashboard access rights.
       

//Page Dashboard Init event tail @1-FCB6E20C
    }
//End Page Dashboard Init event tail

//Page Dashboard Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page Dashboard Event Load
      

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page Dashboard Load event tail @1-FCB6E20C
    }
//End Page Dashboard Load event tail

//Page Dashboard Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page Dashboard Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page Dashboard On PreInit event tail @1-FCB6E20C
    }
//End Page Dashboard On PreInit event tail
//End Label Label2 Before Show event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

