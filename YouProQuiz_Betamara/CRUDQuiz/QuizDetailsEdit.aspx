﻿<%--<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-394A929D-->--%>
<%@ Page language="C#" CodeFile="QuizDetailsEditEvents.aspx.cs" Inherits="YouProQuiz_Beta.QuizDetailsEdit_Page"  MasterPageFile="~/Dashboard.master" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/CRUDQuiz/UC_Quiz_edit.ascx" TagPrefix="uc1" TagName="UC_Quiz_edit" %>
<%@ Register Src="~/CRUDQuiz/UC_Questions.ascx" TagPrefix="uc1" TagName="UC_Questions" %>

<%--
<!--End ASPX page header-->

<!--ASPX page @1-B875A1B1-->
<html>
<head>--%>
<asp:Content ContentPlaceHolderID="head" runat="server">
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>NewPage2</title>


<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script src="ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>
<%--</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">--%>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UC_Quiz_edit runat="server" ID="UC_Quiz_edit" />

    <h1>Material</h1>
    <uc1:UC_Questions runat="server" ID="UC_Questions" />


<p>&nbsp;</p>
<p>&nbsp;</p>
    </asp:Content>
<%--</form>
</body>
</html>

<!--End ASPX page-->--%>

