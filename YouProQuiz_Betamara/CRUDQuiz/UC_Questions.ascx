<!--ASCX page header @1-602E1A7A-->
<%@ Control language="C#" CodeFile="UC_QuestionsEvents.ascx.cs" Inherits="LernersQuiz.UC_Questions_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-C7D07CD9-->




<mt:MTPanel ID="___link_panel_029267214506503303" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/Basic/Style_doctype.css">
</mt:MTPanel>

<mt:MTPanel ID="___link_panel_03919516361832163" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/Basic/jquery-ui.css">
</mt:MTPanel>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="NewEditableGrid1DataSource" AllowInsert="False" AllowUpdate="False" EmptyRows="0" ID="NewEditableGrid1" runat="server">
<HeaderTemplate><div data-emulate-form="UC_QuestionsNewEditableGrid1" id="UC_QuestionsNewEditableGrid1">





 <div class="container">
  <div class="col-lg-12">   
 <div class="table-responsive">
        <table class="table" >
            <td><strong>Question List</strong></td> 

          <mt:MTPanel id="Error" visible="False" runat="server">
          <tr class="Error">
            <td><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
          </tr>
 </mt:MTPanel>
          <tr >
            <th>Quiz Quest Name</th>
 
            <th>Quiz Quest Status</th>
 
            <th>Quiz Quest Point</th>
 
            <th>Quiz Quest Time Limit</th>
 
            <th>&nbsp;</th>
 
            <th>Delete</th>
 
          </tr>
 
          
</HeaderTemplate>
<ItemTemplate>
          <mt:MTPanel id="RowError" visible="False" runat="server">
          <tr class="Error">
            <td ><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
          </tr>
 </mt:MTPanel>
          <tr >
            <td><mt:MTLabel Source="Quiz_Quest_Name" ID="Quiz_Quest_Name" runat="server"/></td> 
            <td><mt:MTLabel Source="Quiz_Quest_status" ID="Quiz_Quest_status" runat="server"/></td> 
            <td><mt:MTLabel Source="Quiz_Quest_point" DataType="Integer" ID="Quiz_Quest_point" runat="server"/></td> 
            <td><mt:MTLabel Source="Quiz_Quest_Time_Limit" DataType="Integer" ID="Quiz_Quest_Time_Limit" runat="server"/></td> 
            <td>&nbsp;<mt:MTLink Text="Edit" ID="Link1" data-id="UC_QuestionsNewEditableGrid1Link1_{NewEditableGrid1:rowNumber}" runat="server" HrefSource="~/Edit_question.aspx" PreserveParameters="Get"><Parameters>
              <mt:UrlParameter Name="Quiz_Quest_id" SourceType="DataSourceColumn" Source="Quiz_Quest_id" Format="yyyy-MM-dd"/>
            </Parameters></mt:MTLink></td> 
            <td>
              <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label style="display: none;" for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>">Delete</label> 
              <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="NewEditableGrid1CheckBox_Delete_Init" OnLoad="NewEditableGrid1CheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UC_QuestionsNewEditableGrid1CheckBox_Delete_PanelCheckBox_Delete_{NewEditableGrid1:rowNumber}" runat="server"/></mt:MTPanel>&nbsp;</td> 
          </tr>
 
</ItemTemplate>
<FooterTemplate>
          <mt:MTPanel id="NoRecords" visible="False" runat="server">
          <tr class="NoRecords">
            <td ">No records</td> 
          </tr>
 </mt:MTPanel>
          <tr >
            <td>
              <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="&amp;nbsp;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/>
              <mt:MTButton CommandName="Submit" Text="Submit" CssClass="btn btn-primary" ID="Button_Submit" data-id="UC_QuestionsNewEditableGrid1Button_Submit" runat="server"/></td> 
          </tr>
 
        </table>
 </td> 
    </tr>
 
  </table>
    
</div>
    </div>
    </div>
    </div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="NewEditableGrid1DataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" CountCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM TBL_Quiz_Questions {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM TBL_Quiz_Questions
   </CountCommand>
   <DeleteCommand>
DELETE FROM TBL_Quiz_Questions
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="Quiz_Quest_id" Type="Integer" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Integer" Format="#,##0;;;;; ;" Operation="And" Condition="Equal" SourceColumn="Quiz_id"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>
<hr/>
<div class="container" align="center">
  <div class="col-lg-12">  
     <h3> Add More Questions 
<mt:MTLink Text="Add Questions" ID="Link1" data-id="UC_QuestionsLink1" runat="server" CssClass="btn btn-primary btn-lg" HrefSource="~/AddNewQuiz_Question.aspx" PreserveParameters="Get"><Parameters>
  <mt:UrlParameter Name="Quiz_id" SourceType="Url" Source="Quiz_id" Format="yyyy-MM-dd"/>
</Parameters></mt:MTLink></h3><br>
      </div>
    </div>
<br>
<br>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">



<!--End ASCX page-->

