//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-04D4123F
namespace LernersQuiz
{
//End Namespace

//Page class @1-56098367
public partial class UC_Questions_Page : MTUserControl
{
//End Page class

//Attributes constants @1-52B30067
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UC_Questions Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_Questions Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UC_Questions Init event tail @1-FCB6E20C
    }
//End Page UC_Questions Init event tail

//CheckBox CheckBox_Delete Event Init @46-D7F4F35C
    protected void NewEditableGrid1CheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @46-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @46-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @46-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @46-2ACB06ED
    protected void NewEditableGrid1CheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @46-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @46-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

