//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-7DC13B78
    public partial class QuizDetailsEdit_Page : MTPage
{
//End Page class

//Page NewPage2 Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page NewPage2 Event Init

//Access Denied Page Url @1-92F104EC
        this.AccessDeniedPage = "~/Home.aspx";
//End Access Denied Page Url

//Set Page NewPage2 access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Premium", true);
//End Set Page NewPage2 access rights.

//Page NewPage2 Init event tail @1-FCB6E20C
    }
//End Page NewPage2 Init event tail

//Page NewPage2 Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page NewPage2 Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page NewPage2 Load event tail @1-FCB6E20C
    }
//End Page NewPage2 Load event tail

//Page NewPage2 Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page NewPage2 Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "../Dashboard.master";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page NewPage2 On PreInit event tail @1-FCB6E20C
    }
//End Page NewPage2 On PreInit event tail

//Label Label1 Event Before Show @3-B681DCAB

//End Label Label1 Before Show event tail

//Label Label2 Event Before Show @5-FB69DCCC
   
//End Label Label2 Before Show event tail

//DEL      Response.Write(Session["User ID"]);

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

