<!--ASCX page header @1-EA646F04-->
<%@ Control language="C#" CodeFile="UC_Quiz_editEvents.ascx.cs" Inherits="LernersQuiz.UC_Quiz_edit_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-75CC287E-->




<mt:MTPanel ID="___link_panel_09720457442608479" runat="server"></mt:MTPanel>

<mt:MTPanel ID="___link_panel_011089633358068523" runat="server"></mt:MTPanel>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-C2519ADF
</script>
<link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/Basic/ccs-jquery-ui-calendar.css">
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-570576C7
    $('body').ccsBind(function() {
        features["UC_Quiz_editNewEditableGrid1Time_limit_secsNumericUpDown2"] = $('*:ccsControl(UC_Quiz_edit, NewEditableGrid1, Time_limit_secs)');
        features["UC_Quiz_editNewEditableGrid1Quiz_Total_PointsNumericUpDown1"] = $('*:ccsControl(UC_Quiz_edit, NewEditableGrid1, Quiz_Total_Points)');
    });
//End Controls Selecting

//Feature Parameters @1-1BE5CF12
    params["UC_Quiz_editNewEditableGrid1Time_limit_secsNumericUpDown2"] = { 
        sourceType: "Numeric",
        min: 0,
        max: 500,
        step: 1, 
        altStep: 1
    };
    params["UC_Quiz_editNewEditableGrid1Quiz_Total_PointsNumericUpDown1"] = { 
        sourceType: "Numeric",
        min: 0,
        max: 500,
        step: 1, 
        altStep: 1
    };
//End Feature Parameters

//Feature Init @1-A60767D9
    features["UC_Quiz_editNewEditableGrid1Time_limit_secsNumericUpDown2"].ccsBind(function() {
        this.ccsNumericUpDown(params["UC_Quiz_editNewEditableGrid1Time_limit_secsNumericUpDown2"]);
    });
    features["UC_Quiz_editNewEditableGrid1Quiz_Total_PointsNumericUpDown1"].ccsBind(function() {
        this.ccsNumericUpDown(params["UC_Quiz_editNewEditableGrid1Quiz_Total_PointsNumericUpDown1"]);
    });
//End Feature Init

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<mt:EditableGrid PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="NewEditableGrid1DataSource" AllowInsert="False" AllowDelete="False" EmptyRows="0" ID="NewEditableGrid1" runat="server">
<HeaderTemplate><div data-emulate-form="UC_Quiz_editNewEditableGrid1" id="UC_Quiz_editNewEditableGrid1">





   
    <div class="container form-horizontal">
  <div class="col-lg-12">
            <h3>Edit Quiz</h3> 
  
            <mt:MTPanel id="Error" visible="False" runat="server">
          <span class="Error">
            <mt:MTLabel id="ErrorLabel" runat="server"/>
          </span>
 </mt:MTPanel>
          
</HeaderTemplate>
<ItemTemplate>
          <mt:MTPanel id="RowError" visible="False" runat="server">
          <span class="Error">
          <mt:MTLabel id="RowErrorLabel" runat="server"/>
          </span>
 </mt:MTPanel>

     <div class="form-group">
        <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").ClientID %>" class="control-label col-sm-2">Quiz Title</label>
  <div class="col-sm-10">
            <mt:MTTextBox Source="Quiz_Title" Required="True" Caption="Quiz Title" maxlength="100" class="form-control" ID="Quiz_Title" data-id="UC_Quiz_editNewEditableGrid1Quiz_Title_{NewEditableGrid1:rowNumber}" runat="server"/> 
         </div>
    </div>
             
              <div class="form-group">
    <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Name").ClientID %>" class="control-label col-sm-2">Quiz Name</label>
 <div class="col-sm-10">
            <mt:MTTextBox Source="Quiz_Name" Required="True" Caption="Quiz Name" maxlength="50" class="form-control" ID="Quiz_Name" data-id="UC_Quiz_editNewEditableGrid1Quiz_Name_{NewEditableGrid1:rowNumber}" runat="server"/>
     </div>
         </div>
     <div class="form-group">
                <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").ClientID %>" class="control-label col-sm-2">Quiz Descp</label>
 <div class="col-sm-10">
           
<mt:MTTextBox TextMode="Multiline" Source="Quiz_Descp" Required="True" Caption="Quiz Descp" Rows="3" class="form-control" ID="Quiz_Descp" data-id="UC_Quiz_editNewEditableGrid1Quiz_Descp_{NewEditableGrid1:rowNumber}" runat="server"/>
       
     </div>  </div>
    
      <div class="form-group">
                <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").ClientID %>" class="control-label col-sm-2">Time Limit Secs</label>
 <div class="col-sm-10">
            <mt:MTTextBox Source="Time_limit_secs" DataType="Integer" Caption="Time Limit Secs" maxlength="10" class="form-control" ID="Time_limit_secs" data-id="UC_Quiz_editNewEditableGrid1Time_limit_secs_{NewEditableGrid1:rowNumber}" runat="server"/> 
          </div>
          </div>
               <div class="form-group">
    <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").ClientID %>" class="control-label col-sm-2">Quiz Mode</label></th>
 
           <div class="col-sm-10">
       <mt:MTListBox Rows="1" Source="Quiz_Mode" DataType="Integer" Required="True" Caption="Quiz Mode" DataSourceID="Quiz_ModeDataSource" DataValueField="id" DataTextField="Quiz_Mode" ID="Quiz_Mode" data-id="UC_Quiz_editNewEditableGrid1Quiz_Mode_{NewEditableGrid1:rowNumber}" runat="server" class="form-control">
                <asp:ListItem Value="" Selected="True" Text="Select Value"/>
              </mt:MTListBox>
              <mt:MTDataSource ID="Quiz_ModeDataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" runat="server">
                 <SelectCommand>
SELECT * 
FROM TBL_Quiz_Mode {SQL_Where} {SQL_OrderBy}
                 </SelectCommand>
              </mt:MTDataSource>
               </div>
                   </div>
    
      <div class="form-group">
    <label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").ClientID %>" class="control-label col-sm-2">Quiz Total Points</label></th>
  <div class="col-sm-10">
    <mt:MTTextBox Source="Quiz_Total_Points" DataType="Integer" Caption="Quiz Total Points" maxlength="10" class="form-control" ID="Quiz_Total_Points" data-id="UC_Quiz_editNewEditableGrid1Quiz_Total_Points_{NewEditableGrid1:rowNumber}" runat="server"/></td> 
    
      </div>
          </div>
</ItemTemplate>
<FooterTemplate>
          <mt:MTPanel id="NoRecords" visible="False" runat="server">
          <span class="NoRecords">
            No records 
          </span>
 </mt:MTPanel>
     <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
              <mt:MTButton CommandName="Submit" Text="Submit" CssClass="btn btn-primary" ID="Button_Submit" data-id="UC_Quiz_editNewEditableGrid1Button_Submit" runat="server"/></td> 
         </div>
         </div>
    </div>
    </div>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="NewEditableGrid1DataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" CountCommandType="Table" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM TBL_Quiz
   </CountCommand>
   <UpdateCommand>
UPDATE TBL_Quiz SET [Quiz_Title]={Quiz_Title}, [Quiz_Name]={Quiz_Name}, [Quiz_Descp]={Quiz_Descp}, [Time_limit_secs]={Time_limit_secs}, [Quiz_Mode]={Quiz_Mode}, [Quiz_Total_Points]={Quiz_Total_Points}
  </UpdateCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="Quiz_id" Type="Integer" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_id"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>
<br>
<br>




<!--End ASCX page-->

