//Using statements @1-AFBC0BCB
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-04D4123F
namespace LernersQuiz
{
//End Namespace

//Page class @1-2F5FBE59
public partial class UC_Quiz_edit_Page : MTUserControl
{
//End Page class

//Attributes constants @1-52B30067
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UC_Quiz_edit Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_Quiz_edit Event Init

//ScriptIncludesInit @1-8CDDD28D
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/external/jquery.mousewheel.js|js/jquery/ui/jquery.ui.core.js|js/jquery/ui/jquery.ui.widget.js|js/jquery/ui/jquery.ui.button.js|js/jquery/ui/jquery.ui.spinner.js|js/jquery/spinner/ccs-numeric-up-down.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page UC_Quiz_edit Init event tail @1-FCB6E20C
    }
//End Page UC_Quiz_edit Init event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

