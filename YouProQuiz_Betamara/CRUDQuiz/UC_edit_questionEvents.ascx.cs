//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-04D4123F
namespace LernersQuiz
{
//End Namespace

//Page class @1-0E05830A
public partial class UC_edit_question_Page : MTUserControl
{
//End Page class

//Attributes constants @1-93D58DF7
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page UC_edit_question Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_edit_question Event Init

//ScriptIncludesInit @1-194CC51C
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|ckeditor/ckeditor.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page UC_edit_question Init event tail @1-FCB6E20C
    }
//End Page UC_edit_question Init event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

