<!--ASCX page header @1-15E7D6CC-->
<%@ Control language="C#" CodeFile="UC_edit_questionEvents.ascx.cs" Inherits="LernersQuiz.UC_edit_question_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-8BE6CC17-->




<mt:MTPanel ID="___link_panel_05534116352707026" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/Style_doctype.css">
</mt:MTPanel>

<mt:MTPanel ID="___link_panel_04537098695692105" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/jquery-ui.css">
</mt:MTPanel>
<script src="ckeditor/ckeditor.js"></script>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<mt:Record PreserveParameters="Get" DataSourceID="NewRecord1DataSource" AllowInsert="False" AllowDelete="False" ID="NewRecord1" runat="server"><ItemTemplate><div data-emulate-form="UC_edit_questionNewRecord1" id="UC_edit_questionNewRecord1">


    
     <div class="container form-horizontal">
  <div class="col-lg-12">


            <h2>Quiz Questions </h2> 
 
             <mt:MTPanel id="Error" visible="False" runat="server">
          <span id="ErrorBlock" class="Error">
            <mt:MTLabel id="ErrorLabel" runat="server"/> 
          </span>
 </mt:MTPanel>
       <div class="form-group">
    <label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_id").ClientID %>" class="control-label col-sm-2">Quiz Id</label>
             <div class="col-sm-10">
              <mt:MTListBox Rows="1" Source="Quiz_id" DataType="Integer" Required="True" Caption="Quiz Id" DataSourceID="Quiz_idDataSource" DataValueField="Quiz_id" DataTextField="Quiz_Name" ID="Quiz_id" data-id="UC_edit_questionNewRecord1Quiz_id" runat="server" class="form-control" >
                <asp:ListItem Value="" Selected="True" Text="Select Value"/>
              </mt:MTListBox>
              <mt:MTDataSource ID="Quiz_idDataSource" ConnectionString="InMotion:LearnersClub" SelectCommandType="Table" runat="server">
                 <SelectCommand>
SELECT * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
                 </SelectCommand>
              </mt:MTDataSource>
 
    </div>
      </div>


          <div class="form-group">
        <label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Quest_Name").ClientID %>" class="control-label col-sm-2">Quest Name</label>
        <div class="col-sm-10">
                <mt:MTTextBox Source="Quiz_Quest_Name" Required="True" ErrorControl="Name" Caption="Quiz Quest Name" maxlength="250" class="form-control" ID="Quiz_Quest_Name" data-id="UC_edit_questionNewRecord1Quiz_Quest_Name" runat="server"/><mt:MTLabel ID="Name" runat="server"/>
        </div>
      </div>

           <div class="form-group">
    <label for="UC_edit_questionNewRecord1Quiz_Quest_Text" class="control-label col-sm-2" >Quiz Quest Text</label>
  <div class="col-sm-10">
<mt:MTTextBox TextMode="Multiline" Source="Quiz_Quest_Text" Required="True" ErrorControl="Textarea" Rows="3" class="form-control" ID="TextArea1" data-id="UC_edit_questionNewRecord1TextArea1" runat="server"/>
<mt:MTLabel ID="Textarea" runat="server"/>
    </div>
      </div>
    

            <div class="form-group">
    <label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Quest_status").ClientID %>" class="control-label col-sm-2">Quiz Quest Status</label>
   <div class="col-sm-10">
                 <mt:MTTextBox Source="Quiz_Quest_status" Caption="Quiz Quest Status" maxlength="50" class="form-control" ID="Quiz_Quest_status" data-id="UC_edit_questionNewRecord1Quiz_Quest_status" runat="server"/>
        </div>
         </div>
 
          <div class="form-group">
            <label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Quest_point").ClientID %>" class="control-label col-sm-2" >Quiz Quest Point</label>
          <div class="col-sm-10">
                <mt:MTTextBox Source="Quiz_Quest_point" DataType="Integer" Caption="Quiz Quest Point" maxlength="10"  class="form-control" ID="Quiz_Quest_point" data-id="UC_edit_questionNewRecord1Quiz_Quest_point" runat="server"/> 
        </div>
         </div>
          
 
         <div class="form-group">
            <label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Ques_answer_type").ClientID %>" class="control-label col-sm-2">Quiz Ques Answer Type</label>
              <div class="col-sm-10">
      <mt:MTListBox Rows="1" Source="Quiz_Ques_answer_type" DataType="Integer" Required="True" ErrorControl="Answer_type_error" class="form-control" Caption="Quiz Ques Answer Type" DataSourceID="Quiz_Ques_answer_typeDataSource" DataValueField="Answer_type_ID" DataTextField="Answer_type" ID="Quiz_Ques_answer_type" data-id="UC_edit_questionNewRecord1Quiz_Ques_answer_type" runat="server">
                <asp:ListItem Value="" Selected="True" Text="Select Value"/>
              </mt:MTListBox>
              <mt:MTDataSource ID="Quiz_Ques_answer_typeDataSource" ConnectionString="InMotion:LearnersClub" SelectCommandType="Table" runat="server">
                 <SelectCommand>
SELECT * 
FROM TBL_Answer_Types {SQL_Where} {SQL_OrderBy}
                 </SelectCommand>
              </mt:MTDataSource>
 <mt:MTLabel ID="Answer_type_error" runat="server"/> 
                   </div>
         </div>

        <div class="form-group">
<label for="<%# NewRecord1.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Quest_Time_Limit").ClientID %>" class="control-label col-sm-2">Quiz Quest Time Limit</label>
            <div class="col-sm-10">
               <mt:MTTextBox Source="Quiz_Quest_Time_Limit" DataType="Integer" Caption="Quiz Quest Time Limit" maxlength="10" class="form-control" ID="Quiz_Quest_Time_Limit" data-id="UC_edit_questionNewRecord1Quiz_Quest_Time_Limit" runat="server"/>
          </div>
              </div>
                  <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="btn btn-primary" ID="Button_Update" data-id="UC_edit_questionNewRecord1Button_Update" runat="server"/> 
    </div>
                      </div>
      </div>
         </div>


</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="NewRecord1DataSource" ConnectionString="InMotion:LearnersClub" SelectCommandType="Table" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_Quiz_Questions {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <UpdateCommand>
UPDATE TBL_Quiz_Questions SET [Quiz_id]={Quiz_id}, [Quiz_Quest_Name]={Quiz_Quest_Name}, [Quiz_Quest_status]={Quiz_Quest_status}, [Quiz_Quest_point]={Quiz_Quest_point}, [Quiz_Ques_answer_type]={Quiz_Ques_answer_type}, [Quiz_Quest_Time_Limit]={Quiz_Quest_Time_Limit}, [Quiz_Quest_Text]={Quiz_Quest_Text}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_Quest_id" SourceType="URL" Source="Quiz_Quest_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_Quest_id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//UC_edit_questionNewRecord1TextArea1OnLoad Event Start @-C56C4EBD
    actions["UC_edit_questionNewRecord1TextArea1OnLoad"] = function (eventType, parameters) {
        var result = true;
//End UC_edit_questionNewRecord1TextArea1OnLoad Event Start

//Attach CKeditor @17-0F9F6667
        try {
            var UC_edit_questionNewRecord1TextArea1_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "400",
                width: "100%",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UC_edit_questionNewRecord1TextArea1OnLoad Event End @-A5B9ECB8
        return result;
    };
//End UC_edit_questionNewRecord1TextArea1OnLoad Event End

//Event Binding @1-50EBABE9
    $('*:ccsControl(UC_edit_question, NewRecord1, TextArea1)').ccsBind(function() {
        this.each(function(){ actions["UC_edit_questionNewRecord1TextArea1OnLoad"].call(this); });
    });
//End Event Binding

//Common Script End @1-562554DE
});
//End Common Script End


//End CCS script


</script>



<!--End ASCX page-->

