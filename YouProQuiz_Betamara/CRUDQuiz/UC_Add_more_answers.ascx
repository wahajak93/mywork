<!--ASCX page header @1-8F57DE8B-->
<%@ Control language="C#" CodeFile="UC_Add_more_answersEvents.ascx.cs" Inherits="LernersQuiz.UC_Add_more_answers_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-55346E43-->

<mt:Record PreserveParameters="Get" DataSourceID="TBL_Quiz_AnswersDataSource" AllowDelete="False" ID="TBL_Quiz_Answers" runat="server"><ItemTemplate><div data-emulate-form="UC_Add_more_answersTBL_Quiz_Answers" id="UC_Add_more_answersTBL_Quiz_Answers">







  <h2>Quiz Answers </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="UC_Add_more_answersTBL_Quiz_AnswersQuiz_id"></label></td> 
      <td><label for="UC_Add_more_answersTBL_Quiz_AnswersQuiz_id" style="display: none;">Quiz Id</label>
<mt:MTLabel Source="Quiz_id" DataType="Integer" ID="Quiz_id" runat="server"/>
</td> 
    </tr>
 
    <tr>
      <td><label for="UC_Add_more_answersTBL_Quiz_AnswersQuiz_Quest_id"></label></td> 
      <td><label for="UC_Add_more_answersTBL_Quiz_AnswersQuiz_Quest_id" style="display: none;">Quiz Quest Id</label>
<mt:MTLabel Source="Quiz_Quest_id" DataType="Integer" ID="Quiz_Quest_id" runat="server"/>
</td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz_Answers.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Answer_Status").ClientID %>">Quiz Answer Status</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Answer_Status" DataType="Integer" Caption="Quiz Answer Status" DataSourceID="Quiz_Answer_StatusDataSource" DataValueField="id" DataTextField="Answer_Status_type" ID="Quiz_Answer_Status" data-id="UC_Add_more_answersTBL_Quiz_AnswersQuiz_Answer_Status" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_Answer_StatusDataSource" ConnectionString="InMotion:LearnersClub" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM TBL_Answer_Status {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td><label for="<%# TBL_Quiz_Answers.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Answer_Point").ClientID %>">Quiz Answer Point</label></td> 
      <td><mt:MTTextBox Source="Quiz_Answer_Point" DataType="Integer" Caption="Quiz Answer Point" maxlength="10" Columns="10" ID="Quiz_Answer_Point" data-id="UC_Add_more_answersTBL_Quiz_AnswersQuiz_Answer_Point" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="UC_Add_more_answersTBL_Quiz_AnswersQuiz_Answer_Text">Quiz Answer Text</label></td> 
      <td><label for="<%# TBL_Quiz_Answers.GetControl<InMotion.Web.Controls.MTTextBox>("TextArea1").ClientID %>" style="display: none;">TextArea1</label> 
<mt:MTTextBox TextMode="Multiline" Source="Quiz_Answer_Text" Caption="TextArea1" Rows="3" Columns="10" ID="TextArea1" data-id="UC_Add_more_answersTBL_Quiz_AnswersTextArea1" runat="server"/>
</td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UC_Add_more_answersTBL_Quiz_AnswersButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="UC_Add_more_answersTBL_Quiz_AnswersButton_Update" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="TBL_Quiz_AnswersDataSource" ConnectionString="InMotion:LearnersClub" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_Quiz_Answers {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>
INSERT INTO TBL_Quiz_Answers([Quiz_id], [Quiz_Quest_id], [Quiz_Answer_Status], [Quiz_Answer_Point], [Quiz_Answer_Text]) VALUES ({Quiz_id}, {Quiz_Quest_id}, {Quiz_Answer_Status}, {Quiz_Answer_Point}, {Quiz_Answer_Text})
  </InsertCommand>
   <UpdateCommand>
UPDATE TBL_Quiz_Answers SET [Quiz_Answer_Status]={Quiz_Answer_Status}, [Quiz_Answer_Point]={Quiz_Answer_Point}, [Quiz_Answer_Text]={Quiz_Answer_Text}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_Answer_id" SourceType="URL" Source="Quiz_Answer_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_Answer_id" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_id" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_Quest_id" SourceType="URL" Source="Quiz_Quest_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Quiz_Quest_id" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="Quiz_id" SourceType="URL" Source="Quiz_id" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Quiz_Quest_id" SourceType="URL" Source="Quiz_Quest_id" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Quiz_Answer_Status" SourceType="Control" Source="Quiz_Answer_Status" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Quiz_Answer_Point" SourceType="Control" Source="Quiz_Answer_Point" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="Quiz_Answer_Text" SourceType="Control" Source="TextArea1" DataType="Text" IsOmitEmptyValue="True"/>
   </InsertParameters>
</mt:MTDataSource><br>
<br>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad Event Start @-784E4332
    actions["UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad"] = function (eventType, parameters) {
        var result = true;
//End UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad Event Start

//Attach CKeditor @16-84B6F82E
        try {
            var UC_Add_more_answersTBL_Quiz_AnswersTextArea1_CK_BasePath = "<%#ResolveClientUrl("~/")%>ckeditor/";
            var id = this.getAttribute("id");
            var editor = CKEDITOR.instances[id];
            try { if (editor) editor.destroy(true); } catch (e) {}
            editor = CKEDITOR.replace(id, {
                height: "600",
                width: "500",
                toolbar: "Full"
            });
            var $this = $(this);
            editor.needChanging = false;
            editor.nowChanging = false;
            var onChange = function () {
                if (editor.needChanging && !editor.nowChanging) {
                    editor.needChanging = false;
                    editor.nowChanging = true;
                    editor.setData($this.val(), function() {
                        editor.nowChanging = false;
                        onChange();
                    });
                } else
                    editor.needChanging = true;
            };
            $this.bind("change", function() {
                editor.needChanging = true;
                onChange();
            });
        } catch (e) {}
//End Attach CKeditor

//UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad Event End @-A5B9ECB8
        return result;
    };
//End UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad Event End

//Event Binding @1-1C174FC9
    $('*:ccsControl(UC_Add_more_answers, TBL_Quiz_Answers, TextArea1)').ccsBind(function() {
        this.each(function(){ actions["UC_Add_more_answersTBL_Quiz_AnswersTextArea1OnLoad"].call(this); });
    });
//End Event Binding

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<p>&nbsp;</p>




<!--End ASCX page-->

