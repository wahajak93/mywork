//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-AD851838
public partial class KMS_SectionQuestions_Page : MTPage
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants
   public int RowNumber = 0;
    private string DepartsEmtyRowId;
//Page KMS_SectionQuestions Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page KMS_SectionQuestions Event Init
        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page Dashboard access rights. @1-89146346
        this.Restricted = true;
        this.UserRights.Add("Free", false);
        this.UserRights.Add("Premium", true);

        //this
//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page KMS_SectionQuestions Init event tail @1-FCB6E20C
    }
//End Page KMS_SectionQuestions Init event tail

//Page KMS_SectionQuestions Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page KMS_SectionQuestions Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page KMS_SectionQuestions Load event tail @1-FCB6E20C
    }
//End Page KMS_SectionQuestions Load event tail

//Page KMS_SectionQuestions Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page KMS_SectionQuestions Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page KMS_SectionQuestions On PreInit event tail @1-FCB6E20C
    }
//End Page KMS_SectionQuestions On PreInit event tail

//DEL          ViewState["DepsRowIDs"] = DepartsEmtyRowId.TrimEnd(new Char[] { ',' });

//DEL          MTListBox lbSkills = uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id");
//DEL          
//DEL          ((HtmlControl)uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow")).Attributes["data-id"] = "uc_recSkilldepartmentsrow_" + (++RowNumber).ToString();
//DEL         
//DEL          if (uc_recSkilldepartments.CurrentItem.ItemType == ListItemType.Item || uc_recSkilldepartments.CurrentItem.ItemType == ListItemType.AlternatingItem)
//DEL          {
//DEL              if (uc_recSkilldepartments.CurrentItem.DataItem == null)
//DEL              {
//DEL                  DepartsEmtyRowId += uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow").ClientID + ",";
//DEL                  ((HtmlControl)uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow")).Attributes.Add("style", "display:none");
//DEL              }
//DEL              
//DEL              if(lbSkills.SelectedValue != "")
//DEL              {
//DEL              	lbSkills.Enabled = false;
//DEL              }
//DEL          }

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          // -------------------------
//DEL          
//DEL         MTListBox lbSkills = uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id");
//DEL         
//DEL        /* if(lbSkills.SelectedValue == "")
//DEL         {
//DEL         		uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Text = "";
//DEL         		uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Text = "";
//DEL         		//uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsModifiedOn").Text = "";		
//DEL         } */
//DEL         	string tutorID = Session["UserID"].ToString();
//DEL          
//DEL          string dTutorID = DataUtility.DLookup<MTText>("TutorID", "tbl_TutorProfileSkills", "TutorID='"+Session["UserID"]+"'", "InMotion:LearnersClub");
//DEL          string skills = DataUtility.DLookup<MTText>("TutorSkillsID", "tbl_TutorProfileSkills", "TutorSkillsID='"+lbSkills.SelectedValue+"'", "InMotion:LearnersClub");
//DEL          
//DEL          if((tutorID != "") && (skills != ""))
//DEL          {
//DEL          	if((tutorID == dTutorID) && (skills == lbSkills.SelectedValue) && (lbSkills.Enabled == true))
//DEL          	{
//DEL          	//Response.Redirect("~/skills.aspx");
//DEL          	uc_recSkilldepartments.Errors.Add("Already Inserted");
//DEL          	}
//DEL          }

//DEL          foreach (RepeaterItem item in ((EditableGrid)sender).Items)
//DEL          {
//DEL              if (!((EditableGrid)sender).IsRowEmpty(item))
//DEL              {
//DEL                  ((HtmlControl)(item.FindControl("uc_recSkilldepartmentsrow"))).Attributes.Remove("style");
//DEL                  //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
//DEL                  //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
//DEL                  
//DEL              }
//DEL              
//DEL          }
//DEL          
//DEL          /*if(uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id").SelectedValue != "Selected Value")
//DEL          {
//DEL          	uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
//DEL                  uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
//DEL          } */

//DEL          /*Connection conn = AppConfig.GetConnection("IntranetDB");
//DEL          DataCommand command = (DataCommand)conn.CreateCommand();
//DEL          command.CommandText = "UPDATE employees SET department_id = NULL WHERE department_id = {dep_id}";
//DEL          command.Parameters.Clear();
//DEL          command.Parameters.Add(new InMotion.Data.SqlParameter("dep_id", DataType.Integer));
//DEL          //Get the where parameters
//DEL          command.Parameters["dep_id"].Value = e.Command.WhereParameters["PKdepartment_id"].Value;
//DEL          //Delete project employees links
//DEL          command.ExecuteNonQuery();*/

//DEL        //  ViewState["DepsRowIDs"] = DepartsEmtyRowId.TrimEnd(new Char[] { ',' });

//DEL          ((HtmlControl)departments.CurrentItem.FindControl("departmentsrow")).Attributes["data-id"] = "departmentsrow_" + (++RowNumber).ToString();
//DEL          if (departments.CurrentItem.ItemType == ListItemType.Item || departments.CurrentItem.ItemType == ListItemType.AlternatingItem)
//DEL          {
//DEL              if (departments.CurrentItem.DataItem == null)
//DEL              {
//DEL                  DepartsEmtyRowId += departments.CurrentItem.FindControl("departmentsrow").ClientID + ",";
//DEL                  ((HtmlControl)departments.CurrentItem.FindControl("departmentsrow")).Attributes.Add("style", "display:none");
//DEL              }
//DEL          }

//DEL          foreach (RepeaterItem item in ((EditableGrid)sender).Items)
//DEL          {
//DEL              if (!((EditableGrid)sender).IsRowEmpty(item))
//DEL              {
//DEL                  ((HtmlControl)(item.FindControl("departmentsrow"))).Attributes.Remove("style");
//DEL              }
//DEL          }

//DEL          Connection conn = AppConfig.GetConnection("IntranetDB");
//DEL          DataCommand command = (DataCommand)conn.CreateCommand();
//DEL          command.CommandText = "UPDATE employees SET department_id = NULL WHERE department_id = {dep_id}";
//DEL          command.Parameters.Clear();
//DEL          command.Parameters.Add(new InMotion.Data.SqlParameter("dep_id", DataType.Integer));
//DEL          //Get the where parameters
//DEL          command.Parameters["dep_id"].Value = e.Command.WhereParameters["PKdepartment_id"].Value;
//DEL          //Delete project employees links
//DEL          command.ExecuteNonQuery();

//EditableGrid uc_recSkilldepartments Event Before Show @21-E3037C9C
    protected void uc_recSkilldepartments_BeforeShow(object sender, EventArgs e) {
//End EditableGrid uc_recSkilldepartments Event Before Show

//EditableGrid uc_recSkilldepartments Event Before Show. Action Custom Code @36-2A29BDB7
        ViewState["DepsRowIDs"] = DepartsEmtyRowId.TrimEnd(new Char[] { ',' });
//End EditableGrid uc_recSkilldepartments Event Before Show. Action Custom Code

//EditableGrid uc_recSkilldepartments Before Show event tail @21-FCB6E20C
    }
//End EditableGrid uc_recSkilldepartments Before Show event tail

//EditableGrid uc_recSkilldepartments Event Before Show Row @21-B84C3FAF
    protected void uc_recSkilldepartments_BeforeShowRow(object sender, EventArgs e) {
//End EditableGrid uc_recSkilldepartments Event Before Show Row

//EditableGrid uc_recSkilldepartments Event Before Show Row. Action Custom Code @34-2A29BDB7
        MTListBox lbSkills = uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id");
        
        ((HtmlControl)uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow")).Attributes["data-id"] = "uc_recSkilldepartmentsrow_" + (++RowNumber).ToString();
       
        if (uc_recSkilldepartments.CurrentItem.ItemType == ListItemType.Item || uc_recSkilldepartments.CurrentItem.ItemType == ListItemType.AlternatingItem)
        {
            if (uc_recSkilldepartments.CurrentItem.DataItem == null)
            {
                DepartsEmtyRowId += uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow").ClientID + ",";
                ((HtmlControl)uc_recSkilldepartments.CurrentItem.FindControl("uc_recSkilldepartmentsrow")).Attributes.Add("style", "display:none");
            }
            
            if(lbSkills.SelectedValue != "")
            {
            	lbSkills.Enabled = false;
            }
        }
//End EditableGrid uc_recSkilldepartments Event Before Show Row. Action Custom Code

//EditableGrid uc_recSkilldepartments Before Show Row event tail @21-FCB6E20C
    }
//End EditableGrid uc_recSkilldepartments Before Show Row event tail

//EditableGrid uc_recSkilldepartments Event On Validate Row @21-6F3CDCD5
    protected void uc_recSkilldepartments_RowValidating(object sender, ValidatingEventArgs e) {
//End EditableGrid uc_recSkilldepartments Event On Validate Row

        if (uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("ListBox1").SelectedValue == "")
        {

            uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("ListBox1").Errors.Add("'Question' or 'Section' cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }

        if (uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id").SelectedValue == "")
        {

            uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }


//EditableGrid uc_recSkilldepartments Event On Validate Row. Action Custom Code @38-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End EditableGrid uc_recSkilldepartments Event On Validate Row. Action Custom Code

//EditableGrid uc_recSkilldepartments On Validate Row event tail @21-FCB6E20C
    }
//End EditableGrid uc_recSkilldepartments On Validate Row event tail

//EditableGrid uc_recSkilldepartments Event Before Submit @21-C82106A9
    protected void uc_recSkilldepartments_BeforeSubmit(object sender, EventArgs e) {
//End EditableGrid uc_recSkilldepartments Event Before Submit

//EditableGrid uc_recSkilldepartments Event Before Submit. Action Custom Code @37-2A29BDB7
        foreach (RepeaterItem item in ((EditableGrid)sender).Items)
        {
            if (!((EditableGrid)sender).IsRowEmpty(item))
            {
                ((HtmlControl)(item.FindControl("uc_recSkilldepartmentsrow"))).Attributes.Remove("style");
                //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
                //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
                
            }
            
        }
        
        /*if(uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id").SelectedValue != "Selected Value")
        {
        	uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
                uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
        } */
//End EditableGrid uc_recSkilldepartments Event Before Submit. Action Custom Code

//EditableGrid uc_recSkilldepartments Before Submit event tail @21-FCB6E20C
    }
//End EditableGrid uc_recSkilldepartments Before Submit event tail

//EditableGrid uc_recSkilldepartments Event Before Execute Delete @21-72411A56
    protected void uc_recSkilldepartments_BeforeExecuteDelete(object sender, DataOperationEventArgs e) {
//End EditableGrid uc_recSkilldepartments Event Before Execute Delete

//EditableGrid uc_recSkilldepartments Event Before Execute Delete. Action Custom Code @35-2A29BDB7
        /*Connection conn = AppConfig.GetConnection("IntranetDB");
        DataCommand command = (DataCommand)conn.CreateCommand();
        command.CommandText = "UPDATE employees SET department_id = NULL WHERE department_id = {dep_id}";
        command.Parameters.Clear();
        command.Parameters.Add(new InMotion.Data.SqlParameter("dep_id", DataType.Integer));
        //Get the where parameters
        command.Parameters["dep_id"].Value = e.Command.WhereParameters["PKdepartment_id"].Value;
        //Delete project employees links
        command.ExecuteNonQuery();*/
//End EditableGrid uc_recSkilldepartments Event Before Execute Delete. Action Custom Code

//EditableGrid uc_recSkilldepartments Before Execute Delete event tail @21-FCB6E20C
    }
//End EditableGrid uc_recSkilldepartments Before Execute Delete event tail

//CheckBox CheckBox_Delete Event Init @25-745CD900
    protected void uc_recSkilldepartmentsCheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @25-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @25-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @25-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @25-89632CB1
    protected void uc_recSkilldepartmentsCheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @25-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @25-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

