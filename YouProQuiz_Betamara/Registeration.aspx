﻿<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-EE3C3B3B-->
<%@ Page language="C#" CodeFile="RegisterationEvents.aspx.cs" Inherits="YouProQuiz_Beta.Registeration_Page"  %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="uc_TutorSignup" Src="uc_TutorSignup.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-75BF367B-->
<html>
<head>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Registeration</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script src='ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>' type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
     <div class="navbar navbar-fixed-top" >
      <div class="navbar-inner">
        <div class="container">
        
          <a class="brand" href="learners.club">
             <img src="bootstrap-3.3.6-dist/learnersclub.png" alt="Learners Quiz" /></a>
        </div>
      </div>
    </div>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">


<YouProQuiz_Beta:uc_TutorSignup ID="uc_TutorSignup" runat="server"/>

</form>
      <footer class="white navbar-fixed-bottom">
      Already Registered  <mt:MTLink Text="Sign Up Here" ID="Link1" data-id="Link1" runat="server" HrefSource="~/Home.aspx" PreserveParameters="Get" class="btn btn-black"/>
    </footer>
<script src="login_css/jquery.js"></script>
    <script src="login_css/bootstrap.js"></script>
    <script src="login_css/backstretch.min.js"></script>
    <script src="login_css/typica-login.js"></script>

  

<div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 855px; width: 1280px; z-index: -999999; position: fixed;"><img src="bg1.png" class="deleteable" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1520.74px; height: 855px; max-width: none; z-index: -999999; left: -120.371px; top: 0px;"><img src="bg2.png" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1520.74px; height: 855px; max-width: none; z-index: -999999; left: -120.371px; top: 0px; opacity: 0.828293;"></div>       <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
</body>
</html>

<!--End ASPX page-->

