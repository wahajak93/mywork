<!--ASCX page header @1-F0738C9D-->
<%@ Control language="C#" CodeFile="uc_TutorSignupEvents.ascx.cs" Inherits="YouProQuiz_Beta.uc_TutorSignup_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->
<link href="login_css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="login_css/bootstrap.min.css" rel="stylesheet" />
<link href="login_css/typica-login.css" rel="stylesheet" />

<!--ASCX page @1-DC79B52C-->

<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-7D8DD2A1
</script>
<script type="text/javascript" charset="utf-8" src='../ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//DEL          // -------------------------
//DEL          // Write your own code here.
//DEL          Label1.Value = "Your account has been created. Please check your email for verification. Thank You";
//DEL          // -------------------------


//End CCS script
</script>
<mt:Record PreserveParameters="Get" DataSourceID="tbl_TutorDataSource" AllowRead="False" AllowDelete="False" AllowUpdate="False" OnAfterInsert="tbl_Tutor_AfterInsert" ID="tbl_Tutor" runat="server" OnValidating="tbl_Tutor_Validating"><ItemTemplate><div data-emulate-form="uc_signuptbl_Tutor" id="uc_signuptbl_Tutor">






 
        <div class="container">

        <div id="login-wraper">

            <div class="form login-form">
                  <legend>Sign up</legend>
  <div class="body">
    <mt:MTPanel id="Error" visible="False" runat="server">
  
            <mt:MTLabel id="ErrorLabel" runat="server"/>
    
 </mt:MTPanel>
  
      <label for="<%# tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorEmail").ClientID %>">Email</label>
      <mt:MTTextBox Source="user_email"  Unique="True" ErrorControl="errEmail" Caption="Email" maxlength="50" OnValidating="tbl_TutorTutorEmail_Validating" ID="TutorEmail" data-id="uc_signuptbl_TutorTutorEmail" runat="server"/> 
     <br/>
       <p class="text-error"><mt:MTLabel ID="errEmail" runat="server"/></p>
    
      

<label for="<%# tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").ClientID %>">Password</label>
<mt:MTTextBox TextMode="Password" Source="user_password" Required="True" ErrorControl="errPassword" Caption="Password" maxlength="50" OnValidating="tbl_TutorTutorPassword_Validating" ID="TutorPassword" data-id="uc_signuptbl_TutorTutorPassword" runat="server"/>
   <br/>
     <p class="text-error"> <mt:MTLabel ID="errPassword" runat="server"/></p>
  
      
      
      <label for="<%# tbl_Tutor.GetControl<InMotion.Web.Controls.MTTextBox>("confirmPassword").ClientID %>">Confirm Password</label>  
      <mt:MTTextBox TextMode="Password" SourceType="CodeExpression" Required="True" ErrorControl="errCnfPassword" Caption="Confirm Password" maxlength="50" Columns="50" ID="confirmPassword" data-id="uc_signuptbl_TutorconfirmPassword" runat="server"/>
      <br/>
      <p class="text-error"> <mt:MTLabel SourceType="CodeExpression" ID="errCnfPassword" runat="server"/></p>
   </div>
        <div class="footer">
          <mt:MTButton CommandName="Insert" Text="Join Now" CssClass="btn btn-success" OnClick="tbl_TutorButton_Insert_Click" ID="Button_Insert" data-id="uc_signuptbl_TutorButton_Insert" runat="server"/><mt:MTHidden Source="user_name" Caption="Tutor Username" ID="TutorUsername" data-id="uc_signuptbl_TutorTutorUsername" runat="server"/><mt:MTHidden Source="user_added_on" DataType="Date" Format="MM/dd/yyyy h\:mm tt" Caption="Tutor Added On" ID="TutorAddedOn" data-id="uc_signuptbl_TutorTutorAddedOn" runat="server"/><mt:MTHidden Source="user_modified_on" DataType="Date" Format="d" Caption="Tutor Modified On" ID="TutorModifiedOn" data-id="uc_signuptbl_TutorTutorModifiedOn" runat="server"/><mt:MTHidden Source="user_added_by" DataType="Integer" Caption="Tutor Added By" ID="TutorAddedBy" data-id="uc_signuptbl_TutorTutorAddedBy" runat="server"/><mt:MTHidden Source="user_modified_by" DataType="Integer" Caption="Tutor Modified By" ID="TutorModifiedBy" data-id="uc_signuptbl_TutorTutorModifiedBy" runat="server"/><mt:MTHidden Source="user_type_id" DataType="Integer" Caption="Tutor Group ID" ID="TutorGroupID" data-id="uc_signuptbl_TutorTutorGroupID" runat="server"/><mt:MTHidden Source="user_verification_code" DataType="Float" Caption="Tutor Verification Code" ID="TutorVerificationCode" data-id="uc_signuptbl_TutorTutorVerificationCode" runat="server"/></td> 
   </div>
       
             </div>

            </div>

            </div>                   

</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tbl_TutorDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" ValidateUniqueCommandType="Table" OnInitializeInsertParameters="tbl_Tutor_InitializeInsertParameters" OnBeforeBuildInsert="tbl_Tutor_BeforeBuildInsert" runat="server">
   <SelectCommand>

SELECT * 
FROM TBL_users {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <InsertCommand>

INSERT INTO TBL_users(user_name, user_email, user_password, user_added_on, user_modified_on, user_added_by, user_modified_by, user_type_id, user_verification_code, user_status_id, user_group_id) VALUES ({user_name}, {user_email}, {user_password}, {user_added_on}, {user_modified_on}, {user_added_by}, {user_modified_by}, {user_type_id}, {user_verification_code}, {user_status_id}, {user_group_id})
  </InsertCommand>
   <UpdateCommand>

UPDATE TBL_users SET user_name={user_name}, user_email={user_email}, user_password={user_password}, user_added_on={user_added_on}, user_modified_on={user_modified_on}, user_added_by={user_added_by}, user_modified_by={user_modified_by}, user_type_id={user_type_id}, user_verification_code={user_verification_code}
  </UpdateCommand>
<CountCommand>
</CountCommand>
   <ValidateUniqueCommand>

SELECT COUNT(*)
 FROM TBL_users
  </ValidateUniqueCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlTutorID" SourceType="URL" Source="TutorID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="TutorID" Required="true"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="user_name" SourceType="Control" Source="TutorEmail" DataType="Text" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_email" SourceType="Control" Source="TutorEmail" DataType="Text" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_password" SourceType="Control" Source="TutorPassword" DataType="Text" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_added_on" SourceType="Control" Source="TutorAddedOn" DataType="Date" Format="MM/dd/yyyy h\:mm tt" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_modified_on" SourceType="Control" Source="TutorModifiedOn" DataType="Date" Format="d" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_added_by" SourceType="Control" Source="TutorAddedBy" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_modified_by" SourceType="Control" Source="TutorModifiedBy" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_type_id" SourceType="Control" Source="TutorGroupID" DataType="Integer" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_verification_code" SourceType="Control" Source="TutorVerificationCode" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="user_status_id" SourceType="Expression" DataType="Integer"/>
     <mt:SqlParameter Name="user_group_id" SourceType="Expression" DataType="Integer"/>
   </InsertParameters>
<DeleteCommand>
</DeleteCommand>
</mt:MTDataSource>



<!--End ASCX page-->

