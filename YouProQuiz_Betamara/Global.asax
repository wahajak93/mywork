<%@ Application Language="C#"%>
<%@ Import Namespace="System.Web.Routing" %>
   <%@ Import Namespace="System.Web.Http" %>

<script runat="server">
//Application_Start @1-F5CAC4B5
    protected void Application_Start(Object sender, EventArgs e)
    {
        InMotion.Common.Resources.ResManager = new InMotion.Resources.MTResourceManager("resources.Strings", System.Reflection.Assembly.Load(new System.Reflection.AssemblyName("app_GlobalResources")));
//End Application_Start

        RouteTable.Routes.MapHttpRoute(
             name: "DefaultApi",
             routeTemplate: "api/{controller}/{id}",
              defaults: new { id = System.Web.Http.RouteParameter.Optional }
        );

       
        
//Application_Start tail @1-FCB6E20C
    }
//End Application_Start tail

//Session_Start @1-271D0C25
    protected void Session_Start(Object sender, EventArgs e)
    {
//End Session_Start

//Session_Start tail @1-FCB6E20C
    }
//End Session_Start tail

//Application_BeginRequest @1-DE0B705D
    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        if (Application[Request.PhysicalPath] != null)
            Request.ContentEncoding = System.Text.Encoding.GetEncoding(Application[Request.PhysicalPath].ToString());
//End Application_BeginRequest

//Application_BeginRequest tail @1-FCB6E20C
    }
//End Application_BeginRequest tail

//Application_EndRequest @1-DD310919
    protected void Application_EndRequest(Object sender, EventArgs e)
    {
//End Application_EndRequest

//Application_EndRequest tail @1-FCB6E20C
    }
//End Application_EndRequest tail

//Session_End @1-7F4664F6
    protected void Session_End(Object sender, EventArgs e)
    {
//End Session_End

//Session_End tail @1-FCB6E20C
    }
//End Session_End tail

//Application_End @1-F652CF28
    protected void Application_End(Object sender, EventArgs e)
    {
//End Application_End

//Application_End tail @1-FCB6E20C
    }
//End Application_End tail

</script>
