﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC1_UserOverallResult.ascx.cs" Inherits="YouQuizPro.UC1_UserDashBoard" %>


      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
 <%--     <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>--%>

   <script src="dist/Chart.bundle.js"></script>

    <script>

        var params = {};

        if (location.search) {
            var parts = location.search.substring(1).split('&');

            for (var i = 0; i < parts.length; i++) {
                var nv = parts[i].split('=');
                if (!nv[0]) continue;
                params[nv[0]] = nv[1] || true;
            }
        }


        var us_id = params.user_id;
        var quiz_id=params.quiz_id;


    
 
    

    </script>



    <%--<script>
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Performance Overview'
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 150,
                    y: 100,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'white'
                },
                xAxis: {
                    

                    //categories: [
                    //    'Monday',
                    //    'Tuesday',
                    //    'Wednesday',
                    //    'Thursday',
                    //    'Friday',
                    //    'Saturday',
                    //    'Sunday'
                    //],
                    categories: questions,


                    plotBands: [{ // visualize the weekend
                        from: 4.5,
                        to: 6.5,
                        color: 'rgba(120, 170, 213, .6)'
                    }]
                },
                yAxis: {
                    title: {
                        text: 'Grade Points'
                    }
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' %'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    areaspline: {
                        fillOpacity: 0.5
                    }
                },
                series: [{
                    name: 'John',
                    data: points
                }, {
                    name: 'Average',
                    data: points1
                }]

            });
        });</script>--%>

<script>


    var ctx = document.getElementById("canvas1");

    var data = {
        labels: questions,
        datasets: [


         


           // name: 'Best Scores',
        //data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]




            {
                label: "Overall Average",
                backgroundColor: "rgba(0, 0, 0, 0.8)",
                borderColor: "rgba(255,99,132,1)",
                borderWidth: 0.2,
                hoverBackgroundColor: "rgba(200,99,132,0.4)",
                hoverBorderColor: "rgba(200,99,132,1)",
                data: points1,
                //Overall_Avg_Score

                options: {
                    scale: {
                        reverse: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }
                }
            },


               {
                   label: "Current User's Avg Score",
                   backgroundColor: "rgba(27, 158, 21, 0.64)",
                   borderColor: "rgba(255,99,132,1)",
                   borderWidth: 0.2,
                   hoverBackgroundColor: "rgba(200,99,132,0.4)",
                   hoverBorderColor: "rgba(200,99,132,1)",
                   data: points,

                   options: {
                       scale: {
                           reverse: true,
                           ticks: {
                               beginAtZero: true
                           }
                       }
                   }
               }






        ]
    };



    var myBarChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: data,

        minimum: 0


    });


</script>
















<h1>User Overall Record</h1>

<div id="container" style="min-width: 200px; height: 300px; margin: 0 auto;"></div>

<canvas id="canvas1" style="width:300px;height:200px;"></canvas>



