//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-DC5474EF
public partial class UC1_Edit_Existing_Quiz_Page : MTUserControl
{
//End Page class

//Attributes constants @1-52B30067
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UC1_Edit_Existing_Quiz Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC1_Edit_Existing_Quiz Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit
//Response.Write(Session["UserID"].ToString());
//Page UC1_Edit_Existing_Quiz Init event tail @1-FCB6E20C
    }
//End Page UC1_Edit_Existing_Quiz Init event tail

//Page class tail @1-FCB6E20C
    protected void TBL_QuizSearch_Validating(object sender, ValidatingEventArgs e)
    {
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").Text, "^[0-9]*$"))
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").Errors.Add("must contain digits only");
            e.HasErrors = true;
        }



    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

