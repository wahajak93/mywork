//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-9A866E53
public partial class AdminDashboard_Page : MTPage
{
//End Page class

//Page AdminDashboard Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page AdminDashboard Event Init


//End Set Page AdminDashboard access rights.

//Page AdminDashboard Init event tail @1-FCB6E20C
    }
//End Page AdminDashboard Init event tail

//Page AdminDashboard Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page AdminDashboard Event Load

        this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page Dashboard access rights. @1-89146346



        //Access Denied Page Url @1-2E5EF9F8
        // this.AccessDeniedPage = "Home.aspx";
        //End Access Denied Page Url

        //Set Page AdminDashboard access rights. @1-23BE2062
        this.Restricted = true;
        this.UserRights.Add("premium", true);
        this.UserRights.Add("Free", false);
//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page AdminDashboard Load event tail @1-FCB6E20C
    }
//End Page AdminDashboard Load event tail

//Page AdminDashboard Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page AdminDashboard Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "Dashboard.master";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page AdminDashboard On PreInit event tail @1-FCB6E20C
    }
//End Page AdminDashboard On PreInit event tail

//Page class tail @1-FCB6E20C
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/KMS_AddNewQuiz.aspx");
    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

