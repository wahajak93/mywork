//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-7B319DAC
public partial class KMS_AddExistingQuestion_Page : MTPage
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page KMS_AddExistingQuestion Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page KMS_AddExistingQuestion Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();

   




//End DataBind

//Page KMS_AddExistingQuestion Load event tail @1-FCB6E20C
    }
//End Page KMS_AddExistingQuestion Load event tail

//Page KMS_AddExistingQuestion Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page KMS_AddExistingQuestion Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page KMS_AddExistingQuestion On PreInit event tail @1-FCB6E20C
    }
//End Page KMS_AddExistingQuestion On PreInit event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

