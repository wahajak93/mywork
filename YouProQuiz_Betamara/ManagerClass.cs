﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.IO;
using System.Net.Http.Formatting;
using System.IO;
using System.Net.Http.Formatting;
using System.Data;

namespace YouQuizPro
{
    public class ManagerClass
    {
   
                    

        public static void Add_Quest(HttpRequest http1, int quiz_id, int no_of_answers, string answer_type)
        {

             int Quiz_Answer_Added_By = 2;
        int  Quiz_Answer_Modified_By = 2;
        DateTime  Added_On = DateTime.Now;


            string quest_media_name = "";
            string quest_media_url = "";
            string ques_text = http1.Form["txt_question"];
            string ques_status = "active";
           string pts_dis_separate="";
               if(http1.Form["points_type"]!=null)
               {
                   pts_dis_separate=http1.Form["points_type"];
               }
                 else
               {
                    pts_dis_separate="0";
               }

            /*
             * here the logic for adding the points to the questions according to 
             * the situation...whether the separate marks for each question is 
             * checked or not 
             * 
             */
            


            
            
            
            string quest_point = http1.Form["txt_points"];
            string quest_answer_type = answer_type;
            var quest_media = http1.Files["quest_file"];

            if (quest_media.FileName !="" )
            {
                string maxid="";
                DataTable DTmaxid= clsdb.readdata("SELECT MAX(Quiz_Answer_id) as max_id from TBL_Quiz_Answers");
                if (DTmaxid.Rows.Count != 0)
                {
                    maxid = DTmaxid.Rows[0][0].ToString();
                    
                }

                quest_media_name = maxid+ quest_media.FileName  ;
                quest_media_url = HttpContext.Current.Server.MapPath("~/Media/") +quest_media_name;
                quest_media.SaveAs(quest_media_url);

            }

          
            //Add Questions to the databases


            clsdb.writedata("INSERT INTO TBL_Quiz_Questions(Quiz_id,Quiz_Quest_Text,Quiz_Quest_status,Quiz_Quest_point,Quiz_Ques_answer_type,Quiz_Quest_Media_url,Quiz_Media_Name,Quiz_Quest_Added_On,Quiz_Quest_Added_By,Quiz_Quest_Modified_By,Pts_Dist_Type) VALUES(" + quiz_id + ",'" + ques_text + "','" + ques_status + "'," + quest_point + ",'" + quest_answer_type + "','" + quest_media_url + "','" + quest_media_name + "','"+Added_On+"',"+Quiz_Answer_Added_By+","+Quiz_Answer_Modified_By+","+pts_dis_separate+");");
            DataTable dt1 = clsdb.readdata("SELECT MAX(Quiz_Quest_id) as ques_no FROM TBL_Quiz_Questions");
            string ques_no = dt1.Rows[0]["ques_no"].ToString();

          

            if (answer_type == "2" || answer_type=="1")
            {
                string[] answer_text = new string[30];
                string[] answer_correct_or_not = new string[30];
                string check_name2 = "";
               



                for (int i = 0; i < no_of_answers; i++)
                {   
                    string name1 = "tbans" + (i + 1).ToString() + ".1";
                    string name2 = http1.Form[name1];
                    answer_text[i] = name2;

                    string correct_or_not = "";

                    string check_name = "chk" + (i + 1).ToString();

                    if (http1.Form[check_name] != null)
                    {
                        check_name2 = http1.Form[check_name].ToString();
                        answer_correct_or_not[i] = check_name2;

                        correct_or_not = "1";

                    }
                    else
                    {

                        answer_correct_or_not[i] = "";

                        correct_or_not = "0";
                    }

                    string val_of_point_type; 
                    int points_of_current_answ=0;
                    if (http1.Form["points_type"] != null)
                    {

                        val_of_point_type = http1.Form["points_sep"+(i+1).ToString()]; 
                       if(val_of_point_type!="" || val_of_point_type!=null){
                        
                           points_of_current_answ=int.Parse(val_of_point_type);
                       
                       }
                       
                    }

                    




                    string quest_media_name2="", quest_media_url2="";var quest_media2=http1.Files["file"+(i+1).ToString()];


                    if (quest_media2.FileName !="")
                   {


                       quest_media_name2 = quest_media2.FileName ;
                       quest_media_url2 = HttpContext.Current.Server.MapPath("~/Media/") + quiz_id.ToString()+ques_no.ToString() + quest_media_name2;
                       quest_media2.SaveAs(quest_media_url2);

                   
                   }



                    clsdb.writedata("INSERT INTO TBL_Quiz_Answers(Quiz_id,Quiz_Quest_id,Quiz_Answer_Text,Quiz_Answer_Status,Quiz_Answer_media_name,Quiz_Answer_Point,Quiz_Answer_Added_By,Quiz_Answer_Modified_By,Quiz_Answer_Added_On) VALUES(" + quiz_id + "," + ques_no + ",'" + answer_text[i] + "'," + correct_or_not + ",'" + quest_media_url2 + "'," + points_of_current_answ + "," + Quiz_Answer_Added_By + "," + Quiz_Answer_Modified_By + ",'" + Added_On + "')");



                    if (pts_dis_separate == "1")
                    {

                        if (answer_type == "2")
                        {
                            DataTable dt12 = clsdb.readdata("SELECT SUM(Quiz_Answer_Point) FROM TBL_Quiz_Answers WHERE Quiz_id=" + quiz_id + " AND Quiz_Quest_id=" + ques_no + " AND Quiz_Answer_Status=1");
                            int sum1 = int.Parse(dt12.Rows[0][0].ToString());
                            clsdb.writedata("UPDATE TBL_Quiz_Questions SET Quiz_Quest_point=" + sum1 + " WHERE Quiz_id=" + quiz_id + " AND Quiz_Quest_id=" + ques_no + "");
                        }
                        if (answer_type == "1")
                        {
                            DataTable dt12 = clsdb.readdata("SELECT MAX(Quiz_Answer_Point) FROM TBL_Quiz_Answers WHERE Quiz_id=" + quiz_id + " AND Quiz_Quest_id=" + ques_no + "");
                            int sum1 = int.Parse(dt12.Rows[0][0].ToString());
                            clsdb.writedata("UPDATE TBL_Quiz_Questions SET Quiz_Quest_point=" + sum1 + " WHERE Quiz_id=" + quiz_id + " AND Quiz_Quest_id=" + ques_no + "");
                        
                        
                        }

                        }








                }

            }


            //if (answer_type == "1")
            //{
            //    string[] answer_text = new string[30];
            //    string answer_correct_or_not = "";
            //    string radioname = "";
            //    int numericVal=0;
            //    radioname = "options";

            //    if (http1.Form[radioname] != null)
            //    {
            //        answer_correct_or_not = http1.Form[radioname];

            //        var numbersFromString = new String(answer_correct_or_not.Where(x => x >= '0' && x <= '9').ToArray());

            //        numericVal = Int32.Parse(numbersFromString);

            //    }




            



            //    for (int i = 0; i < no_of_answers; i++)
            //    {
            //        string name1 = "tbans" + (i + 1).ToString() + ".1";
            //        string name2 = http1.Form[name1];
            //        answer_text[i] = name2;




            //        string val_of_point_type;
            //        int points_of_current_answ = 0;
            //        if (http1.Form["points_type"] != null)
            //        {

            //            val_of_point_type = http1.Form["points_sep" + (i + 1).ToString()];
            //            if (val_of_point_type != "" || val_of_point_type != null)
            //            {

            //                points_of_current_answ = int.Parse(val_of_point_type);

            //            }

            //        }
         




            //        string quest_media_name2 = "", quest_media_url2 = ""; var quest_media2 = http1.Files["file" + (i + 1).ToString()];


            //        if (quest_media2.FileName != "")
            //        {


            //            quest_media_name2 = quest_media2.FileName;
            //            quest_media_url2 = HttpContext.Current.Server.MapPath("~/Media/") + quiz_id.ToString() + ques_no.ToString() + DateTime.Now.Date + DateTime.Now.TimeOfDay + quest_media_name2;
            //            quest_media2.SaveAs(quest_media_url2);


            //        }







            //           if(numericVal-1 == i)
            //        {

            //            answer_correct_or_not = "1";
            //        }
                
            //    else
            //    {

            //        answer_correct_or_not = "0";

            //    }

            //           clsdb.writedata("INSERT INTO TBL_Quiz_Answers(Quiz_id,Quiz_Quest_id,Quiz_Answer_Text,Quiz_Answer_Status,Quiz_Answer_media_name,Quiz_Answer_Added_By,Quiz_Answer_Modified_By,Quiz_Answer_Added_On,Quiz_Answer_Modified_On,Quiz_Answer_Point) VALUES(" + quiz_id + "," + ques_no + ",'" + answer_text[i] + "'," + answer_correct_or_not + ",'" + quest_media_url2 + "',"+Quiz_Answer_Added_By+","+Quiz_Answer_Modified_By+",'"+Added_On+"',''"+
            //        "," + points_of_current_answ + ")");




            //    }










            //}




        }






      







    }
}
