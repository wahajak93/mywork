﻿<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-614FEE3F-->
<%@ Page language="C#" CodeFile="KMSAddQuestionToQuizEvents.aspx.cs" Inherits="YouProQuiz_Beta.KMSAddQuestionToQuiz_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASPX page header-->

<!--ASPX page @1-F399B06A-->
<html>
<head>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>KMSAddQuestionToQuiz</title>


<link rel="stylesheet" type="text/css" href="Styles/Basic/Style_doctype.css">
<link rel="stylesheet" type="text/css" href="Styles/Basic/jquery-ui.css">
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">


<p>&nbsp;</p>
<p>
<mt:Grid PageSizeLimit="100" RecordsPerPage="1" DataSourceID="tblQuestionDataSource" ID="tblQuestion" runat="server">
<HeaderTemplate>
<h2>List of Tbl Question</h2>
<p>
<table>
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <th scope="row">Question ID</th>
 
    <td><mt:MTLabel Source="QuestionID" DataType="Float" ID="QuestionID" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Name</th>
 
    <td><mt:MTLabel Source="Question_Name" ID="Question_Name" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question</th>
 
    <td><mt:MTLabel Source="Question" ID="Question" runat="server" ContentType="Html" Mode="PassThrough" />&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Inserted On</th>
 
    <td><mt:MTLabel Source="QuestionInsertedOn" DataType="Date" ID="QuestionInsertedOn" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Inserted By</th>
 
    <td><mt:MTLabel Source="TBL_users_user_name" ID="QuestionInsertedBy" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Updated On</th>
 
    <td><mt:MTLabel Source="QuestionUpdatedOn" DataType="Date" ID="QuestionUpdatedOn" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Updated By</th>
 
    <td><mt:MTLabel Source="TBL_users1_user_name" ID="QuestionUpdatedBy" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Question Status </th>
 
    <td><mt:MTLabel Source="Status" ID="QuestionStatusID" runat="server"/>&nbsp;</td>
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="2">No records</td>
  </tr>
  </mt:MTPanel>
</table>
&nbsp;</p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="tblQuestionDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} QuestionID, Question_Name, Question, QuestionInsertedOn, QuestionInsertedBy, QuestionUpdatedOn, QuestionUpdatedBy, QuestionStatusID,
Status, TBL_users.user_name AS TBL_users_user_name, TBL_users1.user_name AS TBL_users1_user_name 
FROM ((tblQuestion LEFT JOIN tblRefStatus ON
tblQuestion.QuestionStatusID = tblRefStatus.StatusID) LEFT JOIN TBL_users ON
tblQuestion.QuestionInsertedBy = TBL_users.id) LEFT JOIN TBL_users TBL_users1 ON
tblQuestion.QuestionUpdatedBy = TBL_users1.id {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM ((tblQuestion LEFT JOIN tblRefStatus ON
tblQuestion.QuestionStatusID = tblRefStatus.StatusID) LEFT JOIN TBL_users ON
tblQuestion.QuestionInsertedBy = TBL_users.id) LEFT JOIN TBL_users TBL_users1 ON
tblQuestion.QuestionUpdatedBy = TBL_users1.id
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tblQuestion.QuestionID"/>
   </SelectParameters>
</mt:MTDataSource>
<p></p>
<p>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tblChoiceDataSource" ID="tblChoice" runat="server" OnBeforeShow="tblChoice_BeforeShow">
<HeaderTemplate>
<h2>List of Tbl Choice</h2>
<p>
<table>
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <th scope="row">Choice ID</th>
 
    <td><mt:MTLabel Source="ChoiceID" DataType="Float" ID="ChoiceID" runat="server"/>&nbsp;</td>
  </tr>
 
  <tr>
    <th scope="row">Choice</th>
 
    <td><mt:MTLabel Source="Choice" ID="Choice" runat="server" ContentType="Html" Mode="PassThrough" />&nbsp;</td>
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="2">No records</td>
  </tr>
  </mt:MTPanel>
  <tr>
    <td colspan="2">
      <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" ID="Navigator" runat="server"/>&nbsp;</td>
  </tr>
</table>
&nbsp;</p>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="tblChoiceDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblChoice
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
   </SelectParameters>
</mt:MTDataSource>
<p></p>
<p>
<mt:MTButton Text="Add To Current Quiz" ReturnPage="~/KMSAddQuestionToQuiz.aspx" CssClass="Button" OnClick="Button1_Click" ID="Button1" data-id="Button1" runat="server" OnBeforeShow="Button1_BeforeShow"/></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
&nbsp;</p>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="1" DataSourceID="tbl_Quiz_Question1DataSource" AllowInsert="False" EmptyRows="0" ID="tbl_Quiz_Question1" runat="server">
<HeaderTemplate><div data-emulate-form="tbl_Quiz_Question1" id="tbl_Quiz_Question1">


   
  <h2>Add/Edit Tbl Quiz Question </h2>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="RowErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quiz_ID" DataType="Float" ID="Quiz_ID" data-id="tbl_Quiz_Question1Quiz_ID_{tbl_Quiz_Question1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quiz_Quest_ID" DataType="Float" ID="Quiz_Quest_ID" data-id="tbl_Quiz_Question1Quiz_Quest_ID_{tbl_Quiz_Question1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question1.GetControl("Quiz_Quest_Category_ID").ClientID %>">Question Category</label></th>
 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Category_ID" DataType="Float" Caption="Quiz Quest Category ID" DataSourceID="Quiz_Quest_Category_IDDataSource" DataValueField="CategoryID" DataTextField="Category" ID="Quiz_Quest_Category_ID" data-id="tbl_Quiz_Question1Quiz_Quest_Category_ID_{tbl_Quiz_Question1:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_Quest_Category_IDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionCategory {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td>
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question1.GetControl("Quiz_Quest_Type_ID").ClientID %>">Question Type</label></th>
 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Quest_Type_ID" DataType="Float" Caption="Quiz Quest Type ID" DataSourceID="Quiz_Quest_Type_IDDataSource" DataValueField="Quest_Type_ID" DataTextField="Quest_Type" ID="Quiz_Quest_Type_ID" data-id="tbl_Quiz_Question1Quiz_Quest_Type_ID_{tbl_Quiz_Question1:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_Quest_Type_IDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionType {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td>
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question1.GetControl("Quiz_Quest_Time").ClientID %>">Question Time</label></th>
 
      <td><mt:MTTextBox Source="Quiz_Quest_Time" DataType="Float" Caption="Quiz Quest Time" maxlength="12" Columns="12" ID="Quiz_Quest_Time" data-id="tbl_Quiz_Question1Quiz_Quest_Time_{tbl_Quiz_Question1:rowNumber}" runat="server"/></td>
    </tr>
 
    <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server">
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question1.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>">Delete</label></th>
 
      <td>
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="tbl_Quiz_Question1CheckBox_Delete_Init" OnLoad="tbl_Quiz_Question1CheckBox_Delete_Load" ID="CheckBox_Delete" data-id="tbl_Quiz_Question1CheckBox_Delete_PanelCheckBox_Delete_{tbl_Quiz_Question1:rowNumber}" runat="server"/></td>
    </tr>
 </mt:MTPanel>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="2">No records</td>
    </tr>
    </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="tbl_Quiz_Question1Button_Submit" runat="server"/></td>
    </tr>
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tbl_Quiz_Question1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tbl_Quiz_Question {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question
   </CountCommand>
   <UpdateCommand>
UPDATE tbl_Quiz_Question SET [Quiz_ID]={Quiz_ID}, [Quiz_Quest_ID]={Quiz_Quest_ID}, [Quiz_Quest_Category_ID]={Quiz_Quest_Category_ID}, [Quiz_Quest_Type_ID]={Quiz_Quest_Type_ID}, [Quiz_Quest_Time]={Quiz_Quest_Time}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tbl_Quiz_Question
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="Quiz_ID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_Quest_ID"/>
   </SelectParameters>
</mt:MTDataSource>
<p></p>
<p>&nbsp;</p>
<mt:EditableGrid PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tbl_Quiz_Question_Choice1DataSource" AllowInsert="False" AllowDelete="False" EmptyRows="0" ID="tbl_Quiz_Question_Choice1" runat="server">
<HeaderTemplate><div data-emulate-form="tbl_Quiz_Question_Choice1" id="tbl_Quiz_Question_Choice1">


   
  <h2>Add/Edit Tbl Quiz Question Choice </h2>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="RowErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="ID" DataType="Float" ID="ID" data-id="tbl_Quiz_Question_Choice1ID_{tbl_Quiz_Question_Choice1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quiz_ID" DataType="Float" Required="True" Caption="Quiz ID" ID="Quiz_ID" data-id="tbl_Quiz_Question_Choice1Quiz_ID_{tbl_Quiz_Question_Choice1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Choice_ID" DataType="Float" Required="True" Caption="Choice ID" ID="Choice_ID" data-id="tbl_Quiz_Question_Choice1Choice_ID_{tbl_Quiz_Question_Choice1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quest_ID" DataType="Float" Required="True" Caption="Quest ID" ID="Quest_ID" data-id="tbl_Quiz_Question_Choice1Quest_ID_{tbl_Quiz_Question_Choice1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question_Choice1.GetControl("Choice_Score").ClientID %>">Choice Score</label></th>
 
      <td><mt:MTTextBox Source="Choice_Score" DataType="Integer" Caption="Choice Score" maxlength="10" Columns="10" ID="Choice_Score" data-id="tbl_Quiz_Question_Choice1Choice_Score_{tbl_Quiz_Question_Choice1:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row">&nbsp;</th>
 
      <td>&nbsp;<mt:MTLabel Source="Choice" ID="Label1" runat="server" Mode="PassThrough" ContentType="Html" /></td>
    </tr>
 
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="2">No records</td>
    </tr>
    </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" ID="Navigator" runat="server"/>
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="tbl_Quiz_Question_Choice1Button_Submit" runat="server"/></td>
    </tr>
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tbl_Quiz_Question_Choice1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} Choice, tbl_Quiz_Question_Choice.* 
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question_Choice LEFT JOIN tblChoice ON
tbl_Quiz_Question_Choice.Choice_ID = tblChoice.ChoiceID
   </CountCommand>
   <InsertCommand>
INSERT INTO (ID, [Quiz_ID], [Choice_ID], [Quest_ID], [Choice_Score]) VALUES ({ID}, {Quiz_ID}, {Choice_ID}, {Quest_ID}, {Choice_Score})
  </InsertCommand>
   <UpdateCommand>
UPDATE tbl_Quiz_Question_Choice SET [Choice_Score]={Choice_Score}
  </UpdateCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="ID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quest_ID"/>
     <mt:WhereParameter Name="UrlQuiz_ID" SourceType="URL" Source="Quiz_ID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
   </SelectParameters>
   <UpdateParameters>
     <mt:WhereParameter Name="FldID" SourceType="DataSourceColumn" Source="ID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="ID" Required="true"/>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quest_ID" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_ID" SourceType="URL" Source="Quiz_ID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID" Required="true"/>
     <mt:SqlParameter Name="Choice_Score" SourceType="Control" Source="Choice_Score" DataType="Integer"/>
   </UpdateParameters>
</mt:MTDataSource><br>
<br>
<mt:EditableGrid PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tbl_Quiz_Question_AnswersDataSource" AllowInsert="False" AllowDelete="False" EmptyRows="0" ID="tbl_Quiz_Question_Answers" runat="server">
<HeaderTemplate><div data-emulate-form="tbl_Quiz_Question_Answers" id="tbl_Quiz_Question_Answers">


   
  <h2>Add/Edit Tbl Quiz Question Answers, Tbl Choice </h2>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="RowErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="id" DataType="Float" ID="id" data-id="tbl_Quiz_Question_Answersid_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quiz_ID" DataType="Float" ID="Quiz_ID" data-id="tbl_Quiz_Question_AnswersQuiz_ID_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="QuestionID" DataType="Float" ID="QuestionID" data-id="tbl_Quiz_Question_AnswersQuestionID_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row">Choice ID</th>
 
      <td><mt:MTLabel Source="ChoiceID" DataType="Float" ID="ChoiceID" runat="server"/></td>
    </tr>
 
    <tr>
      <th scope="row">Choice</th>
 
      <td><mt:MTLabel Source="Choice" ID="Choice" runat="server" ContentType="Html" Mode="PassThrough" /></td>
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# tbl_Quiz_Question_Answers.GetControl("Score").ClientID %>">Score</label></th>
 
      <td><mt:MTTextBox Source="Score" DataType="Float" Caption="Score" maxlength="12" Columns="12" ID="Score" data-id="tbl_Quiz_Question_AnswersScore_{tbl_Quiz_Question_Answers:rowNumber}" runat="server"/></td>
    </tr>
 
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="2">No records</td>
    </tr>
    </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="tbl_Quiz_Question_AnswersButton_Submit" runat="server"/></td>
    </tr>
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tbl_Quiz_Question_AnswersDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} tbl_Quiz_Question_Answers.*, Choice 
FROM tbl_Quiz_Question_Answers LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID AND tbl_Quiz_Question_Answers.QuestionID = tblChoice.QuestionID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tbl_Quiz_Question_Answers LEFT JOIN tblChoice ON
tbl_Quiz_Question_Answers.ChoiceID = tblChoice.ChoiceID AND tbl_Quiz_Question_Answers.QuestionID = tblChoice.QuestionID
   </CountCommand>
   <InsertCommand>
INSERT INTO (id, [Quiz_ID], [QuestionID], [Score]) VALUES ({id}, {Quiz_ID}, {QuestionID}, {Score})
  </InsertCommand>
   <UpdateCommand>
UPDATE tbl_Quiz_Question_Answers SET [Score]={Score}
  </UpdateCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="id" Type="Float" />
     <mt:PrimaryKeyInfo FieldName="QuestionID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID"/>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tbl_Quiz_Question_Answers.QuestionID"/>
   </SelectParameters>
   <UpdateParameters>
     <mt:WhereParameter Name="Fldid" SourceType="DataSourceColumn" Source="id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="id" Required="true"/>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID" Required="true"/>
     <mt:SqlParameter Name="Score" SourceType="Control" Source="Score" DataType="Float" IsOmitEmptyValue="True"/>
   </UpdateParameters>
</mt:MTDataSource><br>
<p><br>
<br>
&nbsp;</p>
<p></p>

</form>
<center><font face="Arial"><small>&#71;&#101;&#110;er&#97;te&#100; <!-- SCC -->w&#105;&#116;h <!-- SCC -->C&#111;d&#101;Charg&#101; <!-- SCC -->&#83;&#116;ud&#105;o.</small></font></center></body>
</html>

<!--End ASPX page-->

