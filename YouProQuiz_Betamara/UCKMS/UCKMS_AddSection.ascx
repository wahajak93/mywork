<!--ASCX page header @1-89D0F816-->
<%@ Control language="C#" CodeFile="UCKMS_AddSectionEvents.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_AddSection_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-C4EA1B2D-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//DEL          var hiddenRows = $("tr:ccsControl(uc_recSkilldepartments, row)").filter(function () {
//DEL              return $(this).css("display") == "none";
//DEL          });
//DEL          hiddenRows.filter(":first").css("display", "");
//DEL          if (hiddenRows.length <= 1) $("*:ccsControl(uc_recSkilldepartments, AddRowBtn)").prop('disabled', true);
//DEL                          result = false;


//End CCS script
</script>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tblSectionDataSource" EmptyRows="1" ID="tblSection" runat="server">
<HeaderTemplate><div data-emulate-form="UCKMS_AddSectiontblSection" id="UCKMS_AddSectiontblSection">







   
  <h2>Add/Edit Section </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col"></th>
 
      <th scope="col">Quiz ID</th>
 
      <th scope="col">Section Max Time(Sec)</th>
 
      <th scope="col">Section Title</th>
 
      <th scope="col">Show Random</th>
 
      <th scope="col">Section Note</th>
 
      <th scope="col">Delete</th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="7"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td></td> 
      <td><mt:MTHidden Source="QuizID" DataType="Float" Caption="Quiz ID" ID="QuizID" data-id="UCKMS_AddSectiontblSectionQuizID_{tblSection:rowNumber}" runat="server"/></td> 
      <td><label for="<%# tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionMaxTime").ClientID %>" style="display: none;">Section Max Time</label><mt:MTTextBox type="number" Source="SectionMaxTime" DataType="Integer" Required="True" ErrorControl="Label2" Caption="Section Max Time" maxlength="10" Columns="10" ID="SectionMaxTime" data-id="UCKMS_AddSectiontblSectionSectionMaxTime_{tblSection:rowNumber}"  runat="server"/>
      <mt:MTLabel id="Label2" runat="server"/>
        </td> 
      <td><label for="<%# tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionTitle").ClientID %>" style="display: none;">Section Title</label><mt:MTTextBox Source="SectionTitle" Required="True" ErrorControl="Label3" Caption="Section Title" maxlength="50" Columns="50" ID="SectionTitle" data-id="UCKMS_AddSectiontblSectionSectionTitle_{tblSection:rowNumber}" runat="server"/>
      <mt:MTLabel id="Label3" runat="server"/>
        </td> 
      <td><label for="<%# tblSection.GetControl<InMotion.Web.Controls.MTListBox>("Show_Random").ClientID %>" style="display: none;">Show Random</label> 
        <mt:MTListBox Rows="1" Source="Show_Random" DataType="Integer" Caption="Show Random" ID="Show_Random" data-id="UCKMS_AddSectiontblSectionShow_Random_{tblSection:rowNumber}" runat="server" ErrorControl="Label4">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
            <asp:ListItem Value="1" Text="Yes"/>
             <asp:ListItem Value="0" Text="No"/>
        </mt:MTListBox>
          <mt:MTLabel id="Label4" runat="server"/>
 </td> 
      <td><label for="<%# tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("Section_Note").ClientID %>" style="display: none;">Section Note</label><mt:MTTextBox Source="Section_Note" ErrorControl="Label5" Caption="Section Note" maxlength="150" Columns="50" ID="Section_Note" data-id="UCKMS_AddSectiontblSectionSection_Note_{tblSection:rowNumber}" runat="server"/>
         <mt:MTLabel id="Label5" runat="server"/>
        </td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# tblSection.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;">Delete</label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="tblSectionCheckBox_Delete_Init" OnLoad="tblSectionCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_AddSectiontblSectionCheckBox_Delete_PanelCheckBox_Delete_{tblSection:rowNumber}" runat="server"/></mt:MTPanel>&nbsp;</td> 
    </tr>
 
    <tr>
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
      <td>
        <p>&nbsp;<mt:MTLink Text="Add Questions to this Section" ID="Link1" data-id="UCKMS_AddSectiontblSectionLink1_{tblSection:rowNumber}" runat="server" HrefSource="~/KMS_SectionQuestions.aspx" PreserveParameters="Get"><Parameters>
          <mt:UrlParameter Name="SectionID" SourceType="DataSourceColumn" Source="SectionID" Format="yyyy-MM-dd"/>
          <mt:UrlParameter Name="QuizID" SourceType="DataSourceColumn" Source="QuizID" Format="yyyy-MM-dd"/>
        </Parameters></mt:MTLink></p>
 
  
 </td> 
      <td>&nbsp;</td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="7">No records</td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="7">
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="UCKMS_AddSectiontblSectionButton_Submit" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tblSectionDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tblSection {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblSection
   </CountCommand>
   <InsertCommand>
INSERT INTO tblSection([QuizID], [SectionMaxTime], [SectionTitle], [Show_Random], [Section_Note]) VALUES ({QuizID}, {SectionMaxTime}, {SectionTitle}, {Show_Random}, {Section_Note})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblSection SET [QuizID]={QuizID}, [SectionMaxTime]={SectionMaxTime}, [SectionTitle]={SectionTitle}, [Show_Random]={Show_Random}, [Section_Note]={Section_Note}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tblSection
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="SectionID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuizID"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuizID" SourceType="URL" Source="Quiz_id" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="SectionMaxTime" SourceType="Control" Source="SectionMaxTime" DataType="Integer"/>
     <mt:SqlParameter Name="SectionTitle" SourceType="Control" Source="SectionTitle" DataType="Text"/>
     <mt:SqlParameter Name="Show_Random" SourceType="Control" Source="Show_Random" DataType="Integer"/>
     <mt:SqlParameter Name="Section_Note" SourceType="Control" Source="Section_Note" DataType="Text"/>
   </InsertParameters>
</mt:MTDataSource>
<p><br>
&nbsp; <br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>




<!--End ASCX page-->

