//Using statements @1-AFBC0BCB
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-844B4D5E
public partial class UCKMS_EditQuizDetails_Page : MTUserControl
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UCKMS_EditQuizDetails Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_EditQuizDetails Event Init

//ScriptIncludesInit @1-A11377B6
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/ui/jquery.ui.core.js|js/jquery/ui/jquery.ui.widget.js|js/jquery/ui/jquery.ui.datepicker.js|js/jquery/datepicker/ccs-date-timepicker.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page UCKMS_EditQuizDetails Init event tail @1-FCB6E20C
    }
//End Page UCKMS_EditQuizDetails Init event tail

//EditableGrid TBL_Quiz Event After Execute Delete @2-21A7D3CA
    protected void TBL_Quiz_AfterExecuteDelete(object sender, DataOperationEventArgs e) {
//End EditableGrid TBL_Quiz Event After Execute Delete
Response.Redirect("~/AdminDashboard.aspx");
//EditableGrid TBL_Quiz Event After Execute Delete. Action Custom Code @66-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End EditableGrid TBL_Quiz Event After Execute Delete. Action Custom Code

//EditableGrid TBL_Quiz After Execute Delete event tail @2-FCB6E20C
    }
//End EditableGrid TBL_Quiz After Execute Delete event tail

//Button Button3 Event On Click @70-FC309BE1
    protected void TBL_QuizButton3_Click(object sender, EventArgs e) {
    	
    	string quiz_id=Request.QueryString["Quiz_id"].ToString();
    	
//End Button Button3 Event On Click		string quiz = Request.QueryString["Quiz_id"];
 Connection conn = (Connection)DataUtility.GetConnectionObject("InMotion:KMS");
       DataCommand del=(DataCommand)conn.CreateCommand();
  del.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
del.CommandText = "Delete FROM TBL_Quiz WHERE Quiz_id="+quiz_id+"";


    DataCommand del2=(DataCommand)conn.CreateCommand();
  del2.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
del2.CommandText = "Delete FROM tbl_Quiz_Question_Answers WHERE Quiz_id="+quiz_id+"";


    DataCommand del3=(DataCommand)conn.CreateCommand();
  del3.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
del3.CommandText = "Delete FROM tbl_Quiz_Question_Choice WHERE Quiz_id="+quiz_id+"";

    DataCommand del4=(DataCommand)conn.CreateCommand();
  del4.MTCommandType = MTCommandType.Table;
  //select1.CommandText = "INSERT INTO tbl_Quiz_Question_Choice(Quiz_ID,Quest_ID,Choice_ID) VALUES ("+quiz_id+","+quest_id+",(SELECT ChoiceID From tblChoice WHERE QuestionID="+quest_id+"))";
del4.CommandText = "Delete FROM tbl_Quiz_Question WHERE Quiz_id="+quiz_id+"";




del.ExecuteNonQuery();
del2.ExecuteNonQuery();
del3.ExecuteNonQuery();

del4.ExecuteNonQuery();

Response.Redirect("~/AdminDashboard.aspx");



//Button Button3 Event On Click. Action Custom Code @71-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Button Button3 Event On Click. Action Custom Code

//Button Button3 Event On Click. Action Retrieve Value for Variable @72-7B7239C4
        
//End Button Button3 Event On Click. Action Retrieve Value for Variable

//Button Button3 Event On Click. Action Custom Code @73-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Button Button3 Event On Click. Action Custom Code

//Button Button3 On Click event tail @70-FCB6E20C
    }
//End Button Button3 On Click event tail

//Button Button1 Event On Click @33-67A57B05
    protected void Button1_Click(object sender, EventArgs e) {
//End Button Button1 Event On Click
string maxid="";
 Connection conn = (Connection)DataUtility.GetConnectionObject("InMotion:KMS");
       DataCommand select=(DataCommand)conn.CreateCommand();
  select.MTCommandType = MTCommandType.Table;
  select.CommandText = "Select COALESCE(MAX(QuestionID),0) as maxid from tblQuestion";
  
  DataRowCollection newDr = select.Execute().Tables[0].Rows;
////for (int i = 0; i < newDr.Count; i++)
////{
if(newDr.Count>=1)
{

foreach(DataRow dr in newDr)
{

int x=Convert.ToInt32(dr["maxid"].ToString());

maxid=(x+1).ToString();
}

}
else
{
	maxid="1";
	
	}

string quiz_id=Request.QueryString["Quiz_id"].ToString();
Response.Redirect("~/KMS_AddNewQuestion.aspx?Quiz_id="+quiz_id+"&QuestionID="+maxid+"");

//Button Button1 Event On Click. Action Custom Code @38-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Button Button1 Event On Click. Action Custom Code

//Button Button1 On Click event tail @33-FCB6E20C
    }
//End Button Button1 On Click event tail

//Button Button2 Event On Click @34-3CB2CA10
    protected void Button2_Click(object sender, EventArgs e) {
//End Button Button2 Event On Click
string quiz_id=Request.QueryString["Quiz_id"].ToString();

Response.Redirect("~/KMS_AddExistingQuestion.aspx?Quiz_id="+quiz_id+"");
//Button Button2 Event On Click. Action Custom Code @65-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Button Button2 Event On Click. Action Custom Code

//Button Button2 On Click event tail @34-FCB6E20C
    }
//End Button Button2 On Click event tail

//Page class tail @1-FCB6E20C
    protected void TBL_Quiz_ControlsValidating(object sender, ValidatingEventArgs e)
    {
        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").Text == "")

        //if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label2").Value!="")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").Errors.Add("Cannot be empty");


            e.HasErrors = true;
        }

        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").Errors.Add("Cannot be empty");
            e.HasErrors = true;

        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Errors.Add("Cannot be empty");

            // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Text, "^[0-9]*$"))
        {
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Errors.Add("Must contain digits only");

            e.HasErrors = true;
        }

        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").SelectedValue == "")
        {

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").SelectedValue == "")
        {

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Errors.Add("<w1 style=\"color:red;\">Cannot be empty</w1>");
            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Errors.Add("<h1>Can only contain digits</h1>");
            e.HasErrors = true;
        }







        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Errors.Add("Cannot be empty");
            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Errors.Add("Can only contain digits");
            e.HasErrors = true;
        }




    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

