<!--ASCX page header @1-66A5715F-->
<%@ Control language="C#" CodeFile="UCKMS_EditQuizDetailsEvents.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_EditQuizDetails_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-117BC76D-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<style>

 


</style>

<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-06BC39F4
</script>
<mt:MTPanel ID="___link_panel_08425126140156162" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/jquery-ui.css">
</mt:MTPanel>
<mt:MTPanel ID="___link_panel_05030484692186711" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/ccs-jquery-ui-calendar.css">
</mt:MTPanel>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-EDADBA3B
    $('body').ccsBind(function() {
        features["UCKMS_EditQuizDetailsTBL_QuizAdded_onInlineDatePicker1"] = $('*:ccsControl(UCKMS_EditQuizDetails, TBL_Quiz, Added_on)');
    });
//End Controls Selecting

//Feature Parameters @1-4B800134
    params["UCKMS_EditQuizDetailsTBL_QuizAdded_onInlineDatePicker1"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
//End Feature Parameters

//Feature Init @1-8063C9C6
    features["UCKMS_EditQuizDetailsTBL_QuizAdded_onInlineDatePicker1"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_EditQuizDetailsTBL_QuizAdded_onInlineDatePicker1"]);
    });
//End Feature Init

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<mt:EditableGrid PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="TBL_QuizDataSource" EmptyRows="0" ID="TBL_Quiz" runat="server" OnControlsValidating="TBL_Quiz_ControlsValidating" OnValidating="TBL_Quiz_ControlsValidating" >
<HeaderTemplate><div data-emulate-form="UCKMS_EditQuizDetailsTBL_Quiz" id="UCKMS_EditQuizDetailsTBL_Quiz">







   
  <h2>Add/Edit TBL Quiz </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="2"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="row">Quiz Id</th>
 
      <td><mt:MTLabel Source="Quiz_id" DataType="Float" ID="Quiz_id" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").ClientID %>">Quiz Title</label></th>
 
      <td><mt:MTTextBox Source="Quiz_Title" ErrorControl="Label1" Caption="Quiz Title" maxlength="100" Columns="50" ID="Quiz_Title" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Title_{TBL_Quiz:rowNumber}" runat="server"/><mt:MTLabel ID="Label1" runat="server" /></td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").ClientID %>">Quiz Descp</label></th>
 
      <td>
<mt:MTTextBox TextMode="Multiline" Source="Quiz_Descp" DataType="Memo" ErrorControl="Label2" Caption="Quiz Descp" Rows="3" Columns="50" ID="Quiz_Descp" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Descp_{TBL_Quiz:rowNumber}" runat="server"/>
<mt:MTLabel ID="Label2" runat="server"/> </td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").ClientID %>">Time Limit Secs</label></th>
 
      <td><mt:MTTextBox Source="Time_limit_secs" Caption="Time Limit Secs" Columns="10" ID="Time_limit_secs" type="number" data-id="UCKMS_EditQuizDetailsTBL_QuizTime_limit_secs_{TBL_Quiz:rowNumber}" runat="server" ErrorControl="Label5" Required="False" />
          <mt:MTLabel ID="Label5" runat="server" />
        </td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").ClientID %>">Quiz Mode</label></th>
 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Mode" DataType="Integer" Required="False" ErrorControl="Label3" Caption="Quiz Mode" DataSourceID="Quiz_ModeDataSource" DataValueField="id" DataTextField="Quiz_Mode" ID="Quiz_Mode" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Mode_{TBL_Quiz:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_ModeDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tbl_Quiz_Mode {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label3" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTTextBox Source="Added_on" DataType="Date" Format="d" Caption="Added On" maxlength="100" Columns="8" ID="Added_on" data-id="UCKMS_EditQuizDetailsTBL_QuizAdded_on_{TBL_Quiz:rowNumber}" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Modified_on" DataType="Date" Format="d" Caption="Modified On" ID="Modified_on" data-id="UCKMS_EditQuizDetailsTBL_QuizModified_on_{TBL_Quiz:rowNumber}" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Added_By" DataType="Integer" Caption="Added By" ID="Added_By" data-id="UCKMS_EditQuizDetailsTBL_QuizAdded_By_{TBL_Quiz:rowNumber}" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Modified_By" DataType="Integer" Caption="Modified By" ID="Modified_By" data-id="UCKMS_EditQuizDetailsTBL_QuizModified_By_{TBL_Quiz:rowNumber}" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").ClientID %>">Quiz Total Points</label></th>
 
      <td><mt:MTTextBox Source="Quiz_Total_Points"   Caption="Quiz Total Points" maxlength="10" Columns="10" ID="Quiz_Total_Points" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Total_Points_{TBL_Quiz:rowNumber}" runat="server" type="number" ErrorControl="MTLabel2"/>
          <mt:MTLabel ID="MTLabel2" runat="server" />
          <br />
        </td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").ClientID %>">Quiz Category</label></th>
 
      <td>
        <mt:MTListBox Rows="1" Source="Quiz_Category" DataType="Integer" ErrorControl="Label4" Caption="Quiz Category" DataSourceID="Quiz_CategoryDataSource" DataValueField="id" DataTextField="Category_Name" ID="Quiz_Category" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Category_{TBL_Quiz:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="Quiz_CategoryDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuizCategory {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 <mt:MTLabel ID="Label4" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"></th>
 
      <td><mt:MTHidden Source="Quiz_Value_Type" DataType="Integer" Caption="Quiz Value Type" ID="Quiz_Value_Type" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_Value_Type_{TBL_Quiz:rowNumber}" runat="server"/></td> 
    </tr>
 
    <tr>
      <th scope="row"><label for="<%# TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").ClientID %>">Quiz User Attempt Limit</label></th>
 
      <td><mt:MTTextBox Source="Quiz_User_Attempt_Limit" DataType="Integer" Caption="Quiz User Attempt Limit" type="number"  Columns="10" ID="Quiz_User_Attempt_Limit" data-id="UCKMS_EditQuizDetailsTBL_QuizQuiz_User_Attempt_Limit_{TBL_Quiz:rowNumber}" runat="server" ErrorControl="MTLabel_for_limit"/><mt:MTLabel ID="MTLabel_for_limit" runat="server"/></td> 
    </tr>
 
    <tr>
      <td colspan="2">&nbsp;</td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="2">No records</td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton Text="Delete this quiz" CssClass="Button" OnClick="TBL_QuizButton3_Click" ID="Button3" data-id="UCKMS_EditQuizDetailsTBL_QuizButton3" runat="server"/>&nbsp; 
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="UCKMS_EditQuizDetailsTBL_QuizButton_Submit" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="TBL_QuizDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" ValidateUniqueCommandType="Table" OnAfterExecuteDelete="TBL_Quiz_AfterExecuteDelete" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM TBL_Quiz {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM TBL_Quiz
   </CountCommand>
   <InsertCommand>
INSERT INTO TBL_Quiz([Quiz_Title], [Quiz_Descp], [Time_limit_secs], [Quiz_Mode], [Added_on], [Modified_on], [Added_By], [Modified_By], [Quiz_Total_Points], [Quiz_Category], [Quiz_Value_Type], [Quiz_User_Attempt_Limit]) VALUES ({Quiz_Title}, {Quiz_Descp}, {Time_limit_secs}, {Quiz_Mode}, {Added_on}, {Modified_on}, {Added_By}, {Modified_By}, {Quiz_Total_Points}, {Quiz_Category}, {Quiz_Value_Type}, {Quiz_User_Attempt_Limit})
  </InsertCommand>
   <UpdateCommand>
UPDATE TBL_Quiz SET [Quiz_Title]={Quiz_Title}, [Quiz_Descp]={Quiz_Descp}, [Time_limit_secs]={Time_limit_secs}, [Quiz_Mode]={Quiz_Mode}, [Added_on]={Added_on}, [Modified_on]={Modified_on}, [Added_By]={Added_By}, [Modified_By]={Modified_By}, [Quiz_Total_Points]={Quiz_Total_Points}, [Quiz_Category]={Quiz_Category}, [Quiz_Value_Type]={Quiz_Value_Type}, [Quiz_User_Attempt_Limit]={Quiz_User_Attempt_Limit}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM TBL_Quiz
  </DeleteCommand>
   <ValidateUniqueCommand>
SELECT COUNT(*)
FROM TBL_Quiz
  </ValidateUniqueCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="Quiz_id" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" Condition="Equal" SourceColumn="Quiz_id"/>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="Added_By"/>
   </SelectParameters>
</mt:MTDataSource>
<p>&nbsp;</p>
<h1>Questions </h1><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tblQuestion_tbl_Quiz_QuesDataSource" ID="tblQuestion_tbl_Quiz_Ques" runat="server">
<HeaderTemplate>

<p>
<table>
  <tr>
    <th scope="col">Quiz ID</th>
 
    <th scope="col">Quiz Quest Category</th>
 
    <th scope="col">Quiz Quest Type</th>
 
    <th scope="col">Question Time</th>
 
    <th scope="col">Question Name</th>
 
    <th scope="col">Question</th>
 
    <th scope="col">Question Inserted By</th>
 
    <th scope="col">Question ID</th>
 
    <th scope="col">&nbsp;</th>
 
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTLabel Source="Quiz_ID" DataType="Float" ID="Quiz_ID" runat="server"/></td> 
    <td><mt:MTLabel Source="Category" ID="Quiz_Quest_Category_ID" runat="server"/></td> 
    <td><mt:MTLabel Source="Quest_Type" ID="Quiz_Quest_Type_ID" runat="server"/></td> 
    <td><mt:MTLabel Source="Quiz_Quest_Time" DataType="Float" ID="Quiz_Quest_Time" runat="server"/></td> 
    <td><mt:MTLabel Source="Question_Name" ID="Question_Name" runat="server"/></td> 
    <td><mt:MTLabel Source="Question" ID="Question" runat="server" ContentType="Html" /></td> 
    <td><mt:MTLabel Source="user_name" ID="QuestionInsertedBy" runat="server"/></td> 
    <td><mt:MTLabel Source="Quiz_Quest_ID" DataType="Float" ID="Quiz_Quest_ID" runat="server"/></td> 
    <td>&nbsp;<mt:MTLink Text="edit" ID="Link1" data-id="UCKMS_EditQuizDetailstblQuestion_tbl_Quiz_QuesLink1_{tblQuestion_tbl_Quiz_Ques:rowNumber}" runat="server" HrefSource="~/KMS_QuestionEdit.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="Quiz_ID" SourceType="DataSourceColumn" Source="Quiz_ID" Format="yyyy-MM-dd"/>
      <mt:UrlParameter Name="QuestionID" SourceType="DataSourceColumn" Source="Quiz_Quest_ID" Format="yyyy-MM-dd"/>
    </Parameters></mt:MTLink></td> 
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="9">No records</td> 
  </tr>
 </mt:MTPanel>
</table>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="tblQuestion_tbl_Quiz_QuesDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} Question_Name, Question, Quiz_Quest_Category_ID, Quiz_Quest_Type_ID, Quiz_Quest_Time, QuestionInsertedBy, Quiz_ID, Quiz_Quest_ID,
Quest_Type, Category, user_name 
FROM ((tbl_Quiz_Question LEFT JOIN (tblQuestion LEFT JOIN TBL_users ON
tblQuestion.QuestionInsertedBy = TBL_users.id) ON
tbl_Quiz_Question.Quiz_Quest_ID = tblQuestion.QuestionID) LEFT JOIN tblQuestionType ON
tbl_Quiz_Question.Quiz_Quest_Type_ID = tblQuestionType.Quest_Type_ID) LEFT JOIN tblQuestionCategory ON
tbl_Quiz_Question.Quiz_Quest_Category_ID = tblQuestionCategory.CategoryID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM ((tbl_Quiz_Question LEFT JOIN (tblQuestion LEFT JOIN TBL_users ON
tblQuestion.QuestionInsertedBy = TBL_users.id) ON
tbl_Quiz_Question.Quiz_Quest_ID = tblQuestion.QuestionID) LEFT JOIN tblQuestionType ON
tbl_Quiz_Question.Quiz_Quest_Type_ID = tblQuestionType.Quest_Type_ID) LEFT JOIN tblQuestionCategory ON
tbl_Quiz_Question.Quiz_Quest_Category_ID = tblQuestionCategory.CategoryID
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuiz_ID" SourceType="URL" Source="Quiz_ID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="tbl_Quiz_Question.Quiz_ID"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
&nbsp; 
<mt:MTButton Text="Add New" CssClass="Button" OnClick="Button1_Click" ID="Button1" data-id="UCKMS_EditQuizDetailsButton1" runat="server"/>&nbsp;&nbsp;&nbsp; 
<mt:MTButton Text="Add Existing " CssClass="Button" OnClick="Button2_Click" ID="Button2" data-id="UCKMS_EditQuizDetailsButton2" runat="server"/>&nbsp;<br>
&nbsp; 
<p><br>
&nbsp;</p>
<p><br>
&nbsp;</p>
<p><br>
&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
</p>




<!--End ASCX page-->

