<!--ASCX page header @1-0CC8592F-->
<%@ Control language="C#" CodeFile="UCKMS_AdminApprovalEvents.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_AdminApproval_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-9C6A5F46-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>' src="ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick Event Start @-961EA782
    actions["UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick"] = function (eventType, parameters) {
        var result = true;
//End UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick Event Start

//Confirmation Message @20-257FDD03
        return confirm('Submit records?');
//End Confirmation Message

//UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick Event End @-A5B9ECB8
        return result;
    };
//End UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick Event End

//Event Binding @1-26A38E92
    $('*:ccsControl(UCKMS_AdminApproval, TBL_users, Button_Submit)').ccsBind(function() {
        this.bind("click", actions["UCKMS_AdminApprovalTBL_usersButton_SubmitOnClick"]);
    });
//End Event Binding

//Plugin Calls @1-F2D74A2D
    $('*:ccsControl(UCKMS_AdminApproval, TBL_users, Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<br>
<br>
<mt:Record ID="TBL_usersSearch" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_AdminApprovalTBL_usersSearch" id="UCKMS_AdminApprovalTBL_usersSearch">







  <h2>Search </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# TBL_usersSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_user_email").ClientID %>">Email</label></td> 
      <td><mt:MTTextBox Source="user_email" Caption="User Email" maxlength="50" Columns="50" ID="s_user_email" data-id="UCKMS_AdminApprovalTBL_usersSearchs_user_email" runat="server"/></td> 
    </tr>
 
    <tr>
<%--      <td><label for="<%# TBL_usersSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_user_group_id").ClientID %>">Group</label></td> --%>
      <td><mt:MTHidden Source="user_group_id" DataType="Integer" Caption="User Group Id" maxlength="10" Columns="10" ID="s_user_group_id" data-id="UCKMS_AdminApprovalTBL_usersSearchs_user_group_id" runat="server"/></td> 
    </tr>
 
    <tr>
<%--      <td><label for="<%# TBL_usersSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_user_status_id").ClientID %>">Status</label></td> --%>
      <td><mt:MTHidden Source="user_status_id" DataType="Integer" Caption="User Status Id" maxlength="10" Columns="10" ID="s_user_status_id" data-id="UCKMS_AdminApprovalTBL_usersSearchs_user_status_id" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="UCKMS_AdminApprovalTBL_usersSearchButton_DoSearch" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record><br>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="TBL_usersDataSource" AllowInsert="False" EmptyRows="0" ID="TBL_users" runat="server">
<HeaderTemplate><div data-emulate-form="UCKMS_AdminApprovalTBL_users" id="UCKMS_AdminApprovalTBL_users">


   
 
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="5"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Id" ID="Sorter_id" data-id="UCKMS_AdminApprovalTBL_usersSorter_id" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="user_email" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="User Email" ID="Sorter_user_email" data-id="UCKMS_AdminApprovalTBL_usersSorter_user_email" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="user_group_id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="User Group" ID="Sorter_user_group_id" data-id="UCKMS_AdminApprovalTBL_usersSorter_user_group_id" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="{page:pathToRoot}Styles/None/Images/Asc.gif" DescOnImage="{page:pathToRoot}Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="user_status_id" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="User Status" ID="Sorter_user_status_id" data-id="UCKMS_AdminApprovalTBL_usersSorter_user_status_id" runat="server"/></th>
 
      <th scope="col">Delete</th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="5"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><mt:MTLabel Source="id" DataType="Integer" ID="id" runat="server"/></td> 
      <td><label for="<%# TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_email").ClientID %>" style="display: none;">User Email</label><mt:MTTextBox Source="user_email" Caption="User Email" maxlength="50" Columns="50" ID="user_email" data-id="UCKMS_AdminApprovalTBL_usersuser_email_{TBL_users:rowNumber}" runat="server"/></td> 
      <td><label for="<%# TBL_users.GetControl<InMotion.Web.Controls.MTListBox>("user_group_id").ClientID %>" style="display: none;">User Group</label> 
        <mt:MTListBox Rows="1" Source="user_group_id" DataType="Integer" Caption="Group" DataSourceID="user_group_idDataSource" DataValueField="UserGroupID" DataTextField="GroupText" ID="user_group_id" data-id="UCKMS_AdminApprovalTBL_usersuser_group_id_{TBL_users:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="user_group_idDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblUserGroup {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
      <td><label for="<%# TBL_users.GetControl<InMotion.Web.Controls.MTListBox>("user_status_id").ClientID %>" style="display: none;">User Status</label> 
        <mt:MTListBox Rows="1" Source="user_status_id" DataType="Integer" Caption="User Status Id" ID="user_status_id" data-id="UCKMS_AdminApprovalTBL_usersuser_status_id_{TBL_users:rowNumber}" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
 </td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# TBL_users.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;">Delete</label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="TBL_usersCheckBox_Delete_Init" OnLoad="TBL_usersCheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_AdminApprovalTBL_usersCheckBox_Delete_PanelCheckBox_Delete_{TBL_users:rowNumber}" runat="server"/></mt:MTPanel>&nbsp;</td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="5">No records</td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="5">
        <mt:MTNavigator FirstOnValue="&lt;img alt=&quot;{First_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/First.gif&quot;/&gt; " FirstOffValue="&lt;img alt=&quot;{First_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/FirstOff.gif&quot;/&gt;" PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Prev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/PrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Next.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/NextOff.gif&quot;/&gt;" LastOnValue="&lt;img alt=&quot;{Last_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/Last.gif&quot;/&gt; " LastOffValue="&lt;img alt=&quot;{Last_URL}&quot; src=&quot;{page:pathToRoot}Styles/None/Images/LastOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageOnly" TotalPagesText="of" PostPageNumberCaption="of" ShowTotalPages="true" ID="Navigator" runat="server"/>
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="UCKMS_AdminApprovalTBL_usersButton_Submit" runat="server"/>
        <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" CssClass="Button" ID="Cancel" data-id="UCKMS_AdminApprovalTBL_usersCancel" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="TBL_usersDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM TBL_users {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM TBL_users
   </CountCommand>
   <UpdateCommand>
UPDATE TBL_users SET user_email={user_email}, user_group_id={user_group_id}, user_status_id={user_status_id}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM TBL_users
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="id" Type="Integer" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_user_email" SourceType="URL" Source="s_user_email" DataType="Text" Operation="And" Condition="Contains" SourceColumn="user_email"/>
     <mt:WhereParameter Name="Urls_user_group_id" SourceType="URL" Source="s_user_group_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="user_group_id"/>
     <mt:WhereParameter Name="Urls_user_status_id" SourceType="URL" Source="s_user_status_id" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="user_status_id"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">



<!--End ASCX page-->

