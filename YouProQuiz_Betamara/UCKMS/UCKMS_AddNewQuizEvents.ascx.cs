//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-A2CAB1C2
public partial class UCKMS_AddNewQuiz_Page : MTUserControl
{
//End Page class

//Attributes constants @1-52B30067
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UCKMS_AddNewQuiz Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_AddNewQuiz Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UCKMS_AddNewQuiz Init event tail @1-FCB6E20C
    }
//End Page UCKMS_AddNewQuiz Init event tail

//DEL        

//Record TBL_Quiz Event Before Insert @2-2F8ACA51
    protected void TBL_Quiz_BeforeInsert(object sender, EventArgs e) {
//End Record TBL_Quiz Event Before Insert

//Record TBL_Quiz Event Before Execute Insert. Action Retrieve Value for Control @33-303E9136
        TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_By").Value = Session["UserID"].ToString();
  TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_on").Value =DateTime.Now.ToString();

//Record TBL_Quiz Event Before Insert. Action Custom Code @34-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record TBL_Quiz Event Before Insert. Action Custom Code

//Record TBL_Quiz Before Insert event tail @2-FCB6E20C
    }
//End Record TBL_Quiz Before Insert event tail

//Hidden Added_By Event Init @13-13DB822B
    protected void TBL_QuizAdded_By_Init(object sender, EventArgs e) {
//End Hidden Added_By Event Init

//Set Attributes @13-BCD2EB53
        ((InMotion.Web.Controls.MTHidden)sender).Attributes.Add("aaa", Convert.ToString(System.Web.HttpContext.Current.Session["UserID"]));
//End Set Attributes

//Hidden Added_By Init event tail @13-FCB6E20C
    }
//End Hidden Added_By Init event tail

//Hidden Added_By Event On Validate @13-91B6BEB8
    protected void TBL_QuizAdded_By_Validating(object sender, ValidatingEventArgs e) {
//End Hidden Added_By Event On Validate
    TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_By").Value = Session["UserID"].ToString();
  TBL_Quiz.GetControl<InMotion.Web.Controls.MTHidden>("Added_on").Value =DateTime.Now.ToString();

//Hidden Added_By Event On Validate. Action Custom Code @93-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Hidden Added_By Event On Validate. Action Custom Code

//Hidden Added_By On Validate event tail @13-FCB6E20C
    }
//End Hidden Added_By On Validate event tail

//Record TBL_Quiz Event Before Execute Insert @2-E41D91C1

//End Record TBL_Quiz Before Execute Insert event tail

//Record TBL_Quiz Event After Insert @2-7C288F94
 
//End Record TBL_Quiz After Insert event tail

//Hidden Added_By Event Init @13-13DB822B
  
//End Hidden Added_By Init event tail

//Hidden Quiz_Value_Type Event Load @17-1F25DD5D
    protected void TBL_QuizQuiz_Value_Type_Load(object sender, EventArgs e) {
//End Hidden Quiz_Value_Type Event Load

//Set Default Value @17-8869B3A0
        ((InMotion.Web.Controls.MTHidden)sender).DefaultValue = 1;
//End Set Default Value

//Hidden Quiz_Value_Type Load event tail @17-FCB6E20C
    }
//End Hidden Quiz_Value_Type Load event tail

//Grid TBL_Quiz1 Event Initialize Select Parameters @35-C168C7F4
    protected void TBL_Quiz1_InitializeSelectParameters(object sender, EventArgs e) {
//End Grid TBL_Quiz1 Event Initialize Select Parameters

//Initialize expression parameters and default values  @35-9B059B48
        ((MTDataSourceView)sender).SelectParameters["SesUserID"].DefaultValue = 0;
//End Initialize expression parameters and default values 

//Grid TBL_Quiz1 Initialize Select Parameters event tail @35-FCB6E20C
    }
//End Grid TBL_Quiz1 Initialize Select Parameters event tail

//Page class tail @1-FCB6E20C
    protected void TBL_Quiz_Validating(object sender, ValidatingEventArgs e)
    {
    

      
        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").Text=="")

            //if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label2").Value!="")
        
        
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Descp").Errors.Add("Cannot be empty");
            
     
            e.HasErrors = true;
        }

        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").Text =="")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Title").Errors.Add("Cannot be empty");
            e.HasErrors = true;
        
        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Errors.Add("Cannot be empty");
            
           // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Text, "^[0-9]*$"))
        {
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Time_limit_secs").Errors.Add("Must contain digits only");
            
            e.HasErrors = true;
        }

        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").SelectedValue=="")
        {

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Mode").Errors.Add("Cannot be empty");
          
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").SelectedValue == "")
        {

            TBL_Quiz.GetControl<InMotion.Web.Controls.MTListBox>("Quiz_Category").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }


        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Errors.Add("Cannot be empty");
            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_User_Attempt_Limit").Errors.Add("Can only contain digits");
            e.HasErrors = true;
        }







        if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Errors.Add("Cannot be empty");
            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            TBL_Quiz.GetControl<InMotion.Web.Controls.MTTextBox>("Quiz_Total_Points").Errors.Add("Can only contain digits");
            e.HasErrors = true;
        }





    }
    protected void TBL_QuizSearch_ControlsValidating(object sender, ValidatingEventArgs e)
    {




        if (!System.Text.RegularExpressions.Regex.IsMatch(TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("s_Quiz_id").Errors.Add("Can only contain digits");
            e.HasErrors = true;
        }



    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

