//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-89621C26
public partial class UCKMS_Verifier_Page : MTUserControl
{
//End Page class

//Page UCKMS_Verifier Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_Verifier Event Init


        this.BeforeShow += new EventHandler<EventArgs>(Page_BeforeShow);

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page Events @1-AE91ECB1
      //  this.BeforeShow += new EventHandler<EventArgs>(Page_BeforeShow);
//End Page Events

//DEL          //this.BeforeShow += new EventHandler<EventArgs>(Page_BeforeShow);


//Page UCKMS_Verifier Init event tail @1-FCB6E20C
    }
//End Page UCKMS_Verifier Init event tail

//Page UCKMS_Verifier Event Before Show @1-2B5592D4
    protected void Page_BeforeShow(object sender, EventArgs e) {
//End Page UCKMS_Verifier Event Before Show



  string email = Request.QueryString["email"];
        string code = Request.QueryString["code"];
        
        string DEmail = DataUtility.DLookup<MTText>("TutorEmail", "tbl_Tutor", "TutorEmail='"+email+"'", "InMotion:LearnersClub");
        string DCode = DataUtility.DLookup<MTText>("TutorVerificationCode", "tbl_Tutor", "TutorEmail='"+email+"'", "InMotion:LearnersClub");
        int DStatusID = DataUtility.DLookup<MTInteger>("TutorStatusID", "tbl_Tutor", "TutorEmail='"+email+"'", "InMotion:LearnersClub");
        int statusID = 2;
        
        if(email.Equals(DEmail) && code.Equals(DCode) && DStatusID !=2)
        {

                        Connection conn = (Connection)DataUtility.GetConnectionObject("InMotion:LearnersClub");
                                DataCommand update=(DataCommand)conn.CreateCommand();
                                update.MTCommandType = MTCommandType.Table;
                                update.CommandText = "UPDATE tbl_Tutor SET TutorStatusID='"+statusID+"' WHERE TutorEmail='"+email+"'";
                                update.Parameters.Add(new InMotion.Data.SqlParameter("statusID", DataType.Integer));
                                update.Parameters.Add(new InMotion.Data.SqlParameter("email", DataType.Text));
                                update.ExecuteNonQuery();
                                Session["activationText"] = "Activation Successful";
                                Response.Redirect("~/Default.aspx");
        }
        else if(email.Equals(DEmail) && code.Equals(DCode) && DStatusID == 2)
        {
            	Response.Redirect("~/Default.aspx");
            	Session["activationText"] = "Already Verified";
        }
















//Page UCKMS_Verifier Event Before Show. Action Custom Code @2-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Page UCKMS_Verifier Event Before Show. Action Custom Code

//Page UCKMS_Verifier Before Show event tail @1-FCB6E20C
    }
//End Page UCKMS_Verifier Before Show event tail

 

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

