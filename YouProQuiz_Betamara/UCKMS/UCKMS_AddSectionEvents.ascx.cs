//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-AED5FCA0
public partial class UCKMS_AddSection_Page : MTUserControl
{
//End Page class

//Attributes constants @1-2F9F6C23
   // public const string Attribute_rowNumber = "rowNumber";
    
      public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants
   public int RowNumber = 0;
    private string DepartsEmtyRowId;
    
//End Attributes constants

//Page UCKMS_AddSection Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_AddSection Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UCKMS_AddSection Init event tail @1-FCB6E20C
    }
    
    
    
     protected void Page_Load(object sender, EventArgs e) {
//End Page KMS_SectionQuestions Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page KMS_SectionQuestions Load event tail @1-FCB6E20C
    }
    
    
//End Page UCKMS_AddSection Init event tail

//CheckBox CheckBox_Delete Event Init @12-FC417B88
    protected void tblSectionCheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @12-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @12-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @12-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @12-017E8E39
    protected void tblSectionCheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @12-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @12-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//DEL          ViewState["DepsRowIDs"] = DepartsEmtyRowId.TrimEnd(new Char[] { ',' });

//DEL          //ViewState["DepsRowIDs"] = DepartsEmtyRowId.TrimEnd(new Char[] { ',' });




//DEL          foreach (RepeaterItem item in ((EditableGrid)sender).Items)
//DEL          {
//DEL              if (!((EditableGrid)sender).IsRowEmpty(item))
//DEL              {
//DEL                  ((HtmlControl)(item.FindControl("uc_recSkilldepartmentsrow"))).Attributes.Remove("style");
//DEL                  //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
//DEL                  //uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
//DEL                  
//DEL              }
//DEL              
//DEL          }
//DEL          
//DEL          /*if(uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTListBox>("manager_id").SelectedValue != "Selected Value")
//DEL          {
//DEL          	uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorID").Value = Session["UserID"];
//DEL                  uc_recSkilldepartments.GetControl<InMotion.Web.Controls.MTHidden>("TutorProfileSkillsAddedOn").Value = DateTime.Now;
//DEL          } */

//DEL          /*Connection conn = AppConfig.GetConnection("IntranetDB");
//DEL          DataCommand command = (DataCommand)conn.CreateCommand();
//DEL          command.CommandText = "UPDATE employees SET department_id = NULL WHERE department_id = {dep_id}";
//DEL          command.Parameters.Clear();
//DEL          command.Parameters.Add(new InMotion.Data.SqlParameter("dep_id", DataType.Integer));
//DEL          //Get the where parameters
//DEL          command.Parameters["dep_id"].Value = e.Command.WhereParameters["PKdepartment_id"].Value;
//DEL          //Delete project employees links
//DEL          command.ExecuteNonQuery();*/

//Page class tail @1-FCB6E20C
    //protected void tblSection_Validating(object sender, ValidatingEventArgs e)
    //{
    //    if (tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionMaxTime").Text == "")

    //    //if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label2").Value!="")
    //    {
    //        //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
    //        tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionMaxTime").Errors.Add("Cannot be empty");


    //        e.HasErrors = true;
    //    }


    //    if (tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionTitle").Text == "")

    //    //if (TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label2").Value!="")
    //    {
    //        //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
    //        tblSection.GetControl<InMotion.Web.Controls.MTTextBox>("SectionTitle").Errors.Add("Cannot be empty");


    //        e.HasErrors = true;
    //    }

    //    if (tblSection.GetControl<InMotion.Web.Controls.MTListBox>("Show_Random").SelectedValue == "")
    //    {

    //        tblSection.GetControl<InMotion.Web.Controls.MTListBox>("Show_Random").Errors.Add("Cannot be empty");

    //        //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

    //        e.HasErrors = true;
    //    }
        

    //}
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

