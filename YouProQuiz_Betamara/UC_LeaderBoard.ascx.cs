﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;


public partial class UC_LeaderBoard : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string quiz_id = Request.QueryString["Quiz_id"];


        string reder = "progress-bar progress-bar-danger";
        string greener = "progress-bar progress-bar-success";
        string bluer = "progress-bar progress-bar-info";
        string yellower = "progress-bar progress-bar-warning";
        string class1 = "";
        DataTable dt1 = clsdb.readdataKMS("SELECT top 10 TBL_users.user_name , dbo.tblUserQuizResultData.* FROM dbo.tblUserQuizResultData INNER JOIN "+
                         "TBL_users ON tblUserQuizResultData.UserID = dbo.TBL_users.id WHERE tblUserQuizResultData.QuizID=" + quiz_id + "" +
                          " order by ResultInPercent desc;");
        if (dt1.Rows.Count > 0)
        {
            for (int a = 0; a < dt1.Rows.Count; a++)
            {
                //string classbar="progress-bar progress-bar-danger progress-bar-striped";
                if (double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) < 50)
                {
                    class1 = reder;
                }
                else if (double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) >= 50 && double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) < 65)
                {
                    class1 = yellower;

                }
                else if (double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) >= 65 && double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) < 85)
                {

                    class1 = bluer;

                }
                else if (double.Parse(dt1.Rows[a]["ResultInPercent"].ToString()) >= 85)
                {

                    class1 = greener;

                }



                Panel1.Controls.Add(new LiteralControl("<p>" + dt1.Rows[a]["user_name"] + "</p><div class='progress'>" +
    "<div class='" + class1 + "' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:" + dt1.Rows[a]["ResultInPercent"] + "%'>" +
    "" + dt1.Rows[a]["ResultInPercent"] + "% " +
   "</div>" +
  "</div><p>Attempt# " + dt1.Rows[a]["AttemptID"] + ",Attempted On " + dt1.Rows[a]["DateAttempted"] + "</p><br><hr>"));


            }

        }
    }
}