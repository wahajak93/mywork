//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-44C1F42A
public partial class UC_User_Record_Page : MTUserControl
{
//End Page class

//Attributes constants @1-3EE564F5
    public const string Attribute_rowNumber = "rowNumber";
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page UC_User_Record Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC_User_Record Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UC_User_Record Init event tail @1-FCB6E20C
    }
//End Page UC_User_Record Init event tail

//Page class tail @1-FCB6E20C
    protected void View_for_User_Quiz_Record1_Validating(object sender, ValidatingEventArgs e)
    {

       
        if (!System.Text.RegularExpressions.Regex.IsMatch(View_for_User_Quiz_Record1.GetControl<InMotion.Web.Controls.MTTextBox>("s_TBL_Quiz_Quiz_id").Text, "^[0-9]*$"))
        {
            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label7").Value = "Can only contain digits";
            View_for_User_Quiz_Record1.GetControl<InMotion.Web.Controls.MTTextBox>("s_TBL_Quiz_Quiz_id").Errors.Add("The value in this field is not valid");
            e.HasErrors = true;
        }
    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

