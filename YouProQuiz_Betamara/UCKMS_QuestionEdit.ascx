<!--ASCX page header @1-327F2176-->
<%@ Control language="C#" CodeFile="UCKMS_QuestionEditEvents.ascx.cs" Inherits="YouProQuiz_Beta.UCKMS_QuestionEdit_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-92C62052-->

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-D00081A2
</script>
<mt:MTPanel ID="___link_panel_03278965672209388" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/jquery-ui.css">
</mt:MTPanel>
<mt:MTPanel ID="___link_panel_06875828210002595" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/None/ccs-jquery-ui-calendar.css">
</mt:MTPanel>
<script type="text/javascript">
//End Include User Scripts

//Common Script Start @1-8BFA436B
jQuery(function ($) {
    var features = { };
    var actions = { };
    var params = { };
//End Common Script Start

//Controls Selecting @1-47673A4D
    $('body').ccsBind(function() {
        features["UCKMS_QuestionEdittblQuestionQuestionInsertedOnJDateTimePicker1"] = $('*:ccsControl(UCKMS_QuestionEdit, tblQuestion, QuestionInsertedOn)');
        features["UCKMS_QuestionEdittblQuestionQuestionUpdatedOnInlineDatePicker2"] = $('*:ccsControl(UCKMS_QuestionEdit, tblQuestion, QuestionUpdatedOn)');
    });
//End Controls Selecting

//Feature Parameters @1-A3A3A481
    params["UCKMS_QuestionEdittblQuestionQuestionInsertedOnJDateTimePicker1"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
    params["UCKMS_QuestionEdittblQuestionQuestionUpdatedOnInlineDatePicker2"] = { 
        dateFormat: "ShortDate",
        isWeekend: true
    };
//End Feature Parameters

//Feature Init @1-C78A25C8
    features["UCKMS_QuestionEdittblQuestionQuestionInsertedOnJDateTimePicker1"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_QuestionEdittblQuestionQuestionInsertedOnJDateTimePicker1"]);
    });
    features["UCKMS_QuestionEdittblQuestionQuestionUpdatedOnInlineDatePicker2"].ccsBind(function() {
        this.ccsDateTimePicker(params["UCKMS_QuestionEdittblQuestionQuestionUpdatedOnInlineDatePicker2"]);
    });
//End Feature Init

//Plugin Calls @1-80045578
    $('*:ccsControl(UCKMS_QuestionEdit, tblQuestion, Button_Delete)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
    $('*:ccsControl(UCKMS_QuestionEdit, tblChoice2, Cancel)').ccsBind(function() {
        this.bind("click", function(){ $("body").data("disableValidation", true); });
    });
//End Plugin Calls

//Common Script End @1-562554DE
});
//End Common Script End

//End CCS script
</script>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
&nbsp; 
<p>//from her it will&nbsp;need custom code for entering values of question and choices and answers</p>
//&nbsp;<br>
<mt:Record PreserveParameters="Get" DataSourceID="tblQuestionDataSource" AllowInsert="False" AllowDelete="False" AllowUpdate="False" OnBeforeInsert="tblQuestion_BeforeInsert" OnAfterInsert="tblQuestion_AfterInsert" ID="tblQuestion" runat="server"><ItemTemplate><div data-emulate-form="UCKMS_QuestionEdittblQuestion" id="UCKMS_QuestionEdittblQuestion">







  <h2>Add/Edit Tbl Question </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="ErrorBlock">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").ClientID %>">Question Name</label></td> 
      <td><mt:MTTextBox Source="Question_Name" Caption="Question Name" maxlength="50" Columns="50" ID="Question_Name" data-id="UCKMS_QuestionEdittblQuestionQuestion_Name" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question").ClientID %>">Question</label></td> 
      <td><mt:MTTextBox Source="Question" Caption="Question" maxlength="250" Columns="50" ID="Question" data-id="UCKMS_QuestionEdittblQuestionQuestion" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("QuestionInsertedOn").ClientID %>">Question Inserted On</label></td> 
      <td><mt:MTTextBox Source="QuestionInsertedOn" DataType="Date" Format="d" Caption="Question Inserted On" maxlength="100" Columns="8" ID="QuestionInsertedOn" data-id="UCKMS_QuestionEdittblQuestionQuestionInsertedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionInsertedBy").ClientID %>">Question Inserted By</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="QuestionInsertedBy" DataType="Float" Caption="Question Inserted By" DataSourceID="QuestionInsertedByDataSource" DataValueField="UserID" DataTextField="UserEmail" ID="QuestionInsertedBy" data-id="UCKMS_QuestionEdittblQuestionQuestionInsertedBy" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="QuestionInsertedByDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblKmsUser {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("QuestionUpdatedOn").ClientID %>">Question Updated On</label></td> 
      <td><mt:MTTextBox Source="QuestionUpdatedOn" DataType="Date" Format="d" Caption="Question Updated On" maxlength="100" Columns="8" ID="QuestionUpdatedOn" data-id="UCKMS_QuestionEdittblQuestionQuestionUpdatedOn" runat="server"/></td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionUpdatedBy").ClientID %>">Question Updated By</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="QuestionUpdatedBy" DataType="Float" Caption="Question Updated By" DataSourceID="QuestionUpdatedByDataSource" DataValueField="UserID" DataTextField="UserEmail" ID="QuestionUpdatedBy" data-id="UCKMS_QuestionEdittblQuestionQuestionUpdatedBy" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="QuestionUpdatedByDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblKmsUser {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td><label for="<%# tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionStatusID").ClientID %>">Question Status ID</label></td> 
      <td>
        <mt:MTListBox Rows="1" Source="QuestionStatusID" DataType="Float" Caption="Question Status ID" DataSourceID="QuestionStatusIDDataSource" DataValueField="StatusID" DataTextField="Status" ID="QuestionStatusID" data-id="UCKMS_QuestionEdittblQuestionQuestionStatusID" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="QuestionStatusIDDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblRefStatus {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td>Question Type For Current Quiz </td> 
      <td>
        <mt:MTListBox Rows="1" DataSourceID="ListBox1DataSource" DataValueField="Quest_Type_ID" DataTextField="Quest_Type" ID="ListBox1" data-id="UCKMS_QuestionEdittblQuestionListBox1" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionType {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td>Question Category For Current Quiz</td> 
      <td>
        <mt:MTListBox Rows="1" DataSourceID="ListBox2DataSource" DataValueField="CategoryID" DataTextField="Category" ID="ListBox2" data-id="UCKMS_QuestionEdittblQuestionListBox2" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox2DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblQuestionCategory {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
    </tr>
 
    <tr>
      <td>Question Time For Current Quiz</td> 
      <td><mt:MTTextBox ID="TextBox1" data-id="UCKMS_QuestionEdittblQuestionTextBox1" runat="server"/></td> 
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Insert" Text="Add" CssClass="Button" ID="Button_Insert" data-id="UCKMS_QuestionEdittblQuestionButton_Insert" runat="server"/>
        <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="UCKMS_QuestionEdittblQuestionButton_Update" runat="server"/>
        <mt:MTButton CommandName="Delete" EnableValidation="False" Text="Delete" CssClass="Button" ID="Button_Delete" data-id="UCKMS_QuestionEdittblQuestionButton_Delete" runat="server"/></td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="tblQuestionDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
   <SelectCommand>
SELECT Question_Name, tbl_Quiz_Question.*, Question, QuestionInsertedOn, QuestionInsertedBy, QuestionUpdatedOn, QuestionUpdatedBy 
FROM tbl_Quiz_Question LEFT JOIN tblQuestion ON
tbl_Quiz_Question.Quiz_Quest_ID = tblQuestion.QuestionID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID" Required="true"/>
     <mt:WhereParameter Name="UrlQuiz_id" SourceType="URL" Source="Quiz_id" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="Quiz_ID" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
&nbsp; <br>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tblChoice2DataSource" EmptyRows="1" ID="tblChoice2" runat="server">
<HeaderTemplate><div data-emulate-form="UCKMS_QuestionEdittblChoice2" id="UCKMS_QuestionEdittblChoice2">


   
  <h2>Add/Edit Tbl Choice </h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="4"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col">
      <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="ChoiceID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice ID" ID="Sorter_ChoiceID" data-id="UCKMS_QuestionEdittblChoice2Sorter_ChoiceID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="tblChoice.QuestionID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Question ID" ID="Sorter_QuestionID" data-id="UCKMS_QuestionEdittblChoice2Sorter_QuestionID" runat="server"/></th>
 
      <th scope="col">
      <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Choice" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Choice" ID="Sorter_Choice" data-id="UCKMS_QuestionEdittblChoice2Sorter_Choice" runat="server"/></th>
 
      <th scope="col">Delete</th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="4"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td><mt:MTLabel Source="ChoiceID" DataType="Float" ID="ChoiceID" runat="server"/></td> 
      <td><mt:MTLabel Source="QuestionID" DataType="Float" ID="QuestionID" runat="server"/></td> 
      <td><label for="<%# tblChoice2.GetControl<InMotion.Web.Controls.MTTextBox>("Choice").ClientID %>" style="display: none;">Choice</label><mt:MTTextBox Source="Choice" Caption="Choice" maxlength="250" Columns="50" ID="Choice" data-id="UCKMS_QuestionEdittblChoice2Choice_{tblChoice2:rowNumber}" runat="server"/></td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# tblChoice2.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;">Delete</label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="tblChoice2CheckBox_Delete_Init" OnLoad="tblChoice2CheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_QuestionEdittblChoice2CheckBox_Delete_PanelCheckBox_Delete_{tblChoice2:rowNumber}" runat="server"/></mt:MTPanel></td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="4">No records</td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="4">
        <mt:MTNavigator FirstOnValue="First " FirstOffValue="First " PreviousOnValue="Prev " PreviousOffValue="Prev " NextOnValue="Next " NextOffValue="Next " LastOnValue="Last " LastOffValue="Last " PageOffPreValue="" PageOffPostValue="" DisplayStyle="Links" PageLinksNumber="10" PageNumbersStyle="CurrentPageCentered" TotalPagesText="of" PageSizeItems="1;5;10;25;50" ShowTotalPages="true" ID="Navigator" runat="server"/>
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="UCKMS_QuestionEdittblChoice2Button_Submit" runat="server"/>
        <mt:MTButton CommandName="Cancel" EnableValidation="False" Text="Cancel" CssClass="Button" ID="Cancel" data-id="UCKMS_QuestionEdittblChoice2Cancel" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="tblChoice2DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblChoice
   </CountCommand>
   <InsertCommand>
INSERT INTO tblChoice([Choice], [QuestionID]) VALUES ({Choice}, {QuestionID})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblChoice SET [Choice]={Choice}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tblChoice
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="QuestionID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="Choice" SourceType="Control" Source="Choice" DataType="Text"/>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Float" IsOmitEmptyValue="True"/>
   </InsertParameters>
</mt:MTDataSource><br>
<br>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
<p>&nbsp;</p>
&nbsp; 
<p>
<mt:EditableGrid DeleteControl="CheckBox_Delete" PreserveParameters="Get" RecordsPerPage="20" DataSourceID="NewEditableGrid1DataSource" EmptyRows="1" ID="NewEditableGrid1" runat="server">
<HeaderTemplate><div data-emulate-form="UCKMS_QuestionEditNewEditableGrid1" id="UCKMS_QuestionEditNewEditableGrid1">


   
  <h2>Answers</h2>
 
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr>
      <td colspan="3"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <th scope="col">Choice</th>
 
      <th scope="col"></th>
 
      <th scope="col">Delete</th>
 
    </tr>
 
    
</HeaderTemplate>
<ItemTemplate>
    <mt:MTPanel id="RowError" visible="False" runat="server">
    <tr>
      <td colspan="3"><mt:MTLabel id="RowErrorLabel" runat="server"/></td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td>Choice 
        <mt:MTListBox Rows="1" Source="ChoiceID" DataType="Float" DataSourceID="ListBox1DataSource" DataValueField="ChoiceID" DataTextField="Choice" ID="ListBox1" data-id="UCKMS_QuestionEditNewEditableGrid1ListBox1" runat="server">
          <asp:ListItem Value="" Selected="True" Text="Select Value"/>
        </mt:MTListBox>
        <mt:MTDataSource ID="ListBox1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" runat="server">
           <SelectCommand>
SELECT * 
FROM tblChoice {SQL_Where} {SQL_OrderBy}
           </SelectCommand>
        </mt:MTDataSource>
 </td> 
      <td></td> 
      <td>
        <mt:MTPanel GenerateDiv="false" ID="CheckBox_Delete_Panel" runat="server"><label for="<%# NewEditableGrid1.GetControl<InMotion.Web.Controls.MTCheckBox>("CheckBox_Delete").ClientID %>" style="display: none;">Delete</label> 
        <mt:MTCheckBox SourceType="CodeExpression" DataType="Boolean" OnInit="NewEditableGrid1CheckBox_Delete_Init" OnLoad="NewEditableGrid1CheckBox_Delete_Load" ID="CheckBox_Delete" data-id="UCKMS_QuestionEditNewEditableGrid1CheckBox_Delete_PanelCheckBox_Delete" runat="server"/></mt:MTPanel></td> 
    </tr>
 
</ItemTemplate>
<FooterTemplate>
    <mt:MTPanel id="NoRecords" visible="False" runat="server">
    <tr>
      <td colspan="3">No records</td> 
    </tr>
 </mt:MTPanel>
    <tr>
      <td style="TEXT-ALIGN: right" colspan="3">
        <mt:MTButton CommandName="Submit" Text="Submit" CssClass="Button" ID="Button_Submit" data-id="UCKMS_QuestionEditNewEditableGrid1Button_Submit" runat="server"/></td> 
    </tr>
 
  </table>
</div>
</FooterTemplate>
</mt:EditableGrid>
<mt:MTDataSource ID="NewEditableGrid1DataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" InsertCommandType="Table" UpdateCommandType="Table" DeleteCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} * 
FROM tblAnswer {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM tblAnswer
   </CountCommand>
   <InsertCommand>
INSERT INTO tblAnswer([QuestionID], [ChoiceID]) VALUES ({QuestionID}, {ChoiceID})
  </InsertCommand>
   <UpdateCommand>
UPDATE tblAnswer SET [ChoiceID]={ChoiceID}
  </UpdateCommand>
   <DeleteCommand>
DELETE FROM tblAnswer
  </DeleteCommand>
   <PrimaryKeys>
     <mt:PrimaryKeyInfo FieldName="AnswerID" Type="Float" />
   </PrimaryKeys>
   <SelectParameters>
     <mt:WhereParameter Name="UrlQuestionID" SourceType="URL" Source="QuestionID" DataType="Float" Operation="And" UseIsNullIfEmpty="True" Condition="Equal" SourceColumn="QuestionID"/>
   </SelectParameters>
   <InsertParameters>
     <mt:SqlParameter Name="QuestionID" SourceType="URL" Source="QuestionID" DataType="Float" IsOmitEmptyValue="True"/>
     <mt:SqlParameter Name="ChoiceID" SourceType="Control" Source="ListBox1" DataType="Float" IsOmitEmptyValue="True"/>
   </InsertParameters>
</mt:MTDataSource><br>
<br>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<p>&nbsp;&nbsp; </p>




<!--End ASCX page-->

