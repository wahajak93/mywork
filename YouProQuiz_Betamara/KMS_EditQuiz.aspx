﻿


<%@ Page language="C#" CodeFile="KMS_EditQuizEvents.aspx.cs" Inherits="YouProQuiz_Beta.KMS_EditQuiz_Page"   MasterPageFile="~/Dashboard.master"  %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UCKMS_EditQuizDetails" Src="~/UCKMS/UCKMS_EditQuizDetails.ascx" %>
<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UCKMS_AddSection" Src="~/UCKMS/UCKMS_AddSection.ascx" %>

<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/UC_LeaderBoard.ascx" TagPrefix="YouProQuiz_Beta" TagName="UC_LeaderBoard" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>AdminDashboard</title>


<script language="JavaScript" type="text/javascript">
    //Begin CCS script
    //Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
    //End Include Common JSFunctions

    //Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
    //End Include User Scripts

    //End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>
<%--<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">--%>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<p><YouProQuiz_Beta:UCKMS_EditQuizDetails ID="UCKMS_EditQuizDetails" runat="server"/> 
<p>&nbsp;</p>
<p><YouProQuiz_Beta:UCKMS_AddSection ID="UCKMS_AddSection" runat="server"/>

   <h3>LeaderBoard</h3>
    <YouProQuiz_Beta:UC_LeaderBoard runat="server" ID="UC_LeaderBoard" />

<center><font face="Arial"><small>&#71;&#101;&#110;&#101;r&#97;te&#100; <!-- SCC -->&#119;i&#116;&#104; <!-- SCC -->&#67;&#111;&#100;&#101;&#67;ha&#114;&#103;e <!-- SCC -->S&#116;&#117;dio.</small></font></center></body>




    </asp:Content>
