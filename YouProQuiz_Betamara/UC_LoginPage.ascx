<!--ASCX page header @1-1CB05776-->
<%@ Control language="C#" CodeFile="UC_LoginPageEvents.ascx.cs" Inherits="YouProQuiz_Beta.UC_LoginPage_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-53EB71EE-->
<link href="login_css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="login_css/bootstrap.min.css" rel="stylesheet" />
<link href="login_css/typica-login.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<mt:Record ReturnPage="~/Dashboard.aspx" ID="Login" runat="server">
    <ItemTemplate><div data-emulate-form="UC_LoginPageLogin" id="UC_LoginPageLogin" class="form login-form">







  
          
  

        <div class="container">

        <div id="login-wraper">

            <div class="form login-form">
                  <legend>Sign in</legend>
  <div class="body">
      <label for="<%# Login.GetControl<InMotion.Web.Controls.MTTextBox>("login1").ClientID %>">Login</label> 
     <mt:MTTextBox Source="user_email" Required="True" maxlength="100"  ID="login1" data-id="UC_LoginPageLoginlogin1" runat="server" />

    <label for="<%# Login.GetControl<InMotion.Web.Controls.MTTextBox>("password").ClientID %>">Password</label>
     <mt:MTTextBox TextMode="Password" Source="user_password" Required="True" maxlength="100" ID="password" data-id="UC_LoginPageLoginpassword" runat="server" />
   
       <div>
             <mt:MTLabel id="ErrorLabel" runat="server"/>
       </div>
      
       </div>
            
          <div class="footer">
                    <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> Remember me
                    </label>
                                
                  &nbsp;<mt:MTImageButton ImageUrl="Styles/None/Images/en/ButtonLogin.gif"  AlternateText="Login" OnClick="LoginButton_DoLogin_Click" ID="Button_DoLogin" data-id="UC_LoginPageLoginButton_DoLogin" runat="server"  />
                </div>         
                                

             </div>

            </div>

            </div>                   
       <div align="center" style="padding:30px 10px 10px 10px ; color:white;">
            <mt:MTPanel id="Error" visible="False" runat="server">
    <span id="<%= Variables["@error-block"]%>">
   
    </span>
 </mt:MTPanel>
           </div>
     

</div></ItemTemplate>

</mt:Record>



<p>
    &nbsp;</p>
<p>
   
</p>




<!--End ASCX page-->

