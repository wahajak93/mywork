//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Common
{
    /// <summary>
    /// Represent a DateTime value
    /// </summary>
    [Serializable]
    public struct MTDate : IMTField
    {
        private bool _isNull;
        private DateTime _value;

        /// <summary>
        /// Returns a <see cref="DataType"/> of the specified <see cref="IMTField"/> object.
        /// </summary>
        public DataType DataType
        {
            get { return DataType.Date; }
        }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MTDate"/> structure.
        /// </summary>
        /// <param name="IsNullable">The supplied <see cref="bool"/> IsNullable indicates that the value of the new <see cref="MTDate"/> structure is null.</param>
        private MTDate(bool IsNullable)
            : this(DateTime.MinValue)
        {
            _isNull = IsNullable;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTDate"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="DateTime"/> value that will be used as the value of the new <see cref="MTDate"/> structure.</param>
        public MTDate(DateTime value)
        {
            _isNull = false;
            _value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTDate"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="DateTime"/> value that will be used as the value of the new <see cref="MTDate"/> structure.</param>
        public MTDate(DateTime? value)
        {
            _isNull = (value == null);
            _value = value.GetValueOrDefault(DateTime.MinValue);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTDate"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="TimeSpan"/> value that will be used as the value of the new <see cref="MTDate"/> structure.</param>
        public MTDate(TimeSpan value)
            : this(DateTime.MinValue + value)
        {

        }
        #endregion

        #region Properties
        /// <summary>
        /// Represents the largest possible value of an MTDate. This field is constant.
        /// </summary>
        public static MTDate MaxValue
        {
            get
            {
                return new MTDate(DateTime.MaxValue);
            }
        }

        /// <summary>
        /// Represents the smallest possible value of an MTDate. This field is constant.
        /// </summary>
        public static MTDate MinValue
        {
            get
            {
                return new MTDate(DateTime.MinValue);
            }
        }

        /// <summary>
        /// Gets the <see cref="MTDate"/> structure's value.
        /// </summary>
        public DateTime Value
        {
            get { return _value; }
        }

        /// <summary>
        /// Indicates whether a structure is null. This property is read-only.
        /// </summary>
        public bool IsNull
        {
            get { return _isNull; }
        }

        /// <summary>
        /// Represents a null value that can be assigned to the Value property of an instance of the <see cref="MTDate"/> structure.
        /// </summary>
        public static MTDate Null
        {
            get { return new MTDate(true); }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Converts the string representation of a Date/Time to its <see cref="MTDate"/> equivalent.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <param name="format">A format string.</param>
        /// <returns>The <see cref="MTDate"/> value equivalent to the Date/Time contained in value.</returns>
        /// <exception cref="FormatException">The string does not represent valid DateTime value.</exception>
        public static MTDate Parse(object value, string format)
        {
            if (value is MTDate) return (MTDate)value;
            try { return ToMTDate(value); }
            catch
            {
                IMTField val = value as IMTField;
                if (value == null || val != null && val.IsNull
                    || value.Equals(DBNull.Value) || value.ToString().Length == 0)
                    return MTDate.Null;
                if (String.IsNullOrEmpty(format))
                {
                    format = InMotion.Configuration.AppConfig.DefaultDateFormat;
                }
                if (val != null) value = val.ToString("");

                try
                {
                    string v = value as string;
                    if (v != null)
                    {
                        if (String.IsNullOrEmpty(format))
                            return (MTDate)DateTime.Parse(v);
                        return (MTDate)DateTime.ParseExact(v, format, CultureInfo.CurrentCulture.DateTimeFormat);
                    }
                    else return (MTDate)Convert.ToDateTime(value);
                }
                catch (Exception e)
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError("Unable to parse DateTime.\n{0}", e);
                    throw new FormatException("Unable to parse DateTime: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Converts the string representation of a Date/Time to its <see cref="MTDate"/> equivalent.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <returns>The <see cref="MTDate"/> value equivalent to the Date/Time contained in value.</returns>
        /// <exception cref="FormatException">The string does not represent valid DateTime value.</exception>
        public static MTDate Parse(object value)
        {
            return Parse(value, "");
        }

        private static MTDate ToMTDate(object obj)
        {
            MTDate result;
            try
            {
                if (obj is DateTime)
                    result = (MTDate)(DateTime)obj;
                else if (obj is TimeSpan)
                    result = (MTDate)(TimeSpan)obj;
                else
                    result = (MTDate)obj;
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError(e.ToString());
                throw new InvalidCastException();
            }
            return result;
        }

        /// <summary>
        /// Tests for equality between two <see cref="MTDate"/> objects.
        /// </summary>
        /// <param name="obj">An <see cref="object"/> value to compare to this instance.</param>
        /// <returns>true if obj has the same value as this instance; otherwise, false</returns>
        public override bool Equals(object obj)
        {
            MTDate i;
            try
            {
                i = ToMTDate(obj);
            }
            catch { return false; }
            if (i.IsNull || this.IsNull)
                return this.IsNull == i.IsNull;
            return i.Value == this.Value;
        }

        /// <summary>
        /// Serves as a hash function for a particular type. GetHashCode is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>A hash code for the current <see cref="MTDate"/>.</returns>
        public override int GetHashCode()
        {
            if (this.IsNull) return 0;
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Returns a String that represents the current MTDate.
        /// </summary>
        /// <returns>A string representation of value of this instance.</returns>
        public override string ToString()
        {
            return this.ToString("");
        }

        /// <summary>
        /// Converts the DateTime value of this instance to its equivalent string representation.
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <returns>A string representation of value of this instance as specified by format.</returns>
        public string ToString(string format)
        {
            return this.ToString(format, null);
        }

        /// <summary>
        /// Converts the DateTime value of this instance to its equivalent string representation using the specified format and culture-specific format information. 
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <param name="formatProvider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (this.IsNull) return "";
            if (String.IsNullOrEmpty(format))
                format = InMotion.Configuration.AppConfig.DefaultDateFormat;
            else if (format != null && format == "wi")
                return System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)((DateTime)Value).DayOfWeek].Substring(0, 1);
            return Value.ToString(format, formatProvider);
        }

        /// <summary>
        /// Adds the value of the specified <see cref="TimeSpan"/> to the value of this instance.
        /// </summary>
        /// <param name="value">A <see cref="TimeSpan"/> that contains the interval to add.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the time interval represented by value.</returns>
        public MTDate Add(TimeSpan value)
        {
            return this.IsNull ? MTDate.Null : new MTDate(this.Value.Add(value));
        }

        /// <summary>
        /// Subtracts the specified time or duration from this instance.
        /// </summary>
        /// <param name="value">An instance of <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="TimeSpan"/> interval equal to the date and time represented by this instance minus the date and time represented by value.</returns>
        public TimeSpan Subtract(MTDate value)
        {
            if (value.IsNull || this.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be Null.");
            }
            return this.Value.Subtract(value.Value);
        }

        /// <summary>
        /// Subtracts the specified time or duration from this instance.
        /// </summary>
        /// <param name="value">An instance of <see cref="DateTime"/>.</param>
        /// <returns>A <see cref="TimeSpan"/> interval equal to the date and time represented by this instance minus the date and time represented by value.</returns>
        public TimeSpan Subtract(DateTime value)
        {
            if (this.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be Null");
            }
            return this.Value.Subtract(value);
        }

        /// <summary>
        /// Subtracts the specified time or duration from this instance.
        /// </summary>
        /// <param name="value">An instance of <see cref="TimeSpan"/>.</param>
        /// <returns>A <see cref="MTDate"/> equal to the date and time represented by this instance minus the time interval represented by value.</returns>
        public MTDate Subtract(TimeSpan value)
        {
            return this.IsNull ? MTDate.Null : new MTDate(this.Value.Subtract(value));
        }
        #endregion

        #region IComparable Members
        /// <summary>
        /// Compares this instance to a specified <see cref="MTDate"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(MTDate op1)
        {
            if (op1.IsNull && this.IsNull) return 0;
            if (op1.IsNull) return 1;
            if (this.IsNull) return -1;
            return this.Value.CompareTo(op1.Value);
        }

        /// <summary>
        /// Compares this instance to an <b>Object</b> and returns an indication of their relative values.
        /// </summary>
        /// <param name="obj">An <see cref="Object"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(object obj)
        {
            if (obj is MTDate)
                return CompareTo((MTDate)obj);
            else
                return CompareTo(Parse(obj));
        }
        #endregion

        #region DateTime mappings
        /// <summary>
        /// Adds the specified number of days to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional days. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of days represented by value.</returns>
        public MTDate AddDays(double value)
        {
            return new MTDate(Value.AddDays(value));
        }

        /// <summary>
        /// Adds the specified number of hours to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional hours. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of hours represented by value.</returns>
        public MTDate AddHours(double value)
        {
            return new MTDate(Value.AddHours(value));
        }

        /// <summary>
        /// Adds the specified number of milliseconds to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional milliseconds. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of milliseconds represented by value.</returns>
        public MTDate AddMilliseconds(double value)
        {
            return new MTDate(Value.AddMilliseconds(value));
        }

        /// <summary>
        /// Adds the specified number of minutes to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional minutes. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of minutes represented by value.</returns>
        public MTDate AddMinutes(double value)
        {
            return new MTDate(Value.AddMinutes(value));
        }

        /// <summary>
        /// Adds the specified number of months to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional months. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of months represented by value.</returns>
        public MTDate AddMonths(int value)
        {
            return new MTDate(Value.AddMonths(value));
        }

        /// <summary>
        /// Adds the specified number of seconds to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional seconds. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of seconds represented by value.</returns>
        public MTDate AddSeconds(double value)
        {
            return new MTDate(Value.AddSeconds(value));
        }

        /// <summary>
        /// Adds the specified number of ticks to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional ticks. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of ticks represented by value.</returns>
        public MTDate AddTicks(long value)
        {
            return new MTDate(Value.AddTicks(value));
        }

        /// <summary>
        /// Adds the specified number of years to the value of this instance.
        /// </summary>
        /// <param name="value">A number of whole and fractional years. The value parameter can be negative or positive.</param>
        /// <returns>A <see cref="MTDate"/> whose value is the sum of the date and time represented by this instance and the number of years represented by value.</returns>
        public MTDate AddYears(int value)
        {
            return new MTDate(Value.AddYears(value));
        }

        /// <summary>
        /// Gets the day of the month represented by this instance.
        /// </summary>
        public int Day
        {
            get
            {
                return Value.Day;
            }
        }

        /// <summary>
        /// Gets the date component of this instance. 
        /// </summary>
        public MTDate Date
        {
            get { return this._value.Date; }
        }


        /// <summary>
        /// Gets the number of whole days represented by the current instance.
        /// </summary>
        public int Days
        {
            get
            {
                return ((TimeSpan)this).Days;
            }
        }

        /// <summary>
        /// Gets the day of the week represented by this instance.
        /// </summary>
        public DayOfWeek DayOfWeek
        {
            get
            {
                return Value.DayOfWeek;
            }
        }

        /// <summary>
        /// Gets the day of the year represented by this instance.
        /// </summary>
        public int DayOfYear
        {
            get
            {
                return Value.DayOfYear;
            }
        }

        /// <summary>
        /// Gets the hour component of the date represented by this instance.
        /// </summary>
        public int Hour
        {
            get
            {
                return Value.Hour;
            }
        }

        /// <summary>
        /// The hour component of the current instance. The return value ranges from -23 through 23.
        /// </summary>
        public int Hours
        {
            get
            {
                return ((TimeSpan)this).Hours;
            }
        }

        /// <summary>
        /// Gets the milliseconds component of the date represented by this instance.
        /// </summary>
        public int Millisecond
        {
            get
            {
                return Value.Millisecond;
            }
        }

        /// <summary>
        /// Gets the number of whole milliseconds represented by the current instance.
        /// </summary>
        public int Milliseconds
        {
            get
            {
                return ((TimeSpan)this).Milliseconds;
            }
        }

        /// <summary>
        /// Gets the minute component of the date represented by this instance.
        /// </summary>
        public int Minute
        {
            get
            {
                return Value.Minute;
            }
        }

        /// <summary>
        /// Gets the number of whole minutes represented by the current instance.
        /// </summary>
        public int Minutes
        {
            get
            {
                return ((TimeSpan)this).Minutes;
            }
        }

        /// <summary>
        /// Gets the month component of the date represented by this instance.
        /// </summary>
        public int Month
        {
            get
            {
                return Value.Month;
            }
        }

        /// <summary>
        /// Gets a <see cref="MTDate"/> object that is set to the current date and time on this computer, expressed as the local time.
        /// </summary>
        public static MTDate Now
        {
            get
            {
                return (MTDate)DateTime.Now;
            }
        }

        /// <summary>
        /// Gets the seconds component of the date represented by this instance.
        /// </summary>
        public int Second
        {
            get
            {
                return Value.Second;
            }
        }

        /// <summary>
        /// Gets the number of whole seconds represented by the current instance.
        /// </summary>
        public int Seconds
        {
            get
            {
                return ((TimeSpan)this).Seconds;
            }
        }

        /// <summary>
        /// Gets the number of ticks that represent the date and time of this instance.
        /// </summary>
        public long Ticks
        {
            get
            {
                return Value.Ticks;
            }
        }

        /// <summary>
        /// Gets the year component of the date represented by this instance.
        /// </summary>
        public int Year
        {
            get
            {
                return Value.Year;
            }
        }

        /// <summary>
        /// Gets the value of the current instance expressed in whole and fractional days.
        /// </summary>
        public double TotalDays
        {
            get
            {
                return ((TimeSpan)this).TotalDays;
            }
        }

        /// <summary>
        /// Gets the value of the current instance expressed in whole and fractional hours.
        /// </summary>
        public double TotalHours
        {
            get
            {
                return ((TimeSpan)this).Hours;
            }
        }

        /// <summary>
        /// Gets the value of the current instance expressed in whole and fractional milliseconds.
        /// </summary>
        public double TotalMilliseconds
        {
            get
            {
                return ((TimeSpan)this).TotalMilliseconds;
            }
        }

        /// <summary>
        /// Gets the value of the current instance expressed in whole and fractional minutes.
        /// </summary>
        public double TotalMinutes
        {
            get
            {
                return ((TimeSpan)this).TotalMinutes;
            }
        }

        /// <summary>
        /// Gets the value of the current instance expressed in whole and fractional seconds.
        /// </summary>
        public double TotalSeconds
        {
            get
            {
                return ((TimeSpan)this).TotalSeconds;
            }
        }
        #endregion

        #region Static operators
        /// <summary>
        /// Determines whether two specified instances of MTDate are equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator ==(MTDate op1, MTDate op2)
        {
            return op1.Equals(op2);
        }

        /// <summary>
        /// Determines whether two specified instances of MTDate are not equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> do not represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator !=(MTDate op1, MTDate op2)
        {
            return !op1.Equals(op2);
        }

        /// <summary>
        /// Determines whether first specified instances of MTDate are greater than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> greater than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value.CompareTo(op2.Value) > 0);
        }

        /// <summary>
        /// Determines whether first specified instances of MTDate are greater than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> greater than or equal to <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator >=(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(!(op1.Value.CompareTo(op2.Value) < 0));
        }

        /// <summary>
        /// Determines whether first specified instances of MTDate are less than second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value.CompareTo(op2.Value) < 0);
        }

        /// <summary>
        /// Determines whether first specified instances of MTDate are less than or equal to second instance.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> less than or equal to <i>op2</i>; otherwise <b>false</b>.;</returns>
        public static MTBoolean operator <=(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(!(op1.Value.CompareTo(op2.Value) > 0));
        }

        /// <summary>
        /// Subtracts a specified date and time from another specified date and time, yielding a time interval.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/> (the minuend).</param>
        /// <param name="op2">A <b>MTDate</b> (the subtrahend).</param>
        /// <returns>A <b>MTDate</b> whose value is the value of <i>op1</i> minus the value of <i>op2</i>.</returns>
        public static MTDate operator -(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTDate.Null;
            return new MTDate(op1.Value.Subtract((TimeSpan)op2));
        }

        /// <summary>
        /// Adds a specified date and time to another specified date and time, yielding a time interval.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <param name="op2">A <b>MTDate</b>.</param>
        /// <returns>A <b>MTDate</b> whose value is the value of <i>op1</i> plus the value of <i>op2</i>.</returns>
        public static MTDate operator +(MTDate op1, MTDate op2)
        {
            if (op1.IsNull || op2.IsNull) return MTDate.Null;
            return new MTDate(op1.Value.Add((TimeSpan)op2));
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTDate"/> instance that value represent the value of specified <see cref="DateTime"/>.
        /// </summary>
        /// <param name="op1">A <see cref="DateTime"/>.</param>
        /// <returns>A <see cref="MTDate"/> instance.</returns>
        public static implicit operator MTDate(DateTime op1)
        {
            return new MTDate(op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTDate"/> instance that value represent the value of specified <see cref="DateTime"/>.
        /// </summary>
        /// <param name="op1">A <see cref="DateTime"/>.</param>
        /// <returns>A <see cref="MTDate"/> instance.</returns>
        public static implicit operator MTDate(DateTime? op1)
        {
            return new MTDate(op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTDate"/> instance that value represent the value of specified <see cref="TimeSpan"/>.
        /// </summary>
        /// <param name="op1">A <see cref="TimeSpan"/>.</param>
        /// <returns>A <see cref="MTDate"/> instance that value represent <see cref="DateTime.MinValue"/> plus <i>op1</i>.</returns>
        public static implicit operator MTDate(TimeSpan op1)
        {
            return new MTDate(DateTime.MinValue + op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="MTDate"/> instance that value represent the value of specified <see cref="TimeSpan"/>.
        /// </summary>
        /// <param name="op1">A <see cref="TimeSpan"/>.</param>
        /// <returns>A <see cref="MTDate"/> instance that value represent <see cref="DateTime.MinValue"/> plus <i>op1</i>.</returns>
        public static implicit operator MTDate(TimeSpan? op1)
        {
            if (op1 == null)
                return new MTDate(true);
            return new MTDate(DateTime.MinValue + op1);
        }

        /// <summary>
        /// Implicitly creates a <see cref="DateTime"/> instance that value represent the value of specified <see cref="MTDate"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="DateTime"/> instance.</returns>
        public static implicit operator DateTime(MTDate op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be Null.");
            }
            return op1.Value;
        }

        /// <summary>
        /// Implicitly creates a <see cref="DateTime"/> instance that value represent the value of specified <see cref="MTDate"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="DateTime"/> instance.</returns>
        public static implicit operator DateTime?(MTDate op1)
        {
            if (op1.IsNull)
                return null;
            return op1.Value;
        }

        /// <summary>
        /// Explicitly creates a <see cref="TimeSpan"/> instance that value represent the value of specified <see cref="MTDate"/> minus <see cref="DateTime.MinValue"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="TimeSpan"/> instance.</returns>
        public static explicit operator TimeSpan(MTDate op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be Null.");
            }
            return op1.Value - DateTime.MinValue;
        }

        /// <summary>
        /// Explicitly creates a <see cref="TimeSpan"/> instance that value represent the value of specified <see cref="MTDate"/> minus <see cref="DateTime.MinValue"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="TimeSpan"/> instance.</returns>
        public static explicit operator TimeSpan?(MTDate op1)
        {
            if (op1.IsNull)
                return null;
            return op1.Value - DateTime.MinValue;
        }

        /// <summary>
        /// Explicitly creates a <see cref="string"/> instance that value represent the value of specified <see cref="MTDate"/>.
        /// </summary>
        /// <param name="op1">A <see cref="MTDate"/>.</param>
        /// <returns>A <see cref="string"/> instance.</returns>
        public static explicit operator string(MTDate op1)
        {
            if (op1.IsNull)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Operand can not be Null.\n{0}", Environment.StackTrace);
                throw new InvalidCastException("Operand can not be null.");
            }
            return op1.ToString();
        }
        #endregion

        #region IConvertible Members
        /// <summary>
        /// Returns the <see cref="TypeCode"/> for this instance.
        /// </summary>
        /// <returns>The enumerated constant that is the <see cref="TypeCode"/> of the class or value type that implements this interface.</returns>
        public TypeCode GetTypeCode()
        {
            return ((IConvertible)Value).GetTypeCode();
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToBoolean"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToBoolean(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToChar"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        char IConvertible.ToChar(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToChar(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDateTime"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDateTime(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDecimal"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDecimal(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDouble"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDouble(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt64(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSingle"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSingle(provider);
        }

        /// <summary>
        /// Converts the date and time value of this instance to its equivalent string representation using the specified culture-specific format information. 
        /// </summary>
        /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by provider.</returns>
        public string ToString(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToString(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToType"/>. 
        /// </summary>
        /// <param name="conversionType"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return ((IConvertible)Value).ToType(conversionType, provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt64(provider);
        }
        #endregion
    }
}