//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace InMotion.Common
{
    /// <summary>
    /// Defines a set of interfaces and methods that a IM types implement.
    /// </summary>
    [ComVisible(false)]
    public interface IMTField : INullable, IComparable, IFormattable,IConvertible
    {
           
        /// <summary>
        /// Converts the value of this instance to its equivalent string representation.
        /// </summary>
        /// <returns>A <see cref="String"/> object</returns>
        string ToString();

        /// <summary>
        /// Converts the value of this instance to its equivalent string representation using the specified format. 
        /// Returns a <see cref="String"/> that represents the current object. 
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <returns>A string representation of value of this instance as specified by format. </returns>
        string ToString(string format);

        /// <summary>
        /// Gets a <see cref="DataType"/> value.
        /// </summary>
        DataType DataType { get; }
    }
}
