//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Threading;
using System.Diagnostics;
using InMotion.Globalization;
using InMotion.Configuration;

namespace InMotion.Common
{
    /// <summary>
    /// Represent a Boolean value
    /// </summary>
    [Serializable]
    public struct MTBoolean : IMTField
    {
        private const int NullVal = 0;
        private const int TrueVal = 1;
        private const int FalseVal = 2;
        private int _value;

        /// <summary>
        /// Returns a <see cref="DataType"/> of the specified <see cref="IMTField"/> object.
        /// </summary>
        public DataType DataType
        {
            get { return DataType.Boolean; }
        }

        #region Constructors
        private MTBoolean(int nullable)
        {
            _value = nullable;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTBoolean"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="bool"/> value that will be used as the value of the new <see cref="MTBoolean"/> structure.</param>
        public MTBoolean(bool value)
        {
            _value = value ? TrueVal : FalseVal;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MTBoolean"/> structure.
        /// </summary>
        /// <param name="value">The supplied <see cref="bool"/> value that will be used as the value of the new <see cref="MTBoolean"/> structure.</param>
        public MTBoolean(bool? value)
        {
            _value = ((value == null)? NullVal : (value.GetValueOrDefault() ? TrueVal : FalseVal));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Indicates whether a structure is null. This property is read-only.
        /// </summary>
        public bool IsNull
        {
            get { return _value == NullVal; }
        }

        /// <summary>
        /// Indicates whether the current <see cref="Value"/> is <b>true</b>. 
        /// </summary>
        public bool IsTrue
        {
            get { return _value == TrueVal; }
        }

        /// <summary>
        /// Indicates whether the current <see cref="Value"/> is <b>false</b>. 
        /// </summary>
        public bool IsFalse
        {
            get { return _value == FalseVal; }
        }

        /// <summary>
        /// Represents a null value that can be assigned to the Value property of an instance of the <see cref="MTBoolean"/> structure.
        /// </summary>
        public static MTBoolean Null
        {
            get { return new MTBoolean(NullVal); }
        }

        /// <summary>
        /// Represents a true value that can be assigned to the Value property of an instance of the <see cref="MTBoolean"/> structure.
        /// </summary>
        public static MTBoolean True
        {
            get { return new MTBoolean(TrueVal); }
        }

        /// <summary>
        /// Represents a false value that can be assigned to the Value property of an instance of the <see cref="MTBoolean"/> structure.
        /// </summary>
        public static MTBoolean False
        {
            get { return new MTBoolean(FalseVal); }
        }

        /// <summary>
        /// Gets the <see cref="MTBoolean"/> structure's value.
        /// </summary>
        public bool Value
        {
            get { return IsTrue; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Converts the string representation of a Boolean in a specified format to its <see cref="MTBoolean"/> equivalent.
        /// </summary>
        /// <param name="value">An object containing the value to convert.</param>
        /// <param name="format">A number format string.</param>
        /// <returns>The <see cref="MTBoolean"/> value equivalent to the number contained in value.</returns>
        public static MTBoolean Parse(object value, string format)
        {
            if (value is MTBoolean) return (MTBoolean)value;
            IMTField val = value as IMTField;
            if (value == null || val!=null && val.IsNull
                || value.Equals(DBNull.Value) || value.ToString().Length == 0)
                return MTBoolean.Null;
            if (String.IsNullOrEmpty(format))
            {
                if (Thread.CurrentThread.CurrentCulture is MTCultureInfo)
                    format = ((MTCultureInfo)Thread.CurrentThread.CurrentCulture).BooleanFormat;
            }
            if (val !=null) value = val.ToString("");

            try
            {
                string v = value as string;
                if (v!=null)
                {
                    char[] chDelim = { ';' };
                    if (format == null || format.Length == 0)
                    {
                        return (MTBoolean)bool.Parse(v);
                    }
                    else
                    {
                        string[] Tokens = format.Split(chDelim);
                        if (v == Tokens[0])
                            return MTBoolean.True;
                        if (v == Tokens[1])
                            return MTBoolean.False;
                        if (Tokens.Length == 3 &&
                            v == Tokens[2])
                            return MTBoolean.Null;
                    }
                    return MTBoolean.False;
                }
                else return (MTBoolean)Convert.ToBoolean(value);
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Unable to parse Boolean.\n{0}", e);
                throw new FormatException("Unable to parse Boolean.", e);
            }
        }

        /// <summary>
        /// Converts the string representation of a number to its <see cref="MTSingle"/> equivalent.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <returns>The <see cref="MTSingle"/> value equivalent to the number contained in value.</returns>
        public static MTBoolean Parse(object value)
        {
            return Parse(value, "");
        }

        /// <summary>
        /// Tests for equality between two <see cref="MTBoolean"/> objects.
        /// </summary>
        /// <param name="obj">An <see cref="object"/> value to compare to this instance.</param>
        /// <returns><b>true</b> if obj has the same value as this instance; otherwise, <b>false</b></returns>
        public override bool Equals(object obj)
        {
            MTBoolean i;
            try
            {
                if (obj is bool) i = (MTBoolean)(bool)obj;
                else i = (MTBoolean)obj;
            }
            catch { return false; }
            if (i.IsNull || this.IsNull)
                return this.IsNull == i.IsNull;
            return i.Value == this.Value;
        }

        /// <summary>
        /// Serves as a hash function for a particular type. GetHashCode is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>A hash code for the current <see cref="MTBoolean"/>.</returns>
        public override int GetHashCode()
        {
            if (this.IsNull) return 0;
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Converts the boolean value of this instance to its equivalent string representation.
        /// </summary>
        /// <returns>The string representation of the value of this instance.</returns>
        public override string ToString()
        {
            return this.ToString("");
        }

        /// <summary>
        /// Converts the boolean value of this instance to its equivalent string representation, using the specified format. 
        /// </summary>
        /// <param name="format">A format string. </param>
        /// <returns>The string representation of the value of this instance as specified by format. </returns>
        public string ToString(string format)
        {
            return this.ToString(format, null);
        }

        /// <summary>
        /// Converts the boolean value of this instance to its equivalent string representation using the specified format and culture-specific format information. 
        /// </summary>
        /// <param name="format">A format string.</param>
        /// <param name="formatProvider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by format and provider.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (String.IsNullOrEmpty(format))
            {
                if (Thread.CurrentThread.CurrentCulture is MTCultureInfo)
                    format = ((MTCultureInfo)Thread.CurrentThread.CurrentCulture).BooleanFormat;
            }
            char[] chDelim = { ';' };
            try
            {
                if ((format == null || format.Length == 0) && !IsNull)
                    return Value.ToString();
                else if ((format == null || format.Length == 0) && IsNull)
                    return "";
                string[] Tokens = format.Split(chDelim);
                if (IsNull && Tokens.Length > 2)
                    return Tokens[2];
                if (IsNull)
                    return "";
                if ((bool)Value)
                    return Tokens[0];
                if (!(bool)Value)
                    return Tokens[1];
            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Unable to format Boolean.\n{0}", e);
                throw new FormatException("Unable to format Boolean.", e);
            }
            return "";
        }
        #endregion

        #region IComparable Members
        /// <summary>
        /// Compares this instance to a specified <see cref="MTBoolean"/> and returns an indication of their relative values.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(MTBoolean op1)
        {
            if (op1.IsNull && this.IsNull) return 0;
            if (op1.IsNull) return 1;
            if (this.IsNull) return -1;
            return this.Value.CompareTo(op1.Value);
        }

        /// <summary>
        /// Compares this instance to an <b>Object</b> and returns an indication of their relative values.
        /// </summary>
        /// <param name="obj">An <see cref="Object"/> to compare to this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects
        /// being compared. The return value has these meanings: Value Meaning Less than
        /// zero This instance is less than obj. Zero This instance is equal to obj.
        /// Greater than zero This instance is greater than obj.
        /// </returns>
        public int CompareTo(object obj)
        {
            if (obj is MTBoolean || obj is bool)
            {
                return CompareTo((MTBoolean)obj);
            }
            else
            {
                return CompareTo(Parse(obj));
            }
        }
        #endregion

        #region Static operators
        /// <summary>
        /// Determines whether two specified instances of MTBoolean are equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <param name="op2">A <b>MTBoolean</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator ==(MTBoolean op1, MTBoolean op2)
        {
            return op1.Equals(op2);
        }

        /// <summary>
        /// Determines whether two specified instances of MTBoolean are not equal.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <param name="op2">A <b>MTBoolean</b>.</param>
        /// <returns><b>true</b> if <i>op1</i> and <i>op2</i> do not represent the same value; otherwise <b>false</b>.;</returns>
        public static bool operator !=(MTBoolean op1, MTBoolean op2)
        {
            return !op1.Equals(op2);
        }

        /// <summary>
        /// Performs the logical negation of operand.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <returns>Returns <b>true</b> if and only if its <i>op1</i> is <b>false</b>.</returns>
        public static MTBoolean operator !(MTBoolean op1)
        {
            if (op1.IsNull) return op1;
            return new MTBoolean(!op1.Value);
        }

        /// <summary>
        /// Computes the logical AND of operands.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <param name="op2">A <b>MTBoolean</b>.</param>
        /// <returns><b>true</b> if and only if both <i>op1</i> and <i>op2</i> are <b>true</b>.</returns>
        public static MTBoolean operator &(MTBoolean op1, MTBoolean op2)
        {
            if (op1._value == 2 || op2._value == 2)
                return MTBoolean.False;
            if (op1._value == 1 && op2._value == 1)
                return MTBoolean.True;
            return MTBoolean.Null;
        }

        /// <summary>
        /// Computes the logical OR of operands.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <param name="op2">A <b>MTBoolean</b>.</param>
        /// <returns><b>false</b> if and only if both <i>op1</i> and <i>op2</i> are <b>false</b>.</returns>
        public static MTBoolean operator |(MTBoolean op1, MTBoolean op2)
        {
            if (op1._value == 1 || op2._value == 1)
                return MTBoolean.True;
            if (op1._value == 2 && op2._value == 2)
                return MTBoolean.False;
            return MTBoolean.Null;
        }

        /// <summary>
        /// Computes the bitwise exclusive-OR of operands.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <param name="op2">A <b>MTBoolean</b>.</param>
        /// <returns><b>true</b> if exactly one of <i>op1</i> and <i>op2</i> are <b>true</b>.</returns>
        public static MTBoolean operator ^(MTBoolean op1, MTBoolean op2)
        {
            if (op1.IsNull || op2.IsNull) return MTBoolean.Null;
            return new MTBoolean(op1.Value ^ op2.Value);
        }

        /// <summary>
        /// Returns the <see cref="bool"/> value true to indicate true and returns false otherwise.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <returns><b>true</b> if and only if <b>MTBoolean</b> represents <b>true</b> value.</returns>
        public static bool operator true(MTBoolean op1)
        {
            return op1.IsTrue;
        }

        /// <summary>
        /// Returns the <see cref="bool"/> value true to indicate false and returns false otherwise.
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/>.</param>
        /// <returns><b>true</b> if and only if <b>MTBoolean</b> represents <b>true</b> value.</returns>
        public static bool operator false(MTBoolean op1)
        {
            return op1.IsFalse;
        }

        /// <summary>
        /// Implicity creates <b>MTBoolean</b> from the specified bool value;
        /// </summary>
        /// <param name="op1">A <see cref="bool"/> value</param>
        /// <returns>A <b>MTBoolean</b> that represents bool value.</returns>
        public static implicit operator MTBoolean(bool op1)
        {
            return new MTBoolean(op1);
        }

        /// <summary>
        /// Implicity creates <b>MTBoolean</b> from the specified bool value;
        /// </summary>
        /// <param name="op1">A <see cref="bool"/> value</param>
        /// <returns>A <b>MTBoolean</b> that represents bool value.</returns>
        public static implicit operator MTBoolean(bool? op1)
        {
            return new MTBoolean(op1);
        }

        /// <summary>
        /// Implicity creates bool from the specified <b>MTBoolean</b> value;
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/> value.</param>
        /// <returns>A <b>bool</b> that represents bool value.</returns>
        public static implicit operator bool(MTBoolean op1)
        {
            return op1.IsTrue;
        }

        /// <summary>
        /// Implicity creates bool from the specified <b>MTBoolean</b> value;
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/> value.</param>
        /// <returns>A <b>bool</b> that represents bool value.</returns>
        public static implicit operator bool?(MTBoolean op1)
        {
            if (op1.IsNull)
                return null;
            return op1.IsTrue;
        }

        /// <summary>
        /// Explicity creates string representation from the specified <b>MTBoolean</b> value;
        /// </summary>
        /// <param name="op1">A <see cref="MTBoolean"/> value.</param>
        /// <returns>A <b>bool</b> that represents bool value.</returns>
        public static explicit operator string(MTBoolean op1)
        {
            return op1.ToString();
        }
        #endregion

        #region IConvertible Members
        /// <summary>
        /// Returns the <see cref="TypeCode"/> for this instance.
        /// </summary>
        /// <returns>The enumerated constant that is the <see cref="TypeCode"/> of the class or value type that implements this interface.</returns>
        public TypeCode GetTypeCode()
        {
            return ((IConvertible)Value).GetTypeCode();
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToBoolean"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToBoolean(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        byte IConvertible.ToByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToChar"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        char IConvertible.ToChar(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToChar(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDateTime"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDateTime(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDecimal"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDecimal(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToDouble"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        double IConvertible.ToDouble(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToDouble(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        short IConvertible.ToInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        long IConvertible.ToInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToInt64(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSByte"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSByte(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToSingle"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        float IConvertible.ToSingle(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToSingle(provider);
        }

        /// <summary>
        /// Converts the bool value of this instance to its equivalent string representation using the specified culture-specific format information. 
        /// </summary>
        /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
        /// <returns>The string representation of the value of this instance as specified by provider.</returns>
        public string ToString(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToString(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToType"/>. 
        /// </summary>
        /// <param name="conversionType"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return ((IConvertible)Value).ToType(conversionType, provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt16"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt16(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt32"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt32(provider);
        }

        /// <summary>
        /// For a description of this member, see <see cref="IConvertible.ToUInt64"/>. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            return ((IConvertible)Value).ToUInt64(provider);
        }
        #endregion
    }
}