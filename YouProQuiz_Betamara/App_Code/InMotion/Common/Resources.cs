//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Resources;
using System.Reflection;

namespace InMotion.Common
{
    /// <summary>
    /// Provide the access to the default instance of <see cref="MTResourceManager"/> class.
    /// </summary>
    public static class Resources
    {
        private static MTResourceManager _manager;
        /// <summary>
        /// Gets or sets the default <see cref="MTResourceManager"/> object.
        /// </summary>
        public static InMotion.Resources.MTResourceManager ResManager
        {
            get
            {
                if (_manager == null) _manager = new MTResourceManager("resources.strings", Assembly.Load(new System.Reflection.AssemblyName("app_GlobalResources")));
                return _manager;
            }
            set
            {
                _manager = value;
            }
        }
    }
}
