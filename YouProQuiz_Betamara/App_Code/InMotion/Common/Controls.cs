using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
namespace InMotion.Common
{
    /// <summary>
    /// Common functions for InMotion controls
    /// </summary>
    public class Controls
    {
        public static Control FindControlIterative(Control control, string id)
        {
            Control ctl = control;
            LinkedList<Control> controls = new LinkedList<Control>();
            while (ctl != null)
            {
                if (ctl.ID == id)
                    return ctl;
                foreach (Control child in ctl.Controls)
                {
                    if (child.ID == id)
                        return child;
                    if (child.HasControls())
                        controls.AddLast(child);
                }
                if (controls.Count == 0)
                    return null;
                ctl = controls.First.Value;
                controls.Remove(ctl);
            }
            return null;
        }
    }
}