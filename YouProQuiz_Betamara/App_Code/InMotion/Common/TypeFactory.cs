//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Diagnostics;
using InMotion.Configuration;

namespace InMotion.Common
{
    static class TypeFactory
    {
        public static IMTField CreateTypedField(DataType type, object value, string format)
        {
            IMTField val = null;
            switch (type)
            {
                case DataType.Integer:
                    val = MTInteger.Parse(value, format);
                    break;
                case DataType.Float:
                    val = MTFloat.Parse(value, format);
                    break;
                case DataType.Text:
                    val = MTText.Parse(value);
                    break;
                case DataType.Memo:
                    val = MTMemo.Parse(value);
                    break;
                case DataType.Boolean:
                    val = MTBoolean.Parse(value, format);
                    break;
                case DataType.Date:
                    val = MTDate.Parse(value, format);
                    break;
                case DataType.Single:
                    val = MTSingle.Parse(value, format);
                    break;
                default:
                    break;
            }
            return val;
        }

        /// <summary>
        /// Converts IMTField objects into simple types like String or DateTime
        /// </summary>
        public static object ToSimpleType(object value)
        {
            if (!(value is IMTField)) return value;
            if (value == null || ((IMTField)value).IsNull) return null;
            if (value is MTInteger) return ((MTInteger)value).Value;
            if (value is MTFloat) return ((MTFloat)value).Value;
            if (value is MTSingle) return ((MTSingle)value).Value;
            if (value is MTBoolean) return ((MTBoolean)value).Value;
            if (value is MTDate) return ((MTDate)value).Value;
            if (value is MTMemo) return ((MTMemo)value).Value;
            if (value is MTText) return ((MTText)value).Value;

            return null;
        }

        public static string FormatNumber(object value, string format)
        {
            char[] chDelim = { ';' };
            try
            {
                if (format == null || format.Length == 0)
                    return value.ToString();
                NumberFormatInfo nfi = (NumberFormatInfo)System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.Clone();
                string[] Tokens = format.Split(chDelim);
                if (Tokens.Length > 4 && Tokens[4].Length > 0)
                {
                    nfi.NumberDecimalSeparator = Tokens[4];
                    nfi.PercentDecimalSeparator = Tokens[4];
                }
                if (Tokens.Length > 5 && Tokens[5].Length > 0)
                {
                    nfi.NumberGroupSeparator = Tokens[5];
                    nfi.PercentGroupSeparator = Tokens[5];
                }
                if ((value == null || value is DBNull) && Tokens.Length > 3)
                    return Tokens[3];
                if (value == null || value is DBNull)
                    return "null";

                string formatStr = "";
                if (Tokens.Length > 0)
                    formatStr += Tokens[0] + ";";
                if (Tokens.Length > 1)
                    formatStr += Tokens[1] + ";";
                if (Tokens.Length > 2)
                    formatStr += Tokens[2] + ";";

                if (value is int)
                    return ((int)value).ToString(formatStr, nfi);

                if (value is Int64)
                    return ((Int64)value).ToString(formatStr, nfi);

                if (value is Double)
                    return ((Double)value).ToString(formatStr, nfi);

                if (value is Single)
                    return ((Single)value).ToString(formatStr, nfi);

                if (value is Decimal)
                    return ((Decimal)value).ToString(formatStr, nfi);

            }
            catch (Exception e)
            {
                if (AppConfig.IsMTErrorHandlerUse("all"))
                    Trace.TraceError("Unable to format Number.\n{0}", e);
                throw (new FormatException("Unable to format Number:" + e.Message));
            }
            return "";
        }

    }
}
