//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using InMotion.Web.Controls;
using InMotion.Configuration;
using System.Collections.ObjectModel;

namespace InMotion.Web
{
    /// <summary>
    /// Provides an object representation of a uniform resource identifier (URI) and easy access to the parts of the URI.
    /// </summary>
    [Serializable]
    public class Url
    {
        /// <summary>
        /// Initializes a new instance of the Uri class with the specified URI.
        /// </summary>
        /// <param name="url">A URI.</param>
        public Url(string url)
        {
            Address = url;
        }

        private string _address = "";
        /// <summary>
        /// Gets or sets the address of URI.
        /// </summary>
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                string p_addr = "";
                /*if (value.StartsWith("~/"))
                    value = value.Substring(2);*/
                int idx = value.IndexOf("?");
                if (idx == -1)
                {
                    _address = value;
                    p_addr = "";
                }
                else
                {
                    _address = value.Substring(0, idx);
                    p_addr = value.Substring(idx + 1, value.Length - (idx + 1)).TrimEnd('&') + "&";
                }

                if (p_addr.Length > 0)
                {
                    string[] pars = p_addr.Split(new Char[] { '&' });
                    System.Web.HttpServerUtility Server = System.Web.HttpContext.Current.Server;
                    for (int i = 0; i < pars.Length; i++)
                    {
                        string[] name_value = pars[i].Split(new Char[] { '=' });
                        string pname = name_value[0];
                        if (pname.Length == 0) continue;
                        string[] pvalues = name_value.Length > 1 && name_value[1].Length > 0 ? Server.UrlDecode(name_value[1]).Split(new Char[] { ',' }) : new string[] { "" };
                        for (int j = 0; j < pvalues.Length; j++)
                            Parameters.Add(pname, pvalues[j], true);
                    }
                }
            }
        }

        private UrlParameterCollection _parameters = new UrlParameterCollection();
        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> that contains parameters of this URI.
        /// </summary>
        public UrlParameterCollection Parameters
        {
            get { return _parameters; }
        }

        private string _removeParameters = "";
        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get { return _removeParameters; }
            set { _removeParameters = value; }
        }

        private PreserveParameterType _preserveParameters = PreserveParameterType.None;
        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get { return _preserveParameters; }
            set { _preserveParameters = value; }
        }

        private UrlType _urlType = UrlType.None;
        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType Type
        {
            get { return _urlType; }
            set { _urlType = value; }
        }

        /// <summary>
        /// Initializes a new instance of the Uri class with the specified URI.
        /// </summary>
        /// <param name="address">The address of URI.</param>
        /// <param name="removeParameters">Semicolon-separated list of parameters that should be removed from the hyperlink.</param>
        /// <param name="preserveParameters">Indicates whether Get or Post parameters should be preserved.</param>
        /// <param name="type">Indicates whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).</param>
        public Url(string address, string removeParameters, PreserveParameterType preserveParameters, UrlType type)
        {
            this.Address = address;
            this.RemoveParameters = removeParameters;
            this.PreserveParameters = preserveParameters;
            this.Type = type;
        }

        /// <summary>
        /// Initializes a new instance of the Uri class.
        /// </summary>
        public Url()
        {
            if (System.Web.HttpContext.Current != null)
            {
                Address = System.Web.HttpContext.Current.Request.Url.ToString();
                Parameters.Clear();
            }
        }

        /// <summary>
        /// Clears address and parameters of current Url.
        /// </summary>
        public void Clear()
        {
            Parameters.Clear();
            Address = "";
            RemoveParameters = "";
            PreserveParameters = PreserveParameterType.None;
            Type = UrlType.None;
        }

        internal void DataBind(IMTControl sender)
        {
            foreach (UrlParameter p in Parameters)
                p.DataBind(sender);
        }

        /// <summary>
        /// Returns the string representation of current Url for control which includes many links (Sorter, Navigator).
        /// </summary>
        /// <param name="IsParameter">If True function was replace amp parameters.</param>
        /// <returns>String representation of current Url.</returns>
        public string ToString(bool IsParameter)
        {
            string result = ToString();
            if (AppConfig.UseAmp && IsParameter)
                result = result.Replace("&", "&amp;");
            return result;
        }

        /// <summary>
        /// Returns the string representation of current Url.
        /// </summary>
        /// <returns>String representation of current Url.</returns>
        public override string ToString()
        {
            return ToString(null);
        }

        /// <summary>
        /// Returns the string representation of current Url.
        /// </summary>
        /// <param name="owner">Owner page.</param>
        /// <returns>String representation of current Url.</returns>
        internal string ToString(MTPage owner)
        {
            string result = Address;
            string removeStateParameters = "";
            if (owner != null)
            {
                ReadOnlyCollection<IStateUrlParameterProvider> stateControls = owner.GetControls<IStateUrlParameterProvider>();
                foreach (IStateUrlParameterProvider item in stateControls)
                {
                    if (PreserveParameters == PreserveParameterType.Get || PreserveParameters == PreserveParameterType.GetAndPost)
                        Parameters.Add(item.PreserveStateParameters);

                    if (item.RemoveStateParameters.Length > 0)
                    {
                        if (removeStateParameters.Length > 0) removeStateParameters += ";";
                        removeStateParameters += item.RemoveStateParameters;
                    }
                }
            }
            string p = Parameters.ToString(PreserveParameters, RemoveParameters + ";" + removeStateParameters);
            if (p.Length > 0)
                result += "?";
            result += p;
            if (System.Web.HttpContext.Current != null)
            {
                string relativePath = System.Web.HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
                relativePath = relativePath.Substring(2, relativePath.LastIndexOf('/') - 1);
                switch (Type)
                {
                    case UrlType.None:
                        break;
                    case UrlType.Absolute:
                        result = InMotion.Configuration.AppConfig.ServerUrl + relativePath + result;
                        break;
                    case UrlType.SSL:
                        result = InMotion.Configuration.AppConfig.SecureUrl + relativePath + result;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        /// <summary>
        /// Resets the Address string and the Parameters collection.
        /// </summary>
        public void Reset()
        {
            this.Parameters.Clear();
            this.Address = "";
        }

        /// <summary>
        /// Implicitly creates a <see cref="Url"/> instance that value represent the value of specified URI string.
        /// </summary>
        /// <param name="op1">A URI.</param>
        /// <returns>The <see cref="Url"/> instance.</returns>
        public static implicit operator Url(string op1)
        {
            Url _url = new Url("");
            _url.Address = op1;
            return _url;
        }

        /// <summary>
        /// Implicitly creates a <see cref="String"/> instance that value represent the value of specified Url instance.
        /// </summary>
        /// <param name="op1">An <see cref="Url"/> instance.</param>
        /// <returns>A string.</returns>
        public static explicit operator string(Url op1)
        {
            return op1.ToString();
        }
    }
}
