//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for <see cref="InMotion.Web.Controls.ReportSection.BeforeShow"/> event of the <see cref="InMotion.Web.Controls.ReportSection"/> component.
    /// </summary>
    public class BeforeShowSectionEventArgs : EventArgs
    {
        private ReportSectionTemplate m_Item;

        /// <summary>
        /// Gets the report section associated with the event.
        /// </summary>
        public ReportSectionTemplate Item
        {
            get { return m_Item; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BeforeShowSectionEventArgs"/> class. 
        /// </summary>
        /// <param name="item">The item associated with the event. The <see cref="Item"/> property is set to this value. </param>
        public BeforeShowSectionEventArgs(ReportSectionTemplate item)
        {
            this.m_Item = item;
        }
    }

}
