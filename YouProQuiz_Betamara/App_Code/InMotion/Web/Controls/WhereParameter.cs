//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using InMotion.Data;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Summary description for WhereParameter
    /// </summary>
    public class WhereParameter : DataParameter
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="WhereParameter"/>.
        /// </summary>
        public WhereParameter()
        {
        }

        #region Properties
        /// <summary>
        /// Gets or sets condition to be used to match the field value and the parameter value.
        /// </summary>
        public WhereParameterCondition Condition
        {
            get
            {
                if (ViewState["__Condition"] == null)
                    ViewState["__Condition"] = WhereParameterCondition.Equal;
                return (WhereParameterCondition)ViewState["__Condition"];
            }
            set { ViewState["__Condition"] = value; }
        }

        /// <summary>
        /// Gets or sets the logical operator to including parameter into where clause.
        /// </summary>
        public WhereParameterOperation Operation
        {
            get
            {
                if (ViewState["__Operation"] == null)
                    ViewState["__Operation"] = WhereParameterOperation.And;
                return (WhereParameterOperation)ViewState["__Operation"];
            }
            set { ViewState["__Operation"] = value; }
        }
        /// <summary>
        /// Gets or sets value indicating whether "Is Null" expressionshould be used whenever the parameter is empty.
        /// </summary>
        public bool UseIsNullIfEmpty
        {
            get
            {
                if (ViewState["__UseIsNullIfEmpty"] == null)
                    ViewState["__UseIsNullIfEmpty"] = false;
                return (bool)ViewState["__UseIsNullIfEmpty"];
            }
            set { ViewState["__UseIsNullIfEmpty"] = value; }
        }

        /// <summary>
        /// Prefix part of SQL expression like a opening bracket.
        /// </summary>
        public string Prefix
        {
            get
            {
                if (ViewState["__Prefix"] == null)
                    ViewState["__Prefix"] = String.Empty;
                return (string)ViewState["__Prefix"];
            }
            set { ViewState["__Prefix"] = value; }
        }

        /// <summary>
        /// Postfix part of SQL expression like a closing bracket.
        /// </summary>
        public string Postfix
        {
            get
            {
                if (ViewState["__Postfix"] == null)
                    ViewState["__Postfix"] = String.Empty;
                return (string)ViewState["__Postfix"];
            }
            set { ViewState["__Postfix"] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the source column mapped to the DataSet 
        /// and used for loading or returning the value.
        /// </summary>
        public string SourceColumn
        {
            get
            {
                if (ViewState["__SourceColumn"] == null)
                    ViewState["__SourceColumn"] = String.Empty;
                return (string)ViewState["__SourceColumn"];
            }
            set { ViewState["__SourceColumn"] = value; }
        }
        #endregion
    }
}