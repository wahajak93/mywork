//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.ComponentModel;

namespace InMotion.Web.Controls
{

    /// <summary>
    /// Represents a Image control, which displays image on a Web page.
    /// </summary>
    [ParseChildren(false, "Text")]
    public class MTImage : System.Web.UI.WebControls.Image, IMTControlWithValue, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTImage"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (_value != null) _value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        private string _format = String.Empty;

        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;

        /// <summary>
        /// Format that will be used to extract as well as place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private bool _valUpdated;

        private bool _textUpdated
        {
            get
            {
                return ViewState["_textUpdated"] != null && (bool)ViewState["_textUpdated"];
            }
            set
            {
                ViewState["_textUpdated"] = value;
            }
        }

        private object _value;
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                if (_value == null) return DefaultValue;
                return _value;
            }
            set
            {
                object val = value;

                val = TypeFactory.CreateTypedField(DataType, val, Format);
                _value = val;
                _valUpdated = true;

            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;

                val = TypeFactory.CreateTypedField(DataType, val, Format);
                __DefaultValue = val;

            }
        }

        private string _baseImageUrl;

        /// <summary>
        /// Gets or sets the URL of the image source for the <see cref="MTImage"/> control.
        /// </summary>
        public override string ImageUrl
        {
            get
            { 
                return Text; 
            }
            set 
            {
                Text = value;
                if (_baseImageUrl == null)
                    _baseImageUrl = value;
            }
        }

        public string BaseImageUrl
        {
            get
            {
                return _baseImageUrl;
            }
            set
            {
                _baseImageUrl = value;
            }
        }
        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }

        private string DisplayedText;
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public string Text
        {
            get
            {
                if (DisplayedText != null)
                {
                    return DisplayedText;
                }
                if (!_textUpdated)
                {
                    if (_valUpdated)
                        return ((IMTField)Value).ToString(_format);
                    else
                        return ((IMTField)DefaultValue).ToString(_format);
                }
                else
                {
                    return base.ImageUrl;
                }
            }
            set
            {
                _textUpdated = true;
                base.ImageUrl = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (!DesignMode)
            {
                object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
                if (val != null || val is IMTField && !((IMTField)val).IsNull)
                    Value = val;
            }
            OnBeforeShow(EventArgs.Empty);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            if (!_valUpdated)
            {
                if (ViewState["ImageUrl"] != null && string.IsNullOrEmpty(Text)) Text = (string)ViewState["ImageUrl"];
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Text = Page.Request.QueryString[ID];
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                    Value = Page.Request.Form[ID];
            }
        }


        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Page.IsPostBack || _valUpdated)
            {
                IMTField val = (IMTField)Value;
                if (_baseImageUrl != null) DisplayedText = _baseImageUrl;
                if (_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                    DisplayedText = DisplayedText.Replace("{0}", val.ToString(Format));
                }

                if (this.ImageUrl.IndexOf("{CCS_PathToMasterPage}") != -1 || this.ImageUrl.IndexOf("{page:pathToCurrentPage}") != -1)
                {
                    Control parent = Parent;
                    while (parent != null)
                    {
                        if (parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)parent).MasterPageFile))
                        {
                            this.ImageUrl =
                                DisplayedText =
                                this.ImageUrl.Replace("{CCS_PathToMasterPage}", ((MTPanel)parent).PathToMasterPage);
                            this.ImageUrl =
                                DisplayedText =
                                this.ImageUrl.Replace("{page:pathToCurrentPage}", ((MTPanel)parent).PathToMasterPage);
                            break;
                        }
                        else if (parent is MTPage && !string.IsNullOrEmpty(((MTPage)parent).MasterPageFile))
                        {
                            this.ImageUrl =
                                DisplayedText =
                                this.ImageUrl.Replace("{CCS_PathToMasterPage}", ((MTPage)parent).PathToMasterPage);
                            this.ImageUrl =
                                DisplayedText =
                                this.ImageUrl.Replace("{page:pathToCurrentPage}", ((MTPage)parent).PathToMasterPage);
                            break;
                        }
                        parent = parent.Parent;
                    }
                }
                this.ImageUrl =
                    DisplayedText =
                    this.ImageUrl.Replace("{page:pathToRoot}", Page.ResolveClientUrl("~/"));

                if (!val.IsNull) base.ImageUrl = Text;
                if (!string.IsNullOrEmpty(AlternateText))
                    AlternateText = InMotion.Web.Controls.ControlsHelper.ReplaceResource(AlternateText);
            }
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }
        #endregion

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }
    }
}