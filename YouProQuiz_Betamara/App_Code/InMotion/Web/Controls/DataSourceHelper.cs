//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Diagnostics;
using InMotion.Configuration;
using System.Reflection;

namespace InMotion.Web.Controls
{
    static internal class DataSourceHelper
    {
        internal static object GetFieldValue(string name, object dataItem)
        {
            if (dataItem is IDictionary)
            {
                IDictionary ds = (IDictionary)dataItem;
                return ds[name];
            }
            else if (dataItem is DataRowView)
            {
                DataRowView ds = (DataRowView)dataItem;
                return ds[name];
            }
            else if (dataItem is Directory.DummySource)
            {
                if (name == ((Directory.DummySource)dataItem).CategoryIdName)
                    return ((Directory.DummySource)dataItem).CategoryId;
                if (name == ((Directory.DummySource)dataItem).SubcategoryIdName)
                    return ((Directory.DummySource)dataItem).SubcategoryId;
            }
            else
            {
                try
                {
                    Type t = dataItem.GetType();
                    return t.InvokeMember(name, 
                        BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public, 
                        null, 
                        dataItem, 
                        null); 
                }
                catch (MissingFieldException e)
                {
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        Trace.TraceError("Unable to obtain value from the specified object.\n{0}", Environment.StackTrace);
                    throw new InvalidOperationException("Unable to obtain value from the specified object", e);
                }
            }
            return null;
        }

        internal static bool IsFieldNull(string name, object dataItem)
        {
            object val = GetFieldValue(name, dataItem);
            return val == null || val == System.DBNull.Value;
        }
    }
}
