//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for ControlsValidating event.
    /// </summary>
    public class ValidatingEventArgs : EventArgs
    {

        private bool _HasErrors;
        /// <summary>
        /// Specifies whether validation was succesfull.
        /// </summary>
        public bool HasErrors
        {
            get { return _HasErrors; }
            set { _HasErrors = value; }
        }
    }
}
