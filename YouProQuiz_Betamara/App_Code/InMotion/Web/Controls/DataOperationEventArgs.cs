//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Data;

namespace InMotion.Web.Controls
{

    /// <summary>
    /// Provides data for data operations events.
    /// </summary>
    public class DataOperationEventArgs : EventArgs
    {
        private bool _Cancel;

        /// <summary>
        /// Gets or sets value that indicating whether operation will be executed.
        /// </summary>
        public bool Cancel
        {
            get { return _Cancel; }
            set { _Cancel = value; }
        }

        private DataOperationType _operationType;
        /// <summary>
        /// Gets the <see cref="DataOperationType"/> of operation.
        /// </summary>
        public DataOperationType OperationType
        {
            get { return _operationType; }
        }
        private object _operationTesult;

        /// <summary>
        /// Gets or sets the result of operation execution. 
        /// </summary>
        public object OperationResult
        {
            get { return _operationTesult; }
            set { _operationTesult = value; }
        }

        private Exception _ex;
        /// <summary>
        /// Gets or sets the <see cref="Exception"/> bound with operation, if it is thrown during execution.
        /// </summary>
        public Exception OperationException
        {
            get { return _ex; }
            set { _ex = value; }
        }

        private DataCommand _dataCommand;
        /// <summary>
        /// Gets or sets the <see cref="DataCommand"/> for operation.
        /// </summary>
        public DataCommand Command
        {
            get { return _dataCommand; }
            set { _dataCommand = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="command">The <see cref="DataCommand"/> for operation.</param>
        public DataOperationEventArgs(DataOperationType type, object result, DataCommand command)
        {
            _operationType = type;
            OperationResult = result;
            Command = command;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="command">The <see cref="DataCommand"/> for operation.</param>
        /// <param name="ex">The <see cref="Exception"/> bound with operation, if it is thrown during execution.</param>
        public DataOperationEventArgs(DataOperationType type, object result, DataCommand command, Exception ex)
            : this(type, result, command)
        {
            _ex = ex;
        }
    }
}
