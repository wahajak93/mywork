//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a collection of Parameter and Parameter-derived 
    /// objects that are used by data source controls in advanced 
    /// data-binding scenarios.
    /// </summary>
    public class ParameterCollection : StateManagedCollection
    {
        /// <summary>
        /// Gets or sets the <see cref="Parameter"/> object in the collection.
        /// </summary>
        /// <param name="index">The index of the <see cref="Parameter"/> to retrieve from the collection.</param>
        /// <returns>The Parameter at the specified index in the collection.</returns>
        public virtual Parameter this[int index]
        {
            get
            {
                return (Parameter)((IList)this)[index];
            }
            set
            {
                ((IList)this).Insert(index, value);
            }

        }
        /// <summary>
        /// Gets or sets the <see cref="Parameter"/> object in the collection.
        /// </summary>
        /// <param name="name">The <see cref="Parameter.Name"/> of the <see cref="Parameter"/> to retrieve from the collection.</param>
        /// <returns>The Parameter with the specified name in the collection. 
        /// If the Parameter is not found in the collection, the indexer returns a null reference</returns>
        public virtual Parameter this[string name]
        {
            get
            {
                foreach (Parameter var in (IList)this)
                {
                    if (var.Name == name) return var;
                }
                return null;
            }
            set
            {
                foreach (Parameter var in (IList)this)
                {
                    if (var.Name == name)
                    {
                        IList i = (IList)this;
                        i[i.IndexOf(var)] = value;
                        return;
                    }
                }
                ((IList)this).Add(value);
            }
        }

        /// <summary>
        /// Appends the specified Parameter object to the end of the collection.
        /// </summary>
        /// <param name="value">The <see cref="Parameter"/> to append to the collection. </param>
        /// <returns>The index value of the added item.</returns>
        public int Add(Parameter value)
        {
            return ((IList)this).Add(value);
        }
        /// <summary>
        /// Creates a Parameter object with the specified name, 
        /// data type and default value, 
        /// and appends it to the end of the collection.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="type">The <see cref="DataType"/> of the parameter.</param>
        /// <param name="defaultValue">A object that serves as a default value for the parameter.</param>
        /// <returns>The index value of the added item.</returns>
        public int Add(string name, InMotion.Common.DataType type, object defaultValue)
        {
            Parameter p = new Parameter();
            p.Name = name;
            p.DataType = type;
            p.DefaultValue = defaultValue;
            return ((IList)this).Add(p);
        }
        /// <summary>
        /// Determines whether the ParameterCollection collection contains a specific value 
        /// </summary>
        /// <param name="value">The <see cref="Parameter"/> to locate in the <see cref="ParameterCollection"/>.</param>
        /// <returns>true if the object is found in the ParameterCollection; 
        /// otherwise, false. 
        /// If a null reference is passed for the value parameter, 
        /// false is returned.</returns>
        public bool Contains(Parameter value)
        {
            return ((IList)this).Contains(value);
        }
        /// <summary>
        /// Determines the index of a specified Parameter object in the ParameterCollection collection.
        /// </summary>
        /// <param name="value">The <see cref="Parameter"/> to locate in the <see cref="ParameterCollection"/>.</param>
        /// <returns>The index of parameter, if it is found in the collection; otherwise, -1.</returns>
        public int IndexOf(Parameter value)
        {
            return ((IList)this).IndexOf(value);
        }
        /// <summary>
        /// Inserts the specified <see cref="Parameter"/> object into the <see cref="ParameterCollection"/> collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which the <see cref="Parameter"/> is inserted.</param>
        /// <param name="value">The Parameter to insert.</param>
        public void Insert(int index, Parameter value)
        {
            ((IList)this).Insert(index, value);
        }

        /// <summary>
        /// Removes the specified Parameter object from the ParameterCollection collection.
        /// </summary>
        /// <param name="value">The <see cref="Parameter"/> to remove from the <see cref="ParameterCollection"/>.</param>
        public void Remove(Parameter value)
        {
            ((IList)this).Remove(value);
        }

        /// <summary>
        /// Removes the Parameter object at the specified index from the <see cref="ParameterCollection"/> collection.
        /// </summary>
        /// <param name="index">The index of the <see cref="Parameter"/> to remove.</param>
        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }
        /// <summary>
        /// Gets an array of Parameter types that the ParameterCollection collection can contain.
        /// </summary>
        /// <returns>An array of Parameter types that the ParameterCollection collection can contain.</returns>
        protected override Type[] GetKnownTypes()
        {
            return new Type[] { typeof(DataParameter) };
        }

        /// <summary>
        /// Creates an instance of a default Parameter object.
        /// </summary>
        /// <param name="index">The index of the type of Parameter to create from the ordered list of types returned by GetKnownTypes.</param>
        /// <returns>A default instance of a Parameter.</returns>
        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new DataParameter();
                default:
                    if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                        Trace.TraceError("Unknown Type.\n{0}", Environment.StackTrace);
                    throw new ArgumentOutOfRangeException("Unknown Type");
            }
        }

        /// <summary>
        /// Copies the elements of the ParameterCollections to an array of Parameter, starting at a particular Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ICollection. The Array must have zero-based indexing. </param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        public void CopyTo(Parameter[] array, int index)
        {
            CopyTo((Array)array, index);
        }

        /// <summary>
        /// Instructs an object contained by the collection to record its entire state to view state, rather than recording only change information.
        /// </summary>
        /// <param name="o">The <see cref="IStateManager"/> that should serialize itself completely.</param>
        protected override void SetDirtyObject(object o)
        {
            ((Parameter)o).SetDirty(true);
        }
    }
}
