//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// The exception that is thrown when a control configuration error has occurred.
    /// </summary>
    public class InvalidControlConfigurationException:SystemException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidControlConfigurationException"/> class.
        /// </summary>
        /// <param name="message">A message describing why this <see cref="InvalidControlConfigurationException"/> exception was thrown.</param>
        public InvalidControlConfigurationException(string message):base(message)
        {
        }
    }
}
