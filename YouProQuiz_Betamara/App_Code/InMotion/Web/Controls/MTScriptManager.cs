//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems

using System;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Manages ajax script libraries and script files, partial-page rendering, and client proxy class generation for Web and application services.
    /// </summary>
    public class MTScriptManager : ScriptManager
    {
        protected bool _wasInitialized;
        protected override void OnInit(System.EventArgs e)
        {
            if (Page.Items["smi"] == null)
            {
                base.OnInit(e);
                Page.Items["smi"] = true;
                _wasInitialized = true;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (_wasInitialized)
                base.Render(writer);
        }
    }
}
