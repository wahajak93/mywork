//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the interface to the all InMotion button controls.
    /// </summary>
    public interface IMTButtonControl:IMTControl
    {
        /// <summary>
        /// Gets or sets the type of url conversion.
        /// </summary>
        /// <value>One of the <see cref="UrlType"/> values.</value>
        UrlType ConvertUrl { get; set; }
        /// <summary>
        /// Gets or sets the value indicating whether control cause form validation.
        /// </summary>
        bool EnableValidation { get; set; }
        /// <summary>
        /// Gets or sets semicolon separated list of parameter's names to remove from preserved parameters collection.
        /// </summary>
        string RemoveParameters { get; set; }
        /// <summary>
        /// Gets or sets the name of return page.
        /// </summary>
        string ReturnPage { get; set; }
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        bool PerformRedirect { get; set; }
    }
}
