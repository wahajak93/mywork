//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.UI.Design.WebControls;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides a grouping mechanism for organizing controls.
    /// </summary>
    [Designer(typeof(ContainerControlDesigner)), ParseChildren(false)]
    public class MTPanel : PlaceHolder, IMTControlContainer, IClientScriptHelper
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTPanel"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        private bool _generateDiv;
        /// <summary>
        /// Gets or sets the value that indicating whether content of panel will be placed into div-tag.
        /// </summary>
        public bool GenerateDiv
        {
            get { return _generateDiv; }
            set { _generateDiv = value; }
        }

        private string _pathID;
        /// <summary>
        /// Gets or sets the id-attribute of div-tag.
        /// </summary>
        public string PathID
        {
            get { return _pathID; }
            set { _pathID = value; }
        }

        private string _masterPageFile;
        /// <summary>
        /// Gets or sets path to master page.
        /// </summary>
        public string MasterPageFile
        {
            get { return _masterPageFile; }
            set { _masterPageFile = value; }
        }
        private string _placeholderName;
        /// <summary>
        /// Gets or sets of associated placeholder.
        /// </summary>
        public string PlaceHolderName
        {
            get { return _placeholderName; }
            set { _placeholderName = value; }
        }
        private string _pathToMasterPage;

        public string PathToMasterPage
        {
            get 
            {
                if (_masterPageFile == null)
                    _pathToMasterPage = "";
                if (_pathToMasterPage == null)
                    _pathToMasterPage = Page.ResolveClientUrl(System.Text.RegularExpressions.Regex.Replace(this.MasterPageFile, @"\w*\.ascx$", ""));
                return _pathToMasterPage;
            }
        }
        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        /// <summary>
        /// Find all child controls of type <i>T</i>.
        /// </summary>
        /// <typeparam name="T">The type of controls to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <returns>the <see cref="ReadOnlyCollection&lt;T&gt;"/></returns>
        public ReadOnlyCollection<T> GetControls<T>()
            where T : IMTControl
        {
            List<T> result = new List<T>();
            ControlsHelper.PopulateControlCollection<T>(result, Controls);
            return new ReadOnlyCollection<T>(result);
        }

        /// <summary>
        /// Find a control of type <i>T</i> with specified <see cref="Control.ID"/>
        /// </summary>
        /// <typeparam name="T">The type of control to find. It must implement <see cref="IMTControl"/> interface.</typeparam>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>T</i>.</returns>
        public T GetControl<T>(string id)
            where T : Control
        {
            return (T)FindControl(id);
        }

        /// <summary>
        /// Find a control with specified <see cref="Control.ID"/>
        /// </summary>
        /// <param name="id">The id of the control to find.</param>
        /// <returns>Control of type <i>Control</i></returns>
        public Control GetControl(string id)
        {
            return GetControl<Control>(id);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnBeforeShow(EventArgs.Empty);
        }

        protected override void OnInit(EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.MasterPageFile))
                this.MasterPageFile = this.MasterPageFile.Replace("{CCS_PathToMasterPage}", ((MTPage)this.Page).PathToDesign);
            if (!string.IsNullOrEmpty(_masterPageFile))
            {
                Control masterPageControl = Page.LoadControl(_masterPageFile);
                Dictionary<string, Control> contentPlaceHolders = new Dictionary<string, Control>();
                foreach (Control c in Controls)
                    if (c is MTPanel && !string.IsNullOrEmpty(((MTPanel)c).PlaceHolderName))
                        contentPlaceHolders[((MTPanel)c).PlaceHolderName] = c;
                foreach (KeyValuePair<string, Control> pair in contentPlaceHolders)
                    if (masterPageControl.FindControl(pair.Key) != null)
                        masterPageControl.FindControl(pair.Key).Controls.Add(pair.Value);
                Controls.Clear();
                Controls.Add(masterPageControl);
            }
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);
            else if (Page is MTPage)
                ((MTPage)Page).RegisterClientControl(this);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (_generateDiv)
            {
                writer.WriteBeginTag("div");
                writer.WriteAttribute("id", string.IsNullOrEmpty(PathID) ? ClientID : PathID);
                writer.Write(HtmlTextWriter.TagRightChar);
                base.Render(writer);
                writer.WriteEndTag("div");
            }
            else
                base.Render(writer);
        }

        /// <summary>
        /// Register a control instance in client script array.
        /// </summary>
        /// <param name="control">The control instance to register.</param>
        public void RegisterClientControl(Control control)
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTControlsControls", String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTControlsControls", String.Format("'{0}','{1}'", control.ID, control.ClientID));
            }
        }
    }
}