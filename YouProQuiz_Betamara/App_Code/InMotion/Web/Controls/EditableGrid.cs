//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Data;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represent the Editable Grid control. The Editable Grid brings together the 
    /// functionality of grid and record forms by allowing the user to browse, insert, 
    /// update or delete multiple records from the same form. 
    /// </summary>
    [Designer(typeof(EditableGridDesigner))]
    public class EditableGrid : Grid, IForm, ISortable, INavigable, IErrorHandler, IErrorProducer, IValidator
    {

        private new System.Web.UI.ITemplate AlternatingItemTemplate
        {
            get
            {
                return base.AlternatingItemTemplate;
            }
            set
            {
                base.AlternatingItemTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the client JScript code for form onLoad event.
        /// </summary>
        public string ClientOnLoad
        {
            get
            {
                if (ViewState["__clientOnLoad"] == null)
                    ViewState["__clientOnLoad"] = String.Empty;
                return (string)ViewState["__clientOnLoad"];
            }
            set { ViewState["__clientOnLoad"] = value; }
        }

        /// <summary>
        /// Gets or sets the client JScript code for form onSubmit event.
        /// </summary>
        public string ClientOnSubmit
        {
            get
            {
                if (ViewState["__clientOnSubmit"] == null)
                    ViewState["__clientOnSubmit"] = String.Empty;
                return (string)ViewState["__clientOnSubmit"];
            }
            set { ViewState["__clientOnSubmit"] = value; }
        }

        private bool __IsSubmitted;
        /// <summary>
        /// Gets the value indicating whether Editable Grid is submitted by user.
        /// </summary>
        public bool IsSubmitted
        {
            get { return __IsSubmitted; }
        }

        private bool _allowInsert = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Insert operation for current user.
        /// </summary>
        public bool AllowInsert
        {
            get { return _allowInsert; }
            set { _allowInsert = value; }
        }

        private bool _allowUpdate = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allows Update operation for current user.
        /// </summary>
        public bool AllowUpdate
        {
            get { return _allowUpdate; }
            set { _allowUpdate = value; }
        }

        private bool _allowDelete = true;
        /// <summary>
        /// Gets or sets the value indicating whether form allow Delete operation for current user.
        /// </summary>
        public bool AllowDelete
        {
            get { return _allowDelete; }
            set { _allowDelete = value; }
        }

        private string __ErrorControl = "ErrorLabel";
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying form-level error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                return __ErrorControl;
            }
            set { __ErrorControl = value; }
        }

        private string __ErrorPanel = "Error";
        /// <summary>
        /// Gets or sets the ID of the <see cref="MTPanel"/> to be contain error message block.
        /// </summary>
        public string ErrorPanel
        {
            get
            {
                return __ErrorPanel;
            }
            set { __ErrorPanel = value; }
        }

        private string __RowErrorControl = "RowErrorLabel";
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying row-level error messages.
        /// </summary>
        public string RowErrorControl
        {
            get
            {
                return __RowErrorControl;
            }
            set { __RowErrorControl = value; }
        }

        private string __RowErrorPanel = "RowError";
        /// <summary>
        /// Gets or sets the ID of the <see cref="MTPanel"/> to be contain error message block.
        /// </summary>
        public string RowErrorPanel
        {
            get
            {
                return __RowErrorPanel;
            }
            set { __RowErrorPanel = value; }
        }

        private Url __RedirectUrl = new Url();
        /// <summary>
        /// Gets or sets the url to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public Url RedirectUrl
        {
            get
            {
                return __RedirectUrl;
            }
            set { __RedirectUrl = value; }
        }

        /// <summary>
        /// Gets or sets the page name to which the user is directed to after the form has been submitted successfully.
        /// </summary>
        public string ReturnPage
        {
            get
            {
                if (!DesignMode)
                    return base.ResolveClientUrl(RedirectUrl.ToString());
                else
                    return "";
            }
            set
            {
                RedirectUrl.Parameters.Clear();
                RedirectUrl.Address = value;
            }
        }

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get
            {
                return RedirectUrl.RemoveParameters;
            }
            set { RedirectUrl.RemoveParameters = value; }
        }

        private bool _PerformRedirect = true;
        /// <summary>
        /// Gets or sets the value indicating whether form will perform redirect after the successfull submit.
        /// </summary>
        public bool PerformRedirect
        {
            get { return _PerformRedirect; }
            set { _PerformRedirect = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get
            {
                return RedirectUrl.Type;
            }
            set { RedirectUrl.Type = value; }
        }

        private bool __EnableValidation = true;
        /// <summary>
        /// Gets or sets the value indicating whether form should validate user input.
        /// </summary>
        public bool EnableValidation
        {
            get
            {
                return __EnableValidation;
            }
            set { __EnableValidation = value; }
        }

        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get
            {
                return RedirectUrl.PreserveParameters;
            }
            set
            {
                RedirectUrl.PreserveParameters = value;
            }
        }

        private Dictionary<int, Dictionary<string, object>> CachedColumnsValues
        {
            get
            {
                if (ViewState["__CachedColumns"] == null)
                    ViewState["__CachedColumns"] = new Dictionary<int, Dictionary<string, object>>();
                return (Dictionary<int, Dictionary<string, object>>)ViewState["__CachedColumns"];
            }
        }

        private Collection<string> _cachedDataFields;
        /// <summary>
        /// Gets the list that contain cached data fields.
        /// </summary>
        public Collection<string> CachedDataFields
        {
            get
            {
                if (_cachedDataFields == null) _cachedDataFields = new Collection<string>();
                return _cachedDataFields;
            }
        }
        /// <summary>
        /// Gets or sets the number of empty rows.
        /// </summary>
        public int EmptyRows
        {
            get
            {
                if (ViewState["__EmptyRows"] == null)
                    ViewState["__EmptyRows"] = 1;
                return (int)ViewState["__EmptyRows"];
            }
            set { ViewState["__EmptyRows"] = value; }
        }

        /// <summary>
        /// Gets or sets the control ID used as a deletion indicator for the rows of the Editable Grid.
        /// </summary>
        public string DeleteControl
        {
            get
            {
                if (ViewState["__DeleteControl"] == null)
                    ViewState["__DeleteControl"] = String.Empty;
                return (string)ViewState["__DeleteControl"];
            }
            set { ViewState["__DeleteControl"] = value; }
        }

        private bool _needValidation = true;

        /// <summary>
        /// Perform the value validation of child controls.
        /// </summary>
        /// <returns><b>true</b> if the validation is succesfull; otherwise <b>false</b>.</returns>
        public virtual bool Validate()
        {
            if (EnableValidation)
            {
                ValidatingEventArgs e = new ValidatingEventArgs();
                OnControlsValidating(e);
            }
            return (RowErrors[CurrentItem.ItemIndex].Count == 0 && _errorControls[CurrentItem.ItemIndex].Count == 0);
        }

        /// <summary>
        /// Perform the value validation of child controls for currently proccesed row.
        /// </summary>
        /// <returns><b>true</b> if the validation is succesfull; otherwise <b>false</b>.</returns>
        public virtual bool ValidateRow()
        {
            if (EnableValidation)
            {
                ValidatingEventArgs e = new ValidatingEventArgs();
                OnControlsValidating(e);
            }
            return (RowErrors[CurrentItem.ItemIndex].Count == 0);
        }

        private void InstantiateErrorInControl(MTLabel errorInstance, ControlErrorCollection errorMessages)
        {
            errorInstance.ContentType = ContentType.Html;
            errorInstance.EnableViewState = false;

            foreach (string s in errorMessages)
            {
                if (errorInstance.Text.Length > 0)
                    errorInstance.Text += "<br/>";
                errorInstance.Text += s;
            }
        }

        /// <summary>
        /// Process the errors collection of child controls and form itself.
        /// </summary>
        /// <returns>false if any error found, otherwise true.</returns>
        protected virtual bool ProcessErrors()
        {
            bool isErrorProcessed = false;
            bool isRowErrorProcessed;

            for (int i = 0; i < Items.Count; i++)
            {
                isRowErrorProcessed = false;
                MTLabel ErrorInstance = (MTLabel)Items[i].FindControl(RowErrorControl);
                MTPanel ErrorPanelControls = (MTPanel)Items[i].FindControl(RowErrorPanel);

                if (_errorControls.ContainsKey(Items[i].ItemIndex))
                {
                    foreach (IErrorProducer ep in _errorControls[Items[i].ItemIndex])
                    {
                        if (ep.Errors.Count > 0)
                        {
                            if (ep.ErrorControl.Length > 0)
                            {
                                InstantiateErrorInControl((MTLabel)Items[i].FindControl(ep.ErrorControl), ep.Errors);
                            }
                            else if (ErrorInstance != null)
                            {
                                InstantiateErrorInControl(ErrorInstance, ep.Errors);
                            }
                            else
                            {
                                continue;
                            }

                            isRowErrorProcessed = true;
                        }
                    }
                }

                if (ErrorInstance != null && RowErrors[Items[i].ItemIndex].Count > 0)
                {
                    InstantiateErrorInControl(ErrorInstance, RowErrors[Items[i].ItemIndex]);
                    isRowErrorProcessed = true;
                }
                ErrorPanelControls.Visible = isRowErrorProcessed;
                isErrorProcessed = isErrorProcessed || isRowErrorProcessed;
            }

            MTLabel inst = GetControl<MTLabel>(ErrorControl);
            MTPanel pan = GetControl<MTPanel>(ErrorPanel);
            if (inst != null && Errors.Count > 0)
            {
                InstantiateErrorInControl(inst, Errors);
                pan.Visible = true;
                isErrorProcessed = true;
            }
            return !isErrorProcessed;
        }

        private Dictionary<int, Dictionary<string, object>> PKFields
        {
            get
            {
                if (ViewState["__PKFields"] == null)
                    ViewState["__PKFields"] = new Dictionary<int, Dictionary<string, object>>();
                return (Dictionary<int, Dictionary<string, object>>)ViewState["__PKFields"];
            }
        }

        private Dictionary<string, object> GetPKFields(int id)
        {
            if (PKFields.ContainsKey(id))
                return PKFields[id];
            else
                return null;
        }

        private List<int> NewRowIndexes
        {
            get
            {
                if (ViewState["__NewRowIndexes"] == null)
                    ViewState["__NewRowIndexes"] = new List<int>();
                return (List<int>)ViewState["__NewRowIndexes"];
            }
        }

        private ArrayList itemsArray;
        /// <summary>
        /// This method supports the InMotion Framework infrastructure and is not 
        /// intended to be used directly from your code. 
        /// Creates a control hierarchy, with or without the specified data source. 
        /// </summary>
        /// <param name="useDataSource">Indicates whether to use the specified data source.</param>
        protected override void CreateControlHierarchy(bool useDataSource)
        {
            if (itemsArray != null)
                itemsArray.Clear();
            else
                itemsArray = new ArrayList();
            NewRowIndexes.Clear();
            int itemsCount = -1;
            IEnumerable dataSource = null;
            Collection<PrimaryKeyInfo> _pkFields = null;
            if (!DesignMode)
            {
                if (!useDataSource)
                {
                    itemsCount = (int)ViewState["_!ItemCount"];
                    dataSource = new DummyDataSource(itemsCount);
                    itemsArray.Capacity = itemsCount;
                }
                else
                {
                    dataSource = GetData();
                    DataSourceView dv = GetDataSourceView();
                    if (dv is MTDataSourceView) ((MTDataSourceView)dv).Owner = this;
                    if (dv is MTDataSourceView && ((MTDataSourceView)dv).PrimaryKeys.Count > 0)
                        _pkFields = ((MTDataSourceView)dv).PrimaryKeys;
                    ICollection ds = dataSource as ICollection;
                    if (ds != null)
                        itemsArray.Capacity = ds.Count;
                    PKFields.Clear();
                }
            }

            itemsCount = 0;
            if (HeaderTemplate != null)
                this.CreateItem(-1, ListItemType.Header, useDataSource, null);
            RepeaterItem ri;
            int index = 0;
            if (dataSource != null)
            {
                foreach (object di in dataSource)
                {
                    if (index != 0 && SeparatorTemplate != null)
                    {
                        ri = this.CreateItem(index, ListItemType.Separator, useDataSource, null);
                        index++;
                    }
                    ri = this.CreateItem(index, ListItemType.Item, useDataSource, di);
                    if (_pkFields != null && useDataSource)
                    {
                        Dictionary<string, object> arr = new Dictionary<string, object>();
                        foreach (PrimaryKeyInfo pk in _pkFields)
                            arr.Add(pk.FieldName, DataSourceHelper.GetFieldValue(pk.FieldName, di));
                        PKFields.Add(CurrentItem.ItemIndex, arr);
                    }
                    if (CachedDataFields.Count != 0 && useDataSource)
                    {
                        Dictionary<string, object> arr = new Dictionary<string, object>();
                        foreach (string name in CachedDataFields)
                            arr.Add(name, DataSourceHelper.GetFieldValue(name, di));
                        CachedColumnsValues.Add(CurrentItem.ItemIndex, arr);
                    }
                    if ((!AllowDelete || Restricted && !UserRights.AllowDelete) && DeleteControl.Length > 0)
                    {
                        Control d = ri.FindControl(DeleteControl);
                        if (d != null)
                        {
                            Control p = ri.FindControl(DeleteControl + "_Panel");
                            if (p != null && p is MTPanel)
                                p.Visible = false;
                            d.Visible = false;
                        }
                        else
                        {
                            if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                                Trace.TraceError("Unable to find control specified in DeleteControl property. ID: {0}\n{1}", DeleteControl, Environment.StackTrace);
                            throw new InvalidControlConfigurationException(String.Format("Unable to find control specified in DeleteControl property. ID: {0}", DeleteControl));
                        }
                    }
                    index++;
                    itemsArray.Add(ri);
                    itemsCount++;
                }
            }
            if (DesignMode)
            {
                ri = this.CreateItem(index, ListItemType.Item, useDataSource, null);
                NewRowIndexes.Add(CurrentItem.ItemIndex);
                itemsArray.Add(ri);
            }
            else if (AllowInsert && (!Restricted || UserRights.AllowInsert))
            {
                for (int i = 0; i < EmptyRows; i++)
                {
                    ri = this.CreateItem(index, ListItemType.Item, useDataSource, null);
                    if (DeleteControl.Length > 0)
                    {
                        Control d = ri.FindControl(DeleteControl);
                        if (d != null)
                        {
                            Control p = ri.FindControl(DeleteControl + "_Panel");
                            if (p != null && p is MTPanel)
                                p.Visible = false;
                            d.Visible = false;
                        }
                        else
                        {
                            if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                                Trace.TraceError("Unable to find control specified in DeleteControl property. ID: {0}\n{1}", DeleteControl, Environment.StackTrace);
                            throw new InvalidControlConfigurationException(String.Format("Unable to find control specified in DeleteControl property. ID: {0}", DeleteControl));
                        }
                    }
                    NewRowIndexes.Add(CurrentItem.ItemIndex);
                    index++;
                    itemsArray.Add(ri);
                }
            }

            if (FooterTemplate != null)
                this.CreateItem(-1, ListItemType.Footer, useDataSource, null);

            if (useDataSource)
            {
                ViewState["_!ItemCount"] = itemsCount;
                ShowNoRecords();
                OnBeforeShow(EventArgs.Empty);
            }
        }


        private RepeaterItem CreateItem(int itemIndex, ListItemType itemType, bool dataBind, object dataItem)
        {
            RepeaterItem repeaterItem = CreateItem(itemIndex, itemType);
            RepeaterItemEventArgs repeaterItemEventArgs = new RepeaterItemEventArgs(repeaterItem);
            InitializeItem(repeaterItem);
            if (dataBind)
                repeaterItem.DataItem = dataItem;
            OnItemCreated(repeaterItemEventArgs);
            base.Controls.Add(repeaterItem);
            if (dataBind)
            {
                repeaterItem.DataBind();
                OnItemDataBound(repeaterItemEventArgs);
                repeaterItem.DataItem = null;
            }
            return repeaterItem;
        }
        RepeaterItemCollection itemsCollection;
        /// <summary>
        /// Gets a collection of <see cref="RepeaterItem"/> objects in the <see cref="EditableGrid"/>.
        /// </summary>
        /// <value>
        /// A collection of RepeaterItem objects. The default is an empty <see cref="RepeaterItemCollection"/>.
        /// </value>
        public override RepeaterItemCollection Items
        {
            get
            {
                if (itemsCollection == null)
                {
                    if (itemsArray == null)
                        EnsureChildControls();
                    itemsCollection = new RepeaterItemCollection(itemsArray);
                }
                return itemsCollection;
            }
        }

        /// <summary>
        /// Proccess the command from a child control and raises the ItemCommand event.
        /// </summary>
        /// <param name="e">The <see cref="RepeaterCommandEventArgs"/> that contains the event data.</param>
        protected override void OnItemCommand(RepeaterCommandEventArgs e)
        {
            object source = e.CommandSource;
            __IsSubmitted = true;
            if (source is IButtonControl)
            {
                if (source is IMTButtonControl)
                {
                    IMTButtonControl b = (IMTButtonControl)source;
                    this.EnableValidation = b.EnableValidation;
                    PerformRedirect = PerformRedirect && b.PerformRedirect;
                    if (PerformRedirect)
                    {
                        if (b.ReturnPage.Length > 0)
                            RedirectUrl.Address = b.ReturnPage;
                        if (b.ConvertUrl != UrlType.None)
                            RedirectUrl.Type = b.ConvertUrl;
                        if (b.RemoveParameters.Length > 0)
                            RedirectUrl.RemoveParameters = b.RemoveParameters;
                        if (RedirectUrl.Address.Length == 0)
                            RedirectUrl.Address = Page.Request.CurrentExecutionFilePath;
                    }
                }

                switch (((IButtonControl)source).CommandName.ToLower(CultureInfo.CurrentCulture))
                {
                    case "submit":
                        Update();
                        break;
                    case "cancel":
                        Cancel();
                        break;
                    default:
                        break;
                }
            }
            base.OnItemCommand(e);
        }

        /// <summary>
        /// Perform the cancel operation and redirect page using the defined redirect url.
        /// </summary>
        public void Cancel()
        {
            if (PerformRedirect)
                Page.Response.Redirect(base.ResolveClientUrl(RedirectUrl.ToString()), false);
            else
                DataBind();
        }

        private ArrayList DataItemsHolder
        {
            get
            {
                if (ViewState["__DataItemsHolder"] == null)
                    ViewState["__DataItemsHolder"] = new ArrayList();
                return (ArrayList)ViewState["__DataItemsHolder"];
            }
        }

        private void SaveDataItem(int index)
        {
            for (int i = DataItemsHolder.Count - 1; i < index; i++)
                DataItemsHolder.Add(null);

            if (DataItem.Value is DataRowView)
            {
                DataRow dr = ((DataRowView)DataItem.Value).Row;
                DataTable dt = dr.Table.Clone();
                DataRow dr2 = dt.NewRow();
                dr2.ItemArray = (object[])dr.ItemArray.Clone();
                dt.Rows.Add(dr2);
                DataItemsHolder[index] = dt;
            }
        }

        private void LoadDataItem(int index)
        {
            object val = null;
            if (index >= 0 && index < DataItemsHolder.Count)
            {
                if (DataItemsHolder[index] is DataTable)
                {
                    DataTable dt = (DataTable)DataItemsHolder[index];
                    val = dt.DefaultView[0];
                }
            }
            _dataItem = new DataItem(val);
        }

        /// <summary>
        /// Raises the BeforeShowRow event. This allows you to provide a custom handler for the event.
        /// </summary>
        /// <param name="e">A <see cref="RepeaterItemEventArgs"/> that contains event data.</param>
        protected override void OnItemDataBound(RepeaterItemEventArgs e)
        {
            base.OnItemDataBound(e);
            SaveDataItem(e.Item.ItemIndex);
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Page.IsPostBack)
            {
                ViewState["__DataItemsHolder"] = null;
            }
        }

        public bool ValidateChilds()
        {
            _needValidation = false;
            bool validateResult = true;
            for (int i = 0; i < Items.Count; i++)
            {
                __currentItem = Items[i];
                LoadDataItem(Items[i].ItemIndex);
                if (Items[i].ItemType != ListItemType.Item || !Items[i].Visible) continue;
                List<Control> sourceControls = new List<Control>();
                getEditableControls(Items[i], sourceControls);
                CheckBox d = Items[i].FindControl(DeleteControl) as CheckBox;
                if (NewRowIndexes.Contains(Items[i].ItemIndex) && IsNewRowEmpty(sourceControls) ||
                    (d != null && d.Checked)) continue;
                validateResult &= ValidateRow() && (!_errorControls.ContainsKey(Items[i].ItemIndex) || _errorControls[Items[i].ItemIndex].Count == 0);
                ValidatingEventArgs arg = new ValidatingEventArgs();
                arg.HasErrors = !validateResult;
                OnRowValidating(arg);
            }

            OnValidating(new ValidatingEventArgs());
            return ProcessErrors();
        }

        /// <summary>
        /// Performs the update operation.
        /// </summary>
        public void Update()
        {
            if (Restricted && UserRights.None) return;
            DataSourceView dv = GetDataSourceView();
            if (dv is MTDataSourceView)
                ((MTDataSourceView)dv).Owner = this;
            OnBeforeSubmit(EventArgs.Empty);

            if (_needValidation && !ValidateChilds())
                return;

            for (int i = 0; i < Items.Count; i++)
            {
                __currentItem = Items[i];
                LoadDataItem(Items[i].ItemIndex);
                if (Items[i].ItemType != ListItemType.Item || !Items[i].Visible) continue;
                List<Control> sourceControls = new List<Control>();
                getEditableControls(Items[i], sourceControls);
                if (NewRowIndexes.Contains(Items[i].ItemIndex))
                {
                    if (!IsNewRowEmpty(sourceControls))
                        InsertItem(dv, new ReadOnlyCollection<Control>(sourceControls));
                }
                else
                {
                    if (CachedDataFields.Count != 0)
                        _dataItem = new DataItem(CachedColumnsValues[Items[i].ItemIndex]);
                    Control d = Items[i].FindControl(DeleteControl);
                    if (d != null && d is CheckBox && ((CheckBox)d).Checked)
                        DeleteItem(dv, new ReadOnlyCollection<Control>(sourceControls), GetPKFields(Items[i].ItemIndex));
                    else
                        UpdateItem(dv, new ReadOnlyCollection<Control>(sourceControls), GetPKFields(Items[i].ItemIndex));
                }
            }
            DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Batch, null, RedirectUrl, null);
            OnAfterSubmit(args);
            if (!ProcessErrors())
                return;
            if (args.PerformRedirect && PerformRedirect)
                Page.Response.Redirect(base.ResolveClientUrl(RedirectUrl.ToString()), false);
            else
                DataBind();
        }

        /// <summary>
        /// Performs the inserting of row data into database.
        /// </summary>
        /// <param name="dv"><see cref="DataSourceView"/> that is used for inserting.</param>
        /// <param name="sourceControls">The list of row controls which values should be inserted.</param>
        protected virtual void InsertItem(DataSourceView dv, ReadOnlyCollection<Control> sourceControls)
        {
            if (dv == null || !AllowInsert || Restricted && !UserRights.AllowInsert) return;
            if (!dv.CanInsert)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Insert operation does not supported by the EditableGrid {0}", ID);
                return;
            }
            //OnBeforeInsert(new DataOperationEventArgs(DataOperationType.Insert));
            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (Control c in sourceControls)
            {
                IMTEditableControl ce = c as IMTEditableControl;
                if (ce != null && ce.SourceType == SourceType.DatabaseColumn && ce.Source.Length > 0)
                {
                    if (dv is MTDataSourceView)
                    {
                        TypedValue val = ControlsHelper.EvaluateTypedValue(ce.DBValue, ((MTDataSourceView)dv).GetConnection());
                        val.IsEmpty = ce.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || ce.IsOmitEmptyValue);
                        values.Add(ce.Source, val);
                    }
                    else
                    {
                        values.Add(ce.Source, ce.Value);
                    }
                }
                else
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (dv is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }
            }

            dv.Insert(values, InsertOperationCallback);
        }


        /// <summary>
        /// Returns the value which indicating whethere row is empty (contains no data).
        /// </summary>
        /// <param name="ri">The RepeaterItem to check</param>
        /// <returns>true, if row is empty; otherwise, false.</returns>
        public bool IsRowEmpty(RepeaterItem ri)
        {
            List<Control> sourceControls = new List<Control>();
            getEditableControls(ri, sourceControls);
            if (NewRowIndexes.Contains(ri.ItemIndex))
            {
                return IsNewRowEmpty(sourceControls);
            }
            return false;
        }

        private bool IsNewRowEmpty(IList sourceControls)
        {
            bool valuesChanged = false;
            foreach (Control c in sourceControls)
            {
                if (c is IMTEditableControl && !(c is CheckBox))
                    valuesChanged = !((IMTField)((IMTEditableControl)c).Value).IsNull || valuesChanged;
                else
                {
                    TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                    if (val.Value is IMTField)
                        valuesChanged = !((IMTField)val.Value).IsNull || valuesChanged;
                    else
                    {
                        string temp = val.Value as string;
                        valuesChanged = !String.IsNullOrEmpty(temp) || valuesChanged;
                    }
                }
            }
            return !valuesChanged;
        }

        private bool InsertOperationCallback(int affectedRecords, Exception ex)
        {
            /*DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Insert, affectedRecords, RedirectUrl, ex);
            OnAfterInsert(args);*/
            if (ex != null)
            {
                RowErrors[CurrentItem.ItemIndex].Add(ex.Message);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Insert operation fails for the Editable Grid {0}.\n{1}", ID, ex);
            }
            else 
            {
                int i;
                for (i=0;i<CurrentItem.Controls.Count;i++)
                    if (CurrentItem.Controls[i] is MTFileUpload)
                    {
                        ((MTFileUpload)CurrentItem.Controls[i]).SaveFile();
                    }
            }

            return true;
        }

        /// <summary>
        /// Performs the updating of row data in database.
        /// </summary>
        /// <param name="dv"><see cref="DataSourceView"/> that is used for updating.</param>
        /// <param name="sourceControls">The list of row controls which values should be updated.</param>
        /// <param name="primaryKeys">The list of primary key values for current row.</param>
        protected virtual void UpdateItem(DataSourceView dv, ReadOnlyCollection<Control> sourceControls, IDictionary primaryKeys)
        {
            if (dv == null || !AllowUpdate || Restricted && !UserRights.AllowUpdate) return;
            if (!dv.CanUpdate)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Update operation does not supported the Editable Grid {0}.\n{1}", ID, Environment.StackTrace);
                return;
            }
            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (Control c in sourceControls)
            {
                IMTEditableControl ce = c as IMTEditableControl;
                if (ce != null && ce.SourceType == SourceType.DatabaseColumn && ce.Source.Length > 0)
                {
                    if (dv is MTDataSourceView)
                    {
                        TypedValue val = ControlsHelper.EvaluateTypedValue(ce.DBValue, ((MTDataSourceView)dv).GetConnection());
                        val.IsEmpty = ce.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || ce.IsOmitEmptyValue);
                        values.Add(ce.Source, val);
                    }
                    else
                    {
                        values.Add(ce.Source, ce.Value);
                    }
                }
                else
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (dv is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }

            }
            dv.Update(primaryKeys, values, null, UpdateOperationCallback);
        }

        private bool UpdateOperationCallback(int affectedRecords, Exception ex)
        {
            /*DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Update, affectedRecords, RedirectUrl, ex);
            OnAfterUpdate(args);*/
            if (ex != null)
            {
                RowErrors[CurrentItem.ItemIndex].Add(ex.Message);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Update operation fails for the Editable Grid {0}.\n{1}", ID, ex);
            }
            else
            {
                int i;
                for (i = 0; i < CurrentItem.Controls.Count; i++)
                    if (CurrentItem.Controls[i] is MTFileUpload)
                    {
                        ((MTFileUpload)CurrentItem.Controls[i]).SaveFile();
                    }
            }
            return true;
        }

        /// <summary>
        /// Performs the deleting of row data in database.
        /// </summary>
        /// <param name="dv"><see cref="DataSourceView"/> that is used for deleting.</param>
        /// <param name="sourceControls">The list of row controls.</param>
        /// <param name="primaryKeys">The list of primary key values for current row.</param>
        protected virtual void DeleteItem(DataSourceView dv, ReadOnlyCollection<Control> sourceControls, IDictionary primaryKeys)
        {
            if (!AllowDelete || Restricted && !UserRights.AllowDelete) return;
            if (!dv.CanDelete)
            {
                if (AppConfig.IsMTErrorHandlerUse("uncritical"))
                    Trace.TraceError("Delete operation does not supported the Editable Grid {0}.\n{1}", ID, Environment.StackTrace);
                return;
            }
            //OnBeforeDelete(new DataOperationEventArgs(DataOperationType.Delete));
            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (Control c in sourceControls)
            {
                IMTEditableControl ce = c as IMTEditableControl;
                if (ce != null && ce.SourceType == SourceType.DatabaseColumn && ce.Source.Length > 0)
                {
                    if (dv is MTDataSourceView)
                    {
                        TypedValue val = ControlsHelper.EvaluateTypedValue(ce.DBValue, ((MTDataSourceView)dv).GetConnection());
                        val.IsEmpty = ce.IsValueNotPassed && (AppConfig.ExcludeMissingDataParameters || ce.IsOmitEmptyValue);
                        values.Add(ce.Source, val);
                    }
                    else
                    {
                        values.Add(ce.Source, ce.Value);
                    }
                }
                else
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        if (dv is MTDataSourceView)
                        {
                            TypedValue val = ControlsHelper.EvaluateTypedValue(c);
                            values.Add(fieldAttr, val);
                        }
                        else
                        {
                            values.Add(fieldAttr, ControlsHelper.GetValue(c));
                        }
                }
            }
            dv.Delete(primaryKeys, null, DeleteOperationCallback);
        }

        private bool DeleteOperationCallback(int affectedRecords, Exception ex)
        {
            /*DataOperationCompletingEventArgs args = new DataOperationCompletingEventArgs(DataOperationType.Delete, affectedRecords, RedirectUrl, ex);
            OnAfterDelete(args);*/
            if (ex != null)
            {
                RowErrors[CurrentItem.ItemIndex].Add(ex.Message);
                if (AppConfig.IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Delete operation fails for the EditableGrid {0}.\n{1}", ID, ex);
            }
            return true;
        }

        private void getEditableControls(Control item, List<Control> list)
        {
            foreach (Control c in item.Controls)
            {
                if (c.Controls.Count > 0)
                    getEditableControls(c, list);
                if (c is IMTEditableControl)
                    list.Add(c);
                else
                {
                    String fieldAttr = ControlsHelper.GetMTField(c);
                    if (fieldAttr != null)
                        list.Add(c);
                }
            }
        }

        #region IErrorHandler Members
        private Dictionary<int, List<IErrorProducer>> _errorControls = new Dictionary<int, List<IErrorProducer>>();
        /// <summary>
        /// Performs an error registration.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> of error.</param>
        /// <param name="innerException">The initial exception, if any.</param>
        public void RegisterError(IErrorProducer control, Exception innerException)
        {
            //get the container of the control
            RepeaterItem container = GetControlContainer(control as Control);
            if (!_errorControls.ContainsKey(container.ItemIndex))
                _errorControls.Add(container.ItemIndex, new List<IErrorProducer>());
            if (!_errorControls[container.ItemIndex].Contains(control))
                _errorControls[container.ItemIndex].Add(control);
        }

        /// <summary>
        /// Performs an error removal.
        /// </summary>
        /// <param name="control">The <see cref="IErrorProducer"/> of error.</param>
        public void RemoveError(IErrorProducer control)
        {
            //get the container of the control
            RepeaterItem container = GetControlContainer(control as Control);
            if (_errorControls.ContainsKey(container.ItemIndex))
                _errorControls[container.ItemIndex].Remove(control);
        }

        private static RepeaterItem GetControlContainer(Control control)
        {
            Control c = control.NamingContainer;
            while (!(c is RepeaterItem))
                c = c.NamingContainer;
            return c as RepeaterItem;
        }
        #endregion

        #region IErrorProducer Members

        private Dictionary<int, ControlErrorCollection> __rowErrors;
        /// <summary>
        /// Gets the <see cref="Dictionary&lt;TKey,TValue&gt;"/> that contain Lists&gt;string&lt; of row level errors messages.
        /// </summary>
        public Dictionary<int, ControlErrorCollection> RowErrors
        {
            get
            {
                if (__rowErrors == null)
                {
                    __rowErrors = new Dictionary<int, ControlErrorCollection>(Items.Count);
                    for (int i = 0; i < Items.Count; i++)
                    {
                        __rowErrors.Add(Items[i].ItemIndex, new ControlErrorCollection());
                    }
                }
                return __rowErrors;
            }
        }

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the <see cref="Collection&lt;T&gt;"/> that contain list of form level errors messages.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null) _errors = new ControlErrorCollection();
                return _errors;
            }
        }

        #endregion

        /// <summary>
        /// Registers a JavaScript array declaration with the page object.
        /// </summary>
        protected override void RegisterArrayDeclaration()
        {
            if (Page is MTPage)
            {
                ((MTPage)Page).MTClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Items.Count, ClientOnLoad, ClientOnSubmit));
            }
            else
            {
                Page.ClientScript.RegisterArrayDeclaration("MTForms", String.Format("'{0}','{1}','{2}','{3}'", ID, Items.Count, ClientOnLoad, ClientOnSubmit));
            }
        }

        /// <summary>
        /// Occurs when a form performs a common validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;

        /// <summary>
        /// Raises the <see cref="Validating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (Validating != null)
                Validating(this, e);
        }

        /// <summary>
        /// Occurs when a form validated row controls.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> RowValidating;

        /// <summary>
        /// Raises the <see cref="RowValidating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnRowValidating(ValidatingEventArgs e)
        {
            if (RowValidating != null)
                RowValidating(this, e);
        }

        /// <summary>
        /// Raises the <see cref="ControlsValidating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnControlsValidating(ValidatingEventArgs e)
        {
            EventHandler<ValidatingEventArgs> h = (EventHandler<ValidatingEventArgs>)_rowEvents[CurrentItem];
            if (h != null)
                h(this, e);
        }

        EventHandlerList _rowEvents = new EventHandlerList();
        /// <summary>
        /// Occurs when a form calls a child controls validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> ControlsValidating
        {
            add
            {
                _rowEvents.AddHandler(CurrentItem, value);
            }
            remove
            {
                _rowEvents.RemoveHandler(CurrentItem, value);
            }
        }

        /// <summary>
        /// Occurs before the current data operation is proccesed.
        /// </summary>
        public event EventHandler<EventArgs> BeforeSubmit;

        /// <summary>
        /// Occurs after the current data operation is proccesed.
        /// </summary>
        public event EventHandler<DataOperationCompletingEventArgs> AfterSubmit;

        /// <summary>
        /// Raises the <see cref="BeforeSubmit"/> event.
        /// </summary>
        /// <param name="e">A <see cref="EventArgs"/> that contains event data.</param>
        protected virtual void OnBeforeSubmit(EventArgs e)
        {
            if (BeforeSubmit != null)
                BeforeSubmit(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterSubmit"/> event.
        /// </summary>
        /// <param name="e">A <see cref="DataOperationCompletingEventArgs"/> that contains event data.</param>
        protected virtual void OnAfterSubmit(DataOperationCompletingEventArgs e)
        {
            if (AfterSubmit != null)
                AfterSubmit(this, e);
        }

        /// <summary>
        /// Performs the validating uniqueness of supplied <see cref="IMTEditableControl"/> value against the database.
        /// </summary>
        /// <param name="control">The <see cref="IMTEditableControl"/> that value should be validated.</param>
        /// <returns>true if the validation was successfull; otherwise false.</returns>
        public bool ValidateUnique(IMTEditableControl control)
        {
            DataSourceView ds = GetDataSourceView();
            if (!(ds is MTDataSourceView)) return true;
            MTDataSourceView dv = ds as MTDataSourceView;
            if (!dv.CanValidateUnique) return true;
            RepeaterItem container = GetControlContainer((Control)control);
            string controlDbValue = control.DBValue.ToString();
            Collection<object> pkKeys = new Collection<object>();
            pkKeys.Add(GetPKFields(container.ItemIndex));
            for (int i = container.ItemIndex - 1; i >= 0; i--)
            {
                if (!Items[i].Visible) continue;
                CheckBox d = Items[i].FindControl(DeleteControl) as CheckBox;
                if (d != null && d.Checked)
                {
                    pkKeys.Add(GetPKFields(i));
                    continue;
                }
                IMTEditableControl instance = (IMTEditableControl)Items[i].FindControl(((Control)control).ID);
                if (instance != null && instance.DBValue.ToString() == controlDbValue) return false;
            }
            TypedValue val = ControlsHelper.EvaluateTypedValue(control.DBValue, ((MTDataSourceView)dv).GetConnection());
            return dv.ValidateUnique(pkKeys, control.Source, val);
        }

        /// <summary>
        /// Manages the visibility of the "No Records" panel.
        /// </summary>
        protected override void ShowNoRecords()
        {
            if (NewRowIndexes.Count == 0)
                base.ShowNoRecords();
        }
    }

    /// <summary>
    /// Provides a control designer class for extending the design-mode behavior of a <see cref="EditableGrid"/> control.
    /// </summary>
    class EditableGridDesigner : GridDesigner { }
}