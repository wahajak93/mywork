//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Defines the general interface to a InMotion control.
    /// </summary>
    public interface IMTControl
    {
        /// <summary>
        /// Occurs when the control is above to render to its containing Page control.
        /// </summary>
        event EventHandler<EventArgs> BeforeShow;
        /// <summary>
        /// Gets the reference to the parent <see cref="IForm"/> control.
        /// </summary>
        IForm OwnerForm { get;}
    }
}