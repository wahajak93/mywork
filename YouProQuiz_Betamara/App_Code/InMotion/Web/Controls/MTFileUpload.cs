//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using InMotion.Common;
using System.Collections.ObjectModel;
using System.Diagnostics;
using InMotion.Configuration;
using System.ComponentModel;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Displays a control that allow users to select a file to upload to the server.
    /// </summary>
    public class MTFileUpload : WebControl, INamingContainer, IMTControl, IErrorProducer, IMTEditableControl, IMTAttributeAccessor
    {
        public enum FileAllocation { InputControl, TemporaryFolder, FileFolder, None }
        private System.Web.UI.WebControls.CheckBox delControl = new System.Web.UI.WebControls.CheckBox();
        private System.Web.UI.WebControls.Label fileNameLabel = new System.Web.UI.WebControls.Label();
        private System.Web.UI.WebControls.Label fileSizeLabel = new System.Web.UI.WebControls.Label();
        private System.Web.UI.LiteralControl literal1 = new System.Web.UI.LiteralControl("&nbsp;");
        private System.Web.UI.LiteralControl literal2 = new System.Web.UI.LiteralControl("&nbsp;");
        private System.Web.UI.LiteralControl InfoTextHolder = new System.Web.UI.LiteralControl();
        private System.Web.UI.WebControls.Label delLabel = new System.Web.UI.WebControls.Label();
        private HtmlInputFile file = new HtmlInputFile();

        #region Properties

        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        /// <summary>
        /// Gets or sets a semicolon separated list of file masks for files that the user is allowed to upload e.g. *.jpg; *.png; *.gif; *.txt. You can enter an asterisk (*) to allow all file types to be uploaded.
        /// </summary>
        public string AllowedFileMasks
        {
            get
            {
                if (ViewState["_allowedMasks"] == null)
                    return "*";
                else
                    return ViewState["_allowedMasks"] as string;
            }
            set
            {
                if (String.IsNullOrEmpty(value))
                    ViewState["_allowedMasks"] = "*";
                else
                    ViewState["_allowedMasks"] = value;
            }
        }

        private string __ErrorControl;
        /// <summary>
        /// Gets or sets the ID of the control to be used for displaying error messages.
        /// </summary>
        public string ErrorControl
        {
            get
            {
                if (__ErrorControl == null)
                    __ErrorControl = "";
                return __ErrorControl;
            }
            set { __ErrorControl = value; }
        }

        private ControlErrorCollection _errors;
        /// <summary>
        /// Gets the List&lt;string&gt; which is used to collect error messages generated during the control's execution.
        /// </summary>
        public ControlErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new ControlErrorCollection();
                    _errors.ErrorAdded += new EventHandler<EventArgs>(OnErrorAdded);
                    _errors.ErrorRemoved += new EventHandler<EventArgs>(OnErrorRemoved);
                }
                return _errors;

            }
        }

        private void OnErrorAdded(object sender, EventArgs e)
        {
            if (OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RegisterError(this, null);
        }

        private void OnErrorRemoved(object sender, EventArgs e)
        {
            if (Errors.Count == 0 && OwnerForm is IErrorHandler)
                ((IErrorHandler)OwnerForm).RemoveError(this);
        }



        /// <summary>
        /// Gets or sets a semicolon separated list of file masks for files that the user is not allowed to upload e.g. *.exe; *.dll; *.bat.
        /// </summary>
        public string DisallowedFileMasks
        {
            get
            {
                if (ViewState["_disallowedMasks"] == null)
                    return "";
                else
                    return ViewState["_disallowedMasks"] as string;
            }
            set
            {
                ViewState["_disallowedMasks"] = value;
            }
        }

        private void ValidateFileFolder(string value)
        {
            if (DesignMode) return;
            try
            {
                if (!System.IO.Directory.Exists(value))
                    throw (new DirectoryNotFoundException(String.Format(Common.Resources.ResManager.GetString("CCS_FilesFolderNotFound"), ControlsHelper.ReplaceResource(Caption))));
                FileIOPermission fp = new FileIOPermission(FileIOPermissionAccess.Write, value);
                fp.Demand();
                ViewState["_fileFolder"] = value;
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                if (OwnerForm is IErrorHandler)
                {
                    Errors.Add(e.Message);
                    ((IErrorHandler)OwnerForm).RegisterError(this, e);
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets or sets a file system path to a folder on the web server machine where the final uploaded files should be stored. The path specified can be an absolute path e.g. c:\Inetpub\files\images or a relative path e.g. .\files\images
        /// </summary>
        public string FileFolder
        {
            get
            {
                if (DesignMode)
                    return "";
                else if (String.IsNullOrEmpty(ViewState["_fileFolder"].ToString()))
                    return MapPathSecure(TemplateSourceDirectory).TrimEnd(new char[] { '\\', '/' });
                else
                    return ViewState["_fileFolder"].ToString().TrimEnd(new char[] { '\\', '/' });
            }
            set
            {
                string result = GetRootedPath(value, false);
                if (Parent != null) ValidateFileFolder(result);
                if (result != null) ViewState["_fileFolder"] = result;
            }
        }

        private void ValidateTemporaryFolder(string value)
        {
            if (DesignMode) return;
            try
            {
                if (!System.IO.Directory.Exists(value))
                    throw (new DirectoryNotFoundException(String.Format(Common.Resources.ResManager.GetString("CCS_TempFolderNotFound"), ControlsHelper.ReplaceResource(Caption))));
                FileIOPermission fp = new FileIOPermission(FileIOPermissionAccess.Write, value);
                fp.Demand();
                _temporaryFolder = value;
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                if (OwnerForm is IErrorHandler)
                {
                    Errors.Add(e.Message);
                    ((IErrorHandler)OwnerForm).RegisterError(this, e);
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw;
                }
            }
            catch (System.Security.SecurityException e)
            {
                if (OwnerForm is IErrorHandler)
                {
                    Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_TempInsufficientPermissions"), ControlsHelper.ReplaceResource(Caption)));
                    ((IErrorHandler)OwnerForm).RegisterError(this, e);
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw;
                }
            }
        }

        private string _temporaryFolder;
        /// <summary>
        /// Gets or sets file system path to a folder on the web server machine where the upload component can temporarily place files while they are being processed. The path specified can be an absolute path e.g. c:\Inetpub\files\temp or a relative path e.g. .\files\temp
        /// </summary>
        public string TemporaryFolder
        {
            get
            {
                if (ViewState["_temporaryFolder"] == null)
                    return "";
                else
                    return ViewState["_temporaryFolder"].ToString().TrimEnd(new char[] { '\\', '/' });
            }
            set
            {
                string result = GetRootedPath(value, true);
                if (Parent != null) ValidateTemporaryFolder(result);

                if (result != null)
                {
                    _temporaryFolder = result;
                    ViewState["_temporaryFolder"] = _temporaryFolder;
                }
            }
        }

        private string GetRootedPath(string path, bool useEnvironmentVars)
        {
            if (path.StartsWith("%") && useEnvironmentVars)
                path = Environment.GetEnvironmentVariable(path.TrimStart(new char[] { '%' }));
            else if (!System.IO.Path.IsPathRooted(path))
                path = MapPathSecure(path);

            return path ?? String.Empty;
        }

        /// <summary>
        /// Gets or sets the size limit in bytes of the files that the user can upload. Files larger than the limit will not be uploaded.
        /// </summary>
        public Int64 FileSizeLimit
        {
            get
            {
                if (ViewState["_fileSizeLimit"] == null)
                    return -1;
                else
                    return (long)ViewState["_fileSizeLimit"];
            }
            set
            {
                ViewState["_fileSizeLimit"] = value;
            }
        }

        private bool _required;
        /// <summary>
        /// Gets or sets the value that indicating whether control must have non-empty value.
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }

        /// <summary>
        /// Gets the size in bytes of uploaded file.
        /// </summary>
        [Browsable(false), DefaultValue(0)]
        public long FileSize
        {
            get
            {
                if (FileAllocation.FileFolder == Allocation)
                {
                    return new FileInfo(FileFolder + "\\" + Text).Length;
                }
                if (FileAllocation.TemporaryFolder == Allocation)
                {
                    return new FileInfo(TemporaryFolder + "\\" + Text).Length;
                }
                else
                {
                    EnsureChildControls();
                    return file.PostedFile.ContentLength;
                }
            }
        }

        /// <summary>
        /// Gets the original file name (without timestamp).
        /// </summary>
        public string OriginalFileName
        {
            get
            {
                if (FileAllocation.FileFolder == Allocation)
                {
                    return Text.Remove(0, Text.IndexOf(".") + 1);
                }
                else
                {
                    return ViewState["originalFileName"] as String;
                }
            }
            set
            {
                ViewState["originalFileName"] = value;
            }
        }

        /// <summary>
        /// Gets the name of the uploaded file.
        /// </summary>
        public string FileName
        {
            get
            {
                if (Allocation == FileAllocation.InputControl)
                {
                    EnsureChildControls();
                    return file.PostedFile.FileName;
                }
                if (Allocation == FileAllocation.TemporaryFolder || Allocation == FileAllocation.FileFolder)
                {
                    return ViewState["fileName"] as String;
                }
                return "";
            }
            set
            {
                ViewState["fileName"] = value;
            }
        }

        /// <summary>
        /// Gets the value that indicating is the file was uploaded.
        /// </summary>
        public bool IsFilePosted
        {
            get
            {
                return (Allocation != FileAllocation.None);
            }
        }

        public FileAllocation Allocation
        {
            get
            {
                if (ViewState["_allocation"] == null) return FileAllocation.None;
                return (FileAllocation)ViewState["_allocation"];
            }
            set
            {
                ViewState["_allocation"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                return TypeFactory.CreateTypedField(DataType, Text, Format);
            }
            set
            {
                object val = value;
                try
                {
                    Text = TypeFactory.CreateTypedField(DataType, val, Format).ToString();
                }
                catch (FileNotFoundException e)
                {
                    if (OwnerForm is IErrorHandler)
                    {
                        Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_FileNotFound"), value as string == null ? "" : value as string, ControlsHelper.ReplaceResource(Caption)));
                        ((IErrorHandler)OwnerForm).RegisterError(this, e);
                    }
                    else
                    {
                        if (AppConfig.IsMTErrorHandlerUse("all"))
                            Trace.TraceError(e.ToString());
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public string Text
        {
            get
            {
                if (ViewState["_value"] == null)
                    return "";
                else
                    return ViewState["_value"] as string;
            }
            set
            {
                if (!String.IsNullOrEmpty(value) && Allocation == FileAllocation.None && !System.IO.File.Exists(FileFolder + "\\" + value))
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError((new FileNotFoundException("FileNotFound")).Message + "\n" + Environment.StackTrace);
                    throw (new FileNotFoundException("FileNotFound"));
                }
                ViewState["_value"] = value;
            }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get
            {
                return ((IMTField)Value).IsNull && !delControl.Visible;
            }
        }

        /// <summary>
        /// Gets a formatted <see cref="Value"/> representation to be placed into the database.
        /// </summary>
        public TypedValue DBValue
        {
            get
            {
                TypedValue result = new TypedValue();
                result.Value = this.Text;
                result.DataType = this.DataType;
                return result;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="DataType"/> of <see cref="Value"/>.
        /// </summary>
        public DataType DataType
        {
            get { return DataType.Text; }
            set { ; }
        }

        /// <summary>
        /// The property is not useable.
        /// </summary>
        public string Format
        {
            get { return ""; }
            set { ; }
        }

        string _caption;
        /// <summary>
        /// Gets or sets the caption of the control that will be used in validation messages.
        /// </summary>
        public string Caption
        {
            get
            {
                if (String.IsNullOrEmpty(_caption)) _caption = ID ?? String.Empty;
                return _caption;
            }
            set { _caption = value; }
        }

        string _deleteCaption;
        /// <summary>
        /// Gets or sets the caption for delete control.
        /// </summary>
        public string DeleteCaption
        {
            get
            {
                if (_deleteCaption == null) _deleteCaption = "";
                return _deleteCaption;
            }
            set { _deleteCaption = value; }
        }

        private string _infoText;
        /// <summary>
        /// Gets or sets the text that will be used for displaying file info.
        /// </summary>
        public string InfoText
        {
            get
            {
                if (_infoText == null) return String.Empty;
                return _infoText;
            }
            set { _infoText = value; }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs after an uploaded file is saved to the temporary directory but before it is moved to the target folder. This event can be used to check additional conditions or to change the file location or name.
        /// </summary>
        public event EventHandler BeforeProcessFile;
        /// <summary>
        /// Occurs after an uploaded file is moved to the target folder from the temporary folder. It can be used to perform post processing on the file.
        /// </summary>
        public event EventHandler AfterProcessFile;
        /// <summary>
        /// Occurs before a file is deleted from the server. It can be used to check additional permissions or conditions before deleting the file.
        /// </summary>
        public event EventHandler BeforeDeleteFile;
        /// <summary>
        /// Occurs after a file has been deleted from the server. It can be used to perform cleanup operations after removing the file.
        /// </summary>
        public event EventHandler AfterDeleteFile;
        /// <summary>
        /// Raises the <see cref="BeforeProcessFile"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeProcessFile(EventArgs e)
        {
            if (BeforeProcessFile != null)
                BeforeProcessFile(this, e);
        }
        /// <summary>
        /// Raises the <see cref="AfterProcessFile"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterProcessFile(EventArgs e)
        {
            if (AfterProcessFile != null)
                AfterProcessFile(this, e);
        }
        /// <summary>
        /// Raises the <see cref="BeforeDeleteFile"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeDeleteFile(EventArgs e)
        {
            if (BeforeDeleteFile != null)
                BeforeDeleteFile(this, e);
        }
        /// <summary>
        /// Raises the <see cref="AfterDeleteFile"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnAfterDeleteFile(EventArgs e)
        {
            if (AfterDeleteFile != null)
                AfterDeleteFile(this, e);
        }
        #endregion

        #region Validation

        /// <summary>
        /// Occurs after the control performed the value validation.
        /// </summary>
        public event EventHandler<ValidatingEventArgs> Validating;

        private void OwnerForm_Validating(object sender, ValidatingEventArgs e)
        {
            e.HasErrors = !Validate();
        }

        /// <summary>
        /// Raises the <see cref="Validating"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ValidatingEventArgs"/> that contains event data.</param>
        protected virtual void OnValidating(ValidatingEventArgs e)
        {
            if (Validating != null)
                Validating(this, e);
        }

        /// <summary>
        /// Perform the value validation.
        /// </summary>
        /// <returns><b>true</b> if the <see cref="Value"/> is valid; otherwise <b>false</b>.</returns>
        public bool Validate()
        {
            ValidatingEventArgs args = new ValidatingEventArgs();
            if (Required && String.IsNullOrEmpty(Text))
            {
                Errors.Add(String.Format(Common.Resources.ResManager.GetString("CCS_RequiredField"), ControlsHelper.ReplaceResource(Caption)));
                args.HasErrors = true;
            }
            OnValidating(args);
            if (args.HasErrors)
            {
                if (OwnerForm is IErrorHandler)
                    ((IErrorHandler)OwnerForm).RegisterError(this, null);
                return false;
            }
            return true;
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (Text.Length > 0 && Allocation == FileAllocation.None)
            {
                if (System.IO.File.Exists(FileFolder + "\\" + Text))
                {
                    Allocation = FileAllocation.FileFolder;
                    FileName = Text;
                    ViewState["_oldFileName"] = Text;
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(String.Format(Resources.Strings.CCS_FileNotFound, ID, Text) + "\n" + Environment.StackTrace);
                    throw (new FileNotFoundException(String.Format(Resources.Strings.CCS_FileNotFound, ID, Text), Text));
                }
            }

            /*Controls.Add(fileNameLabel);
            Controls.Add(literal1);
            Controls.Add(fileSizeLabel);
            Controls.Add(literal2);*/
            Controls.Add(delLabel);
            Controls.Add(delControl);
            Controls.Add(file);

            Controls.Add(InfoTextHolder);
            InfoTextHolder.ID = "infoText";
            InfoTextHolder.Text = ControlsHelper.ReplaceResourcesAndStyles(InfoText, Page);
            delControl.ID = "delControl";
            delLabel.ID = "delLabel";
            delLabel.Text = ControlsHelper.ReplaceResource(DeleteCaption);
            /*fileNameLabel.ID = "fileName";
            fileSizeLabel.ID = "fileSize";*/
            file.ID = "file";
        }

        private static bool CompareByMask(string value, string mask)
        {
            if (mask == "*") return true;
            mask = (new Regex(@"([\.\$\^\{\[\(\|\)\+\\])").Replace(mask, @"\$1"));
            mask = (new Regex(@"\?").Replace(mask, @"."));
            mask = (new Regex(@"\*").Replace(mask, @".*"));
            mask = "^" + mask + "$";
            return new Regex(mask, RegexOptions.IgnoreCase).IsMatch(value);
        }

        private void CheckConstraints()
        {
            try
            {
                string fileName = System.IO.Path.GetFileName(FileName);
                if (AllowedFileMasks != "*")
                {
                    string[] masks = AllowedFileMasks.Split(new char[] { ';' });
                    bool result = false;
                    for (int i = 0; i < masks.Length; i++)
                        result = CompareByMask(fileName, masks[i]) || result;
                    if (!result)
                    {
                        Allocation = FileAllocation.None;
                        throw (new InvalidOperationException(String.Format(Common.Resources.ResManager.GetString("CCS_WrongType"), ControlsHelper.ReplaceResource(Caption))));
                    }
                }
                if (DisallowedFileMasks.Length > 0)
                {
                    string[] masks = DisallowedFileMasks.Split(new char[] { ';' });
                    bool result = false;
                    for (int i = 0; i < masks.Length; i++)
                        result = CompareByMask(fileName, masks[i]) || result;
                    if (result)
                    {
                        Allocation = FileAllocation.None;
                        throw (new InvalidOperationException(String.Format(Common.Resources.ResManager.GetString("CCS_WrongType"), ControlsHelper.ReplaceResource(Caption))));
                    }
                }
                if (FileSizeLimit != -1)
                {
                    if (file.PostedFile.ContentLength > FileSizeLimit)
                    {
                        Allocation = FileAllocation.None;
                        throw (new InvalidOperationException(String.Format(Common.Resources.ResManager.GetString("CCS_LargeFile"), ControlsHelper.ReplaceResource(Caption))));
                    }
                }
            }
            catch (InvalidOperationException e)
            {
                if (OwnerForm is IErrorHandler)
                {
                    Errors.Add(e.Message);
                    ((IErrorHandler)OwnerForm).RegisterError(this, e);
                }
                else
                {
                    if (AppConfig.IsMTErrorHandlerUse("all"))
                        Trace.TraceError(e.ToString());
                    throw;
                }
            }
            return;
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <param name="mode">The mode of file processing. If mode is 1, execute a file saving; 2, execute moving from temporary to file folder; 3, execute file deleting.</param>
        /// <param name="fileName">The name of file.</param>
        /// <returns>true if the operation was successful; otherwise false.</returns>
        protected bool ProcessFile(int mode, string fileName)
        {
            switch (mode)
            {
                case 1:
                    file.PostedFile.SaveAs(fileName);
                    Allocation = FileAllocation.TemporaryFolder;
                    Text = FileName;
                    break;
                case 2:
                    OnBeforeProcessFile(EventArgs.Empty);
                    System.IO.File.Move(TemporaryFolder + "\\" + FileName, FileFolder + "\\" + FileName);
                    Allocation = FileAllocation.FileFolder;
                    Text = FileName;
                    if (ViewState["_oldFileName"] != null)
                        System.IO.File.Delete(FileFolder + "\\" + ViewState["_oldFileName"] as string);
                    OnAfterProcessFile(EventArgs.Empty);
                    break;
                case 3:
                    OnBeforeDeleteFile(EventArgs.Empty);
                    if (Allocation == FileAllocation.TemporaryFolder)
                    {
                        System.IO.File.Delete(TemporaryFolder + "\\" + FileName);
                        Allocation = FileAllocation.None;
                        Text = "";
                    }
                    if (Allocation == FileAllocation.FileFolder)
                    {
                        System.IO.File.Delete(FileFolder + "\\" + FileName);
                        Allocation = FileAllocation.None;
                        Text = "";
                    }
                    OnAfterDeleteFile(EventArgs.Empty);
                    break;
            }
            return true;
        }

        /// <summary>
        /// Deletes the file from the local disk drive.
        /// </summary>
        /// <returns>true if the operation was successful; otherwise false.</returns>
        public bool DeleteFile()
        {
            return ProcessFile(3, "");
        }

        /// <summary>
        /// Perform the file validation.
        /// </summary>
        /// <returns>true if the validation was successful; otherwise false.</returns>
        public bool ValidateFile()
        {
            EnsureChildControls();
            if (delControl.Checked)
            {
                DeleteFile();
                delControl.Checked = false;
            }
            if ((file.PostedFile == null || FileSize == 0) &&
                Allocation != FileAllocation.FileFolder && Allocation != FileAllocation.TemporaryFolder)
            {
                Allocation = FileAllocation.None;
                return false;
            }
            else if (file.PostedFile != null && file.PostedFile.ContentLength != 0)
            {
                if (Allocation == FileAllocation.TemporaryFolder)
                    ProcessFile(3, "");
                Allocation = FileAllocation.InputControl;
                CheckConstraints();
                if (Allocation != FileAllocation.None)
                {
                    string fileName = System.IO.Path.GetFileName(FileName);
                    OriginalFileName = fileName;
                    string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    int i = 1;
                    string index = "";
                    while (System.IO.File.Exists(TemporaryFolder + "\\" + timeStamp + index + "." + fileName))
                    {
                        index = i.ToString();
                        i++;
                    }
                    FileName = timeStamp + index + "." + fileName;
                    fileName = TemporaryFolder + "\\" + timeStamp + index + "." + fileName;
                    return ProcessFile(1, fileName);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Saves the file on the local disk drive.
        /// </summary>
        /// <returns>true if the operation was successful; otherwise false.</returns>
        public bool SaveFile()
        {
            EnsureChildControls();
            if (delControl.Checked)
            {
                DeleteFile();
                delControl.Checked = false;
            }
            if (Allocation == FileAllocation.InputControl)
                ValidateFile();
            if (Allocation == FileAllocation.TemporaryFolder)
                return ProcessFile(2, "");
            return true;
        }

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (!DesignMode)
            {
                if (_temporaryFolder != null) ValidateTemporaryFolder(_temporaryFolder);
                if (ViewState["_fileFolder"] != null) ValidateFileFolder(ViewState["_fileFolder"].ToString());
            }
            base.OnInit(e);
            if (OwnerForm is IValidator)
                (OwnerForm as IValidator).ControlsValidating += new EventHandler<ValidatingEventArgs>(OwnerForm_Validating);
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Page.IsPostBack)
            {
                _IsValueNotPassed = !IsFilePosted;
                ValidateFile();
                Required = Required && (Errors.Count == 0);
                _IsValueNotPassed = _IsValueNotPassed && (Page.Request.Files[file.UniqueID] == null);
            }
            if (OwnerForm is Record)
            {
                Record OwnerRecord = (Record)OwnerForm;
                OwnerRecord.AfterInsert += new EventHandler<DataOperationCompletingEventArgs>(OwnerRecord_AfterSubmit);
                OwnerRecord.AfterUpdate += new EventHandler<DataOperationCompletingEventArgs>(OwnerRecord_AfterSubmit);
                OwnerRecord.AfterDelete += new EventHandler<DataOperationCompletingEventArgs>(OwnerRecord_AfterDelete);
            }
        }

        /// <summary>
        /// Handles the AfterDelete event of owner form and delete file from the disk.
        /// </summary>
        /// <param name="sender">The source of event.</param>
        /// <param name="e">An <see cref="DataOperationCompletingEventArgs"/> object that contains the event data.</param>
        protected void OwnerRecord_AfterDelete(object sender, DataOperationCompletingEventArgs e)
        {
            if (e.OperationException == null)
                DeleteFile();
        }

        /// <summary>
        /// Handles the AfterSubmit event of owner form and save file from the disk.
        /// </summary>
        /// <param name="sender">The source of event.</param>
        /// <param name="e">An <see cref="DataOperationCompletingEventArgs"/> object that contains the event data.</param>
        protected void OwnerRecord_AfterSubmit(object sender, DataOperationCompletingEventArgs e)
        {
            if (e.OperationException == null)
                SaveFile();
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (OwnerForm is IClientScriptHelper && Visible)
                (OwnerForm as IClientScriptHelper).RegisterClientControl(this);

            if (CssClass.Length > 0)
            {
                file.Attributes.Add("class", CssClass);
                delControl.Attributes.Add("class", CssClass);
            }
            if (Attributes["Size"] != null)
            {
                file.Attributes.Add("size", Attributes["Size"]);
            }
            ControlStyle.Reset();

            foreach (Control c in Page.Controls)
                if (c is HtmlForm)
                {
                    ((HtmlForm)c).Enctype = "multipart/form-data";
                    break;
                }
            if (IsFilePosted)
            {
                file.Visible = false;
                delControl.Visible = true;
                delLabel.Visible = true;
                /*fileNameLabel.Text = OriginalFileName;
                fileSizeLabel.Text = FileSize.ToString();
                fileNameLabel.Visible = true;
                fileSizeLabel.Visible = true;
                literal1.Visible = true;
                literal2.Visible = true;*/
                InfoTextHolder.Text = String.Format(InfoTextHolder.Text, OriginalFileName, FileSize);
                InfoTextHolder.Visible = true;
            }
            else
            {
                delControl.Visible = false;
                delLabel.Visible = false;
                /*fileNameLabel.Visible = false;
                fileSizeLabel.Visible = false;
                
                literal1.Visible = false;
                literal2.Visible = false;*/
                file.Visible = true;
                InfoTextHolder.Visible = false;
            }
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode)
                file.RenderControl(writer);
            else
                base.Render(writer);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, "");
            if (val != null || val is IMTField && !((IMTField)val).IsNull)
                Value = val;
            OnBeforeShow(EventArgs.Empty);
        }
        #endregion

        #region IMTControl Members
        /// <summary>
        /// Occurs after the <see cref="MTFileUpload"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        #endregion

        #region IMTControlWithValue
        private bool _IsValueChanged = false;
        /// <summary>
        /// Gets or sets the value indicating whether value of control changed by the user.
        /// </summary>
        public bool IsValueChanged
        {
            get { return _IsValueChanged; }
        }

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }
        #endregion
        private bool _IsValueNotPassed;
        /// <summary>
        /// Indicates whethere value is empty, i.e. not passed by the client request.
        /// </summary>
        public bool IsValueNotPassed
        {
            get { return _IsValueNotPassed; }
            set { _IsValueNotPassed = value; }
        }

        private bool _IsOmitEmptyValue;
        /// <summary>
        /// Indicates whethere control value will be excluded from data update operation when <see cref="IsValueNotPassed"/> is true;
        /// </summary>
        public bool IsOmitEmptyValue
        {
            get
            {
                return _IsOmitEmptyValue;
            }
            set
            {
                _IsOmitEmptyValue = value;
            }
        }
    }
}
