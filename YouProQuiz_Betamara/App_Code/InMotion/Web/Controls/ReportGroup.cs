//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represent the report group that is used to manage repeating values and totals
    /// </summary>
    [Serializable]
    public class ReportGroup
    {
        private string _name = "";

        /// <summary>
        /// Gets or sets the name of the group.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _field = "";

        /// <summary>
        /// Gets or sets the name of the datasource field for the current <see cref="ReportGroup"/>.
        /// </summary>
        public string Field
        {
            get { return _field; }
            set { _field = value; }
        }

        private string _sqlField = "";

        /// <summary>
        /// Gets or sets the name of the field in the form of tablename.fieldname for the current <see cref="ReportGroup"/> for using in Order By clause of SQL statement.
        /// If it is not initialized, the value of <see cref="Field"/> will be returned.
        /// </summary>
        public string SqlField
        {
            get
            {
                if (String.IsNullOrEmpty(_sqlField))
                    return _field;
                else
                    return _sqlField;
            }
            set { _sqlField = value; }
        }

        private SortDirection _sort = SortDirection.Asc;
        /// <summary>
        /// Gets or sets the sorting order that is used for sorting rows in group.
        /// </summary>
        public SortDirection SortOrder
        {
            get { return _sort; }
            set { _sort = value; }
        }

        /// <summary>
        /// Initialize the new instance of the <see cref="ReportGroup"/>.
        /// </summary>
        /// <param name="name">The name of the group.</param>
        /// <param name="field">The name of the datasource field.</param>
        public ReportGroup(string name, string field)
        {
            Name = name;
            Field = field;
        }
        /// <summary>
        /// Initialize the new instance of the <see cref="ReportGroup"/>.
        /// </summary>
        public ReportGroup()
        {

        }

    }
}
