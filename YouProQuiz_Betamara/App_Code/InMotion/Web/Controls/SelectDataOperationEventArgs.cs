//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using InMotion.Data;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Provides data for data Select operation events.
    /// </summary>
    public class SelectDataOperationEventArgs : DataOperationEventArgs
    {
        /// <summary>
        /// Provides data for data Select operation events.
        /// </summary>
        private DataCommand _countDataCommand;
        /// <summary>
        /// Gets or sets the Count <see cref="DataCommand"/> for operation.
        /// </summary>
        public DataCommand CountCommand
        {
            get { return _countDataCommand; }
            set { _countDataCommand = value; }
        }

        private object _countOperationTesult;

        /// <summary>
        /// Gets or sets the result of count operation execution. 
        /// </summary>
        public object CountOperationResult
        {
            get { return _countOperationTesult; }
            set { _countOperationTesult = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectDataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="countResult">The result of count operation execution.</param>
        /// <param name="command">The <see cref="DataCommand"/> for operation.</param>
        /// <param name="countCommand">The count <see cref="DataCommand"/> for operation.</param>
        public SelectDataOperationEventArgs(DataOperationType type, object result, object countResult, DataCommand command, DataCommand countCommand)
            : base(type, result, command)
        {
            _countDataCommand = countCommand;
            _countOperationTesult = countResult;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectDataOperationEventArgs"/>
        /// </summary>
        /// <param name="type">The <see cref="DataOperationType"/> of operation.</param>
        /// <param name="result">The result of operation execution.</param>
        /// <param name="countResult">The result of count operation execution.</param>
        /// <param name="command">The <see cref="DataCommand"/> for operation.</param>
        /// <param name="countCommand">The count <see cref="DataCommand"/> for operation.</param>
        /// <param name="ex">The <see cref="Exception"/> bound with operation, if it is thrown during execution.</param>
        public SelectDataOperationEventArgs(DataOperationType type, object result, object countResult, DataCommand command, DataCommand countCommand, Exception ex)
            : base(type, result, command, ex)
        {
            _countDataCommand = countCommand;
            _countOperationTesult = countResult;
        }
    }
}
