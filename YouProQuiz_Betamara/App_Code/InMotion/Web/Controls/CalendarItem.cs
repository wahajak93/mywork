//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Specifies the type of an item in a <see cref="Calendar"/> control.
    /// </summary>
    public enum CalendarItemType
    {
        /// <summary>
        /// A header for the <see cref="Calendar"/> control.
        /// </summary>
        Header,
        /// <summary>
        /// A footer for the <see cref="Calendar"/> control.
        /// </summary>
        Footer,
        /// <summary>
        /// A month header for the <see cref="Calendar"/> control.
        /// </summary>
        MonthHeader,
        /// <summary>
        /// A month footer for the <see cref="Calendar"/> control.
        /// </summary>
        MonthFooter,
        /// <summary>
        /// A week day name for the <see cref="Calendar"/> control.
        /// </summary>
        Weekdays,
        /// <summary>
        /// A week day name footer for the <see cref="Calendar"/> control.
        /// </summary>
        WeekdaysFooter,
        /// <summary>
        /// A separator for dividing week day names for the <see cref="Calendar"/> control.
        /// </summary>
        WeekdaySeparator,
        /// <summary>
        /// A week header for the <see cref="Calendar"/> control.
        /// </summary>
        WeekHeader,
        /// <summary>
        /// A week footer for the <see cref="Calendar"/> control.
        /// </summary>
        WeekFooter,
        /// <summary>
        /// A day header for the <see cref="Calendar"/> control.
        /// </summary>
        DayHeader,
        /// <summary>
        /// A day footer for the <see cref="Calendar"/> control.
        /// </summary>
        DayFooter,
        /// <summary>
        /// A event row for the <see cref="Calendar"/> control.
        /// </summary>
        EventRow,
        /// <summary>
        /// A separator for dividing events for the <see cref="Calendar"/> control.
        /// </summary>
        EventSeparator,
        /// <summary>
        /// A separator for dividing days for the <see cref="Calendar"/> control.
        /// </summary>
        DaySeparator,
        /// <summary>
        /// A separator for dividing weeks for the <see cref="Calendar"/> control.
        /// </summary>
        WeekSeparator,
        /// <summary>
        /// A separator for dividing months for the <see cref="Calendar"/> control.
        /// </summary>
        MonthSeparator,
        /// <summary>
        /// A separator for dividing month's rows for the <see cref="Calendar"/> control.
        /// </summary>
        MonthsRowSeparator,
        /// <summary>
        /// A No Events item for the <see cref="Calendar"/> control.
        /// </summary>
        NoEvents,
        /// <summary>
        /// A empty day item for the <see cref="Calendar"/> control.
        /// </summary>
        EmptyDay
    }

    /// <summary>
    /// Represents an item in the <see cref="Calendar"/> control.
    /// </summary>
    public class CalendarItem : Control, INamingContainer
    {
        private int itemIndex;
        private CalendarItemType itemType;
        private object dataItem;
        private DateTime date;
        private Calendar owner;
        private bool hasEvents;
        private string style;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalendarItem"/> class.
        /// </summary>
        /// <param name="itemIndex">The index of the item in the <see cref="Calendar"/> control.</param>
        /// <param name="itemType">One of the <see cref="CalendarItemType"/> values.</param>
        /// <param name="owner">The owner <see cref="Calendar"/> control.</param>
        public CalendarItem(int itemIndex, CalendarItemType itemType, Calendar owner)
        {
            this.itemIndex = itemIndex;
            this.itemType = itemType;
            this.owner = owner;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CalendarItem"/> class.
        /// </summary>
        /// <param name="itemIndex">The index of the item in the <see cref="Calendar"/> control.</param>
        /// <param name="itemType">One of the <see cref="CalendarItemType"/> values.</param>
        /// <param name="owner">The owner <see cref="Calendar"/> control.</param>
        /// <param name="hasEvents">Indicates if the item contains events.</param>
        public CalendarItem(int itemIndex, CalendarItemType itemType, Calendar owner, bool hasEvents)
            : this(itemIndex, itemType, owner)
        {
            this.hasEvents = hasEvents;
        }

        /// <summary>
        /// Gets the owner <see cref="Calendar"/> control.
        /// </summary>
        public Calendar Owner
        {
            get
            {
                return owner;
            }
        }
        /// <summary>
        /// Gets the value indicating whether item contains events.
        /// </summary>
        public new bool HasEvents
        {
            get
            {
                return hasEvents;
            }
        }

        /// <summary>
        /// Gets or sets CSS style string.
        /// </summary>
        public string StyleString
        {
            get
            {
                return style;
            }
            set
            {
                style = value;
            }
        }

        /// <summary>
        /// Gets or sets a data item associated with the CalendarItem object in the Calendar control.
        /// </summary>
        public virtual object DataItem
        {
            get
            {
                return dataItem;
            }
            set
            {
                dataItem = value;
            }
        }

        /// <summary>
        /// Gets the index of the item in the Calendar control.
        /// </summary>
        public virtual int ItemIndex
        {
            get
            {
                return itemIndex;
            }
        }

        /// <summary>
        /// Gets or sets the date that associated with the item.
        /// </summary>
        public virtual DateTime CurrentProcessingDate
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        /// <summary>
        /// Gets the next item <see cref="CurrentProcessingDate"/>.
        /// </summary>
        public virtual DateTime NextProcessingDate
        {
            get
            {
                switch (ItemType)
                {
                    case CalendarItemType.DayFooter:
                    case CalendarItemType.DayHeader:
                    case CalendarItemType.DaySeparator:
                    case CalendarItemType.EmptyDay:
                    case CalendarItemType.EventRow:
                    case CalendarItemType.EventSeparator:
                    case CalendarItemType.NoEvents:
                    case CalendarItemType.WeekFooter:
                    case CalendarItemType.WeekHeader:
                    case CalendarItemType.WeekSeparator:
                        return CurrentProcessingDate.AddDays(1);
                    case CalendarItemType.MonthFooter:
                    case CalendarItemType.MonthHeader:
                    case CalendarItemType.MonthsRowSeparator:
                    case CalendarItemType.MonthSeparator:
                        return CurrentProcessingDate.AddMonths(1);
                    case CalendarItemType.Footer:
                    case CalendarItemType.Header:
                        return CurrentProcessingDate.AddYears(1);

                }
                return CurrentProcessingDate.AddDays(1);
            }

        }
        /// <summary>
        /// Gets the previous item <see cref="CurrentProcessingDate"/>.
        /// </summary>
        public virtual DateTime PrevProcessingDate
        {
            get
            {
                switch (ItemType)
                {
                    case CalendarItemType.DayFooter:
                    case CalendarItemType.DayHeader:
                    case CalendarItemType.DaySeparator:
                    case CalendarItemType.EmptyDay:
                    case CalendarItemType.EventRow:
                    case CalendarItemType.EventSeparator:
                    case CalendarItemType.NoEvents:
                    case CalendarItemType.WeekFooter:
                    case CalendarItemType.WeekHeader:
                    case CalendarItemType.WeekSeparator:
                        return CurrentProcessingDate.AddDays(-1);
                    case CalendarItemType.MonthFooter:
                    case CalendarItemType.MonthHeader:
                    case CalendarItemType.MonthsRowSeparator:
                    case CalendarItemType.MonthSeparator:
                        return CurrentProcessingDate.AddMonths(-1);
                    case CalendarItemType.Footer:
                    case CalendarItemType.Header:
                        return CurrentProcessingDate.AddYears(-1);
                }
                return CurrentProcessingDate.AddDays(-1);
            }

        }

        /// <summary>
        /// Gets the type of the item in the Calendar control.
        /// </summary>
        public virtual CalendarItemType ItemType
        {
            get
            {
                return itemType;
            }
        }

        /// <summary>
        /// This method supports the framework infrastructure and is not intended to be used directly from your code. 
        /// Assigns any sources of the event and its information to the parent <see cref="Calendar"/> control, if the EventArgs parameter is an instance of <see cref="CalendarCommandEventArgs"/>.
        /// </summary>
        /// <param name="source">The source of the event. </param>
        /// <param name="args">An <see cref="EventArgs"/> that contains the event data.</param>
        /// <returns><b>true</b> if the event assigned to the parent was raised, otherwise <b>false</b>.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is CommandEventArgs)
            {

                CalendarCommandEventArgs cargs =
                    new CalendarCommandEventArgs(this, source, (CommandEventArgs)args);

                RaiseBubbleEvent(this, cargs);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="HtmlTextWriter"/> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode)
            {
                if (ItemType == CalendarItemType.Footer && base.Controls.Count > 0)
                {
                    ((System.Web.UI.LiteralControl)base.Controls[0]).Text += Owner.MonthsInRow + ">";// ((System.Web.UI.LiteralControl)base.Controls[0]).Text.Replace("__designer", " colspan=\"" + colspan + "\" __designer");
                }
                else if (base.Controls.Count > 0 && base.Controls[0] is System.Web.UI.LiteralControl)
                {
                    string TestString = ((System.Web.UI.LiteralControl)base.Controls[0]).Text;
                    Regex TestRegExp = new Regex("\\sclass\\s*=\\s*\"[\\w\\s\\.,%\"=-]*\"");
                    if (!TestRegExp.IsMatch(TestString))
                    {
                        TestRegExp = new Regex("\\s*<td\\s*__designer:dtid=\"\\d*\"[\\w\\s\\.,%\"=-]*>\\s*");
                        if (TestRegExp.IsMatch(TestString))
                            ((System.Web.UI.LiteralControl)base.Controls[0]).Text = ((System.Web.UI.LiteralControl)base.Controls[0]).Text.Replace("__designer", style + " __designer");
                    }
                }
            }
            base.Render(writer);
        }

        internal void SetItemType(CalendarItemType itemType)
        {
            this.itemType = itemType;
        }
    }
}