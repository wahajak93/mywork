//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InMotion.Common;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a Image Link control, which displays image with hyperlink on a Web page.
    /// </summary>
    [ParseChildren(true, "Text")]
    [PersistChildren(false)]
    public class MTImageLink : System.Web.UI.WebControls.HyperLink, IMTControlWithValue, IMTAttributeAccessor
    {
        #region Events
        /// <summary>
        /// Occurs after the <see cref="MTImageLink"/> control is loaded but prior to rendering.
        /// </summary>
        public event EventHandler<EventArgs> BeforeShow;
        #endregion

        #region Properties
        private MTImage img = new MTImage();

        /// <summary>
        /// Gets or sets a semicolon-separated list of parameters that should be removed from the hyperlink.
        /// </summary>
        public string RemoveParameters
        {
            get { return Url.RemoveParameters; }
            set { Url.RemoveParameters = value; }
        }

        /// <summary>
        /// Gets or sets whether URLs should be automatically converted to absolute URLs or secure URLs for the SSL protocol (https://).
        /// </summary>
        public UrlType ConvertUrl
        {
            get { return Url.Type; }
            set { Url.Type = value; }
        }

        /// <summary>
        /// Gets or sets whether Get or Post parameters should be preserved.
        /// </summary>
        public PreserveParameterType PreserveParameters
        {
            get { return Url.PreserveParameters; }
            set { Url.PreserveParameters = value; }
        }

        /// <summary>
        /// Gets the <see cref="UrlParameterCollection"/> that contains the parameters of url.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public UrlParameterCollection Parameters
        {
            get { return Url.Parameters; }
        }

        private Url _url;
        /// <summary>
        /// Gets or sets the Url of this <see cref="MTImageLink"/>.
        /// </summary>
        public Url Url
        {
            get
            {
                if (_url == null) _url = new Url("");
                return _url;
            }
            set { _url = value; }
        }

        /// <summary>
        /// Gets or sets the alternate text displayed in the <see cref="MTImageLink"/> control when the image is unavailable. Browsers that support the ToolTips feature display this text as a ToolTip. 
        /// </summary>
        public string AlternateText
        {
            get
            {
                return img.AlternateText;
            }
            set
            {
                img.AlternateText = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of data source field to be used as source of href attribute.
        /// </summary>
        [
        Category("Behavior"),
        Editor(typeof(System.Web.UI.Design.UrlEditor), typeof(string))
        ]
        public string HrefSource
        {
            get
            {
                if (ViewState["__HrefSource"] == null)
                    ViewState["__HrefSource"] = String.Empty;
                return (string)ViewState["__HrefSource"];
            }
            set
            {
                ViewState["__HrefSource"] = value;
                Url.Address = value;
            }
        }

        HrefType _hrefType = HrefType.Page;
        /// <summary>
        /// Gets or sets the type of source where the URL value will come from.
        /// </summary>
        public HrefType HrefType
        {
            get { return _hrefType; }
            set { _hrefType = value; }
        }

        private SourceType _sourceType = SourceType.DatabaseColumn;
        /// <summary>
        /// Gets or sets the type of data source that will provide data for the control.
        /// </summary>
        public SourceType SourceType
        {
            get
            {
                return _sourceType;
            }
            set { _sourceType = value; }
        }

        private string _source = String.Empty;
        /// <summary>
        /// Gets or sets the source of data for the control e.g. the name of a database column.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private DataType _dataType = DataType.Text;
        /// <summary>
        /// Gets or sets the DataType of the control value.
        /// </summary>
        public DataType DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                if (_value != null) _value = TypeFactory.CreateTypedField(DataType, Value, Format);
            }
        }

        private string _format = String.Empty;

        /// <summary>
        /// Gets or sets the format depending on the Data Type property in which control's value will be displayed.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        private string _dBformat = string.Empty;

        /// <summary>
        /// Format that will be used to extract as well as place the control value into the database.
        /// </summary>
        public string DBFormat
        {
            get { return _dBformat; }
            set { _dBformat = value; }
        }

        /// <summary>
        /// Gets the reference on the parent <see cref="IForm"/>
        /// </summary>
        public IForm OwnerForm
        {
            get
            {
                Control _owner = Parent;
                while (_owner != null && !(_owner is IForm))
                {
                    _owner = _owner.Parent;
                }
                return (IForm)_owner;
            }
        }

        private bool _valUpdated;

        private bool _textUpdated
        {
            get
            {
                return ViewState["_textUpdated"] != null && (bool)ViewState["_textUpdated"];
            }
            set
            {
                ViewState["_textUpdated"] = value;
            }
        }

        private object _value;
        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        /// <remarks>
        /// The assigned value will be automatically converted into one of IMTField types according to the Data Type property (MTText will be used by default).
        /// Retrieved value is guaranteed is not null IMTField object of type specified in Data Type property.
        /// </remarks>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object Value
        {
            get
            {
                if (_value == null) return DefaultValue;
                return _value;
            }
            set
            {
                object val = value;

                val = TypeFactory.CreateTypedField(DataType, val, Format);
                _value = val;
                _valUpdated = true;

            }
        }

        private object __DefaultValue;
        /// <summary>
        /// Gets or sets the default value of the control.
        /// </summary>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public object DefaultValue
        {
            get
            {
                if (__DefaultValue == null)
                    __DefaultValue = TypeFactory.CreateTypedField(DataType, null, Format);
                return __DefaultValue;
            }
            set
            {
                object val = value;

                val = TypeFactory.CreateTypedField(DataType, val, Format);
                __DefaultValue = val;

            }
        }

        /// <summary>
        /// Gets or sets the path to an image to display for the control.
        /// </summary>
        public override string ImageUrl
        {
            get
            {
                return base.ImageUrl;
            }
            set
            {
                Text = base.ImageUrl = value;
                img.BaseImageUrl = value;
            }
        }

        /// <summary>
        /// Gets or set string that will be rendered as a style attribute on the outer tag of the image control.
        /// </summary>
        public string ImageStyle
        {
            get
            {
                return img.Style.Value;
            }
            set
            {
                img.Style.Value = value;
            }
        }

        /// <summary>
        /// Gets the value indicating whether <see cref="Value"/> of control is null.
        /// </summary>
        public virtual bool IsEmpty
        {
            get { return ((IMTField)Value).IsNull; }
        }

        private string DisplayedText;
        /// <summary>
        /// Gets or sets the string representation of <see cref="Value"/>.
        /// </summary>
        public new string Text
        {
            get
            {
                if (DisplayedText != null)
                {
                    return DisplayedText;
                }
                if (!_textUpdated && _valUpdated)
                {
                    return ((IMTField)Value).ToString(_format);
                }
                else
                {
                    return img.ImageUrl;
                }
            }
            set
            {
                _textUpdated = true;
                img.ImageUrl = value;
            }
        }

        private bool _textUpdatedUrl
        {
            get
            {
                return ViewState["_tU"] != null && (bool)ViewState["_tU"];
            }
            set
            {
                ViewState["_tU"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL to link to when the control is clicked.
        /// </summary>
        public new string NavigateUrl
        {
            get
            {
                if (!_textUpdatedUrl && Page is MTPage)
                {
                    return Url.ToString((MTPage)Page);
                }
                else
                {
                    return base.NavigateUrl;
                }
            }
            set
            {
                base.NavigateUrl = value;
                _textUpdatedUrl = true;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Controls.Add(img);
        }

        /// <summary>
        /// Raises the <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected virtual void OnBeforeShow(EventArgs e)
        {
            if (BeforeShow != null)
                BeforeShow(this, e);
        }

        /// <summary>
        /// Performs the required data binding operations and raises <see cref="BeforeShow"/> event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnDataBinding(EventArgs e)
        {
            if (!DesignMode)
            {
                base.OnDataBinding(e);
                object val = ControlsHelper.DataBindControl(SourceType, Source, OwnerForm, DataType, DBFormat);
                if (val != null || val is IMTField && !((IMTField)val).IsNull)
                    Value = val;
                if (HrefType == HrefType.Database && HrefSource.Length > 0 && OwnerForm != null)
                {
                    this.Url.Address = MTText.Parse(OwnerForm.DataItem[HrefSource]).ToString();
                }
                OnBeforeShow(EventArgs.Empty);
                Url.DataBind(this);
            }
        }

        /// <summary>
        /// Raises the Load event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!_valUpdated)
            {
                if (ViewState["ImageUrl"] != null && (string)ViewState["ImageUrl"] != "") img.ImageUrl = (string)ViewState["ImageUrl"];
                if (Page.Request.QueryString[ID] != null && !Page.IsPostBack)
                    Text = Page.Request.QueryString[ID];
                if (ID != UniqueID && Page.Request.Form[ID] != null)
                    Value = Page.Request.Form[ID];
            }
            if (HrefType != HrefType.Page || String.IsNullOrEmpty(HrefSource))
            {
                AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            }
            if (SourceType == SourceType.DatabaseColumn || String.IsNullOrEmpty(Source))
            {
                img.AppRelativeTemplateSourceDirectory = Page.AppRelativeTemplateSourceDirectory;
            }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            IMTField val = (IMTField)Value;
            if (!Page.IsPostBack || _valUpdated)
            {
                if (!_textUpdated)
                {
                    if (Text != Value.ToString()) DisplayedText = Text;
                    if (String.IsNullOrEmpty(DisplayedText)) DisplayedText = "{0}";
                }
                else
                {
                    DisplayedText = Text;
                }
                DisplayedText = DisplayedText.Replace("{0}", val.ToString(Format));
                img.ImageUrl = DisplayedText = ControlsHelper.ReplaceResourcesAndStyles(Text, Page);
                if (DisplayedText.IndexOf("{CCS_PathToMasterPage}") != -1)
                {
                    Control parent = Parent;
                    while (parent != null)
                    {
                        if (parent is MTPanel && !string.IsNullOrEmpty(((MTPanel)parent).MasterPageFile))
                        {
                            img.ImageUrl = DisplayedText = DisplayedText.Replace("{CCS_PathToMasterPage}", ((MTPanel)parent).PathToMasterPage);
                            break;
                        }
                        else if (parent is MTPage && !string.IsNullOrEmpty(((MTPage)parent).MasterPageFile))
                        {
                            img.ImageUrl = DisplayedText = DisplayedText.Replace("{CCS_PathToMasterPage}", ((MTPage)parent).PathToMasterPage);
                            break;
                        }
                        parent = parent.Parent;
                    }
                }
                img.Value = Value;
                base.ImageUrl = "";
                if (string.IsNullOrEmpty((img.AlternateText)))
                    img.AlternateText = NavigateUrl;
                ToolTip = ControlsHelper.ReplaceResourcesAndStyles(ToolTip, Page);
                if (String.IsNullOrEmpty(base.NavigateUrl) || !_textUpdatedUrl) base.NavigateUrl = "{0}";
                if (base.NavigateUrl.IndexOf("{0}") > -1)
                    base.NavigateUrl = String.Format(base.NavigateUrl, Url.ToString((MTPage)Page));
            }
            if (NamingContainer is RepeaterItem && !string.IsNullOrEmpty(Attributes["data-id"]))
                Attributes["data-id"] = Regex.Replace(Attributes["data-id"], @"\{\w+:rowNumber\}", (((RepeaterItem)NamingContainer).ItemIndex + 1).ToString());
        }
        #endregion

        /// <summary>
        /// Gets the value of the control as a <see cref="MTFloat"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTFloat GetFloat()
        {
            return (MTFloat)TypeFactory.CreateTypedField(DataType.Float, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTInteger"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTInteger GetInteger()
        {
            return (MTInteger)TypeFactory.CreateTypedField(DataType.Integer, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTSingle"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTSingle GetSingle()
        {
            return (MTSingle)TypeFactory.CreateTypedField(DataType.Single, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTBoolean"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTBoolean GetBoolean()
        {
            return (MTBoolean)TypeFactory.CreateTypedField(DataType.Boolean, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTDate"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTDate GetDate()
        {
            return (MTDate)TypeFactory.CreateTypedField(DataType.Date, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTText"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTText GetText()
        {
            return (MTText)TypeFactory.CreateTypedField(DataType.Text, Value, "");
        }
        /// <summary>
        /// Gets the value of the control as a <see cref="MTMemo"/>.
        /// </summary>
        /// <returns>The value of the control.</returns>
        public MTMemo GetMemo()
        {
            return (MTMemo)TypeFactory.CreateTypedField(DataType.Memo, Value, "");
        }
    }
}
