//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents a collection of Menu Items.
    /// </summary>
    public class MenuItemList : List<MenuItem>
    {
        /// <summary>
        /// Adds an object to the end of the list.
        /// </summary>
        /// <param name="name">Name of the menu Item.</param>
        /// <param name="caption">Caption that user will see.</param>
        public void Add(string name, string caption)
        {
            Add(new MenuItem(name, caption));
        }

        /// <summary>
        /// Adds an object to the end of the list.
        /// </summary>
        /// <param name="name">Name of the menu Item.</param>
        /// <param name="parent">Parent name of the menu Item.</param>
        /// <param name="caption">Caption that user will see.</param>
        public void Add(string name, string parent, string caption)
        {
            Add(new MenuItem(name, parent, caption));
        }

        /// <summary>
        /// Adds an object to the end of the list.
        /// </summary>
        /// <param name="name">Name of the menu Item.</param>
        /// <param name="parent">Parent name of the menu Item.</param>
        /// <param name="caption">Caption that user will see.</param>
        /// <param name="url">The URL to link to when the Menu Item control is clicked.</param>
        public void Add(string name, string parent, string caption, string url)
        {
            Add(new MenuItem(name, parent, caption, url));
        }
    }
}