//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// Represents an item in the <see cref="Menu"/> control.
    /// </summary>
    public class MenuItemControl : WebControl, INamingContainer
    {
        #region Contstructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemControl"/> class.
        /// </summary>
        /// <param name="itemIndex">The index of the item in the <see cref="Menu"/> control.</param>
        /// <param name="owner">The owner <see cref="Menu"/> control.</param>
        /// <param name="itemType">One of the <see cref="MenuItemType"/> values.</param>
        public MenuItemControl(int itemIndex, Menu owner, MenuItemType itemType)
        {
            this.itemIndex = itemIndex;
            this.owner = owner;
            this.itemType = itemType;
            foreach (string key in owner.Attributes.Keys)
                if (Attributes[key] == null)
                    Attributes.Add(key, owner.Attributes[key]);
        }
        #endregion

        #region Properties
        private Menu owner;
        /// <summary>
        /// Gets the owner <see cref="Menu"/> control.
        /// </summary>
        public Menu Owner
        {
            get
            {
                return owner;
            }
        }

        private object dataItem;
        /// <summary>
        /// Gets or sets a data item associated with the <see cref="MenuItemControl"/> object in the <see cref="Menu"/> control.
        /// </summary>
        public virtual object DataItem
        {
            get
            {
                return dataItem;
            }
            set
            {
                dataItem = value;
            }
        }

        private MenuItemType itemType;
        /// <summary>
        /// Gets the type of the item in the <see cref="Menu"/> control.
        /// </summary>
        public MenuItemType ItemType
        {
            get
            {
                return itemType;
            }
        }

        private int itemIndex;
        /// <summary>
        /// Gets the index of the item in the <see cref="Menu"/> control.
        /// </summary>
        public virtual int ItemIndex
        {
            get
            {
                return itemIndex;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Renders the control to the specified HTML writer. 
        /// </summary>
        /// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
        }
        #endregion
    }
}