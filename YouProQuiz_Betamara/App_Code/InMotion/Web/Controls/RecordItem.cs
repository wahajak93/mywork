//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Web.UI;

namespace InMotion.Web.Controls
{
    /// <summary>
    /// The item object in the <see cref="Record"/> control.
    /// </summary>
    public class RecordItem : Control, INamingContainer
    {
        private object dataItem;
        /// <summary>
        /// Gets or sets a data item associated with the <see cref="RecordItem"/> object in the <see cref="Record"/> control.
        /// </summary>
        public virtual object DataItem
        {
            get
            {
                return dataItem;
            }
            set
            {
                dataItem = value;
            }
        }

        /// <summary>
        /// The item object in the <see cref="Record"/> control.
        /// </summary>
        /// <param name="dataItem">Data item associated with the <see cref="RecordItem"/> object in the <see cref="Record"/> control.</param>
        public RecordItem(object dataItem)
        {
            DataItem = dataItem;
        }
    }
}