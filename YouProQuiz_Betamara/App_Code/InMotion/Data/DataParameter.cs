//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using InMotion.Common;
using InMotion.Configuration;

namespace InMotion.Data
{
    /// <summary>
    /// Represents a base implementation of parameter to a <see cref="DataCommand"/> class.
    /// </summary>
    public abstract class DataParameter : DbParameter, IDataParameter
    {
        #region Properties

        private DataCommand _owner;
        /// <summary>
        /// Gets or sets the <see cref="DataCommand"/> that contains parameter.
        /// </summary>
        public DataCommand Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        private DbType _dbType;
        /// <summary>
        /// Gets or sets the <see cref="DbType"/> of the parameter.
        /// </summary>
        public override DbType DbType
        {
            get
            {
                return _dbType;
            }
            set
            {
                _dbType = value;
            }
        }

        private ParameterDirection _direction;
        /// <summary>
        /// Gets or sets a value that indicates whether the parameter is input-only, 
        /// output-only, bidirectional, or a stored procedure return value parameter.
        /// </summary>
        public override ParameterDirection Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
            }
        }

        private bool _isNullable;
        /// <summary>
        /// Gets or sets a value that indicates whether the parameter accepts null values.
        /// </summary>
        public override bool IsNullable
        {
            get
            {
                return _isNullable;
            }
            set
            {
                _isNullable = value;
            }
        }

        private string _parameterName;
        /// <summary>
        /// Gets or sets the name of the <see cref="DbParameter"/>.
        /// </summary>
        public override string ParameterName
        {
            get
            {
                return _parameterName;
            }
            set
            {
                _parameterName = value;
            }
        }

        /// <summary>
        /// Resets the DbType property to its original settings.
        /// </summary>
        public override void ResetDbType()
        {
            if (AppConfig.IsMTErrorHandlerUse("critical"))
                Trace.TraceError("The method or operation is not implemented.\n{0}", Environment.StackTrace);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        private int _size;
        /// <summary>
        /// Gets or sets the maximum size, in bytes, of the data within the column.
        /// </summary>
        public override int Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }

        private string _sourceColumn;
        /// <summary>
        /// Gets or sets the name of the source column mapped to the DataSet 
        /// and used for loading or returning the <see cref="Value"/>.
        /// </summary>
        public override string SourceColumn
        {
            get
            {
                return _sourceColumn;
            }
            set
            {
                _sourceColumn = value;
            }
        }

        private bool _sourceColumnNullMapping;
        /// <summary>
        /// Sets or gets a value which indicates whether the source column is nullable. 
        /// This allows DbCommandBuilder 
        /// to correctly generate Update statements for nullable columns.
        /// </summary>
        public override bool SourceColumnNullMapping
        {
            get
            {
                return _sourceColumnNullMapping;
            }
            set
            {
                _sourceColumnNullMapping = value;
            }
        }

        private DataRowVersion _sourceVersion;
        /// <summary>
        /// Gets or sets the <see cref="DataRowVersion"/> to use when you load Value.
        /// </summary>
        public override DataRowVersion SourceVersion
        {
            get
            {
                return _sourceVersion;
            }
            set
            {
                _sourceVersion = value;
            }
        }


        /// <summary>
        /// Gets or sets the value of the parameter.
        /// </summary>
        public override object Value
        {
            get { return Values[0]; }
            set
            {

                if (value is ICollection)
                {
                    object[] _tvalues = new object[((ICollection)value).Count];
                    ((ICollection)value).CopyTo(_tvalues, 0);
                    this.Values = _tvalues;
                }
                else
                    Values[0] = value;
            }
        }

        private object[] _values = new object[1];
        /// <summary>
        /// Gets or sets the array containing values of the parameter.
        /// </summary>
        public object[] Values
        {
            get { return _values; }
            set
            {
                if (value != null)
                {
                    _values = new object[value.Length];
                    for (int i = 0; i < value.Length; i++)
                    {
                        _values[i] = (IMTField)TypeFactory.CreateTypedField(this.Type, value[i], Format);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value indicating whether parameter contain multiple values.
        /// </summary>
        public bool IsMultiple
        {
            get { return _values.Length > 1; }
        }


        private string _format;
        /// <summary>
        /// Gets or sets the format string of the parameter.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        /// <summary>
        /// Gets the string representation of <see cref="Value"/>.
        /// </summary>
        public string Text
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < Values.Length; i++)
                {
                    result.Append(this.ToString(i));
                    result.Append(",");
                }
                return result.ToString().TrimEnd(',');
            }
        }

        /// <summary>
        /// Returns the string representation of <see cref="Value"/> with specified index.
        /// </summary>
        /// <param name="index">The index of value.</param>
        /// <returns>The string representation of <see cref="Value"/> with specified index.</returns>
        protected virtual string ToString(int index)
        {
            if (Values.Length <= index) return "NULL";
            object val = Values[index];
            if (val is IMTField)
                return ((IMTField)val).ToString(Format);
            else if (val is InMotion.Web.Controls.TypedValue)
            {
                return val.ToString();
            }
            else
            {
                IMTField v = (IMTField)TypeFactory.CreateTypedField(Type, Value, Format);
                return v.ToString(Format);
            }
        }

        private DataType _type;
        /// <summary>
        /// Gets or sets the <see cref="DataType"/> of the parameter.
        /// </summary>
        public virtual DataType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class. 
        /// </summary>
        protected DataParameter()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class that uses 
        /// the parameter name and the value of the new <see cref="DataParameter"/>
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="value">The value of the new <see cref="DataParameter"/> object.</param>
        protected DataParameter(String name, Object value)
            : this(name, DataType.Text, 0, ParameterDirection.Input, String.Empty, DataRowVersion.Default, true, value)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class 
        /// that uses the parameter name and data type. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        protected DataParameter(String name, DataType type)
            : this(name, type, 0)
        { }
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class that uses 
        /// the parameter name, data type, and length. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        protected DataParameter(String name, DataType type, int size)
            : this(name, type, size, String.Empty)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class that uses the parameter name, 
        /// data type, length, and source column name. 
        /// </summary>
        /// <param name="name">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="sourceColumn">The name of the source column.</param>
        protected DataParameter(String name, DataType type, int size, string sourceColumn)
            : this(name, type, size, ParameterDirection.Input, sourceColumn, DataRowVersion.Default, true, null)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class that uses the parameter name, data type,
        /// length, source column name, parameter direction, and other properties.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="direction">One of the <see cref="ParameterDirection"/> values.</param>
        /// <param name="isNullable">true if the value of the field can be null; otherwise false.</param>
        /// <param name="sourceColumn">The name of the data source or source column.</param>
        /// <param name="sourceVersion">One of the <see cref="DataRowVersion"/> values.</param>
        /// <param name="value">An <see cref="Object"/> that is the value of the <see cref="DataParameter"/>.</param>
        protected DataParameter(String parameterName, DataType type, int size, ParameterDirection direction, bool isNullable, string sourceColumn, DataRowVersion sourceVersion, Object value)
        {
            ParameterName = parameterName;
            Type = type;
            Size = size;
            Direction = direction;
            SourceColumn = sourceColumn;
            SourceVersion = sourceVersion;
            IsNullable = isNullable;
            if (value is ICollection)
            {
                object[] _tvalues = new object[((ICollection)value).Count];
                ((ICollection)value).CopyTo(_tvalues, 0);
                this.Values = _tvalues;
            }
            else
                this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameter"/> class that uses the parameter name, data type,
        /// length, source column name, parameter direction, and other properties.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to map.</param>
        /// <param name="type">One of the <see cref="DataType"/> values.</param>
        /// <param name="size">The length of the parameter.</param>
        /// <param name="direction">One of the <see cref="ParameterDirection"/> values.</param>
        /// <param name="sourceColumn">The name of the source column.</param>
        /// <param name="sourceVersion">One of the <see cref="DataRowVersion"/> values.</param>
        /// <param name="sourceColumnNullMapping">Indicates whether the source column is nullable.</param>
        /// <param name="value">An <see cref="Object"/> that is the value of the <see cref="DataParameter"/>.</param>
        protected DataParameter(string parameterName, DataType type, int size, ParameterDirection direction, string sourceColumn, DataRowVersion sourceVersion, bool sourceColumnNullMapping, Object value)
        {
            ParameterName = parameterName;
            Type = type;
            Size = size;
            Direction = direction;
            SourceColumn = sourceColumn;
            SourceColumnNullMapping = sourceColumnNullMapping;
            SourceVersion = sourceVersion;
            if (value is ICollection)
            {
                object[] _tvalues = new object[((ICollection)value).Count];
                ((ICollection)value).CopyTo(_tvalues, 0);
                this.Values = _tvalues;
            }
            else
                this.Value = value;
        }
        #endregion

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="String"/> that represents the current <see cref="Object"/>.
        /// </summary>
        /// <returns>A <see cref="String"/> that represents the current <see cref="Object"/></returns>
        public abstract override string ToString();
    }
}
