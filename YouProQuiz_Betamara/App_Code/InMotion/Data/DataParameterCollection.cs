//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace InMotion.Data
{
    /// <summary>
    /// The base class for a collection of parameters relevant to a <see cref="DataCommand"/>. 
    /// </summary>
    public abstract class DataParameterCollection:DbParameterCollection
    {
        private DataCommand _owner;
        /// <summary>
        /// Gets the <see cref="DataCommand"/> to which this instance belongs.
        /// </summary>
        public DataCommand Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataParameterCollection"/> class. 
        /// </summary>
        /// <param name="owner">The owner of the collection.</param>
        protected DataParameterCollection(DataCommand owner)
        {
            Owner = owner;
        }
    }
}
