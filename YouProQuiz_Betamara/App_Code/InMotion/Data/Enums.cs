//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;

namespace InMotion.Data
{
    /// <summary>
    /// Specify the condition for WhereParameter
    /// </summary>
    public enum WhereParameterCondition
    {
        /// <summary>
        /// The '=' operator will be used.
        /// </summary>
        Equal,
        /// <summary>
        /// The '&lt;&gt;' operator will be used.
        /// </summary>
        NotEqual,
        /// <summary>
        /// The '&lt;' operator will be used.
        /// </summary>
        LessThan,
        /// <summary>
        /// The '&lt;=' operator will be used. 
        /// </summary>
        LessThanOrEqual,
        /// <summary>
        /// The '&gt;' operator will be used. 
        /// </summary>
        GreaterThan,
        /// <summary>
        /// The '&gt;=' operator will be used 
        /// </summary>
        GreaterThanOrEqual,
        /// <summary>
        /// The 'like '...%'' operator will be used
        /// </summary>
        BeginsWith,
        /// <summary>
        /// The 'not like '...%'' operator will be used
        /// </summary>
        NotBeginsWith,
        /// <summary>
        /// The 'like '%...'' operator will be used
        /// </summary>
        EndsWith,
        /// <summary>
        /// The 'not like '...%'' operator will be used
        /// </summary>
        NotEndsWith,
        /// <summary>
        /// The 'like '%...%'' operator will be used
        /// </summary>
        Contains,
        /// <summary>
        /// The 'not like '%...%'' operator will be used
        /// </summary>
        NotContains,
        /// <summary>
        /// The 'Is Not Null' operator will be used
        /// </summary>
        NotNull,
        /// <summary>
        /// The 'Is Null' operator will be used
        /// </summary>
        IsNull,
        /// <summary>
        /// The 'In (...)' operator will be used.
        /// </summary>
        In,
        /// <summary>
        /// The 'Not In (...)' operator will be used.
        /// </summary>
        NotIn,
        /// <summary>
        /// The 'Between ... And ...' operator will be used.
        /// </summary>
        Between,
        /// <summary>
        /// The 'Not Between ... And ...' operator will be used.
        /// </summary>
        NotBetween
    }

    /// <summary>
    /// Define the logical operation for join where parameters
    /// </summary>
    public enum WhereParameterOperation
    {
        /// <summary>
        /// Logical And operator will be used.
        /// </summary>
        And,
        /// <summary>
        /// Logical Or operator will be used.
        /// </summary>
        Or,
    }

    /// <summary>
    /// Specifies how a command string is interpreted.
    /// </summary>
    public enum MTCommandType
    {
        /// <summary>
        /// The name of a table. 
        /// </summary>
        Table,
        /// <summary>
        /// An SQL text command.
        /// </summary>
        Sql,
        /// <summary>
        /// The name of a stored procedure. 
        /// </summary>
        StoredProcedure
    }

}
