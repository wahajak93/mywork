//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace InMotion.Globalization
{
    /// <summary>
    /// represent the InMotion culture information.
    /// </summary>
    public class MTCultureInfo : CultureInfo
    {
        /// <summary>
        /// Initialize the new instance of the <see cref="MTCultureInfo"/> class class based on the culture specified by name.
        /// </summary>
        /// <param name="name">A predefined <see cref="CultureInfo"/> name, Name of an existing CultureInfo, or Windows-only culture name.</param>
        public MTCultureInfo(string name)
            : base(name)
        {
        }
        private string m_BooleanFormat;
        private string m_numberZeroFormat;
        private string m_numberNullFormat;
        private string m_outputEncoding;
        private string m_defaultCountry;
        private string[] m_weekdayNarrowNames;

        /// <summary>
        /// Gets or sets the format string for boolean values.
        /// </summary>
        public string BooleanFormat
        {
            get
            {
                return m_BooleanFormat;
            }
            set
            {
                m_BooleanFormat = value;
            }

        }

        /// <summary>
        /// Gets or sets the format string for numeric value that equals to zero.
        /// </summary>
        public string NumberZeroFormat
        {
            get
            {
                return m_numberZeroFormat;
            }
            set
            {
                m_numberZeroFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets a one-dimensional array of type <see cref="String"/> containing the culture-specific narrow names of the days of the week.
        /// </summary>
        public string[] WeekdayNarrowNames
        {
            get
            {
                return m_weekdayNarrowNames;
            }
            set
            {
                m_weekdayNarrowNames = value;
            }
        }

        /// <summary>
        /// Gets or sets the default country name for the neutral culture.
        /// </summary>
        public string DefaultCountry
        {
            get
            {
                return m_defaultCountry;
            }
            set
            {
                m_defaultCountry = value;
            }
        }

        /// <summary>
        /// Gets or sets the format string for numeric value that equals to null.
        /// </summary>
        public string NumberNullFormat
        {
            get
            {
                return m_numberNullFormat;
            }
            set
            {
                m_numberNullFormat = value;
            }

        }

        /// <summary>
        /// Gets or sets the encoding name that used for response and request.
        /// </summary>
        public string Encoding
        {
            get
            {
                return m_outputEncoding;
            }
            set
            {
                m_outputEncoding = value;
            }

        }
    }

}
