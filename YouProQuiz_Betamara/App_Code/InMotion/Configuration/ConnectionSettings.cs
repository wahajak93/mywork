//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InMotion.Configuration
{
    /// <summary>
    /// Represents a single, named connection in the mtConnections 
    /// configuration file section. 
    /// </summary>
    public class ConnectionSettings : ConfigurationSection
    {
        /// <summary>
        /// Gets or sets the ConnectionSettings name.
        /// </summary>
        [ConfigurationProperty("name", DefaultValue = "Connection1", IsDefaultCollection = false, IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
        /// <summary>
        /// Gets or sets the provider name property.
        /// </summary>
        [ConfigurationProperty("providerName", DefaultValue = "System.Data.OleDb", IsDefaultCollection = false, IsKey = false, IsRequired = true)]
        public string ProviderName
        {
            get { return (string)this["providerName"]; }
            set { this["providerName"] = value; }
        }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        [ConfigurationProperty("connectionString", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = true)]
        public string ConnectionString
        {
            get { return (string)this["connectionString"]; }
            set { this["connectionString"] = value; }
        }

        /// <summary>
        /// Get or sets the default date format string that associated with connection.
        /// </summary>
        [ConfigurationProperty("dateFormat", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public string DateFormat
        {
            get { return (string)this["dateFormat"]; }
            set { this["dateFormat"] = value; }
        }
        
        /// <summary>
        /// Get or sets the default date bool string that associated with connection.
        /// </summary>
        [ConfigurationProperty("boolFormat", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public string BooleanFormat
        {
            get { return (string)this["boolFormat"]; }
            set { this["boolFormat"] = value; }
        }
        /// <summary>
        /// Gets or sets the database name that associated with connection.
        /// </summary>
        [ConfigurationProperty("database", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public string Database
        {
            get { return (string)this["database"]; }
            set { this["database"] = value; }
        }

        /// <summary>
        /// Gets or sets the left delimiter that used for format date value inside the SQL statements.
        /// </summary>
        [ConfigurationProperty("dateLeftDelimiter", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public string DateLeftDelimiter
        {
            get { return (string)this["dateLeftDelimiter"]; }
            set { this["dateLeftDelimiter"] = value; }
        }

        /// <summary>
        /// Gets or sets the right delimiter that used for format date value inside the SQL statements.
        /// </summary>
        [ConfigurationProperty("dateRightDelimiter", DefaultValue = "", IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public string DateRightDelimiter
        {
            get { return (string)this["dateRightDelimiter"]; }
            set { this["dateRightDelimiter"] = value; }
        }

        /// <summary>
        /// Gets or sets the value which indicating whether Select SQL command optimization will be used.
        /// </summary>
        [ConfigurationProperty("useOptimization", DefaultValue = false, IsDefaultCollection = false, IsKey = false, IsRequired = false)]
        public bool UseOptimization
        {
            get { return (bool)this["useOptimization"]; }
            set { this["useOptimization"] = value; }
        }

    }
}
