//This file is part of InMotion Framework ASP.NET 1.09.06
//(c) 2007-2008 by InMotion Systems
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Diagnostics;
using InMotion.Globalization;
using InMotion.Data;

namespace InMotion.Configuration
{
    /// <summary>
    /// The helper class that store application settings and create some common objects.
    /// </summary>
    public static class AppConfig
    {
        private static Connection CreateConnectionObject(ConnectionSettings cs)
        {
            Connection conn = new Connection(cs.ProviderName);
            conn.ConnectionString = cs.ConnectionString;
            conn.DateFormat = cs.DateFormat;
            conn.BooleanFormat = cs.BooleanFormat;
            conn.DateLeftDelimiter = cs.DateLeftDelimiter;
            conn.DateRightDelimiter = cs.DateRightDelimiter;
            conn.Server = cs.Database;
            conn.UseOptimization = cs.UseOptimization;
            return conn;
        }

        /// <summary>
        /// Returns the <see cref="Connection"/> by the name.
        /// </summary>
        /// <param name="name">The name of the connection</param>
        /// <returns>A <see cref="Connection"/> object.</returns>
        /// <exception cref="ConfigurationErrorsException">web.config does not contain mtConnection section
        /// <i>- or -</i>
        /// the configuration settings for connection with supplied name does not exist.
        /// </exception>
        public static Connection GetConnection(string name)
        {
            ConnectionSection sec = ConfigurationManager.GetSection("mtConnections") as ConnectionSection;
            if (sec == null)
            {
                if (IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Unable to load mtConnections section from web.config file.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to load mtConnections section from web.config file");
            }
            ConnectionSettings cs = sec.Connections[name];
            if (cs == null)
            {
                if (IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("The specified connection {0} does not exists.\n{1}", name, Environment.StackTrace);
                throw new ConfigurationErrorsException(String.Format("The specified connection {0} does not exists", name));
            }
            return CreateConnectionObject(cs);
        }

        /// <summary>
        /// Returns the first (default) <see cref="Connection"/> from the configuration file.
        /// </summary>
        /// <returns>A <see cref="Connection"/> object.</returns>
        /// <exception cref="ConfigurationErrorsException">web.config does not contain mtConnection section</exception>
        public static Connection GetDefaultConnection()
        {
            ConnectionSection sec = ConfigurationManager.GetSection("mtConnections") as ConnectionSection;
            if (sec == null)
            {
                if (IsMTErrorHandlerUse("critical"))
                    Trace.TraceError("Unable to load mtConnections section from web.config file.\n{0}", Environment.StackTrace);
                throw new ConfigurationErrorsException("Unable to load mtConnections section from web.config file");
            }
            int validConnectionsCount = 0;
            ConnectionSettings cs = null;
            ConnectionSettings valid_cs = null;
            for (int i = 0; i < sec.Connections.Count; i++)
            {
                cs = sec.Connections[i];
                if (!String.IsNullOrEmpty(cs.ConnectionString))
                {
                    validConnectionsCount++;
                    valid_cs = cs;
                }
            }
            if (validConnectionsCount == 1) return CreateConnectionObject(valid_cs);
            return null;
        }

        internal static Dictionary<string, MTCultureInfo> Locales
        {
            get { return (Dictionary<string, MTCultureInfo>)ConfigurationManager.GetSection("locales"); }
        }

        /// <summary>
        /// Gets the Dictionary&lt;int, Security.SecurityGroup&gt; that contains list of defined for application security groups.
        /// </summary>
        public static Dictionary<int, Security.SecurityGroup> SecurityGroups
        {
            get { return (Dictionary<int, Security.SecurityGroup>)ConfigurationManager.GetSection("securityGroups"); }
        }

        /// <summary>
        /// Gets the application root url that used for resolving links.
        /// </summary>
        public static string ServerUrl
        {
            get { return ConfigurationManager.AppSettings["ServerUrl"]; }
        }

        /// <summary>
        /// Gets the secure server url that used for communication with serevr via HTTPS proptocol.
        /// </summary>
        public static string SecureUrl
        {
            get { return ConfigurationManager.AppSettings["SecureUrl"]; }
        }

        #region Dynamic Styles
        /// <summary>
        /// Indicates is dynamic styles is enabled for application.
        /// </summary>
        /// <exception cref="ConfigurationErrorsException">The UseDynamicStyles key is missed in appSettings section of configuration file.</exception>
        public static bool UseDynamicStyles
        {
            get
            {
                try
                {
                    if (ConfigurationManager.AppSettings["UseDynamicStyles"] != null)
                        return bool.Parse(ConfigurationManager.AppSettings["UseDynamicStyles"]);
                    else
                        return false;
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read UseDynamicStyles parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read UseDynamicStyles parameter from configuration file", e);
                }
            }
        }

        /// <summary>
        /// Indicates is query string parameter is allowed to switch styles.
        /// </summary>
        public static bool UseUrlStyleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["UrlStyleParameter"]);
            }
        }

        /// <summary>
        /// Gets the name of query string parameter that switch styles.
        /// </summary>
        public static string UrlStyleParameterName
        {
            get
            {
                return ConfigurationManager.AppSettings["UrlStyleParameter"];
            }
        }

        /// <summary>
        /// Indicates is cookie is allowed to switch styles.
        /// </summary>
        public static bool UseCookieStyleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["CookieStyleParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of cookie that switch styles.
        /// </summary>
        public static string CookieStyleParameterName
        {
            get { return ConfigurationManager.AppSettings["CookieStyleParameterName"]; }
        }

        /// <summary>
        /// Indicates is session variable is allowed to switch styles.
        /// </summary>
        public static bool UseSessionStyleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SessionStyleParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of session variable that switch styles.
        /// </summary>
        public static string SessionStyleParameterName
        {
            get { return ConfigurationManager.AppSettings["SessionStyleParameterName"]; }
        }

        /// <summary>
        /// Gets the name of default style.
        /// </summary>
        public static string DefaultStyleName
        {
            get { return ConfigurationManager.AppSettings["DefaultStyleName"]; }
        }

        /// <summary>
        /// Gets the number of days until style cookie is expired.
        /// </summary>
        public static int StyleCookieExpiration
        {
            get
            {
                try
                {
                    return Int32.Parse(ConfigurationManager.AppSettings["StyleCookieExpiration"]);
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read StyleCookieExpiration parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read StyleCookieExpiration parameter from configuration file", e);
                }
            }
        }
        #endregion

        #region Dynamic Designs
        /// <summary>
        /// Indicates is dynamic designs is enabled for application.
        /// </summary>
        /// <exception cref="ConfigurationErrorsException">The UseDynamicDesigns key is missed in appSettings section of configuration file.</exception>
        public static bool UseDynamicDesigns
        {
            get
            {
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultDesignName"]);
            }
        }

        /// <summary>
        /// Indicates is query string parameter is allowed to switch designs.
        /// </summary>
        public static bool UseUrlDesignParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["UrlDesignParameter"]);
            }
        }

        /// <summary>
        /// Gets the name of query string parameter that switch designs.
        /// </summary>
        public static string UrlDesignParameterName
        {
            get
            {
                return ConfigurationManager.AppSettings["UrlDesignParameter"];
            }
        }

        /// <summary>
        /// Indicates is cookie is allowed to switch designs.
        /// </summary>
        public static bool UseCookieDesignParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["CookieDesignParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of cookie that switch designs.
        /// </summary>
        public static string CookieDesignParameterName
        {
            get { return ConfigurationManager.AppSettings["CookieDesignParameterName"]; }
        }

        /// <summary>
        /// Indicates is session variable is allowed to switch designs.
        /// </summary>
        public static bool UseSessionDesignParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SessionDesignParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of session variable that switch designs.
        /// </summary>
        public static string SessionDesignParameterName
        {
            get { return ConfigurationManager.AppSettings["SessionDesignParameterName"]; }
        }

        /// <summary>
        /// Gets the name of default design.
        /// </summary>
        public static string DefaultDesignName
        {
            get { return ConfigurationManager.AppSettings["DefaultDesignName"]; }
        }
        /// <summary>
        /// Gets the name of default design.
        /// </summary>
        public static string DesignFolder
        {
            get { return ConfigurationManager.AppSettings["DesignFolder"]; }
        }
        /// <summary>
        /// Gets the number of days until design cookie is expired.
        /// </summary>
        public static int DesignCookieExpiration
        {
            get
            {
                try
                {
                    return Int32.Parse(ConfigurationManager.AppSettings["DesignCookieExpiration"]);
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read DesignCookieExpiration parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read DesignCookieExpiration parameter from configuration file", e);
                }
            }
        }
        #endregion

        /// <summary>
        /// Gets the default language of application.
        /// </summary>
        public static string DefaultLanguage
        {
            get { return ConfigurationManager.AppSettings["DefaultLanguage"]; ; }
        }

        /// <summary>
        /// Gets the date format string that applied by default to the <see cref="Common.MTDate"/> value.
        /// </summary>
        public static string DefaultDateFormat
        {
            get { return ConfigurationManager.AppSettings["DefaultDateFormat"]; }
        }

        /// <summary>
        /// Indicates is necessary to execute the Trace. 
        /// </summary>
        /// <param name="level">Minimal level to execute Trace.</param>
        /// <returns><b>true</b> if obj has the same value as this instance; otherwise, <b>false</b>.</returns>
        public static bool IsMTErrorHandlerUse(string level)
        {
            try
            {
                string[] StrLevels = new string[] { ConfigurationManager.AppSettings["MTErrorHandlerLevel"], level };
                int[] Levels = new int[2];
                for (int i = 0; i < 2; i++)
                    switch (StrLevels[i])
                    {
                        case "all": Levels[i] = 3; break;
                        case "uncritical": Levels[i] = 2; break;
                        case "critical": Levels[i] = 1; break;
                        case "disabled": Levels[i] = 0; break;
                        default: Levels[i] = 0; break;
                    }
                return (Levels[0] >= Levels[1]);
            }
            catch (Exception e)
            {
                Trace.TraceError("Unable to read MTErrorHandlerLevel parameter from configuration file.\n{0}", e);
                return false;
            }
        }

        /// <summary>
        /// Indicates is I18N (internationalization) functionality is enabled.
        /// </summary>
        /// <exception cref="ConfigurationErrorsException"> The UseI18N key does not exists in appSettings section of configuration file.</exception>
        public static bool UseI18N
        {
            get
            {
                bool val;
                if (bool.TryParse(ConfigurationManager.AppSettings["UseI18N"], out val))
                    return val;
                else
                    return false;
            }
        }

        /// <summary>
        /// Indicates is query string parameter is allowed to switch current locale.
        /// </summary>
        public static bool UseUrlLocaleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["UrlLocaleParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of query string parameter that switch current locale.
        /// </summary>
        public static string UrlLocaleParameterName
        {
            get { return ConfigurationManager.AppSettings["UrlLocaleParameterName"]; }
        }

        /// <summary>
        /// Indicates is cookie is allowed to switch current locale.
        /// </summary>
        public static bool UseCookieLocaleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["CookieLocaleParameterName"]);
            }
        }

        /// <summary>
        /// Gets the name of cookie that switch current locale.
        /// </summary>
        public static string CookieLocaleParameterName
        {
            get { return ConfigurationManager.AppSettings["CookieLocaleParameterName"]; }
        }

        /// <summary>
        /// Indicates is session variable is allowed to switch current locale.
        /// </summary>
        public static bool UseSessionLocaleParameter
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SessionLocaleParameterName"]);
            }
        }
        /// <summary>
        /// Gets the name of session variable that switch current locale.
        /// </summary>
        public static string SessionLocaleParameterName
        {
            get { return ConfigurationManager.AppSettings["SessionLocaleParameterName"]; }
        }

        /// <summary>
        /// Gets the name of variable that store current language name.
        /// </summary>
        public static string SessionLanguageParameterName
        {
            get { return ConfigurationManager.AppSettings["SessionLanguageParameterName"]; }
        }

        /// <summary>
        /// Indicates if accept-languages HTTP header is used for selecting current locale.
        /// </summary>
        public static bool UseHttpHeaderLocaleParameter
        {
            get
            {
                try
                {
                    if (ConfigurationManager.AppSettings["UseHTTPHeaderLocaleParameter"] != null)
                        return bool.Parse(ConfigurationManager.AppSettings["UseHTTPHeaderLocaleParameter"]);
                    else
                        return false;
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read UseHTTPHeaderLocaleParameter parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read UseHTTPHeaderLocaleParameter parameter from configuration file", e);
                }
            }
        }

        /// <summary>
        /// Gets the number of days until locale cookie is expired.
        /// </summary>
        public static int LocaleCookieExpiration
        {
            get
            {
                try
                {
                    return Int32.Parse(ConfigurationManager.AppSettings["LocaleCookieExpiration"]);
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read LocaleCookieExpiration parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read LocaleCookieExpiration parameter from configuration file", e);
                }
            }
        }

        /// <summary>
        /// Indicates if text of SQL statement that caused the error will be included into exception message.
        /// </summary>
        public static bool ShowSqlOnError
        {
            get
            {
                try
                {
                    if (ConfigurationManager.AppSettings["ShowSqlOnError"] != null)
                        return bool.Parse(ConfigurationManager.AppSettings["ShowSqlOnError"]);
                    else
                        return false;
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read ShowSqlOnError parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read ShowSqlOnError parameter from configuration file", e);
                }
            }
        }

        /// <summary>
        /// Indicates if missing data parameters will be excluded from the data update operations.
        /// </summary>
        public static bool ExcludeMissingDataParameters
        {
            get
            {
                try
                {
                    if (ConfigurationManager.AppSettings["ExcludeMissingDataParameters"] != null)
                        return bool.Parse(ConfigurationManager.AppSettings["ExcludeMissingDataParameters"]);
                    else
                        return false;
                }
                catch (Exception e)
                {
                    if (IsMTErrorHandlerUse("critical"))
                        Trace.TraceError("Unable to read ExcludeMissingDataParameters parameter from configuration file.\n{0}", e);
                    throw new ConfigurationErrorsException("Unable to read ExcludeMissingDataParameters parameter from configuration file", e);
                }
            }
        }

        /// <summary>
        /// Name of session variable which store id of current user
        /// </summary>
        public static string UserIdSessionVariableName
        {
            get
            {
                Security.MTMembershipProvider conf = (Security.MTMembershipProvider)Membership.Providers["MTMembershipProvider"];
                return conf.UserIdSessionVariable;
            }
        }

        /// <summary>
        /// Name of session variable which store login of current user
        /// </summary>
        public static string UserLoginSessionVariableName
        {
            get
            {
                Security.MTMembershipProvider conf = (Security.MTMembershipProvider)Membership.Providers["MTMembershipProvider"];
                return conf.UserLoginSessionVariable;
            }
        }

        /// <summary>
        /// Name of session variable which store group id of current user
        /// </summary>
        public static string UserGroupSessionVariableName
        {
            get
            {
                Security.MTMembershipProvider conf = (Security.MTMembershipProvider)Membership.Providers["MTMembershipProvider"];
                return conf.UserGroupSessionVariable;
            }
        }

        /// <summary>
        /// Indicates to use &amp;amp; instead of &amp; in links.
        /// </summary>
        public static bool UseAmp
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["UseAmp"]))
                    return false;
                return ConfigurationManager.AppSettings["UseAmp"] == "true";
            }
        }

        /// <summary>
        /// Indicates that project uses jQuery in client scripts.
        /// </summary>
        public static bool UseJQuery
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["UseJQuery"]))
                    return false;
                return ConfigurationManager.AppSettings["UseJQuery"] == "true";
            }
        }

        public static string AutoLoginCookieName
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AutoLoginCookieName"]))
                    return "";
                return ConfigurationManager.AppSettings["AutoLoginCookieName"];
            }
        }

        public static bool SlidingExpiration
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SlidingExpiration"]))
                    return false;
                return ConfigurationManager.AppSettings["SlidingExpiration"] == "true";
            }
        }

        public static int CookieExpired
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["CookieExpired"]))
                    return 0;
                return int.Parse(ConfigurationManager.AppSettings["CookieExpired"]);
            }
        }

        public static string RC4EncryptionKey
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RC4EncryptionKey"]))
                    return "";
                return ConfigurationManager.AppSettings["RC4EncryptionKey"];
            }
        }
        /// <summary>
        /// Type of HTML template.
        /// </summary>
        public static string HTMLTemplateType
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["HTMLTemplateType"]))
                    return "HTML";
                return ConfigurationManager.AppSettings["HTMLTemplateType"];
            }
        }

        /// <summary>
        /// Expression which will be used to encrypt password in data base
        /// </summary>
        public static string ProtectPasswordsExpression
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ProtectPasswordsExpression"]))
                    return "";
                return ConfigurationManager.AppSettings["ProtectPasswordsExpression"];
            }
        }

        /// <summary>
        /// Method which will be used to protect passwords in data base
        /// </summary>
        public static string ProtectPasswordsMethod
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ProtectPasswordsMethod"]))
                    return "";
                return ConfigurationManager.AppSettings["ProtectPasswordsMethod"];
            }
        }
    }
}
