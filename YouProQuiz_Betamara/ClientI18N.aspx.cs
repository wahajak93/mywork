//Client @1-F6C22642
using System;
using System.Web;
using System.Web.UI;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using InMotion.Web;

public partial class __client : MTPage
{
	protected NameValueCollection allowedFiles = new NameValueCollection();
	private void Page_Load(object sender, System.EventArgs e)
	{
		if(Request.QueryString["file"]==null || allowedFiles[Request.QueryString["file"]] == null) Response.End();
		StreamReader sr = new StreamReader(MapPath(Request.QueryString["file"]));
		string rawBody = sr.ReadToEnd();
		sr.Close();
		StringBuilder body = new StringBuilder(rawBody);
		Regex res = new Regex("{res:([^}]*)}",RegexOptions.Multiline);
		MatchCollection positions = res.Matches(rawBody);
		foreach(Match m in positions)
				body.Replace(m.Value, ResManager.GetString(m.Groups[1].Value));
		
		Response.ContentType = allowedFiles[Request.QueryString["file"]];
		Response.Write(body.ToString());
		Response.End();
		
	}

	#region Web Form Designer generated code
	override protected void OnInit(EventArgs e)
	{
		InitializeComponent();
		base.OnInit(e);
		allowedFiles.Add("DatePicker.js","application/x-javascript");
		allowedFiles.Add("Functions.js","application/x-javascript");
		allowedFiles.Add("Globalize.js","application/x-javascript");
	}
	
	private void InitializeComponent()
	{    
		this.Load += new System.EventHandler(this.Page_Load);
	}
	#endregion
}

//End Client

