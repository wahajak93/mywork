//Using statements @1-AFBC0BCB
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-8E577C85
public partial class UCKMS_AddNewQuestion3_Page : MTUserControl
{
//End Page class

//Attributes constants @1-1817FD6B
    public const string Attribute_tbl_Quiz_Question_ChoicetblChoice_EditableGridForm = "tbl_Quiz_Question_ChoicetblChoice_EditableGridForm";
    public const string Attribute_ID = "ID";
    public const string Attribute_Quiz_ID = "Quiz_ID";
    public const string Attribute_Choice_ID = "Choice_ID";
    public const string Attribute_Quest_ID = "Quest_ID";
    public const string Attribute_Choice_Score = "Choice_Score";
    public const string Attribute_Choice = "Choice";
    public const string Attribute_CCS_Delete = "CCS_Delete";
    public const string Attribute_CCS_NoRecords = "CCS_NoRecords";
    public const string Attribute_CCS_RecPerPage = "CCS_RecPerPage";
    public const string Attribute_tbl_Quiz_Question_AnswerstblChoice_EditableGridForm = "tbl_Quiz_Question_AnswerstblChoice_EditableGridForm";
    public const string Attribute_ChoiceID = "ChoiceID";
    public const string Attribute_Score = "Score";
    public const string Attribute_id = "id";
    public const string Attribute_QuestionID = "QuestionID";
    public const string Attribute_CCS_ASC = "CCS_ASC";
    public const string Attribute_pathToRoot = "pathToRoot";
    public const string Attribute_CCS_DESC = "CCS_DESC";
    public const string Attribute_rowNumber = "rowNumber";
    public const string Attribute_CCS_LanguageID = "CCS_LanguageID";
    public const string Attribute_CCS_Update = "CCS_Update";
//End Attributes constants

//Page UCKMS_AddNewQuestion3 Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_AddNewQuestion3 Event Init

//ScriptIncludesInit @1-884C2296
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/ui/jquery.ui.core.js|js/jquery/ui/jquery.ui.widget.js|js/jquery/ui/jquery.ui.datepicker.js|js/jquery/datepicker/ccs-date-timepicker.js|ckeditor/ckeditor.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page UCKMS_AddNewQuestion3 Init event tail @1-FCB6E20C
    }
//End Page UCKMS_AddNewQuestion3 Init event tail

//Record tblQuestion Event Initialize Update Parameters @2-EF04F380
    protected void tblQuestion_InitializeUpdateParameters(object sender, EventArgs e) {
//End Record tblQuestion Event Initialize Update Parameters

//Initialize expression parameters and default values  @2-3460D4B0
        ((MTDataSourceView)sender).UpdateParameters["catid"].DefaultValue = 1;
        ((MTDataSourceView)sender).UpdateParameters["typid"].DefaultValue = 1;
        ((MTDataSourceView)sender).UpdateParameters["time"].DefaultValue = 1;
        ((MTDataSourceView)sender).UpdateParameters["quiz_id"].DefaultValue = 0;
//End Initialize expression parameters and default values 

//Record tblQuestion Initialize Update Parameters event tail @2-FCB6E20C
    }
//End Record tblQuestion Initialize Update Parameters event tail

//Record tblQuestion Event Before Insert @2-8A6DA2E0
    protected void tblQuestion_BeforeInsert(object sender, EventArgs e) {
//End Record tblQuestion Event Before Insert

//Record tblQuestion Event Before Insert. Action Retrieve Value for Control @18-B7C1C283
        tblQuestion.GetControl<InMotion.Web.Controls.MTHidden>("QuestionInsertedBy").Value = Session["UserID"];
//End Record tblQuestion Event Before Insert. Action Retrieve Value for Control

//Record tblQuestion Event Before Insert. Action Retrieve Value for Control @19-7A110D84
       
        tblQuestion.GetControl<InMotion.Web.Controls.MTHidden>("QuestionInsertedOn").Value =DateTime.Now;
//End Record tblQuestion Event Before Insert. Action Retrieve Value for Control

//Record tblQuestion Before Insert event tail @2-FCB6E20C
    }
//End Record tblQuestion Before Insert event tail

//Record tblQuestion Event After Insert @2-64F0261A
    protected void tblQuestion_AfterInsert(object sender, DataOperationCompletingEventArgs e) {
//End Record tblQuestion Event After Insert


string maxid="";
//Record tblQuestion Event Before Insert. Action Retrieve number of records @163-E975C27D
        
        Connection conn = (Connection)DataUtility.GetConnectionObject("InMotion:KMS");
       DataCommand select=(DataCommand)conn.CreateCommand();
  select.MTCommandType = MTCommandType.Table;
  select.CommandText = "Select MAX(QuestionID) as maxid from tblQuestion";
  
  DataRowCollection newDr = select.Execute().Tables[0].Rows;
////for (int i = 0; i < newDr.Count; i++)
////{
foreach(DataRow dr in newDr)
{

maxid=dr["maxid"].ToString();
//maxid=(Convert.ToInt32(maxid)+1).ToString();
}

if(maxid==null)
{
	maxid="1";
	}

string  Quiz_Quest_Type_ID = tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox1").Value.ToString();
string  Quiz_Quest_Time_Taken = tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Value.ToString();
string  Quiz_Quest_Cat_ID = tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox2").Value.ToString();
string quiz_id = Request.QueryString["Quiz_ID"];




DataCommand insert_to_quiz_question=(DataCommand)conn.CreateCommand();
insert_to_quiz_question.MTCommandType = MTCommandType.Table;
insert_to_quiz_question.CommandText = "INSERT INTO tbl_Quiz_Question (Quiz_ID,Quiz_Quest_ID,Quiz_Quest_Category_ID,Quiz_Quest_Type_ID,Quiz_Quest_Time) VALUES ("+quiz_id+","+maxid+","+Quiz_Quest_Cat_ID+","+Quiz_Quest_Type_ID+","+Quiz_Quest_Time_Taken+");";
//update.Parameters.Add(new InMotion.Data.SqlParameter("a", DataType.Integer));
//update.Parameters.Add(new InMotion.Data.SqlParameter("b", DataType.Text));

insert_to_quiz_question.ExecuteNonQuery();

Response.Redirect("~/KMS_AddNewQuestion.aspx?Quiz_id="+quiz_id+"&QuestionID="+maxid+"");











//Record tblQuestion Event After Insert. Action Custom Code @115-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblQuestion Event After Insert. Action Custom Code

//Record tblQuestion After Insert event tail @2-FCB6E20C
    }
//End Record tblQuestion After Insert event tail

//Record tblQuestion Event Before Update @2-1D5F12B8
    protected void tblQuestion_BeforeUpdate(object sender, EventArgs e) {
//End Record tblQuestion Event Before Update
  tblQuestion.GetControl<InMotion.Web.Controls.MTHidden>("QuestionUpdatedOn").Value =DateTime.Now;
  tblQuestion.GetControl<InMotion.Web.Controls.MTHidden>("QuestionUpdatedBy").Value =Session["UserID"];


//Record tblQuestion Event Before Update. Action Custom Code @182-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblQuestion Event Before Update. Action Custom Code

//Record tblQuestion Before Update event tail @2-FCB6E20C
    }
//End Record tblQuestion Before Update event tail

//Record tblQuestion Event After Delete @2-12487CFC
    protected void tblQuestion_AfterDelete(object sender, DataOperationCompletingEventArgs e) {
//End Record tblQuestion Event After Delete


string quiz_id=Request.QueryString["Quiz_id"].ToString();

string QuestionID=Request.QueryString["QuestionID"].ToString();

Response.Redirect("~/KMS_EditQuiz.aspx?Quiz_id="+quiz_id+"");


//Record tblQuestion Event After Delete. Action Custom Code @181-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblQuestion Event After Delete. Action Custom Code

//Record tblQuestion After Delete event tail @2-FCB6E20C
    }
//End Record tblQuestion After Delete event tail

//Hidden QuestionUpdatedOn Event Load @13-27F35CB9
    protected void tblQuestionQuestionUpdatedOn_Load(object sender, EventArgs e) {
//End Hidden QuestionUpdatedOn Event Load

//Set Default Value @13-D42124CF
        ((InMotion.Web.Controls.MTHidden)sender).DefaultValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
//End Set Default Value

//Hidden QuestionUpdatedOn Load event tail @13-FCB6E20C
    }
//End Hidden QuestionUpdatedOn Load event tail

//Record tblChoice Event Initialize Insert Parameters @43-DC5ED6CC
    protected void tblChoice_InitializeInsertParameters(object sender, EventArgs e) {
//End Record tblChoice Event Initialize Insert Parameters

//Initialize expression parameters and default values  @43-4CF1F214
        ((MTDataSourceView)sender).InsertParameters["choiceid"].DefaultValue = 1;
        ((MTDataSourceView)sender).InsertParameters["quiz_id"].DefaultValue = 1;
        ((MTDataSourceView)sender).InsertParameters["score"].DefaultValue = 1;
//End Initialize expression parameters and default values 

//Record tblChoice Initialize Insert Parameters event tail @43-FCB6E20C
    }
//End Record tblChoice Initialize Insert Parameters event tail

//Record tblChoice Event Before Insert @43-5D9093F3
    protected void tblChoice_BeforeInsert(object sender, EventArgs e) {
//End Record tblChoice Event Before Insert

//Record tblChoice Event Before Insert. Action Custom Code @51-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblChoice Event Before Insert. Action Custom Code

//Record tblChoice Before Insert event tail @43-FCB6E20C
    }
//End Record tblChoice Before Insert event tail

//Record tblChoice Event After Insert @43-2B8EEC31
    protected void tblChoice_AfterInsert(object sender, DataOperationCompletingEventArgs e) {
//End Record tblChoice Event After Insert

//Record tblChoice Event After Insert. Action Custom Code @50-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblChoice Event After Insert. Action Custom Code

//Record tblChoice After Insert event tail @43-FCB6E20C
    }
//End Record tblChoice After Insert event tail

//TextBox TextBox1 Event On Validate @100-EAC15C71
    protected void tblAnswerTextBox1_Validating(object sender, ValidatingEventArgs e) {
//End TextBox TextBox1 Event On Validate

//TextBox TextBox1 Event On Validate. Action Validate Phone @334-6264B446
        if (!tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").IsEmpty)
        {
            Regex mask = new Regex(@"^[0-9]*$", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (!mask.Match(tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Text).Success)
            {
                tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Errors.Add(String.Format("Only numbers", tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Caption));
                e.HasErrors = true;
            }
        }
//End TextBox TextBox1 Event On Validate. Action Validate Phone

//TextBox TextBox1 Event On Validate. Action Validate Maximum Value @335-8801444E
        if (!tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").IsEmpty && tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").GetFloat() > 100)
        {
            tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Errors.Add(String.Format("maximum 100 points for each answer", tblAnswer.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Caption, "100"));
            e.HasErrors = true;
        }
//End TextBox TextBox1 Event On Validate. Action Validate Maximum Value

//TextBox TextBox1 On Validate event tail @100-FCB6E20C
    }
//End TextBox TextBox1 On Validate event tail

//CheckBox CheckBox_Delete Event Init @245-936268BC
    protected void tbl_Quiz_Question_ChoiceCheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @245-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @245-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @245-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @245-6E5D9D0D
    protected void tbl_Quiz_Question_ChoiceCheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @245-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @245-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//CheckBox CheckBox_Delete Event Init @295-BB6B687D
    protected void tbl_Quiz_Question_AnswersCheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @295-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @295-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @295-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @295-46549DCC
    protected void tbl_Quiz_Question_AnswersCheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @295-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @295-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C
    protected void tblChoice_BeforeShow(object sender, EventArgs e)
    {

        abc();

           

    }
    protected void tbl_Quiz_Question_Choice_BeforeShow(object sender, EventArgs e)
    {
        abc();
    }



    private void abc()
    {

        string quiz = Request.QueryString["Quiz_id"];
        string quest = Request.QueryString["QuestionID"];

        DataTable dt1 = clsdb.readdataKMS("SELECT * From tbl_Quiz_Question where Quiz_id=" + quiz + " AND Quiz_Quest_ID=" + quest + "");
        // Quiz_id=25&QuestionID=37

        if (dt1.Rows.Count<1)
        {
            tbl_Quiz_Question_Choice.Visible = false;
            tblChoice.Visible = false;
            tbl_Quiz_Question_Answers.Visible = false;
            tblAnswer.Visible = false;
        }
    }

    protected void tblQuestion_Validating(object sender, ValidatingEventArgs e)
    {

        if (tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question_Name").Errors.Add("Cannot be empty");

            // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }

        ///////////////////////////////////////////////////////////////////

        if (tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("Question").Errors.Add("Cannot be empty");

            // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }
        
        
        ///////////////////////////////////////////////////////////



        if (tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionStatusID").SelectedValue == "")
        {

            tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("QuestionStatusID").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }

        ////////////////////////////////////



        if (tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox2").SelectedValue == "")
        {

            tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox2").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }

        ////////////////////////////////////


        if (tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox1").SelectedValue == "")
        {

            tblQuestion.GetControl<InMotion.Web.Controls.MTListBox>("ListBox1").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }

        /////////

        if (tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Text == "")
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");

            tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Errors.Add("Cannot be empty");

            // TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label6").Value = "Cannot be empty";

            e.HasErrors = true;
        }
        if (!System.Text.RegularExpressions.Regex.IsMatch(tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Text, "^[0-9]*$"))
        {
            //   TBL_QuizSearch.GetControl<InMotion.Web.Controls.MTTextBox>("TutorPassword").Errors.Add("The value in field Password is required");
            tblQuestion.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Errors.Add("must contain digits only");
            e.HasErrors = true;
        }





    }
    protected void tblAnswer_BeforeShow(object sender, EventArgs e)
    {

    }
    protected void Label8_BeforeShow(object sender, EventArgs e)
    {


     


    }
    protected void tblChoice_Validating(object sender, ValidatingEventArgs e)
    {
        if (tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("Choice").Text == "")
        {

            tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("Choice").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }

        if (tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Text == "")
        {

            tblChoice.GetControl<InMotion.Web.Controls.MTTextBox>("TextBox1").Errors.Add("Cannot be empty");

            //TBL_Quiz.GetControl<InMotion.Web.Controls.MTLabel>("Label3").Value = "Cannot Be empty";

            e.HasErrors = true;
        }




    }
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

