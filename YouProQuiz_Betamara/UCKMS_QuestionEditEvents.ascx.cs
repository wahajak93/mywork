//Using statements @1-AFBC0BCB
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-D0D1B7D3
public partial class UCKMS_QuestionEdit_Page : MTUserControl
{
//End Page class

//Attributes constants @1-2F9F6C23
    public const string Attribute_rowNumber = "rowNumber";
//End Attributes constants

//Page UCKMS_QuestionEdit Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UCKMS_QuestionEdit Event Init

	


//ScriptIncludesInit @1-A11377B6
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|js/jquery/ui/jquery.ui.core.js|js/jquery/ui/jquery.ui.widget.js|js/jquery/ui/jquery.ui.datepicker.js|js/jquery/datepicker/ccs-date-timepicker.js|";
//End ScriptIncludesInit

//Enable Scripting Support @1-C2BE9EBD
        this.ScriptingSupport = true;
//End Enable Scripting Support

//Page Events @1-AE91ECB1
        this.BeforeShow += new EventHandler<EventArgs>(Page_BeforeShow);
//End Page Events


	

//Page UCKMS_QuestionEdit Init event tail @1-FCB6E20C
    }
//End Page UCKMS_QuestionEdit Init event tail

//Page UCKMS_QuestionEdit Event Before Show @1-2B5592D4
    protected void Page_BeforeShow(object sender, EventArgs e) {
//End Page UCKMS_QuestionEdit Event Before Show
string user_id=Session["UserID"].ToString();
string quest_id=Request.QueryString["QuestionID"].ToString();
string quiz_id=Request.QueryString["Quiz_id"].ToString();

//Page UCKMS_AddNewQuestion Event Before Show. Action DLookup @323-D337DDD3
    string insert_by = DataUtility.DLookup<MTText>("QuestionInsertedBy", "tblQuestion", "QuestionID="+quest_id+"", "InMotion:KMS");
    
    
    if(insert_by!=user_id)
    {
    	
    	Response.Redirect("~/KMSAddQuestionToQuiz.aspx?QuestionID="+quest_id+"&Quiz_id="+quiz_id+"");
    	
    }
    
    else
    {
    	
    	Response.Redirect("~/KMS_AddNewQuestion.aspx?QuestionID="+quest_id+"&Quiz_id="+quiz_id+"");
    	
    	}
//Page UCKMS_QuestionEdit Event Before Show. Action Custom Code @153-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Page UCKMS_QuestionEdit Event Before Show. Action Custom Code

//Page UCKMS_QuestionEdit Before Show event tail @1-FCB6E20C
    }
//End Page UCKMS_QuestionEdit Before Show event tail

//Record tblQuestion Event Before Insert @22-8A6DA2E0
    protected void tblQuestion_BeforeInsert(object sender, EventArgs e) {
//End Record tblQuestion Event Before Insert

//Record tblQuestion Event Before Insert. Action Retrieve number of records @44-E975C27D
    //    ((InMotion.Web.Controls.Record)sender).Value = tblQuestion.TotalRecords;
//End Record tblQuestion Event Before Insert. Action Retrieve number of records

//Record tblQuestion Before Insert event tail @22-FCB6E20C
    }
//End Record tblQuestion Before Insert event tail

//Record tblQuestion Event After Insert @22-64F0261A
    protected void tblQuestion_AfterInsert(object sender, DataOperationCompletingEventArgs e) {
//End Record tblQuestion Event After Insert

//Record tblQuestion Event After Insert. Action Custom Code @41-2A29BDB7
        // -------------------------
        // Write your own code here.
        // -------------------------
//End Record tblQuestion Event After Insert. Action Custom Code

//Record tblQuestion Event After Insert. Action Retrieve Value for Variable @42-81983095
       
//End Record tblQuestion Event After Insert. Action Retrieve Value for Variable

//Record tblQuestion Event After Insert. Action DLookup @43-BB5C8195
      //  ((InMotion.Web.Controls.Record)sender).Value = DataUtility.DLookup<MTText>(QuestionID, tblQuestion, aa, "InMotion:KMS");
//End Record tblQuestion Event After Insert. Action DLookup

//Record tblQuestion After Insert event tail @22-FCB6E20C
    }
//End Record tblQuestion After Insert event tail

//CheckBox CheckBox_Delete Event Init @77-CA0D20E6
    protected void tblChoice2CheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @77-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @77-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @77-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @77-3732D557
    protected void tblChoice2CheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @77-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @77-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//CheckBox CheckBox_Delete Event Init @89-D7F4F35C
    protected void NewEditableGrid1CheckBox_Delete_Init(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Init

//Set Checked Value @89-A3604378
        ((InMotion.Web.Controls.MTCheckBox)sender).CheckedValue = true;
//End Set Checked Value

//Set Unchecked Value @89-4403792B
        ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue = false;
//End Set Unchecked Value

//CheckBox CheckBox_Delete Init event tail @89-FCB6E20C
    }
//End CheckBox CheckBox_Delete Init event tail

//CheckBox CheckBox_Delete Event Load @89-2ACB06ED
    protected void NewEditableGrid1CheckBox_Delete_Load(object sender, EventArgs e) {
//End CheckBox CheckBox_Delete Event Load

//Set Default Value @89-083A3319
        ((InMotion.Web.Controls.MTCheckBox)sender).DefaultValue = ((InMotion.Web.Controls.MTCheckBox)sender).UncheckedValue;
//End Set Default Value

//CheckBox CheckBox_Delete Load event tail @89-FCB6E20C
    }
//End CheckBox CheckBox_Delete Load event tail

//Page class tail @1-FCB6E20C




}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

