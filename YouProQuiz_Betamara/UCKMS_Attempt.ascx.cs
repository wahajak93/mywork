﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Net.Http.Formatting;
using System.Data.SqlClient;
using System.Data;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
using InMotion.Web.Features;
using Newtonsoft.Json;
using System.Web.Services;

public partial class UCKMS_Attempt : System.Web.UI.UserControl
{

    public string attempt_no;
    public string user_id;
    public string quiz_id;
    public int quiz_time_slot=0;
    public int quest_id;
    public int quest_time_slot;
    public List<string> li_sections=new List<string>();
    public List<string> li_total_questions=new List<string>();
    public int time_given;



    protected void Page_Init(Object sender, EventArgs e)
    {
       
      //  Session[""]


        //     Response.Write("asdasdaqrafafaf");
       // string u = 


        //string v =

        user_id = "8"; //Session["UserID"].ToString();
        quiz_id = "23";//Request.QueryString["Quiz_id"];
       // Session["UserID"] = user_id;
       // Session["Quiz_id"] = quiz_id;

        // string v = Session["UserID"].ToString();

        //if (Session["quiz_id"] != null && Session["result"] != null)
        //{
        //    Response.Redirect("~/Result.aspx");

        //}



        if (Session["quitter"] != null)
        {
            Session["quitter"] = "no";
        }

    

    }
 


    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["inuse"] != null)
        //{
        //    Response.Write("<script>alert('page already opened. cannot use multiple instances of the current page');</script>");
        //    Response.Redirect("~/Dashboard.aspx");
        //}

        
       
        if (!string.IsNullOrEmpty(attempt_no) && !string.IsNullOrEmpty(quiz_id))
        {
            Button1.Visible = false;
        }

        //if (Session["UserID"] != null && Session["Quiz_id"] != null)
        //{

        //    Session["inuse"] = "inuse";
        //}

        if (!IsPostBack)
        {

      
           // Session["quest_time_slot"] = 0;
            if (string.IsNullOrEmpty(attempt_no))
            {

                DataTable dt1 = clsdb.readdataKMS("SELECT COALESCE(MAX(AttemptID),1) as u_attempt from tblUserQuizAttempt where QuizID=" + quiz_id + " AND UserID=" + user_id + "");

                attempt_no = dt1.Rows[0][0].ToString();
                attempt_no = (int.Parse(attempt_no) + 1).ToString();

                PlaceHolder3.Controls.Add(new LiteralControl("<h1>this is your attempt no " + attempt_no + "</h1>"));

            }
        }

        




    }

    protected void Button1_Click(object sender, EventArgs e)
    {
       


        Button2.Visible = true;

        int no_of_sections = 0;
        int no_of_total_questions = 0;
        DataTable dt1 = clsdb.readdataKMS("SELECT * from tblSection where QuizID=" + quiz_id + "");

        if (dt1.Rows.Count > 0)
        {
            no_of_sections = dt1.Rows.Count;
            for (int i = 0; i < no_of_sections;i++)
            {
                string sec_id = dt1.Rows[i]["SectionID"].ToString();
                li_sections.Add(sec_id);
              //  Quiz_Attempt_Provider_with_sections(quiz_id, sec_id);
             
            }
          
        }
        else
        {
            DataTable dt2 = clsdb.readdataKMS("SELECT * from tbl_Quiz_Question where Quiz_ID=" + quiz_id + "");
    

            if (dt2.Rows.Count > 0)
            {
                no_of_total_questions = dt2.Rows.Count;
                for (int i = 0; i < no_of_total_questions; i++)
                {
                    string quest_id = dt2.Rows[i]["Quiz_Quest_ID"].ToString();
                    li_total_questions.Add(quest_id);
                    //  Quiz_Attempt_Provider_with_sections(quiz_id, sec_id);
                }
            }


            Session["questionslists"] = li_total_questions;


           // Quiz_Attempt_Provider_without_sections(quiz_id);




        }

        if (no_of_sections != 0)
        {
            Quiz_Maker_Sec(li_sections);
        }
        else if (no_of_total_questions != 0)
        {

            Quiz_Maker_NoSec();
        }


        Button1.Visible = false;
    }

 

    private void Quiz_Maker_Sec(List<string> li_sections)
    {
        throw new NotImplementedException();
    }

    private void Quiz_Maker_NoSec()
    {
        int length = 0;

        if (Session["questionslists"] != null)
        {

            li_total_questions = (List<string>)Session["questionslists"];

        }
        else
        {
            length = li_total_questions.Count;
        }

        if (length == 0)
        {
            Session["quitter"] = "yes";
        }

        Random r = new Random();

        int rInt = r.Next(0, length);
       // Response.Write(rInt.ToString());
        string curr_quest_id = li_total_questions[rInt];
        
        Quiz_Load(curr_quest_id);

        li_total_questions.RemoveAt(rInt);
        Session["current_question"] = curr_quest_id;
        Session["questionslists"] = li_total_questions;

        //
        ShowQuestion(curr_quest_id);
        //

        //throw new NotImplementedException();
    }

    private void ShowQuestion(string p)
    {
        DataTable dt1 = clsdb.readdataKMS("select * from view_for_questions where Quiz_ID="+quiz_id+" AND Quiz_Quest_ID="+p+"");
        quest_time_slot = int.Parse(dt1.Rows[0]["Quiz_Quest_Time"].ToString());
        time_given = int.Parse(dt1.Rows[0]["Quiz_Quest_Time"].ToString());
        Session["timeslot"] = quest_time_slot;
        Label1.Text = "Time Left:" + quest_time_slot.ToString();

        PlaceHolder2.Controls.Add(new LiteralControl("<form name='form1' method='post' action='#' enctype='multipart/form-data'>"));
        PlaceHolder2.Controls.Add(new LiteralControl("<h4>Question:"+dt1.Rows[0]["Quiz_Quest_ID"]+"</h4><p class='questions'>" + dt1.Rows[0]["Question"] + "</p><br><br><hr/>"));
        PlaceHolder2.Controls.Add(new LiteralControl("</form>"));

        UpdatePanel2.Update();
        UpdatePanel1.Update();




       // PlaceHolder2.Controls.Add(new LiteralControl("<h1>"+dt1.Rows[0]["Question"].ToString()+"</h1>"));

        //throw new NotImplementedException();
    }

    //private void Quiz_Attempt_Provider_without_sections(string quiz_id)
    //{
    //    throw new NotImplementedException();
    //}

    //private void Quiz_Attempt_Provider_with_sections(string quiz_id, string section_id)
    //{
    //    List<string> li = new List<string>();

    //    DataTable dt1 = clsdb.readdataKMS("SELECT * FROM tblSectionQuestions WHERE QuizID="+quiz_id+" AND SectionID="+section_id+"");
    //    if (dt1.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt1.Rows.Count; i++)
    //        {
    //            li.Add(dt1.Rows[i]["QuestionID"].ToString());
                    
    //        }
        
    //    }

    //    //throw new NotImplementedException();
    //}







    protected void ticker(object sender, EventArgs e)
    {


        
            quest_time_slot = Convert.ToInt32(Session["timeslot"]) - 1;
            // Session["timeslot"] = Convert.ToInt32(Session["timeslot"])-1;
            if (quest_time_slot > -1)
            {

                Label1.Text = quest_time_slot.ToString();
                UpdatePanel2.Update();
                Session["timeslot"] = quest_time_slot;

            }
            else
            {
                if(li_total_questions.Count<=0)
                {
                    
                }
               Quiz_Maker_NoSec();
            }
  
         
     

        
          


            //if (quest_time_slot > 0 && li_total_questions.Count > -1)
            //{

            //    Label1.Text = quest_time_slot.ToString();
            //    //   UpdatePanel2.Update();



            //    // quest_time_slot = Convert.ToInt32(ViewState["timeslot"]);

            //    //  if (quest_time_slot > 0 && li_total_questions.Count > 0)
            //    //{
            //    //  quest_time_slot--;
            //    //ViewState["timeslot"] = quest_time_slot;
            //    //Label1.Text = quest_time_slot.ToString();
            //    //}



            //}
            //else if (li_total_questions.Count > 0)
            //{
            //    Quiz_Maker_NoSec();
            //}
            //else if (Session["quitter"] == "Yes")
            //{
            //    li_total_questions = null;

            //    Session["questionslists"] = null;
            //    Response.Redirect("~/Home.aspx");

            //}
         //   UpdatePanel2.Update();


        }

    

    protected void Button2_Click(object sender, EventArgs e)
    {

       
        Quiz_Maker_NoSec();
    }

    [WebMethod]
    public void Quiz_Load()
    {
        Quiz_Maker_NoSec();
        
    }


    private void Quiz_Load(string quest_loaded)
    {

        Question qx = new Question();
       // qx.question_id=

        

       // throw new NotImplementedException();
    }



    public class user_answer
    {
        public string question_id;
        public string question_text;

    
    }

    class Question
    {
        public string Question_id;
      public  string Question_Name;
       public string Question_type;
       public string Question_category;
       public string Question_time;
public string question_id;

    
    
    }


}