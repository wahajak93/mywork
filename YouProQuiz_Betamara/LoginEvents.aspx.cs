//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-0E369CFE
public partial class Login_Page : MTPage
{
//End Page class

//Page Login Event Load @1-D9372652
    protected void Page_Load(object sender, EventArgs e) {
//End Page Login Event Load

//DataBind @1-F74EE7F0
        if(!IsPostBack) DataBind();
//End DataBind

//Page Login Load event tail @1-FCB6E20C
    }
//End Page Login Load event tail

//Page Login Event On PreInit @1-6204FEF7
    protected void Page_PreInit(object sender, EventArgs e) {
//End Page Login Event On PreInit

//MasterPageInit @1-60BC116D
        this.DesignMasterPagePath = "";
//End MasterPageInit

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page Login On PreInit event tail @1-FCB6E20C
    }
//End Page Login On PreInit event tail

//DEL          if (Membership.ValidateUser(Login1.GetControl<InMotion.Web.Controls.MTTextBox>("login").Text, InMotion.Security.Cryptography.MD5(Login1.GetControl<InMotion.Web.Controls.MTTextBox>("password").Text)))
//DEL          {
//DEL              FormsAuthentication.SetAuthCookie(Login1.GetControl<InMotion.Web.Controls.MTTextBox>("login").Text, false);
//DEL              if (HttpContext.Current.Request.QueryString["ReturnUrl"] != null)
//DEL                  if (sender is MTImageButton)
//DEL                      ((MTImageButton)sender).ReturnPage = FormsAuthentication.GetRedirectUrl(Login1.GetControl<InMotion.Web.Controls.MTTextBox>("login").Text, false);
//DEL                  else
//DEL                      ((MTButton)sender).RedirectUrl = FormsAuthentication.GetRedirectUrl(Login1.GetControl<InMotion.Web.Controls.MTTextBox>("login").Text, false);
//DEL          Response.Redirect("~/NewPage2.aspx");
//DEL          }
//DEL          else
//DEL          {
//DEL              Login1.Errors.Add(InMotion.Common.Resources.ResManager.GetString("CCS_LoginError"));
//DEL          }

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

