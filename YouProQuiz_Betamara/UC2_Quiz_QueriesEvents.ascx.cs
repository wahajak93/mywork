//Using statements @1-F3CDD7AA
using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Text.RegularExpressions;
using InMotion;
using InMotion.Common;
using InMotion.Configuration;
using InMotion.Data;
using InMotion.Web;
using InMotion.Web.Controls;
//End Using statements

//Namespace @1-FCA00561
namespace YouProQuiz_Beta
{
//End Namespace

//Page class @1-F66189B3
public partial class UC2_Quiz_Queries_Page : MTUserControl
{
//End Page class

//Attributes constants @1-93D58DF7
    public const string Attribute_pathToRoot = "pathToRoot";
//End Attributes constants

//Page UC2_Quiz_Queries Event Init @1-2408D3E3
    protected void Page_Init(object sender, EventArgs e) {
//End Page UC2_Quiz_Queries Event Init

//ScriptIncludesInit @1-3A812FA2
        this.ScriptIncludes = "|js/jquery/jquery.js|js/jquery/event-manager.js|js/jquery/selectors.js|";
//End ScriptIncludesInit

//Page UC2_Quiz_Queries Init event tail @1-FCB6E20C
    }
//End Page UC2_Quiz_Queries Init event tail

//Page class tail @1-FCB6E20C
}
//End Page class tail

//Namespace tail @1-FCB6E20C
}
//End Namespace tail

