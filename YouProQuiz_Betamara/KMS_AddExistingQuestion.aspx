﻿
<%@ Page language="C#" CodeFile="KMS_AddExistingQuestionEvents.aspx.cs" Inherits="YouProQuiz_Beta.KMS_AddExistingQuestion_Page" MasterPageFile="~/Dashboard.master"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Admin Dashboard</title>


<script language="JavaScript" type="text/javascript">
    //Begin CCS script
    //Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
    //End Include Common JSFunctions

    //Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
    //End Include User Scripts

    //End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br>
<mt:Record ReturnPage="~/KMS_AddExistingQuestion.aspx" ID="tbl_Quiz_Question_tblQues1" runat="server"><ItemTemplate><div data-emulate-form="tbl_Quiz_Question_tblQues1" id="tbl_Quiz_Question_tblQues1">


  <h2>Search </h2>
  <table>
    <mt:MTPanel id="Error" visible="False" runat="server">
    <tr id="<%= Variables["@error-block"]%>">
      <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td>
    </tr>
    </mt:MTPanel>
    <tr>
      <td><label for="<%# tbl_Quiz_Question_tblQues1.GetControl("s_QuestionID").ClientID %>">Question ID</label></td> 
      <td><mt:MTTextBox Source="QuestionID" DataType="Float" Caption="Question ID" maxlength="12" Columns="12" ID="s_QuestionID" data-id="tbl_Quiz_Question_tblQues1s_QuestionID" runat="server"/></td>
    </tr>
 
    <tr>
      <td style="TEXT-ALIGN: right" colspan="2">
        <mt:MTButton CommandName="Search" Text="Search" CssClass="Button" ID="Button_DoSearch" data-id="tbl_Quiz_Question_tblQues1Button_DoSearch" runat="server"/></td>
    </tr>
  </table>



</div></ItemTemplate></mt:Record><br>
<mt:Grid PageSizeLimit="100" RecordsPerPage="10" DataSourceID="tbl_Quiz_Question_tblQuesDataSource" ID="tbl_Quiz_Question_tblQues" runat="server">
<HeaderTemplate>
<h2>Add Existing Questions</h2>
<table>
  <tr>
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="QuestionID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Question ID" ID="Sorter_QuestionID" data-id="tbl_Quiz_Question_tblQuesSorter_QuestionID" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="Question_Name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Name" ID="Sorter_Question_Name" data-id="tbl_Quiz_Question_tblQuesSorter_Question_Name" runat="server"/></th>
 
    <th scope="col">Question</th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="QuestionInsertedOn" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Question Inserted On" ID="Sorter_QuestionInsertedOn" data-id="tbl_Quiz_Question_tblQuesSorter_QuestionInsertedOn" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="QuestionUpdatedOn" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Question Updated On" ID="Sorter_QuestionUpdatedOn" data-id="tbl_Quiz_Question_tblQuesSorter_QuestionUpdatedOn" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="QuestionStatusID" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Status" ID="Sorter_QuestionStatusID" data-id="tbl_Quiz_Question_tblQuesSorter_QuestionStatusID" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="TBL_users_user_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Added By" ID="Sorter_TBL_users_user_name" data-id="tbl_Quiz_Question_tblQuesSorter_TBL_users_user_name" runat="server"/></th>
 
    <th scope="col">
    <mt:MTSorter AscOnImage="Styles/None/Images/Asc.gif" DescOnImage="Styles/None/Images/Desc.gif" AscOnAlt="Ascending" DescOnAlt="Descending" SortOrder="TBL_users1_user_name" DisplayStyle="LinkAndImages" SpanClassName="Sorter" Text="Modified By" ID="Sorter_TBL_users1_user_name" data-id="tbl_Quiz_Question_tblQuesSorter_TBL_users1_user_name" runat="server"/></th>
 
    <th scope="col">&nbsp;</th>
  </tr>
 
  
</HeaderTemplate>
<ItemTemplate>
  <tr>
    <td><mt:MTLabel Source="QuestionID" DataType="Float" ID="QuestionID" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Question_Name" ID="Question_Name" runat="server"/>&nbsp;</td> 
    <td>
         <mt:MTLabel Source="Question" DataType="Memo" ID="Question" runat="server" Mode="PassThrough"  SourceType="DatabaseColumn" ContentType="Html" />&nbsp;</td> 
    <td><mt:MTLabel Source="QuestionInsertedOn" DataType="Date" ID="QuestionInsertedOn" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="QuestionUpdatedOn" DataType="Date" ID="QuestionUpdatedOn" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="Status" ID="QuestionStatusID" runat="server" ContentType="Text" DataType="Text" DBFormat="" Format="" SourceType="DatabaseColumn" />&nbsp;</td> 
    <td><mt:MTLabel Source="TBL_users_user_name" ID="TBL_users_user_name" runat="server"/>&nbsp;</td> 
    <td><mt:MTLabel Source="TBL_users1_user_name" ID="TBL_users1_user_name" runat="server"/>&nbsp;</td> 
    <td><mt:MTLink Text="Add" ID="Link1" data-id="tbl_Quiz_Question_tblQuesLink1_{tbl_Quiz_Question_tblQues:rowNumber}" runat="server" HrefSource="~/KMSAddQuestionToQuiz.aspx" PreserveParameters="Get"><Parameters>
      <mt:UrlParameter Name="QuestionID" SourceType="DataSourceColumn" Source="QuestionID" Format="yyyy-MM-dd"/>
    </Parameters></mt:MTLink>&nbsp;</td>
  </tr>
 
</ItemTemplate>
<FooterTemplate>
  <mt:MTPanel id="NoRecords" visible="False" runat="server">
  <tr>
    <td colspan="9">No records</td>
  </tr>
  </mt:MTPanel>
  <tr>
    <td colspan="9">
      <mt:MTNavigator PreviousOnValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrev.gif&quot;/&gt; " PreviousOffValue="&lt;img alt=&quot;{Prev_URL}&quot; src=&quot;Styles/None/Images/en/ButtonPrevOff.gif&quot;/&gt;" NextOnValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNext.gif&quot;/&gt; " NextOffValue="&lt;img alt=&quot;{Next_URL}&quot; src=&quot;Styles/None/Images/en/ButtonNextOff.gif&quot;/&gt;" DisplayStyle="Links" PageLinksNumber="10" PageSizeItems="1;5;10;25;50" ID="Navigator" runat="server"/>&nbsp;</td>
  </tr>
</table>

</FooterTemplate>
</mt:Grid>
<mt:MTDataSource ID="tbl_Quiz_Question_tblQuesDataSource" ConnectionString="InMotion:KMS" SelectCommandType="Table" CountCommandType="Table" runat="server">
   <SelectCommand>
SELECT TOP {SqlParam_endRecord} TBL_users1.user_name AS TBL_users1_user_name, QuestionID, Question_Name, Question, QuestionInsertedOn, QuestionUpdatedOn, QuestionStatusID,
TBL_users.user_name AS TBL_users_user_name, Status 
FROM ((tblQuestion LEFT JOIN TBL_users TBL_users1 ON
tblQuestion.QuestionInsertedBy = TBL_users1.id) LEFT JOIN TBL_users ON
tblQuestion.QuestionUpdatedBy = TBL_users.id) LEFT JOIN tblRefStatus ON
tblQuestion.QuestionStatusID = tblRefStatus.StatusID {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <CountCommand>
SELECT COUNT(*)
FROM ((tblQuestion LEFT JOIN TBL_users TBL_users1 ON
tblQuestion.QuestionInsertedBy = TBL_users1.id) LEFT JOIN TBL_users ON
tblQuestion.QuestionUpdatedBy = TBL_users.id) LEFT JOIN tblRefStatus ON
tblQuestion.QuestionStatusID = tblRefStatus.StatusID
   </CountCommand>
   <SelectParameters>
     <mt:WhereParameter Name="Urls_QuestionID" SourceType="URL" Source="s_QuestionID" DataType="Float" Operation="And" Condition="Equal" SourceColumn="tblQuestion.QuestionID"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<br>

<center><font face="Arial"><small>G&#101;ne&#114;&#97;te&#100; <!-- CCS -->wi&#116;h <!-- SCC -->C&#111;deCha&#114;g&#101; <!-- CCS -->S&#116;u&#100;io.</small></font></center></body>


<!--End ASPX page-->

    </asp:Content>