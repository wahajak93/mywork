﻿<%--<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-1F39CBA5-->--%>
<%@ Page language="C#" CodeFile="AboutUsEvents.aspx.cs" Inherits="YouProQuiz_Beta.AboutUs_Page"  MasterPageFile="~/Dashboard.master" %>

<%@ Register TagPrefix="YouProQuiz_Beta" TagName="UC_LogOut" Src="UC_LogOut.ascx" %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>


<%--<!--End ASPX page header-->

<!--ASPX page @1-9ED9C54D-->
<html>
<head>--%>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990"><meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>AdminDashboard</title>


<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
    </asp:Content>
<%--</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">--%>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<YouProQuiz_Beta:UC_LogOut ID="UC_LogOut" runat="server"/>
Admin Profile Details
<mt:MTImage AlternateText="" ID="Image1" data-id="Image1" runat="server"/>Welcome <mt:MTLabel ID="AdminName" runat="server"/>
<mt:MTLink Text="Reset Settings" ID="Link1" data-id="Link1" runat="server" PreserveParameters="Get"/>
    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Reset_Settings.aspx" >Reset Settings</asp:LinkButton>

    </asp:Content>
<%--</form>
</body>
</html>

<!--End ASPX page-->--%>

