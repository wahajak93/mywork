<!--ASCX page header @1-34312A1E-->
<%@ Control language="C#" CodeFile="uc_resetpasswordEvents.ascx.cs" Inherits="YouProQuiz_Beta.uc_resetpassword_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<!--End ASCX page header-->

<!--ASCX page @1-8EC86247-->




<mt:MTPanel ID="___link_panel_027569217896610765" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/Style_doctype.css">
</mt:MTPanel>

<mt:MTPanel ID="___link_panel_00635181536686027" runat="server"><link rel="stylesheet" type="text/css" href="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/jquery-ui.css">
</mt:MTPanel>
<p>
<mt:Record PreserveParameters="Get" DataSourceID="TBL_usersDataSource" AllowInsert="False" AllowDelete="False" OnBeforeShow="TBL_users_BeforeShow" OnValidating="TBL_users_Validating" OnAfterUpdate="TBL_users_AfterUpdate" ID="TBL_users" runat="server"><ItemTemplate><div data-emulate-form="uc_resetpasswordTBL_users" id="uc_resetpasswordTBL_users">



 

  <table class="MainTable" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td valign="top">
        <table class="Header" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td class="HeaderLeft"><img alt="" src="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/Images/Spacer.gif"/></td> 
            <td class="th"><strong> </strong></td> 
            <td class="HeaderRight"><img alt="" src="<%#ResolveClientUrl("~/")%>Styles/<%#StyleName%>/Images/Spacer.gif"/></td> 
          </tr>
 
        </table>
 
        <table class="Record" cellspacing="0" cellpadding="0">
          <mt:MTPanel id="Error" visible="False" runat="server">
          <tr id="ErrorBlock" class="Error">
            <td colspan="2"><mt:MTLabel id="ErrorLabel" runat="server"/></td> 
          </tr>
 </mt:MTPanel>
          <tr class="Controls">
            <td class="th">&nbsp;Old Password</td> 
            <td>&nbsp;<mt:MTTextBox TextMode="Password" Required="True" OnValidating="TBL_usersoldpassword_Validating" ID="oldpassword" data-id="uc_resetpasswordTBL_usersoldpassword" runat="server" ErrorControl="MTLabel1" /><mt:MTLabel id="MTLabel1" runat="server"/></td> 
          </tr>
 
          <tr class="Controls">
            <td class="th"><label for="<%# TBL_users.GetControl<InMotion.Web.Controls.MTTextBox>("user_password").ClientID %>">New&nbsp;Password</label></td> 
            <td><mt:MTTextBox TextMode="Password" Source="user_password" Required="True" Caption="User Password" maxlength="50" Columns="50" OnValidating="TBL_usersuser_password_Validating" ID="user_password" data-id="uc_resetpasswordTBL_usersuser_password" runat="server" ErrorControl="MTLabel2" Class="validate" /><mt:MTLabel id="MTLabel2" runat="server"/></td> 
          </tr>
 
          <tr class="Controls">
            <td class="th">&nbsp;Confirm Password</td> 
            <td>&nbsp;<mt:MTTextBox TextMode="Password" Required="True" ID="confirmpassword" data-id="uc_resetpasswordTBL_usersconfirmpassword" runat="server" ErrorControl="MTLabel3" /><mt:MTLabel id="MTLabel3" runat="server"/></td> 
          </tr>
 
          <tr class="Bottom">
            <td style="TEXT-ALIGN: right" colspan="2">
              <mt:MTButton CommandName="Update" Text="Submit" CssClass="Button" ID="Button_Update" data-id="uc_resetpasswordTBL_usersButton_Update" runat="server"/><mt:MTHidden ID="user_password_Shadow" data-id="uc_resetpasswordTBL_usersuser_password_Shadow" runat="server"/></td> 
          </tr>
 
        </table>
 </td> 
    </tr>
 
  </table>



</div></ItemTemplate></mt:Record>
<mt:MTDataSource ID="TBL_usersDataSource" ConnectionString="InMotion:DBYouProQuiz" SelectCommandType="Table" UpdateCommandType="Table" OnBeforeBuildUpdate="TBL_users_BeforeBuildUpdate" runat="server">
   <SelectCommand>
SELECT * 
FROM TBL_users {SQL_Where} {SQL_OrderBy}
   </SelectCommand>
   <UpdateCommand>
UPDATE TBL_users SET user_password={user_password}
  </UpdateCommand>
   <SelectParameters>
     <mt:WhereParameter Name="SesUserID" SourceType="Session" Source="UserID" DataType="Integer" Operation="And" Condition="Equal" SourceColumn="id" Required="true"/>
   </SelectParameters>
</mt:MTDataSource><br>
<br>
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<br>
<br>
</p>
<script type="text/javascript" charset="utf-8" src='ClientI18N.aspx?file=globalize.js&amp;locale=<%#ResManager.GetString("CCS_LocaleID")%>'></script>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br>
<br>
&nbsp;</p>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-12EF267B
</script>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">



<!--End ASCX page-->

