﻿<!DOCTYPE HTML> <!--Doctype @1-EDBCE198-->

<!--ASPX page header @1-22BFECDC-->
<%@ Page language="C#" CodeFile="Pg_UserRecordDetailsEvents.aspx.cs" Inherits="YouProQuiz_Beta.Pg_UserRecordDetails_Page"  %>
<%@ Register TagPrefix="mt" Namespace="InMotion.Web.Controls" %>
<%@ Register Src="~/UCKMS_UserAttemptRecordDetails.ascx" TagPrefix="uc1" TagName="UCKMS_UserAttemptRecordDetails" %>
<%@ Register Src="~/UCKMSchartjsResult.ascx" TagPrefix="uc1" TagName="UCKMSchartjsResult" %>


<%--<%@ Register Src="~/WebUserControl.ascx" TagPrefix="uc1" TagName="WebUserControl" %>--%>


<!--End ASPX page header-->

<!--ASPX page @1-1A525BE1-->
<html>
<head>
    
<meta name="GENERATOR" content="CodeCharge Studio 5.1.1.18990">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">


<title>Pg_UserRecordDetails</title>

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-33862889
</script>
<script src='ClientI18N.aspx?file=globalize.js&locale=<%#ResManager.GetString("CCS_LocaleID")%>' type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-DEAA4CDA
</script>
<%=Attributes["scriptIncludes"]%>
<script type="text/javascript">
//End Include User Scripts

//End CCS script
</script>

<mt:MTPanel ID="___head_link_panel" runat="server"></mt:MTPanel>
</head>
<body>
<form id="Form1" runat="server" data-need-form-emulation="data-need-form-emulation">
<%--    <uc1:WebUserControl runat="server" id="WebUserControl" />--%>

  <%--  <uc1:UCKMS_UserAttemptRecordDetails runat="server" ID="UCKMS_UserAttemptRecordDetails" />
   --%> <uc1:UCKMSchartjsResult runat="server" ID="UCKMSchartjsResult" />

</form>
    </body>
</html>

<!--End ASPX page-->

